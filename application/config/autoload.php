<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------
| AUTO-LOADER
| -------------------------------------------------------------------
| This file specifies which systems should be loaded by default.
|
| In order to keep the framework as light-weight as possible only the
| absolute minimal resources are loaded by default. For example,
| the database is not connected to automatically since no assumption
| is made regarding whether you intend to use it.  This file lets
| you globally define which systems you would like loaded with every
| request.
|
| -------------------------------------------------------------------
| Instructions
| -------------------------------------------------------------------
|
| These are the things you can load automatically:
|
| 1. Packages
| 2. Libraries
| 3. Drivers
| 4. Helper files
| 5. Custom config files
| 6. Language files
| 7. Models
|
*/

/*
| -------------------------------------------------------------------
|  Auto-load Packages
| -------------------------------------------------------------------
| Prototype:
|
|  $autoload['packages'] = array(APPPATH.'third_party', '/usr/local/shared');
|
*/
$autoload['packages'] = array();

/*
| -------------------------------------------------------------------
|  Auto-load Libraries
| -------------------------------------------------------------------
| These are the classes located in system/libraries/ or your
| application/libraries/ directory, with the addition of the
| 'database' library, which is somewhat of a special case.
|
| Prototype:
|
|	$autoload['libraries'] = array('database', 'email', 'session');
|
| You can also supply an alternative library name to be assigned
| in the controller:
|
|	$autoload['libraries'] = array('user_agent' => 'ua');
*/
$autoload['libraries'] = array('session','Themes','uuid','encryption','Pdf','database');

/*
| -------------------------------------------------------------------
|  Auto-load Drivers
| -------------------------------------------------------------------
| These classes are located in system/libraries/ or in your
| application/libraries/ directory, but are also placed inside their
| own subdirectory and they extend the CI_Driver_Library class. They
| offer multiple interchangeable driver options.
|
| Prototype:
|
|	$autoload['drivers'] = array('cache');
|.
| You can also supply an alternative property name to be assigned in
| the controller:
|
|	$autoload['drivers'] = array('cache' => 'cch');
|
*/
$autoload['drivers'] = array();

/*
| -------------------------------------------------------------------
|  Auto-load Helper Files
| -------------------------------------------------------------------
| Prototype:
|
|	$autoload['helper'] = array('url', 'file');
*/
$autoload['helper'] = array('url','form','html','cookie','string','file','usersakses');

/*
| -------------------------------------------------------------------
|  Auto-load Config files
| -------------------------------------------------------------------
| Prototype:
|
|	$autoload['config'] = array('config1', 'config2');
|
| NOTE: This item is intended for use ONLY if you have created custom
| config files.  Otherwise, leave it blank.
|
*/
// $autoload['config'] = array();
$autoload['config'] = array('jwt');


/*
| -------------------------------------------------------------------
|  Auto-load Language files
| -------------------------------------------------------------------
| Prototype:
|
|	$autoload['language'] = array('lang1', 'lang2');
|
| NOTE: Do not include the "_lang" part of your file.  For example
| "codeigniter_lang.php" would be referenced as array('codeigniter');
|
*/
$autoload['language'] = array();

/*
| -------------------------------------------------------------------
|  Auto-load Models
| -------------------------------------------------------------------
| Prototype:
|
|	$autoload['model'] = array('first_model', 'second_model');
|
| You can also supply an alternative model name to be assigned
| in the controller:
|
|	$autoload['model'] = array('first_model' => 'first');
*/
$autoload['model'] = array(

//========== auth =============
'auth/m_login', 
'auth/m_register', 
'auth/m_reset_password',
'auth/m_forgot_password',
//========== end auth =============
//========== dashboard =============
'dashboard/M_Dashboard',
//========== end dashboard =============
//========== akad =============
'akad/m_kelebihan_mengajar',
//========== end akad =============
//========== simpeg =============
'simpeg/M_PegawaiPhl',
'simpeg/M_DosenPns',
'simpeg/M_DosenPhl',
'simpeg/M_PegawaiPns',
'simpeg/M_DosenPnsSimremlink',
//========== end simpeg =============
//========== esk =============
'e-sk/m_aktivitas',
//========== end esk =============
//========== esk =============
'lppm/m_penelitian',
'lppm/m_pengabdian',
//========== end esk =============
//========== esk =============
'simremlink/m_data_peran',
'simremlink/m_data_aktivitas',
'simremlink/m_data_periode',
'simremlink/m_data_kdn',
'simremlink/m_data_dosen_tendik_pns',
'simremlink/m_data_golongan_pns',
'simremlink/m_data_grade',
'simremlink/m_data_jabatan_akad',
'simremlink/m_data_jenjang_pendidikan',
'simremlink/m_data_jenis_dokumen',
'simremlink/m_data_jenis_pegawai',
'simremlink/m_data_kelompok_jabatan',
'simremlink/m_data_kelas_jabatan',
'simremlink/m_data_nama_jabatan',
'simremlink/m_data_rekening',
'simremlink/m_data_pekerjaan',
'simremlink/m_data_status_pegawai',
'simremlink/m_data_status_sipil',
'simremlink/m_jabatan_fungsional_dosen_pegawai',
//========== end esk =============
/**
 * 
 * 
 */
//========== DAFTAR API =============
'api/m_api_akad',
'api/m_api_simpeg',
'api/m_api_esk',
'api/m_api_lppm',
'api/m_api_simremlink',
'api/m_api_simremlink_v2',
'api/m_api_transaksi',
'api/m_api_transaksi_v2',
//========== END DAFTAR API =============
/**
 * 
 * 
 */
//========== SINKRON =============
'sinkron/m_sinkron_akad',
'sinkron/m_sinkron_simpeg',
'sinkron/m_sinkron_esk',
'sinkron/m_sinkron_lppm',
'sinkron/m_sinkron_simremlink',
//========== END SINKRON =============
/**
 * 
 * 
 */
//========== SEMUA TRANSAKSI =============
'semua-transaksi/m_filter_kondisi_dosen',
'semua-transaksi/m_filter_kondisi_pegawai',
'semua-transaksi/m_case_1',
'semua-transaksi/m_case_2',
'semua-transaksi/m_case_3',
'semua-transaksi/m_case_4',
'semua-transaksi/m_case_5',
'semua-transaksi/m_case_6',
'semua-transaksi/m_case_7',
'semua-transaksi/m_case_8',
'semua-transaksi/m_case_9',
'semua-transaksi/m_case_10',
'semua-transaksi/m_case_13',
'semua-transaksi/m_case_14',
//========== END SEMUA TRANSAKSI =============
);


