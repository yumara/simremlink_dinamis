<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//==================== auth ====================================
//logout 
$route['keluar']                                               = 'auth/logout';
//login
$route['masuk']                                                = 'auth/login';
$route['proses-login']                                         = 'auth/login/proses';
//forgot password
$route['lupa-katasandi']                                       = 'auth/forgotPassword';
$route['proses-forgot-password']                               = 'auth/forgotPassword/proses';
//reset password 
$route['ganti-katasandi']                                      = 'auth/resetPassword';
$route['proses-reset-password']                                = 'auth/resetPassword/proses';
//register
$route['registrasi']                                           = 'auth/register';
$route['proses-register']                                      = 'auth/register/proses';
//==================== end auth ====================================

//==================== default ====================================
$route['halaman-utama']			                               = 'welcome/halamanUtama';
$route['simpeg']                                               = 'welcome/simpeg';
$route['e-sk']                                                 = 'welcome/esk';
$route['lppm']                                                 = 'welcome/lppm';
$route['simremlink']                                           = 'welcome/simremlink';
$route['web-service']                                          = 'welcome/webService';
$route['akad']                                                 = 'welcome/akad';
$route['sipda']                                                = 'welcome/sipda';
$route['mail-unimed']                                          = 'welcome/mail';
$route['skp-pegawai']                                          = 'welcome/skpPegawai';
$route['bkd']                                                  = 'welcome/bkd';
$route['skp-dosen']                                            = 'welcome/skpDosen';

//==================== end default ====================================
/**
 * 
 */
//==================== dashboard ====================================
$route['dashboard']                                            = 'welcome';
$route['refresh-data']                                         = 'welcome/refreshData';
$route['direct-link-to-dashboard/(:any)']	                   = 'welcome/directLinkToDashboard/$1';
//==================== end dashboard ====================================
/**
 * 
 */
//==================== akad ====================================
$route['akad/kelebihan-mengajar']                              = 'akad/kelebihan-mengajar/kelebihanMengajar';
//==================== end akad ====================================
/**
 * 
 */
//==================== simpeg ====================================
//pegawai phl
$route['simpeg/pegawai-phl']                                   = 'simpeg/pegawai-phl/pegawaiPhl';
$route['simpeg/refresh-pegawai-phl']                           = 'simpeg/pegawai-phl/pegawaiPhl/refreshPegawaiPhl';
//dosen phl
$route['simpeg/dosen-phl']                                     = 'simpeg/dosen-phl/dosenPhl';
$route['simpeg/refresh-dosen-phl']                             = 'simpeg/dosen-phl/dosenPhl/refreshDosenPhl';
//pegawai pns
$route['simpeg/pegawai-pns']                                   = 'simpeg/pegawai-pns/pegawaiPns';
$route['simpeg/refresh-pegawai-pns']                           = 'simpeg/pegawai-pns/pegawaiPns/refreshPegawaiPns';
//dosen pns
$route['simpeg/dosen-pns']                                     = 'simpeg/dosen-pns/DosenPns';
$route['simpeg/refresh-dosen-pns']                             = 'simpeg/dosen-pns/DosenPns/refreshDosenPns';
//dosen pns simremlink
$route['simpeg/dosen-pns-simremlink']                          = 'simpeg/dosen-pns-simremlink/DosenPnsSimremlink';
$route['simpeg/refresh-dosen-pns-simremlink']                  = 'simpeg/dosen-pns-simremlink/DosenPnsSimremlink/refreshDosenPnsSimremlink';
//==================== end simpeg ====================================
/**
 * 
 */
//==================== e-sk ====================================
$route['e-sk/aktivitas']                                       = 'esk/aktivitas/aktivitas';
//==================== end e-sk ====================================
/**
 * 
 */
//==================== lppm ====================================
$route['lppm/penelitian']                                      = 'lppm/penelitian/penelitian';
$route['lppm/pengabdian']                                      = 'lppm/pengabdian/pengabdian';
//==================== end lppm ====================================
/**
 * 
 */
//==================== simremlink ====================================
$route['simremlink/data-peran']                                = 'simremlink/data-peran/dataPeran';
$route['simremlink/data-aktivitas']                            = 'simremlink/data-aktivitas/dataAktivitas';
$route['simremlink/data-periode']                              = 'simremlink/data-periode/dataPeriode';
$route['simremlink/data-kdn']                                  = 'simremlink/data-kdn/dataKdn';
$route['simremlink/data-dosen-tendik-pns']                     = 'simremlink/data-dosen-tendik-pns/dataDosenTendikPns';
$route['simremlink/data-golongan-pns']                         = 'simremlink/data-golongan-pns/dataGolonganPns';
$route['simremlink/data-grade']                                = 'simremlink/data-grade/dataGrade';
$route['simremlink/data-jabatan-akad']                         = 'simremlink/data-jabatan-akad/dataJabatanAkad';
$route['simremlink/data-jenjang-pendidikan']                   = 'simremlink/data-jenjang-pendidikan/dataJenjangPendidikan';
$route['simremlink/data-jenis-dokumen']                        = 'simremlink/data-jenis-dokumen/dataJenisDokumen';
$route['simremlink/data-jenis-pegawai']                        = 'simremlink/data-jenis-pegawai/dataJenisPegawai';
$route['simremlink/data-kelompok-jabatan']                     = 'simremlink/data-kelompok-jabatan/dataKelompokJabatan';
$route['simremlink/data-kelas-jabatan']                        = 'simremlink/data-kelas-jabatan/dataKelasJabatan';
$route['simremlink/data-nama-jabatan']                         = 'simremlink/data-nama-jabatan/dataNamaJabatan';
$route['simremlink/data-rekening']                             = 'simremlink/data-rekening/dataRekening';
$route['simremlink/data-pekerjaan']                            = 'simremlink/data-pekerjaan/dataPekerjaan';
$route['simremlink/data-status-pegawai']                       = 'simremlink/data-status-pegawai/dataStatusPegawai';
$route['simremlink/data-status-sipil']                         = 'simremlink/data-status-sipil/dataStatusSipil';


//==================== end simremlink ====================================
/**
 * 
 */
//==================== daftar api ====================================
$route['daftar-api/akad']                                      = 'daftar-api/daftarApi/akad';
$route['daftar-api/simpeg']                                    = 'daftar-api/daftarApi/simpeg';
$route['daftar-api/esk']                                       = 'daftar-api/daftarApi/esk';
$route['daftar-api/lppm']                                      = 'daftar-api/daftarApi/lppm';
$route['daftar-api/simremlink']                                = 'daftar-api/daftarApi/simremlink';
$route['daftar-api/transaksi']                                 = 'daftar-api/daftarApi/transaksi';
//==================== end daftar api ====================================
/**
 * 
 * 
 * 
 * 
 */
//==================== DAFTAR API ====================================
//==================== akad ====================================
$route['akad/kelebihan-mengajar']                          = 'api/apiAkad/kelebihanMengajar';
$route['akad/pembimbing-akademik']                         = 'api/apiAkad/pembimbingAkademik';
$route['akad/pembimbing-skripsi-tesis-disertasi']          = 'api/apiAkad/pembimbingSkripsiTesisDisertasi';
$route['akad/penguji-skripsi-tesis-disertasi']             = 'api/apiAkad/pengujiSkripsiTesisDisertasi';
$route['akad/mengajar-lainnya']                            = 'api/apiAkad/mengajarLainnya';
//==================== end akad ====================================
/** 
 *  
 */
//==================== simpeg ====================================
$route['simpeg/bagian']                                    = 'api/apiSimpeg/bagian';
$route['simpeg/dosen-phl']                                 = 'api/apiSimpeg/dosenPhl';
$route['simpeg/dosen-pns']                                 = 'api/apiSimpeg/dosenPns';
$route['simpeg/dosen-pns-simremlink']                      = 'api/apiSimpeg/dosenPnsSimremlink';
$route['simpeg/golongan-dosen-pegawai']                    = 'api/apiSimpeg/golonganDosenPegawai';
$route['simpeg/jabatan-fungsional-dosen']                  = 'api/apiSimpeg/jabatanFungsionalDosen';
$route['simpeg/jenis-tugas-tambahan-dosen']                = 'api/apiSimpeg/jenisTugasTambahanDosen';
$route['simpeg/jurusan']                                   = 'api/apiSimpeg/jurusan';
$route['simpeg/kategori-jabatan-remun-pegawai-pns']        = 'api/apiSimpeg/kategoriJabatanRemunPegawaiPns';
$route['simpeg/keududukan-dosen-pegawai']                  = 'api/apiSimpeg/keududkanDosenPegawai';
$route['simpeg/pegawai-phl']                               = 'api/apiSimpeg/pegawaiPhl';
$route['simpeg/pegawai-pns']                               = 'api/apiSimpeg/pegawaiPns';
$route['simpeg/pegawai-pns-simremlink']                    = 'api/apiSimpeg/pegawaiPnsSimremlink';
$route['simpeg/prodi']                                     = 'api/apiSimpeg/prodi';
$route['simpeg/riwayat-jabatan-fungsional']                = 'api/apiSimpeg/riwayatJabatanFungsional';
$route['simpeg/riwayat-jabatan-remun-dosen-pns']           = 'api/apiSimpeg/riwayatJabatanRemunDosenPns';
$route['simpeg/riwayat-jabatan-remun-pegawai-pns']         = 'api/apiSimpeg/riwayatJabatanRemunPegawaiPns';
$route['simpeg/riwayat-jabatan-tugas-tambahan-dosen-pns']  = 'api/apiSimpeg/riwayatJabatanTugasTambahanDosenPns';
$route['simpeg/riwayat-penghargaan-dosen-pns']             = 'api/apiSimpeg/riwayatPenghargaanDosenPns';
$route['simpeg/riwayat-penghargaan-pegawai-pns']           = 'api/apiSimpeg/riwayatPenghargaanPegawaiPns';
$route['simpeg/status']                                    = 'api/apiSimpeg/status';
$route['simpeg/status-aktif-non-aktif']                    = 'api/apiSimpeg/statusAktifNonAktif';
$route['simpeg/unit']                                      = 'api/apiSimpeg/unit';
$route['simpeg/capaian-skp-dosen']                         = 'api/apiSimpeg/capaianSkpDosen';
$route['simpeg/dosen-pegawai-pns']                         = 'api/apiSimpeg/dosenPegawaiPns';
$route['simpeg/nilai-sikap-dan-perilaku-pegawai']          = 'api/apiSimpeg/nilaiSikapDanPerilakuPegawai';
$route['simpeg/capaian-skp-pegawai']                       = 'api/apiSimpeg/capaianSkpPegawai';
$route['simpeg/dosen-pegawai-simremlink']                  = 'api/apiSimpeg/dosenPegawaiSimremlink';
$route['simpeg/dosen-pegawai-00']                          = 'api/apiSimpeg/dosenPegawai00';
$route['simpeg/dosen-pegawai-00-nip/(:any)']               = 'api/apiSimpeg/dosenPegawai00_nip/$1';
$route['simpeg/riwayat-dosen-pegawai-simremlink']          = 'api/apiSimpeg/riwayatDosenPegawaiSimremlink';

//CRUD jabatan fungsional dosen pegawai
$route['simpeg/save-jabatan-fungsional-dosen']             = 'api/apiSimpeg/saveJabatanFungsionalDosen';
$route['simpeg/delete-jabatan-fungsional-dosen']           = 'api/apiSimpeg/deleteJabatanFungsionalDosen';
$route['simpeg/get-jabatan-fungsional-dosen']              = 'api/apiSimpeg/getJabatanFungsionalDosen';
//CRUD jabatan fungsional dosen pegawai
//CRUD jenis tugas tambahan dosen
$route['simpeg/save-jenis-tugas-tambahan-dosen']           = 'api/apiSimpeg/saveJenisTugasTambahanDosen';
$route['simpeg/delete-jenis-tugas-tambahan-dosen']         = 'api/apiSimpeg/deleteJenisTugasTambahanDosen';
$route['simpeg/get-jenis-tugas-tambahan-dosen']            = 'api/apiSimpeg/getJenisTugasTambahanDosen';
//CRUD jenis tugas tambahan dosen
//==================== end simpeg ====================================
/**
 * 
 */
//==================== e-sk ====================================
$route['e-sk/aktivitas']                                   = 'api/apiEsk/aktivitas';
//==================== end e-sk ====================================
/**
 * 
 */
//==================== lppm ====================================
$route['lppm/penelitian']                                  = 'api/apiLppm/penelitian';
$route['lppm/pengabdian']                                  = 'api/apiLppm/pengabdian';
$route['lppm/luaran-penelitian-mandiri']                   = 'api/apiLppm/luaranPenelitianMandiri';
$route['lppm/luaran-pengabdian-mandiri']                   = 'api/apiLppm/luaranPengabdianMandiri';
$route['lppm/data-submission-penelitian']                  = 'api/apiLppm/dataSubmissionPenelitian';
$route['lppm/data-submission-penelitian-anggota']          = 'api/apiLppm/dataSubmissionPenelitianAnggota';
$route['lppm/data-submission-penelitian-ketua']            = 'api/apiLppm/dataSubmissionPenelitianKetua';
$route['lppm/data-submission-penelitian-luaran']           = 'api/apiLppm/dataSubmissionPenelitianLuaran';
$route['lppm/luaran-valid-tambahan']                       = 'api/apiLppm/luaranValidTambahan';
$route['lppm/luaran-valid-wajib']                          = 'api/apiLppm/luaranValidWajib';
$route['lppm/jenis-luaran']                                = 'api/apiLppm/jenisLuaran';
$route['lppm/skim']                                        = 'api/apiLppm/skim';
$route['lppm/status-penulis']                              = 'api/apiLppm/statusPenulis';
$route['lppm/sumberdana']                                  = 'api/apiLppm/sumberDana';
$route['lppm/luaran-valid-gabungan']                       = 'api/apiLppm/luaranValidGabungan';
$route['lppm/luaran-valid']                                = 'api/apiLppm/luaranValid';
$route['lppm/luaran-valid-lainnya']                        = 'api/apiLppm/luaranValidLainnya';
$route['lppm/luaran-valid-pengabdian']                     = 'api/apiLppm/luaranValidPengabdian';
$route['lppm/kode-bkd-0/(:any)']                           = 'api/apiLppm/jenisLuaranByKode/$1/0';
$route['lppm/kode-bkd-13/(:any)']                          = 'api/apiLppm/jenisLuaranByKode/$1/13';
$route['lppm/kode-bkd-14/(:any)']                          = 'api/apiLppm/jenisLuaranByKode/$1/14';
$route['lppm/kode-bkd-15/(:any)']                          = 'api/apiLppm/jenisLuaranByKode/$1/15';
$route['lppm/kode-bkd-16/(:any)']                          = 'api/apiLppm/jenisLuaranByKode/$1/16';
$route['lppm/kode-bkd-19/(:any)']                          = 'api/apiLppm/jenisLuaranByKode/$1/19';
$route['lppm/kode-bkd-20/(:any)']                          = 'api/apiLppm/jenisLuaranByKode/$1/20';
$route['lppm/kode-bkd-21/(:any)']                          = 'api/apiLppm/jenisLuaranByKode/$1/21';
//==================== end lppm ====================================
/**
 * 
 */
//==================== simremlink ====================================
$route['simremlink/data-peran']                            = 'api/apiSimremlink/dataPeran';
$route['simremlink/data-aktivitas']                        = 'api/apiSimremlink/dataAktivitas';
$route['simremlink/data-periode']                          = 'api/apiSimremlink/dataPeriode';
$route['simremlink/data-kdn']                              = 'api/apiSimremlink/dataKdn';
$route['simremlink/data-dosen-tendik-pns']                 = 'api/apiSimremlink/dataDosenTendikPns';
$route['simremlink/data-golongan-pns']                     = 'api/apiSimremlink/dataGolonganPns';
$route['simremlink/data-grade']                            = 'api/apiSimremlink/dataGrade';
$route['simremlink/data-jabatan-akad']                     = 'api/apiSimremlink/dataJabatanAkad';
$route['simremlink/data-jenjang-pendidikan']               = 'api/apiSimremlink/dataJenjangPendidikan';
$route['simremlink/data-jenis-dokumen']                    = 'api/apiSimremlink/dataJenisDokumen';
$route['simremlink/data-jenis-pegawai']                    = 'api/apiSimremlink/dataJenisPegawai';
$route['simremlink/data-kelompok-jabatan']                 = 'api/apiSimremlink/dataKelompokJabatan';
$route['simremlink/data-kelas-jabatan']                    = 'api/apiSimremlink/dataKelasJabatan';
$route['simremlink/data-nama-jabatan']                     = 'api/apiSimremlink/dataNamaJabatan';
$route['simremlink/data-rekening']                         = 'api/apiSimremlink/dataRekening';
$route['simremlink/data-pekerjaan']                        = 'api/apiSimremlink/dataPekerjaan';
$route['simremlink/data-status-pegawai']                   = 'api/apiSimremlink/dataStatusPegawai';
$route['simremlink/data-status-sipil']                     = 'api/apiSimremlink/dataStatusSipil';
$route['simremlink/tusi']                                  = 'api/apiSimremlink/tusi';
$route['simremlink/mengajar']                              = 'api/apiSimremlink/mengajar';
$route['simremlink/mengajar-lainnya']                      = 'api/apiSimremlink/mengajarLainnya';
$route['simremlink/penelitian']                            = 'api/apiSimremlink/penelitian';
$route['simremlink/pengabdian']                            = 'api/apiSimremlink/pengabdian';
$route['simremlink/penghargaan']                           = 'api/apiSimremlink/penghargaan';
$route['simremlink/penunjang']                             = 'api/apiSimremlink/penunjang';
$route['simremlink/riwayat-dosen']                         = 'api/apiSimremlink/riwayatDosen';
$route['simremlink/riwayat-pegawai']                       = 'api/apiSimremlink/riwayatPegawai';
$route['simremlink/detail-riwayat-dosen']                  = 'api/apiSimremlink/riwayatDosenByNip';
$route['simremlink/detail-riwayat-dosen-last']             = 'api/apiSimremlink/riwayatDosenByNipLast';
$route['simremlink/detail-riwayat-pegawai']                = 'api/apiSimremlink/riwayatPegawaiByNip';
$route['simremlink/detail-riwayat-dosen/(:any)/(:any)']    = 'api/apiSimremlink/riwayatDosenByNipDanPeriode';
$route['simremlink/pembayaran-dosen-pegawai']              = 'api/apiSimremlink/pembayaranDosenPegawai';
$route['simremlink/pembayaran-dosen-pegawai-p2']           = 'api/apiSimremlink/pembayaranDosenPegawaiP2';
$route['simremlink/pembayaran-dosen-pegawai-p2-v2']        = 'api/apiSimremlink/pembayaranDosenPegawaiP2v2';
$route['simremlink/pembayaran-p1']                         = 'api/apiSimremlink/pembayaranP1';
$route['simremlink/sync-pembayaran']                       = 'api/apiSimremlinkV2/syncPembayaran';

// khusu perhitungan simremlink
$route['simremlink/riwayat']				               = 'api/apiSimremlink/testing_riwayat';
$route['simremlink/riwayat-by-tgl']		                   = 'api/apiSimremlink/testing_riwayat_by_tgl';
$route['simremlink/riwayat-dt-by-tgl']		               = 'api/apiSimremlink/testing_riwayat_dt_by_tgl';
$route['simremlink/riwayat-dt']			                   = 'api/apiSimremlink/testing_riwayat_dt';
//$route['testing-riwayat']				                       = 'api/apiSimremlink/testing_riwayat';
$route['simremlink/biodata-by-nip']				           = 'api/apiSimremlink/biodata_by_nip';
$route['simremlink/harga-dt']					           = 'api/apiSimremlink/harga_dt';
$route['simremlink/harga-db']					           = 'api/apiSimremlink/harga_db';
$route['simremlink/total-poin-remun']					   = 'api/apiSimremlink/total_poin_remun';
$route['simremlink/total-uang-remun']					   = 'api/apiSimremlink/total_uang_remun';

// khusus perhitungan simremlink v2
$route['simremlink/riwayat-v2']				               = 'api/ApiSimremlinkV2/testing_riwayat';
$route['simremlink/riwayat-by-tgl-v2']		               = 'api/ApiSimremlinkV2/testing_riwayat_by_tgl';
$route['simremlink/riwayat-dt-by-tgl-v2']		           = 'api/ApiSimremlinkV2/testing_riwayat_dt_by_tgl';
$route['simremlink/riwayat-dt-v2']			               = 'api/ApiSimremlinkV2/testing_riwayat_dt';
$route['simremlink/biodata-by-nip-v2']				       = 'api/ApiSimremlinkV2/biodata_by_nip';
$route['simremlink/harga-dt-v2']					       = 'api/ApiSimremlinkV2/harga_dt';
$route['simremlink/harga-db-v2']					       = 'api/ApiSimremlinkV2/harga_db';
$route['simremlink/total-poin-remun-v2']				   = 'api/ApiSimremlinkV2/total_poin_remun';
$route['simremlink/total-uang-remun-v2']				   = 'api/ApiSimremlinkV2/total_uang_remun';
$route['simremlink/total-uang-remun-db-v2']				   = 'api/ApiSimremlinkV2/total_uang_remunDB';

//CRUD peran
$route['simremlink/save-peran']                            = 'api/apiSimremlink/savePeran';
$route['simremlink/delete-peran']                          = 'api/apiSimremlink/deletePeran';
$route['simremlink/get-peran']                             = 'api/apiSimremlink/getPeran';
//CRUD peran
//CRUD periode
$route['simremlink/save-periode']                          = 'api/apiSimremlink/savePeriode';
$route['simremlink/delete-periode']                        = 'api/apiSimremlink/deletePeriode';
$route['simremlink/get-periode']                           = 'api/apiSimremlink/getPeriode';
//CRUD periode
//CRUD nomor rekening
$route['simremlink/save-rekening']                          = 'api/apiSimremlink/saveRekening';
$route['simremlink/delete-rekening']                        = 'api/apiSimremlink/deleteRekening';
$route['simremlink/get-rekening']                           = 'api/apiSimremlink/getRekening';
//CRUD nomor rekening
//==================== end simremlink ====================================
/** 
 * 
 */
//==================== transaksi ====================================
$route['transaksi/kelebihan-mengajar']                          = 'api/apiTransaksi/kelebihanMengajar';
$route['transaksi/pembimbing-akademik']                         = 'api/apiTransaksi/pembimbingAkademik';
$route['transaksi/pembimbing-skripsi-tesis-disertasi']          = 'api/apiTransaksi/pembimbingSkripsiTesisDisertasi';
$route['transaksi/penguji-skripsi-tesis-disertasi']             = 'api/apiTransaksi/pengujiSkripsiTesisDisertasi';
$route['transaksi/mengajar-lainnya']                            = 'api/apiTransaksi/mengajarLainnya';
//--
$route['transaksi/riwayat-penghargaan-dosen']                   = 'api/apiTransaksi/riwayatPenghargaanDosen';
$route['transaksi/riwayat-pengharggan-pns']                     = 'api/apiTransaksi/riwayatPenghargaanPns';
$route['transaksi/capaian-skp-dosen-dt']                        = 'api/apiTransaksi/capaianSkpDosenDt';
$route['transaksi/capaian-skp-pegawai']                         = 'api/apiTransaksi/capaianSkpPegawai';
$route['transaksi/nilai-sikap-dan-perilaku-pegawai']            = 'api/apiTransaksi/nilaiSikapDanPerilakuPegawai';
$route['transaksi/capaian-bkd-dosen']                           = 'api/apiTransaksi/capaianBkdDosen';
$route['transaksi/absensi-pegawai-tendik']                      = 'api/apiTransaksi/absensiPegawaiTendik';
$route['transaksi/absensi-dosen-pns']                           = 'api/apiTransaksi/absensiDosenPns';
$route['transaksi/lhkasn-lhkpn-pegawai-pns']                    = 'api/apiTransaksi/lhkasnLhkpnPegawaiPns';
$route['transaksi/hukuman-disiplin-pegawai-pns']                = 'api/apiTransaksi/hukumanDisiplinPegawaiPns';
$route['transaksi/aktivitas']                                   = 'api/apiTransaksi/aktivitas';

$route['transaksi/table-tupoksi']                               = 'api/apiTransaksi/tableTupoksi';
$route['transaksi/table-mengajar']                              = 'api/apiTransaksi/tableMengajar';
$route['transaksi/table-mengajar-lainnya']                      = 'api/apiTransaksi/tableMengajarLainnya';
$route['transaksi/table-penunjang']                             = 'api/apiTransaksi/tablePenunjang';
$route['transaksi/table-penghargaan']                           = 'api/apiTransaksi/tablePenghargaan';

$route['transaksi/table-tupoksi-v2']                            = 'api/apiTransaksiV2/tableTupoksi';
$route['transaksi/table-mengajar-v2']                           = 'api/apiTransaksiV2/tableMengajar';
$route['transaksi/table-mengajar-lainnya-v2']                   = 'api/apiTransaksiV2/tableMengajarLainnya';
$route['transaksi/table-penunjang-v2']                          = 'api/apiTransaksiV2/tablePenunjang';
$route['transaksi/table-penghargaan-v2']                        = 'api/apiTransaksiV2/tablePenghargaan';

$route['transaksi/capaian-skp-dosen-dt-konversi']               = 'api/apiTransaksi/capaianSkpDosenDtKonversi';
$route['transaksi/capaian-skp-pegawai-konversi']                = 'api/apiTransaksi/capaianSkpPegawaiKonversi';
$route['transaksi/nilai-sikap-dan-perilaku-pegawai-konversi']   = 'api/apiTransaksi/nilaiSikapDanPerilakuPegawaiKonversi';
$route['transaksi/penghargaan']                                 = 'api/apiTransaksi/penghargaan';
//==================== end transaksi ====================================
//==================== END DAFTAR API ====================================               
/**
 * 
 * 
 * 
 * 
 */
//==================== SINKRONISASI ====================================
//==================== akad ====================================
$route['sinkron/akad']                                         = 'sinkron/sinkronAkad';
$route['sinkron/save-akad']                                    = 'sinkron/sinkronAkad/saveSinkronAkad';
//==================== end akad ====================================
/**
 * 
 */
//==================== simpeg ====================================
$route['sinkron/simpeg']                                       = 'sinkron/sinkronSimpeg';
$route['sinkron/save-simpeg']                                  = 'sinkron/sinkronSimpeg/saveSinkronSimpeg';
//==================== end simpeg ====================================
/**
 * 
 */
//==================== e-sk ====================================
$route['sinkron/e-sk']                                         = 'sinkron/sinkronEsk';
$route['sinkron/save-e-sk']                                    = 'sinkron/sinkronEsk/saveSinkronEsk';
//==================== end e-sk ====================================
/**
 * 
 */
//==================== lppm ====================================
$route['sinkron/lppm']                                         = 'sinkron/sinkronLppm';
$route['sinkron/save-lppm']                                    = 'sinkron/sinkronLppm/saveSinkronLppm';
//==================== end lppm ====================================
/**
 * 
 */
//==================== simremlink ====================================
$route['sinkron/simremlink']                                   = 'sinkron/sinkronSimremlink';
$route['sinkron/save-simremlink']                              = 'sinkron/sinkronSimremlink/saveSinkronSimremlink';
//==================== end simremlink ====================================
//==================== END SINKRONISASI ====================================

// Create View
$route['create-view'] = 'api/apiSimremlinkV2/createView';