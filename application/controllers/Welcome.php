<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . 'libraries/JWT.php';
use \Firebase\JWT\JWT;
class Welcome extends CI_Controller {
	public function __construct(){
        parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		//header('Content-Type: application/json');
	}

	public function index(){
		$grup = $this->session->userdata('grup');
		// if($grup != null && $grup == "Admin_Aplikasi"):
			$data['title'] 		   = "Dashboard";
			$data['active']		   = "dashboard";
			$data['link_homepage'] = "https://portal.unimed.ac.id";
			$data['grup']		   = $grup;
			$this->themes->Admin('dashboard/index',$data);
		// else:
		// 	$this->load->view('direct-link/index');
		// endif;
	}

	public function secretKey(){
		$secretKey = "whTsgfn9E6ziWG47t4LNPo9qVbal4OEcODLVj26yXn1kBpKierf3cGvCliCAvWO";
		return $secretKey;
	}

	public function directLinkToDashboard($key){
		$secretKey = $this->secretKey();
		$issuedat_claim = time();
		$notbefore_claim = $issuedat_claim;
		$expire_claim = $issuedat_claim + 43200;
		$token_data = array(
			"iat" => $issuedat_claim,
			"nbf" => $notbefore_claim,
			"exp" => $expire_claim
		);
		$token = JWT::encode($token_data, $secretKey);
		$session = array(
			'grup' => $key,
			'token' => $token,
		);
		$this->session->set_userdata($session);
		return redirect('dashboard');
	}

	public function halamanUtama(){
		return redirect('https://portal.unimed.ac.id/dashboard');
	}

	public function simpeg(){
		return redirect('https://simpeg.unimed.ac.id/phl');
	}
	
	public function akad(){
		return redirect('https://akad.unimed.ac.id/');
	}

	public function esk(){
		return redirect('https://e-sk.unimed.ac.id/');
	}

	public function lppm(){
		return redirect('https://lppm.unimed.ac.id/simppm3');
	}

	public function simremlink(){
		$grup = $this->session->userdata('grup');
		$token = $this->session->userdata('token');	
		return redirect('https://simremlink.unimed.ac.id');
	}

	public function sipda(){
		return redirect('https://sipda.unimed.ac.id/');
	}

	public function webService(){
		return redirect('http://dev.unimed.ac.id/sso-statistik');
	}

	public function mail(){
		return redirect('https://mail.unimed.ac.id');
	}

	public function skpPegawai(){
		return redirect('https://simpeg.unimed.ac.id/jurnal/');
	}

	public function bkd(){
		return redirect('https://simpeg.unimed.ac.id/bkd/');
	}

	public function skpDosen(){
		return redirect('https://simpeg.unimed.ac.id/skpdsn/');
	}

	public function refreshData(){
		$deleted = $this->M_Dashboard->refreshData();
        if($deleted == TRUE){
            $msg = array(
                'msg'  => 'Success updating data',
				'icon' => 'success',
				'link' => 'dashboard'
            );
        }else{
            $msg = array(
                'msg'  => 'Unable to update data',
                'icon' => 'error',
            );
        }
		echo json_encode($msg);
	}
}     