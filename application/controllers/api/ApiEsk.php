<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ApiEsk extends CI_Controller {
	public function __construct(){
        parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin:*');
	}

	public function aktivitas(){
		$data = $this->m_api_esk->aktivitas()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}
}