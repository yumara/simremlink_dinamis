<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ApiLppm extends CI_Controller {
	public function __construct(){
        parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin:*');
	}

	public function penelitian(){
		$data = $this->m_api_lppm->penelitian()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}
	
	public function pengabdian(){
		$data = $this->m_api_lppm->pengabdian()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }
    
    public function luaranPenelitianMandiri(){
		$data = $this->m_api_lppm->luaranPenelitianMandiri()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }
    
    public function luaranPengabdianMandiri(){
		$data = $this->m_api_lppm->luaranPengabdianMandiri()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }
    
    public function dataSubmissionPenelitian(){
		$data = $this->m_api_lppm->dataSubmissionPenelitian()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}

    public function dataSubmissionPenelitianAnggota(){
		$data = $this->m_api_lppm->dataSubmissionPenelitianAnggota()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }
    
    public function dataSubmissionPenelitianKetua(){
		$data = $this->m_api_lppm->dataSubmissionPenelitianKetua()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }
    
    public function dataSubmissionPenelitianLuaran(){
		$data = $this->m_api_lppm->dataSubmissionPenelitianLuaran()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }
    
    public function luaranValidTambahan(){
		$data = $this->m_api_lppm->luaranValidTambahan()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }
    
    public function luaranValidWajib(){
		$data = $this->m_api_lppm->luaranValidWajib()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }
    
    public function jenisLuaran(){
		$data = $this->m_api_lppm->jenisLuaran()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }
    
    public function skim(){
		$data = $this->m_api_lppm->skim()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }
    
    public function statusPenulis(){
		$data = $this->m_api_lppm->statusPenulis()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }
    
    public function sumberDana(){
		$data = $this->m_api_lppm->sumberDana()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }
    
    public function luaranValidGabungan(){
        $data1 = $this->m_api_lppm->luaranValidTambahan()->result();
        $data2 = $this->m_api_lppm->luaranValidWajib()->result();
        $data  = array_merge($data1,$data2);
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }
    
    public function luaranValid(){
		$data = $this->m_api_lppm->luaranValid()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }

    public function luaranValidLainnya(){
		$data = $this->m_api_lppm->luaranValidLainnya()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }

    public function luaranValidPengabdian(){
		$data = $this->m_api_lppm->luaranValidPengabdian()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }

    public function jenisLuaranByKode($nip,$kode_bkd){
		$data = $this->m_api_lppm->jenisLuaranByKode($nip,$kode_bkd);
        //if(count($data) != 0):
            echo json_encode($data,JSON_PRETTY_PRINT);
        /*else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;*/
    }
}