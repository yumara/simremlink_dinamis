<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ApiSimpeg extends CI_Controller {
	public function __construct(){
        parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin:*');
	}

	public function bagian(){
		$data = $this->m_api_simpeg->bagian()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}

	public function dosenPhl(){
		$data = $this->m_api_simpeg->dosenPhl()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}

	public function dosenPns(){
		$data = $this->m_api_simpeg->dosenPns()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}

	public function dosenPnsSimremlink(){
		$data = $this->m_api_simpeg->dosenPnsSimremlink()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}

	public function golonganDosenPegawai(){
		$data = $this->m_api_simpeg->golonganDosenPegawai()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}

	public function jabatanFungsionalDosen(){
		$data = $this->m_api_simpeg->jabatanFungsionalDosen()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}

	public function jenisTugasTambahanDosen(){
		$data = $this->m_api_simpeg->jenisTugasTambahanDosen()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}

	public function jurusan(){
		$data = $this->m_api_simpeg->jurusan()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}

	public function kategoriJabatanRemunPegawaiPns(){
		$data = $this->m_api_simpeg->kategoriJabatanRemunPegawaiPns()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}
	

	public function keududkanDosenPegawai(){
		$data = $this->m_api_simpeg->keududkanDosenPegawai()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}

	public function pegawaiPhl(){
		$data = $this->m_api_simpeg->pegawaiPhl()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}

	public function pegawaiPns(){
		$data = $this->m_api_simpeg->pegawaiPns()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}

	public function pegawaiPnsSimremlink(){
		$data = $this->m_api_simpeg->pegawaiPnsSimremlink()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}

	public function prodi(){
		$data = $this->m_api_simpeg->prodi()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}

	public function riwayatJabatanFungsional(){
		$data = $this->m_api_simpeg->riwayatJabatanFungsional()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}

	public function riwayatJabatanRemunDosenPns(){
		$data = $this->m_api_simpeg->riwayatJabatanRemunDosenPns()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}

	public function riwayatJabatanRemunPegawaiPns(){
		$data = $this->m_api_simpeg->riwayatJabatanRemunPegawaiPns()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}

	public function riwayatJabatanTugasTambahanDosenPns(){
		$data = $this->m_api_simpeg->riwayatJabatanTugasTambahanDosenPns()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}

	public function riwayatPenghargaanDosenPns(){
		$data = $this->m_api_simpeg->riwayatPenghargaanDosenPns()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}

	public function riwayatPenghargaanPegawaiPns(){
		$data = $this->m_api_simpeg->riwayatPenghargaanPegawaiPns()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}

	public function status(){
		$data = $this->m_api_simpeg->status()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}

	public function statusAktifNonAktif(){
		$data = $this->m_api_simpeg->statusAktifNonAktif()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}

	public function unit(){
		$data = $this->m_api_simpeg->unit()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }
    
    public function capaianSkpDosen(){
		$data = $this->m_api_simpeg->capaianSkpDosen()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }
    
    public function dosenPegawaiPns(){
        $data1 = $this->m_api_simpeg->pegawaiPnsSimremlink()->result();
        $data2 = $this->m_api_simpeg->dosenPnsSimremlink()->result();
        $data = array_merge($data1,$data2);
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }
    
    public function nilaiSikapDanPerilakuPegawai(){
		$data = $this->m_api_simpeg->nilaiSikapDanPerilakuPegawai()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }

    public function capaianSkpPegawai(){
		$data = $this->m_api_simpeg->capaianSkpPegawai()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }

    public function dosenPegawaiSimremlink(){
		$data = $this->m_api_simpeg->dosenPegawaiSimremlink()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }

    public function riwayatDosenPegawaiSimremlink(){
        //$data['riwayat_jabatan_remun_dosen']        = $this->m_api_simpeg->riwayatDosenPegawaiSimremlinkSatu()->result();
        //$data['riwayat_jabatan_remun_pegawai']      = $this->m_api_simpeg->riwayatDosenPegawaiSimremlinkDua()->result();
        //$data['riwayat_jabatan_tugas_tambahan']     = $this->m_api_simpeg->riwayatDosenPegawaiSimremlinkTiga()->result();
        //$data['riwayat_jabatan_jabatan_fungsional'] = $this->m_api_simpeg->riwayatDosenPegawaiSimremlinkEmpat()->result();
        $data1 = $this->m_api_simpeg->riwayatDosenPegawaiSimremlinkSatu()->result();
        $data2 = $this->m_api_simpeg->riwayatDosenPegawaiSimremlinkDua()->result();
        $data3 = $this->m_api_simpeg->riwayatDosenPegawaiSimremlinkTiga()->result();
        $data4 = $this->m_api_simpeg->riwayatDosenPegawaiSimremlinkEmpat()->result();
        $data  = array_merge($data1,$data2,$data3,$data4);
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }
    

    //=============== jabatan fungsional dosen pegawai ============================//
    public function saveJabatanFungsionalDosen(){
        if(!empty(trim($this->input->post('id')))){
            $saveData = $this->m_api_simpeg->updateSaveJabatanFungsionalDosen();
        }else{
            $saveData = $this->m_api_simpeg->saveJabatanFungsionalDosen();
        }
		
    	if($saveData['status'] == TRUE){
            $msg = array(
                'msg'  => 'Data berhasil disimpan',
                'icon' => 'success',
                'link' => 'kelompok-jabatan/jabatan-fungsional-dosen-pegawai',
            );
        }else{
            $msg = array(
                'msg'  => $saveData['msg'],
                'icon' => $saveData['icon'],
            );
        }
        echo json_encode($msg);
    }

    public function deleteJabatanFungsionalDosen(){
        $id      = trim($this->input->post('id'));
        $deleted = $this->m_api_simpeg->deleteJabatanFungsionalDosen($id);
        if($deleted == TRUE){
            $msg = array(
                'msg'  => 'Berhasil menghapus data',
                'icon' => 'success',
            );
        }else{
            $msg = array(
                'msg'  => 'Data tidak ditemukan',
                'icon' => 'error',
            );
        }
        echo json_encode($msg);
    }

    public function getJabatanFungsionalDosen(){
        $id             = trim($this->input->post('id'));
        $data  = $this->m_api_simpeg->getJabatanFungsionalDosen($id);
        if($data != false){
            echo json_encode($data);
        }else{
            $msg = array(
                'msg'  => 'Data tidak ditemukan',
                'icon' => 'error',
            );
            echo json_encode($msg);
        }   
    }
    //=============== jabatan fungsional dosen pegawai ============================//
    //=============== jenis tugas tambahan dosen ============================//
    public function saveJenisTugasTambahanDosen(){
        if(!empty(trim($this->input->post('id')))){
            $saveData = $this->m_api_simpeg->updateSaveJenisTugasTambahanDosen();
        }else{
            $saveData = $this->m_api_simpeg->saveJenisTugasTambahanDosen();
        }
		
    	if($saveData['status'] == TRUE){
            $msg = array(
                'msg'  => 'Data berhasil disimpan',
                'icon' => 'success',
                'link' => 'kelompok-jabatan/jenis-tugas-tambahan-dosen',
            );
        }else{
            $msg = array(
                'msg'  => $saveData['msg'],
                'icon' => $saveData['icon'],
            );
        }
        echo json_encode($msg);
    }

    public function deleteJenisTugasTambahanDosen(){
        $id      = trim($this->input->post('id'));
        $deleted = $this->m_api_simpeg->deleteJenisTugasTambahanDosen($id);
        if($deleted == TRUE){
            $msg = array(
                'msg'  => 'Berhasil menghapus data',
                'icon' => 'success',
            );
        }else{
            $msg = array(
                'msg'  => 'Data tidak ditemukan',
                'icon' => 'error',
            );
        }
        echo json_encode($msg);
    }

    public function getJenisTugasTambahanDosen(){
        $id    = trim($this->input->post('id'));
        $data  = $this->m_api_simpeg->getJenisTugasTambahanDosen($id);
        if($data != false){
            echo json_encode($data);
        }else{
            $msg = array(
                'msg'  => 'Data tidak ditemukan',
                'icon' => 'error',
            );
            echo json_encode($msg);
        }   
    }
    //=============== jenis tugas tambahan dosen ============================//

    public function dosenPegawai00(){
		$data = $this->m_api_simpeg->dosenPegawai00();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }

    public function dosenPegawai00_nip($nip){
        $data = $this->m_api_simpeg->dosenPegawai00_nip($nip);
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }

}