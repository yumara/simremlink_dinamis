<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ApiSimremlink extends CI_Controller {
	public function __construct(){
        parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin:*');
	}

	public function dataPeran(){
		$data = $this->m_api_simremlink->dataPeran()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}

	public function dataAktivitas(){
        $data1 = $this->m_api_akad->pengujiSkripsiTesisDisertasiSimremlink()->result();
        $data2 = $this->m_api_akad->pembimbingSkripsiTesisDisertasiSimremlink()->result();
        $data3 = $this->m_api_simpeg->riwayatPenghargaanDosenPnsSimremlink()->result();
        $data4 = $this->m_api_simpeg->riwayatPenghargaanPegawaiPnsSimremlink()->result();
        $data5 = $this->m_api_simpeg->capaianSkpDosenSimremlink()->result();
        $data6 = $this->m_api_simpeg->capaianSkpPeriodeSimremlink()->result();
        $data7 = $this->m_api_simpeg->nilaiSikapPerilakuSimremlink()->result();
        $data8 = $this->m_api_esk->aktivitasSimremlink()->result();
        $data9 = $this->m_api_lppm->luaranPenelitianMandiriSimremlink()->result();
        $data10 = $this->m_api_lppm->luaranPengabddianMandiriSimremlink()->result();
        $data  = array_merge($data1,$data2,$data3,$data4,$data5,$data6,$data7,$data8,$data9,$data10);
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}

	public function dataPeriode(){
		$data = $this->m_api_simremlink->dataPeriode()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}

	public function dataKdn(){
		$data = $this->m_api_simremlink->dataKdn()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}

	public function dataDosenTendikPns(){
		$data = $this->m_api_simremlink->dataDosenTendikPns()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}

	public function dataGolonganPns(){
		$data = $this->m_api_simremlink->dataGolonganPns()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}

	public function dataGrade(){
		$data = $this->m_api_simremlink->dataGrade()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}

	public function dataJabatanAkad(){
		$data = $this->m_api_simremlink->dataJabatanAkad()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}

	public function dataJenjangPendidikan(){
		$data = $this->m_api_simremlink->dataJenjangPendidikan()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}

	public function dataJenisDokumen(){
		$data = $this->m_api_simremlink->dataJenisDokumen()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}

	public function dataJenisPegawai(){
		$data = $this->m_api_simremlink->dataJenisPegawai()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}

	public function dataKelompokJabatan(){
		$data = $this->m_api_simremlink->dataKelompokJabatan()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}

	public function dataKelasJabatan(){
		$data = $this->m_api_simremlink->dataKelasJabatan()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}

	public function dataNamaJabatan(){
		$data = $this->m_api_simremlink->dataNamaJabatan()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}

	public function dataRekening(){
		$data = $this->m_api_simremlink->dataRekening()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}

	public function dataPekerjaan(){
		$data = $this->m_api_simremlink->dataPekerjaan()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}

	public function dataStatusPegawai(){
		$data = $this->m_api_simremlink->dataStatusPegawai()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}

	public function dataStatusSipil(){
		$data = $this->m_api_simremlink->dataStatusSipil()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }
    
    public function tusi(){
        $data = $this->m_api_simremlink->link('tusi');
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }
    
    public function mengajar(){
        $data = $this->m_api_simremlink->link('mengajar');
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }
    
    public function mengajarLainnya(){
        $data = $this->m_api_simremlink->link('mengajar-lainnya');
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }
    
    public function penelitian(){
        $data = $this->m_api_simremlink->link('penelitian');
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }
    
    public function penunjang(){
        $data = $this->m_api_simremlink->link('penunjang');
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }
    
    //=============== peran ============================//
    public function savePeran(){
        if(!empty(trim($this->input->post('id')))){
            $saveData = $this->m_api_simremlink->updateSavePeran();
        }else{
            $saveData = $this->m_api_simremlink->savePeran();
        }
		
    	if($saveData['status'] == TRUE){
            $msg = array(
                'msg'  => 'Data berhasil disimpan',
                'icon' => 'success',
                'link' => 'peran',
            );
        }else{
            $msg = array(
                'msg'  => $saveData['msg'],
                'icon' => $saveData['icon'],
            );
        }
        echo json_encode($msg);
    }

    public function deletePeran(){
        $id      = trim($this->input->post('id'));
        $deleted = $this->m_api_simremlink->deletePeran($id);
        if($deleted == TRUE){
            $msg = array(
                'msg'  => 'Berhasil menghapus data',
                'icon' => 'success',
            );
        }else{
            $msg = array(
                'msg'  => 'Data tidak ditemukan',
                'icon' => 'error',
            );
        }
        echo json_encode($msg);
    }

    public function getPeran(){
        $id    = trim($this->input->post('id'));
        $data  = $this->m_api_simremlink->getPeran($id);
        if($data != false){
            echo json_encode($data);
        }else{
            $msg = array(
                'msg'  => 'Data tidak ditemukan',
                'icon' => 'error',
            );
            echo json_encode($msg);
        }   
    }
    //=============== peran ============================//

    //=============== periode ============================//
    public function savePeriode(){
        if(!empty(trim($this->input->post('id')))){
            $saveData = $this->m_api_simremlink->updateSavePeriode();
        }else{
            $saveData = $this->m_api_simremlink->savePeriode();
        }
		
    	if($saveData['status'] == TRUE){
            $msg = array(
                'msg'  => 'Data berhasil disimpan',
                'icon' => 'success',
                'link' => 'periode',
            );
        }else{
            $msg = array(
                'msg'  => $saveData['msg'],
                'icon' => $saveData['icon'],
            );
        }
        echo json_encode($msg);
    }

    public function deletePeriode(){
        $id      = trim($this->input->post('id'));
        $deleted = $this->m_api_simremlink->deletePeriode($id);
        if($deleted == TRUE){
            $msg = array(
                'msg'  => 'Berhasil menghapus data',
                'icon' => 'success',
            );
        }else{
            $msg = array(
                'msg'  => 'Data tidak ditemukan',
                'icon' => 'error',
            );
        }
        echo json_encode($msg);
    }

    public function getPeriode(){
        $id    = trim($this->input->post('id'));
        $data  = $this->m_api_simremlink->getPeriode($id);
        if($data != false){
            echo json_encode($data);
        }else{
            $msg = array(
                'msg'  => 'Data tidak ditemukan',
                'icon' => 'error',
            );
            echo json_encode($msg);
        }   
    }
    //=============== periode ============================//

    public function riwayatDosen(){
		$data = $this->m_api_simremlink->riwayatDosen()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }

    public function riwayatPegawai(){
		$data = $this->m_api_simremlink->riwayatPegawai();
        echo json_encode($data,JSON_PRETTY_PRINT);
    }

    public function riwayatDosenByNip(){
        $nip    = trim($this->input->post('nip'));
        $data  = $this->m_api_simremlink->riwayatDosenByNip($nip);
        echo json_encode($data);   
    }

    public function riwayatPegawaiByNip(){
        $nip    = trim($this->input->post('nip'));
        $data  = $this->m_api_simremlink->riwayatPegawaiByNip($nip);
        echo json_encode($data);   
    }

    //=============== Rekening ============================//
    public function saveRekening(){
        if(!empty(trim($this->input->post('id')))){
            $saveData = $this->m_api_simremlink->updateSaveRekening();
        }else{
            $saveData = $this->m_api_simremlink->saveRekening();
        }
		
    	if($saveData['status'] == TRUE){
            $msg = array(
                'msg'  => 'Data berhasil disimpan',
                'icon' => 'success',
                'link' => 'rekening',
            );
        }else{
            $msg = array(
                'msg'  => $saveData['msg'],
                'icon' => $saveData['icon'],
            );
        }
        echo json_encode($msg);
    }

    public function deleteRekening(){
        $id      = trim($this->input->post('id'));
        $deleted = $this->m_api_simremlink->deleteRekening($id);
        if($deleted == TRUE){
            $msg = array(
                'msg'  => 'Berhasil menghapus data',
                'icon' => 'success',
            );
        }else{
            $msg = array(
                'msg'  => 'Data tidak ditemukan',
                'icon' => 'error',
            );
        }
        echo json_encode($msg);
    }

    public function getRekening(){
        $id    = trim($this->input->post('id'));
        $data  = $this->m_api_simremlink->getRekening($id);
        if($data != false){
            echo json_encode($data);
        }else{
            $msg = array(
                'msg'  => 'Data tidak ditemukan',
                'icon' => 'error',
            );
            echo json_encode($msg);
        }   
    }
    //=============== Rekening ============================//

    public function testing_riwayat(){
        $data_db = $this->m_api_simremlink->testing_riwayat()->result();
        if($data_db != false):
            echo json_encode($data_db,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }
	
    public function testing_riwayat_by_tgl(){
        $data_db = $this->m_api_simremlink->testing_riwayat_by_tgl();
        if($data_db != false):
            echo json_encode($data_db,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }

    public function testing_riwayat_dt(){
        $data_dt_dosen = $this->m_api_simremlink->testing_riwayat_dt_dosen()->result();
        if($data_dt_dosen != false){
            echo json_encode($data_dt_dosen, JSON_PRETTY_PRINT);
        }else{
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        }
    }
    
    public function testing_riwayat_dt_by_tgl(){
        $data_dt_dosen = $this->m_api_simremlink->testing_riwayat_dt_by_tgl_dosen();
        if($data_dt_dosen != false){
            echo json_encode($data_dt_dosen, JSON_PRETTY_PRINT);
        }else{
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
            
        }
    }

    public function biodata_by_nip(){
        $data_dosen   = $this->m_api_simremlink->biodata_dosen_by_nip()->result();
        
        if($data_dosen != false){
            echo json_encode($data_dosen, JSON_PRETTY_PRINT);
        }else{
            $data_pegawai = $this->m_api_simremlink->biodata_pegawai_by_nip()->result();
            if($data_pegawai != false){
                echo json_encode($data_pegawai, JSON_PRETTY_PRINT);
            }else{
                $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
            }
        }
    }

    public function harga_dt(){
        // $data = $this->m_api_simremlink->harga_dt();
        $data = $this->m_api_simremlink->harga_dt();
        if($data != false){
            echo json_encode($data, JSON_PRETTY_PRINT);
        }else{
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
            
        }
    }

    public function harga_db(){
        $data = $this->m_api_simremlink->harga_db();
        if($data != false){
            echo json_encode($data, JSON_PRETTY_PRINT);
        }else{
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
            
        }
    }

    public function total_uang_remun(){
        // $data = $this->m_api_simremlink->harga_dt();
        $data = $this->m_api_simremlink->total_uang_remun();
        if($data != false){
            echo json_encode($data, JSON_PRETTY_PRINT);
        }else{
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
            
        }
    }

    public function total_poin_remun(){
        $data = $this->m_api_simremlink->total_poin_remun();
    }

    public function riwayatDosenByNipLast(){
        $nip    = trim($this->input->post('nip'));
        $data  = $this->m_api_simremlink->riwayatDosenByNipLast($nip);
        echo json_encode($data);   
    }

    public function pembayaranDosenPegawai(){
        $kode    = trim($this->input->post('kode'));
        $periode    = trim($this->input->post('periode'));
        $data  = $this->m_api_simremlink->pembayaranDosenPegawai($kode,$periode);
        if($data != false){
            echo json_encode($data);
        }else{
            $msg = array(
                'msg'  => 'Data tidak ditemukan',
                'icon' => 'error',
            );
            echo json_encode($msg);
        }   
    }

    public function pembayaranDosenPegawaiP2(){
        $kode    = trim($this->input->post('kode'));
        $periode    = trim($this->input->post('periode'));
        $data  = $this->m_api_simremlink->pembayaranDosenPegawaiP2($kode,$periode);
        if($data != false){
            echo json_encode($data);
        }else{
            $msg = array(
                'msg'  => 'Data tidak ditemukan',
                'icon' => 'error',
            );
            echo json_encode($msg);
        }   
    }

    public function pembayaranDosenPegawaiP2v2(){
        $kode    = trim($this->input->post('kode'));
        $periode    = trim($this->input->post('periode'));
        $data  = $this->m_api_simremlink->pembayaranDosenPegawaiP2v2($kode,$periode);
        if($data != false){
            echo json_encode($data);
        }else{
            $msg = array(
                'msg'  => 'Data tidak ditemukan',
                'icon' => 'error',
            );
            echo json_encode($msg);
        }   
    }

    public function pembayaranP1(){
        $nip    = trim($this->input->post('nip'));
        $periode    = trim($this->input->post('periode'));
        $jenis    = trim($this->input->post('jenis'));
        $data  = $this->m_api_simremlink->pembayaranP1($nip,$periode,$jenis);
        echo json_encode($data);   
    }
}