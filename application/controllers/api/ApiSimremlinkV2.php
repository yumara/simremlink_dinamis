<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ApiSimremlinkV2 extends CI_Controller {
	public function __construct(){
        parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin:*');
	}
    
    public function tusi(){
        $data = $this->m_api_simremlink_v2->link('tusi');
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }
    
    public function mengajar(){
        $data = $this->m_api_simremlink_v2->link('mengajar');
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }
    
    public function mengajarLainnya(){
        $data = $this->m_api_simremlink_v2->link('mengajar-lainnya');
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }
    
    public function penelitian(){
        $data = $this->m_api_simremlink_v2->link('penelitian');
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }
    
    public function penunjang(){
        $data = $this->m_api_simremlink_v2->link('penunjang');
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }

    public function testing_riwayat(){
        $data_db = $this->m_api_simremlink_v2->testing_riwayat()->result();
        if($data_db != false):
            echo json_encode($data_db,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }
	
    public function testing_riwayat_by_tgl(){
        $data_db = $this->m_api_simremlink_v2->testing_riwayat_by_tgl();
        if($data_db != false):
            echo json_encode($data_db,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }

    public function testing_riwayat_dt(){
        $data_dt_dosen = $this->m_api_simremlink_v2->testing_riwayat_dt_dosen()->result();
        if($data_dt_dosen != false){
            echo json_encode($data_dt_dosen, JSON_PRETTY_PRINT);
        }else{
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        }
    }
    
    public function testing_riwayat_dt_by_tgl(){
        $data_dt_dosen = $this->m_api_simremlink_v2->testing_riwayat_dt_by_tgl_dosen();
        if($data_dt_dosen != false){
            echo json_encode($data_dt_dosen, JSON_PRETTY_PRINT);
        }else{
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
            
        }
    }

    public function biodata_by_nip(){
        $data_dosen   = $this->m_api_simremlink_v2->biodata_dosen_by_nip()->result();
        
        if($data_dosen != false){
            echo json_encode($data_dosen, JSON_PRETTY_PRINT);
        }else{
            $data_pegawai = $this->m_api_simremlink_v2->biodata_pegawai_by_nip()->result();
            if($data_pegawai != false){
                echo json_encode($data_pegawai, JSON_PRETTY_PRINT);
            }else{
                $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
            }
        }
    }

    public function harga_dt(){
        $data = $this->m_api_simremlink_v2->harga_dt();
        if($data != false){
            echo json_encode($data, JSON_PRETTY_PRINT);
        }else{
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);   
        }
    }

    public function harga_db(){
        $data = $this->m_api_simremlink_v2->harga_db();
        if($data != false){
            echo json_encode($data, JSON_PRETTY_PRINT);
        }else{
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
            
        }
    }

    public function total_uang_remun(){
        $data = $this->m_api_simremlink_v2->total_uang_remun();
        if($data != false){
            echo json_encode($data, JSON_PRETTY_PRINT);
        }else{
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
            
        }
    }

    public function total_uang_remunDB(){
        $data = $this->m_api_simremlink_v2->total_uang_remun_db();
        if($data != false){
            echo json_encode($data, JSON_PRETTY_PRINT);
        }else{
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
            
        }
    }

    public function total_poin_remun(){
        $data = $this->m_api_simremlink_v2->total_poin_remun();
        echo json_encode($data,JSON_PRETTY_PRINT);
    }

    public function riwayatDosenByNipLast(){
        $nip    = trim($this->input->post('nip'));
        $data  = $this->m_api_simremlink_v2->riwayatDosenByNipLast($nip);
        echo json_encode($data);   
    }

    public function pembayaranDosenPegawai(){
        $kode    = trim($this->input->post('kode'));
        $periode    = trim($this->input->post('periode'));
        $data  = $this->m_api_simremlink_v2->pembayaranDosenPegawai($kode,$periode);
        if($data != false){
            echo json_encode($data);
        }else{
            $msg = array(
                'msg'  => 'Data tidak ditemukan',
                'icon' => 'error',
            );
            echo json_encode($msg);
        }   
    }

    public function pembayaranP1(){
        $nip    = trim($this->input->post('nip'));
        $periode    = trim($this->input->post('periode'));
        $jenis    = trim($this->input->post('jenis'));
        $data  = $this->m_api_simremlink_v2->pembayaranP1($nip,$periode,$jenis);
        echo json_encode($data);   
    }

    public function syncPembayaran(){
		$data = $this->m_api_simremlink_v2->syncPembayaran();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
        // $data = $this->db->get_where("transaksi_pembayaran_p2",['kode_unit' => "C1"])->result();
        // foreach ($data as $key => $value) {
        //     $check = $this->db->get_where("transaksi_pembayaran_p2_v3",['nip'=>$value->nip]);
        //     if($check->num_rows() <= 0){
        //         echo $value->nama."\n";
        //     }
        // }
    }

    public function createView(){
        $this->m_case_1->createTableView();
    }
}