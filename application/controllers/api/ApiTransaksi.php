<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ApiTransaksi extends CI_Controller {
	public function __construct(){
        parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin:*');
	}

	public function kelebihanMengajar(){
		$data = $this->m_api_transaksi->kelebihanMengajar()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }
    
    public function pembimbingAkademik(){
		$data = $this->m_api_transaksi->pembimbingAkademik()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }
    
    public function pembimbingSkripsiTesisDisertasi(){
		$data = $this->m_api_transaksi->pembimbingSkripsiTesisDisertasi()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }
    
    public function pengujiSkripsiTesisDisertasi(){
		$data = $this->m_api_transaksi->pengujiSkripsiTesisDisertasi()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }
    
    public function mengajarLainnya(){
		$data = $this->m_api_transaksi->mengajarLainnya()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }
    
    public function riwayatPenghargaanDosen(){
		$data = $this->m_api_transaksi->riwayatPenghargaanDosen()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }
    
    public function riwayatPenghargaanPns(){
		$data = $this->m_api_transaksi->riwayatPenghargaanPns()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }
    
    public function capaianSkpDosenDt(){
        //$data = $this->m_api_transaksi->capaianSkpDosenDt()->result();
        $data = $this->m_api_transaksi->capaianSkpDosenDtKonversi()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}

    public function capaianSkpPegawai(){
        //$data = $this->m_api_transaksi->capaianSkpPegawai()->result();
        $data = $this->m_api_transaksi->capaianSkpPegawaiKonversi()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }
    
    public function nilaiSikapDanPerilakuPegawai(){
        //$data = $this->m_api_transaksi->nilaiSikapDanPerilakuPegawai()->result();
        $data = $this->m_api_transaksi->nilaiSikapDanPerilakuPegawaiKonversi()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}

    public function capaianBkdDosen(){
		$data = $this->m_api_transaksi->capaianBkdDosen()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}

    public function absensiPegawaiTendik(){
		$data = $this->m_api_transaksi->absensiPegawaiTendik()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
	}

    public function absensiDosenPns(){
		$data = $this->m_api_transaksi->absensiDosenPns();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }
    
    public function lhkasnLhkpnPegawaiPns(){
		$data = $this->m_api_transaksi->lhkasnLhkpnPegawaiPns()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }
    
    public function hukumanDisiplinPegawaiPns(){
		$data = $this->m_api_transaksi->hukumanDisiplinPegawaiPns()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }
    
    public function aktivitas(){
		$data = $this->m_api_transaksi->aktivitas()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }

    public function cekNIP($nip){
        //return "PEG0557";
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );
        $api_pns = file_get_contents('https://dev.unimed.ac.id/sso-api/simremlinkjwt/pegawai',false, stream_context_create($arrContextOptions));
        $json_pns = json_decode($api_pns);
        foreach($json_pns as $key => $item_pns){
            if($nip == $item_pns->nip){
                if(isset($item_pns->kode_pegawai)){
                    return $item_pns->kode_pegawai;
                }else if(isset($item_pns->kode_dosen)){
                    return $item_pns->kode_dosen;
                }
            }
        }
    }

    public function ewkp(){
        $query = $this->db->get('simremlink_data_peran')->result();
        return $query;
    }

    public function tableTupoksi(){
        $periode = trim($this->input->get('periode','true'));
        $kode    = trim($this->input->get('kode','true'));
        $nip     = trim($this->input->get('nip','true'));
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );
        $api_no8 = json_decode(file_get_contents('https://dev.unimed.ac.id/sso-statistik/api/transaksi/capaian-skp-dosen-dt',false, stream_context_create($arrContextOptions)), TRUE);
        $api_no9 = json_decode(file_get_contents('https://dev.unimed.ac.id/sso-statistik/api/transaksi/capaian-skp-pegawai',false, stream_context_create($arrContextOptions)), TRUE);
        $api_no10 = json_decode(file_get_contents('https://dev.unimed.ac.id/sso-statistik/api/transaksi/nilai-sikap-dan-perilaku-pegawai',false, stream_context_create($arrContextOptions)), TRUE);
        $api_no11 = json_decode(file_get_contents('https://dev.unimed.ac.id/sso-statistik/api/transaksi/capaian-bkd-dosen',false, stream_context_create($arrContextOptions)), TRUE);
        $api_no12 = json_decode(file_get_contents('https://dev.unimed.ac.id/sso-statistik/api/transaksi/absensi-pegawai-tendik',false, stream_context_create($arrContextOptions)), TRUE); //Error
        $api_no13 = json_decode(file_get_contents('https://dev.unimed.ac.id/sso-statistik/api/transaksi/absensi-dosen-pns',false, stream_context_create($arrContextOptions)), TRUE);
        $api_no14 = json_decode(file_get_contents('https://dev.unimed.ac.id/sso-statistik/api/transaksi/lhkasn-lhkpn-pegawai-pns',false, stream_context_create($arrContextOptions)), TRUE);
        $api_no15 = json_decode(file_get_contents('https://dev.unimed.ac.id/sso-statistik/api/transaksi/hukuman-disiplin-pegawai-pns',false, stream_context_create($arrContextOptions)), TRUE);
        $merge = array_merge($api_no8,$api_no9,$api_no10,$api_no11,$api_no12,$api_no13,$api_no14,$api_no15);
        // if($kode == null){
        //     $kode = $this->cekNIP($nip);
        // }
        $peran = $this->ewkp();
        $aktivitas = $merge;
        $output = array();
        foreach ($aktivitas as $key => $item) {
            // $kode2 = '';
            // if(isset($item['kode_pegawai'])){
            //     $kode2 = $item['kode_pegawai'];
            // }else if(isset($item['kode_dosen'])){
            //     $kode2 = $item['kode_dosen'];
            // }
            if ($item['kode_pegawai'] == $kode && $item['periode'] == $periode) {
                $kode_peran = $item['peran'];
                foreach($peran as $key_peran => $item_peran){
                    if($item_peran->kdeperan == $kode_peran){
                        if($item['poin_remlink'] == 'njir'){
                            if($item['rumus'] == "yes"){
                                $ewkp = ( $item_peran->ewkp * $item['capaian'] ) / 100;
                            }else{
                                $ewkp = $item_peran->ewkp;
                            }
                        }else{
                            if($item['rumus'] == "yes"){
                                $ewkp = ( $item_peran->ewkp * $item['capaian'] ) / 100;
                                $ewkp = number_format((float)$ewkp,2);
                            }else{
                                $ewkp = number_format((float)$item['poin_remlink'],2);
                            }
                        }
                        $set = [
                            'nama_peran' => $item_peran->nmaperan,
                            'capaian' => $item['capaian'],
                            'ewkp' => $ewkp
                        ];      
                        array_push($output, $set);
                    }
                }
            }
            /*$set = [
                'capaian' => $item['capaian'],
            ];      
            array_push($output, $set);*/
        }
        echo json_encode($output, JSON_PRETTY_PRINT);
            
    }
    
    public function tableMengajar(){
        $periode = trim($this->input->get('periode','true'));
        $kode    = trim($this->input->get('kode','true'));
        $nip     = trim($this->input->get('nip','true'));
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );
        $api_no1 = json_decode(file_get_contents('https://dev.unimed.ac.id/sso-statistik/api/transaksi/kelebihan-mengajar',false, stream_context_create($arrContextOptions)), TRUE);
        if($kode == null){
            $kode = $this->cekNIP($nip);
        }
        $peran = $this->ewkp();
        $aktivitas = $api_no1;
        $output = array();
        foreach ($aktivitas as $key => $item) {
            $kode2 = '';
            if(isset($item['kode_pegawai'])){
                $kode2 = $item['kode_pegawai'];
            }else if(isset($item['kode_dosen'])){
                $kode2 = $item['kode_dosen'];
            }
            if ($kode2 == $kode && $item['periode'] == $periode) {
                $kode_peran = $item['peran'];
                foreach($peran as $key_peran => $item_peran){
                    if($item_peran->kdeperan == $kode_peran){
                        $volume = $item['volume'];
                        $poin = $item_peran->ewkp * $volume;
                        $set = [
                            'nama_peran' => $item_peran->nmaperan,
                            'ewkp' => $poin
                        ];      
                        array_push($output, $set);
                    }
                }
            }
        }
        echo json_encode($output, JSON_PRETTY_PRINT);
    }
    
    public function tableMengajarLainnya(){
        $periode = trim($this->input->get('periode','true'));
        $kode    = trim($this->input->get('kode','true'));
        $nip     = trim($this->input->get('nip','true'));
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );
        $api_no2 = json_decode(file_get_contents('https://dev.unimed.ac.id/sso-statistik/api/transaksi/pembimbing-akademik',false, stream_context_create($arrContextOptions)), TRUE);
        $api_no3 = json_decode(file_get_contents('https://dev.unimed.ac.id/sso-statistik/api/transaksi/pembimbing-skripsi-tesis-disertasi',false, stream_context_create($arrContextOptions)), TRUE);
        $api_no4 = json_decode(file_get_contents('https://dev.unimed.ac.id/sso-statistik/api/transaksi/penguji-skripsi-tesis-disertasi',false, stream_context_create($arrContextOptions)), TRUE);
        $api_no5 = json_decode(file_get_contents('https://dev.unimed.ac.id/sso-statistik/api/transaksi/mengajar-lainnya',false, stream_context_create($arrContextOptions)), TRUE);
        $merge = array_merge($api_no2,$api_no3, $api_no4, $api_no5);
        $peran = $this->ewkp();
        // $aktivitas = $merge;
        $output = array();
        foreach ($merge as $key => $item) {
            $kode2 = '';
            if(isset($item['kode_pegawai'])){
                $kode2 = $item['kode_pegawai'];
            }else if(isset($item['kode_dosen'])){
                $kode2 = $item['kode_dosen'];
            }
            if ($kode2 == $kode && $item['periode'] == $periode) {
                $kode_peran = $item['peran'];
                foreach($peran as $key_peran => $item_peran){
                    if($item_peran->kdeperan == $kode_peran){
                        $volume = $item['volume'];
                        $poin = $item_peran->ewkp * $volume;
                        $set = [
                            'nama_peran' => $item_peran->nmaperan,
                            'volume' => $item['volume'],
                            'ewkp' => number_format($poin, 2) 
                        ];      
                        array_push($output, $set);
                    }
                }
            }
        }
        echo json_encode($output, JSON_PRETTY_PRINT);
    }
    
    public function tablePenelitian(){

    }
    
    public function tablePengabdian(){

    }
    
    public function tablePenunjang(){
        $periode = trim($this->input->get('periode','true'));
        $kode    = trim($this->input->get('kode','true'));
        $nip     = trim($this->input->get('nip','true'));
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );
        $api_no16 = json_decode(file_get_contents('https://dev.unimed.ac.id/sso-statistik/api/transaksi/aktivitas',false, stream_context_create($arrContextOptions)), TRUE);
        if($kode == null){
            $kode = $this->cekNIP($nip);
        }
        $peran = $this->ewkp();
        $aktivitas = $api_no16;
        $output = array();
        foreach ($aktivitas as $key => $item) {
            $kode2 = '';
            if(isset($item['kode_pegawai'])){
                $kode2 = $item['kode_pegawai'];
            }else if(isset($item['kode_dosen'])){
                $kode2 = $item['kode_dosen'];
            }
            if ($kode2 == $kode && $item['periode'] == $periode) {
                $kode_peran = $item['peran'];
                foreach($peran as $key_peran => $item_peran){
                    if($item_peran->kdeperan == $kode_peran){
                        $poin = $item_peran->ewkp * $item['volume'];
                        $set = [
                            'judul' => $item['judul'],
                            'nama_peran' => $item_peran->nmaperan,
                            'volume' => $item['volume'],
                            'ewkp' => $poin,
                            'dok' => $item['dokumen_pendukung']
                        ];      
                        array_push($output, $set);
                    }
                }
            }
        }
        echo json_encode($output, JSON_PRETTY_PRINT);
    }
    
    public function tablePenghargaan(){
        $periode = trim($this->input->get('periode','true'));
        $kode    = trim($this->input->get('kode','true'));
        $nip     = trim($this->input->get('nip','true'));
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );
        $api_no6 = json_decode(file_get_contents('https://dev.unimed.ac.id/sso-statistik/api/transaksi/riwayat-penghargaan-dosen',false, stream_context_create($arrContextOptions)), TRUE);
        $api_no7 = json_decode(file_get_contents('https://dev.unimed.ac.id/sso-statistik/api/transaksi/riwayat-pengharggan-pns',false, stream_context_create($arrContextOptions)), TRUE);
        $api_no17 = json_decode(file_get_contents('https://dev.unimed.ac.id/sso-statistik/api/transaksi/penghargaan',false, stream_context_create($arrContextOptions)), TRUE);
        $merge = array_merge($api_no6,$api_no7, $api_no17);
        if($kode == null){
            $kode = $this->cekNIP($nip);
        }
        $peran = $this->ewkp();
        $aktivitas = $merge;
        $output = array();
        foreach ($aktivitas as $key => $item) {
            $kode2 = '';
            if(isset($item['kode_pegawai'])){
                $kode2 = $item['kode_pegawai'];
            }else if(isset($item['kode_dosen'])){
                $kode2 = $item['kode_dosen'];
            }
            if ($kode2 == $kode && $item['periode'] == $periode) {
                $kode_peran = $item['peran'];
                foreach($peran as $key_peran => $item_peran){
                    if($item_peran->kdeperan == $kode_peran){
                        $set = [
                            'nama_peran' => $item_peran->nmaperan,
                            'ewkp' => $item_peran->ewkp
                        ];      
                        array_push($output, $set);
                    }
                }
            }
        }
        echo json_encode($output, JSON_PRETTY_PRINT);
    }
    //=======================================================
    public function capaianSkpDosenDtKonversi(){
		$data = $this->m_api_transaksi->capaianSkpDosenDtKonversi()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }

    public function capaianSkpPegawaiKonversi(){
		$data = $this->m_api_transaksi->capaianSkpPegawaiKonversi()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }

    public function nilaiSikapDanPerilakuPegawaiKonversi(){
		$data = $this->m_api_transaksi->nilaiSikapDanPerilakuPegawaiKonversi()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }

    public function penghargaan(){
		$data = $this->m_api_transaksi->penghargaan()->result();
        if($data != false):
            echo json_encode($data,JSON_PRETTY_PRINT);
        else:
            $msg = array(
                'msg'    => 'Data tidak ditemukan',
                'status' => FALSE,
            );
            echo json_encode($msg);
        endif;
    }
    
}