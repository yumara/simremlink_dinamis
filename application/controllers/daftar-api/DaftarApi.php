<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DaftarApi extends CI_Controller {
	public function __construct(){
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }

    public function akad(){
        $grup = $this->session->userdata('grup');
		if($grup != null){
			$data['title'] 		   = "Daftar Api AKAD";
			$data['active']		   = "api-a";
			$data['grup']		   = $grup;
			$this->themes->Admin('daftar-api/akad/index',$data);
		}else{
			$this->load->view('direct-link/index');
		}
    }

    public function simpeg(){
        $grup = $this->session->userdata('grup');
		if($grup != null){
			$data['title'] 		   = "Daftar Api SIMPEG";
			$data['active']		   = "api-b";
			$data['grup']		   = $grup;
			$this->themes->Admin('daftar-api/simpeg/index',$data);
		}else{
			$this->load->view('direct-link/index');
		}
    }

    public function esk(){
        $grup = $this->session->userdata('grup');
		if($grup != null){
			$data['title'] 		   = "Daftar Api E-SK";
			$data['active']		   = "api-c";
			$data['grup']		   = $grup;
			$this->themes->Admin('daftar-api/esk/index',$data);
		}else{
			$this->load->view('direct-link/index');
		}
    }

    public function lppm(){
        $grup = $this->session->userdata('grup');
		if($grup != null){
			$data['title'] 		   = "Daftar Api LPPM";
			$data['active']		   = "api-d";
			$data['grup']		   = $grup;
			$this->themes->Admin('daftar-api/lppm/index',$data);
		}else{
			$this->load->view('direct-link/index');
		}
    }

    public function simremlink(){
        $grup = $this->session->userdata('grup');
		if($grup != null){
			$data['title'] 		   = "Daftar Api SIMREMLINK";
			$data['active']		   = "api-e";
			$data['grup']		   = $grup;
			$this->themes->Admin('daftar-api/simremlink/index',$data);
		}else{
			$this->load->view('direct-link/index');
		}
	}
	
	public function transaksi(){
        $grup = $this->session->userdata('grup');
		if($grup != null){
			$data['title'] 		   = "Daftar Api Transaksi";
			$data['active']		   = "api-f";
			$data['grup']		   = $grup;
			$this->themes->Admin('daftar-api/transaksi/index',$data);
		}else{
			$this->load->view('direct-link/index');
		}
    }

}