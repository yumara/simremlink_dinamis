<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class BagianPegawai extends CI_Controller {
	public function __construct(){
        parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
	}

	public function index(){
		$grup = $this->session->userdata('grup');
		if($grup != null){
			$data['title'] 		   = "Bagian Pegawai";
			$data['active']		   = "simpeg-c";
			$data['grup']		   = $grup;
			$this->themes->Admin('simpeg/bagian-pegawai/index',$data);
		}else{
			$this->load->view('direct-link/index');
		}
	}

}