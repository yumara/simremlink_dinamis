<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DosenPhl extends CI_Controller {
	public function __construct(){
        parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
	}

	public function index(){
		$grup = $this->session->userdata('grup');
		if($grup != null){
			$data['title'] 		   = "Dosen Phl";
			$data['active']		   = "simpeg-d";
			$data['grup']		   = $grup;
			$this->themes->Admin('simpeg/dosen-phl/index',$data);
		}else{
			$this->load->view('direct-link/index');
		}
	}

	public function refreshDosenPhl(){
		$deleted = $this->M_DosenPhl->refreshDosenPhl();
        if($deleted == TRUE){
            $msg = array(
                'msg'  => 'Success updating data',
				'icon' => 'success',
				'link' => 'simpeg/dosen-phl'
            );
        }else{
            $msg = array(
                'msg'  => 'Unable to update data',
                'icon' => 'error',
            );
        }
		echo json_encode($msg);
	}
}