<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class DosenPnsSimremlink extends CI_Controller {
	public function __construct(){
        parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
	}

	public function index(){
		$grup = $this->session->userdata('grup');
		if($grup != null){
			$data['title'] 		   = "Dosen PNS Simremlin";
			$data['active']		   = "simpeg-a";
			$data['grup']		   = $grup;
			$this->themes->Admin('simpeg/dosen-pns-simremlink/index',$data);
		}else{
			$this->load->view('direct-link/index');
		}
	}

	public function refreshDosenPnsSimremlink(){
		$deleted = $this->m_dosenpnssimremlink->refreshDosenPnsSimremlink();
        if($deleted == TRUE){
            $msg = array(
                'msg'  => 'Success updating data',
				'icon' => 'success',
				'link' => 'simpeg/dosen-pns-simremlink'
            );
        }else{
            $msg = array(
                'msg'  => 'Unable to update data',
                'icon' => 'error',
            );
        }
		echo json_encode($msg);
	}

	public function tes(){
		$arrContextOptions=array(
			"ssl"=>array(
				"verify_peer"=>false,
				"verify_peer_name"=>false,
			),
		);  
		$linkUnit = "https://simpeg.unimed.ac.id/jsonsimpeg/data/unit.php?api_key=83b956f064c53aca3155c93e2aecd2e7";
		$unit = json_decode(file_get_contents($linkUnit,false, stream_context_create($arrContextOptions)),true);
		echo"Unit ".sizeof($unit)."<br>";

		$linkJurusan = "https://simpeg.unimed.ac.id/jsonsimpeg/data/jurusan.php?api_key=a409bb11ce3545bf46c17f2f96f36770";
		$jurusan = json_decode(file_get_contents($linkJurusan,false, stream_context_create($arrContextOptions)),true);
		echo"jurusan ".sizeof($jurusan)."<br>";

		$linkProdi = "https://simpeg.unimed.ac.id/jsonsimpeg/data/prodi.php?api_key=90a860e51ed04a1c8ac0e79a0e82cb8d";
		$prodi = json_decode(file_get_contents($linkProdi,false, stream_context_create($arrContextOptions)),true);
		echo"prodi ".sizeof($prodi)."<br>";

		$linkKedudukan = "https://simpeg.unimed.ac.id/jsonsimpeg/data/kedudukan.php?api_key=a7bcce93ba39a8614bc17f9528f817d1";
		$kedudukan = json_decode(file_get_contents($linkKedudukan,false, stream_context_create($arrContextOptions)),true);
		echo"kedudukan ".sizeof($kedudukan)."<br>";

		$linkStatus = "https://simpeg.unimed.ac.id/jsonsimpeg/data/status_pegawai.php?api_key=32082e6f1cb84c159520806286dfd96f";
		$status = json_decode(file_get_contents($linkStatus,false, stream_context_create($arrContextOptions)),true);
		echo"status ".sizeof($status)."<br>";

		$linkGolongan = "https://simpeg.unimed.ac.id/jsonsimpeg/data/golongan.php?api_key=732ad3502ece5dc896b916b9b2e4db35";
		$golongan = json_decode(file_get_contents($linkGolongan,false, stream_context_create($arrContextOptions)),true);
		echo"golongan ".sizeof($golongan)."<br>";

		$linkJabatanFungsional = "https://simpeg.unimed.ac.id/jsonsimpeg/data/jabatan_fungsional.php?api_key=7c950102d7f3d7f54a459a18b9896013";
		$jabatanFungsional = json_decode(file_get_contents($linkJabatanFungsional,false, stream_context_create($arrContextOptions)),true);
		echo"jabatanFungsional ".sizeof($jabatanFungsional)."<br>";

		$linkTugasTambahan = "https://simpeg.unimed.ac.id/jsonsimpeg/data/tugas_tambahan.php?api_key=dd7db5d1c2dd1eff113571d530ba1a90";
		$tugasTambahan = json_decode(file_get_contents($linkTugasTambahan,false, stream_context_create($arrContextOptions)),true);
		echo"tugasTambahan ".sizeof($tugasTambahan)."<br>";

		$linkAktif = "https://simpeg.unimed.ac.id/jsonsimpeg/data/status_aktif.php?api_key=7858b904c37985bfa23e2083f4c5478b";
		$aktif = json_decode(file_get_contents($linkAktif,false, stream_context_create($arrContextOptions)),true);
		echo"aktif non aktif".sizeof($aktif)."<br>";
	}

}