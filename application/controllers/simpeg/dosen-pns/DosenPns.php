<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class DosenPns extends CI_Controller {
	public function __construct(){
        parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
	}

	public function index(){
		$grup = $this->session->userdata('grup');
		if($grup != null){
			$data['title'] 		   = "Dosen Pns";
			$data['active']		   = "simpeg-b";
			$data['grup']		   = $grup;
			$this->themes->Admin('simpeg/dosen-pns/index',$data);
		}else{
			$this->load->view('direct-link/index');
		}
	}

	public function refreshDosenPns(){
		$deleted = $this->M_DosenPns->refreshDosenPns();
        if($deleted == TRUE){
            $msg = array(
                'msg'  => 'Success updating data',
				'icon' => 'success',
				'link' => 'simpeg/dosen-pns'
            );
        }else{
            $msg = array(
                'msg'  => 'Unable to update data',
                'icon' => 'error',
            );
        }
		echo json_encode($msg);
	}

}