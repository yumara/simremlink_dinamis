<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class PegawaiPhl extends CI_Controller {
	public function __construct(){
        parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
	}

	public function index(){
		$grup = $this->session->userdata('grup');
		if($grup != null){
			$data['title'] 		   = "Pegawai PHL";
			$data['active']		   = "simpeg-f";
			$data['grup']		   = $grup;
			$this->themes->Admin('simpeg/pegawai-phl/index',$data);
		}else{
			$this->load->view('direct-link/index');
		}
	}

	public function refreshPegawaiPhl(){
		$deleted = $this->M_PegawaiPhl->refreshPegawaiPhl();
        if($deleted == TRUE){
            $msg = array(
                'msg'  => 'Success updating data',
				'icon' => 'success',
				'link' => 'simpeg/pegawai-phl'
            );
        }else{
            $msg = array(
                'msg'  => 'Unable to update data',
                'icon' => 'error',
            );
        }
		echo json_encode($msg);
	}
}