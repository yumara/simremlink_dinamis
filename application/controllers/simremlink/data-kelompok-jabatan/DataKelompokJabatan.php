<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class StatusAktifNonAktif extends CI_Controller {
	public function __construct(){
        parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		//header('Content-Type: application/json');
	}

	public function index(){
		$grup = $this->session->userdata('grup');
		if($grup != null){
			$data['title'] 		   = "Status Aktif Non Aktif Pegawai dan Dosen";
			$data['active']		   = "simpeg-a";
			$data['grup']		   = $grup;
			$this->themes->Admin('simpeg/status-aktif-non-aktif-pegawai-dan-dosen/index',$data);
		}else{
			return redirect('https://portal.unimed.ac.id/sso-unimed/direct-link');
		}
	}
}