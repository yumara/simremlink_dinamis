<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	public function __construct(){
        parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		//header('Content-Type: application/json');
	}

	public function index(){
		$grup = $this->session->userdata('grup');
		if($grup != null){
			$data['title'] 		   = "Dashboard";
			$data['active']		   = "dashboard";
			$data['grup']		   = $grup;
			$this->themes->Admin('dashboard/index',$data);
		}else{
			return redirect('https://portal.unimed.ac.id/sso-unimed/direct-link');
		}
	}

}