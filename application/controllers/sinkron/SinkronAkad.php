<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class SinkronAkad extends CI_Controller {
	public function __construct(){
        parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		//header('Content-Type: application/json');
	}

	public function index(){
		$grup = $this->session->userdata('grup');
		if($grup != null):
			$data['title'] 		   = "Sinkron Akad";
			$data['active']		   = "sinkron-a";
			$data['grup']		   = $grup;
			$this->themes->Admin('sinkron/akad/index',$data);
		else:
			$this->load->view('direct-link/index');
		endif;
	}

	public function saveSinkronAkad(){
        $saveData = $this->m_sinkron_akad->saveSinkronAkad();
        $msg = array(
            'msg'  => 'Data berhasil disinkron',
            'icon' => 'success',
        );
        echo json_encode($msg);
    }
}