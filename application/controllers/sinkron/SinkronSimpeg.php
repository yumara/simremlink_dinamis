<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class SinkronSimpeg extends CI_Controller {
	public function __construct(){
        parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		//header('Content-Type: application/json');
	}

	public function index(){
		$grup = $this->session->userdata('grup');
		if($grup != null):
			$data['title'] 		   = "Sinkron Simpeg";
			$data['active']		   = "sinkron-b";
			$data['grup']		   = $grup;
			$this->themes->Admin('sinkron/simpeg/index',$data);
		else:
			$this->load->view('direct-link/index');
		endif;
	}

	public function saveSinkronSimpeg(){
        $saveData = $this->m_sinkron_simpeg->saveSinkronSimpeg();
        $msg = array(
            'msg'  => 'Data berhasil disinkron',
            'icon' => 'success',
        );
        echo json_encode($msg);
    }

}