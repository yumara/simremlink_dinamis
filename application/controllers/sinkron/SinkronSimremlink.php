<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SinkronSimremlink extends CI_Controller {
	public function __construct(){
        parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		//header('Content-Type: application/json');
	}

	public function index(){
		$grup = $this->session->userdata('grup');
		if($grup != null):
			$data['title'] 		   = "Sinkron Simremlink";
			$data['active']		   = "sinkron-e";
			$data['grup']		   = $grup;
			$this->themes->Admin('sinkron/simremlink/index',$data);
		else:
			$this->load->view('direct-link/index');
		endif;
	}
	
}