<?php
if(!defined('BASEPATH')) exit('no file allowed');
class Themes{
    protected $_ci;
     function __construct(){
        $this->_ci =&get_instance();
    }
    function Admin($theme, $data=null){
        $data['content']    =$this->_ci->load->view($theme,$data,true);
        $data['header']     =$this->_ci->load->view('theme/section/header.php',$data,true);
        $data['navbar']     =$this->_ci->load->view('theme/section/navbar.php',$data,true);
        $data['footer']     =$this->_ci->load->view('theme/section/footer.php',$data,true);
        $data['sidebar']    =$this->_ci->load->view('theme/section/sidebar.php',$data,true);
        $data['modal']      =$this->_ci->load->view('theme/section/modal.php',$data,true);
        $data['js']         =$this->_ci->load->view('theme/section/js.php',$data,true);
        $this->_ci->load->view('theme/AdminTemplate.php', $data);
    }

}