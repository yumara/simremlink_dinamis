<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_api_akad extends CI_Model{
    public function __construct(){
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }

    public function kelebihanMengajar(){
		return $this->db->get('akad_kelebihan_mengajar');
    }
    
    public function pembimbingAkademik(){
		return $this->db->get('akad_pembimbing_akademik');
    }
    
    public function pembimbingSkripsiTesisDisertasi(){
        return $this->db->get('akad_pembimbing_skripsi_tesis_disertasi');
    }

    public function pengujiSkripsiTesisDisertasi(){
        return $this->db->get('akad_penguji_skripsi_tesis_disertasi');
    }

    //============== api aktivitas untuk simremlink ============================
    public function pengujiSkripsiTesisDisertasiSimremlink(){
        return $this->db->query("SELECT priode as periode, kode_dosen as kode_pegawai,kdperan as kode_peran, 
        cast(NULL as char) as capaian, 
        cast(NULL as char) as volume, 
        cast(NULL as char) as dokumen_pendukung, 
        cast('AKAD' as char) as sumber 
        FROM `akad_penguji_skripsi_tesis_disertasi`");
    }

    public function pembimbingSkripsiTesisDisertasiSimremlink(){
        return $this->db->query("SELECT priode as periode, kode_dosen as kode_pegawai,kdperan as kode_peran, 
        cast(NULL as char) as capaian, 
        cast(NULL as char) as volume, 
        cast(NULL as char) as dokumen_pendukung, 
        cast('AKAD' as char) as sumber FROM `akad_pembimbing_skripsi_tesis_disertasi`");
    }
    //============== end api aktivitas untuk simremlink ========================

    public function mengajarLainnya(){
        return $this->db->get('akad_mengajar_lainnya');
    }
}