<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_api_esk extends CI_Model{
    public function __construct(){
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }

    public function aktivitas(){
		return $this->db->get('esk_aktivitas');
    }
    
    //============== api aktivitas untuk simremlink ========================
    public function aktivitasSimremlink(){
        return $this->db->query("SELECT periode, kode_pegawai,capaian,peran as kode_peran,volume,dokumen_pendukung, 
        cast('E-SK' as char) as sumber
        FROM `esk_aktivitas`
        WHERE periode IS NOT NULL AND periode != ''");
      }
    //============== end api aktivitas untuk simremlink ====================
}