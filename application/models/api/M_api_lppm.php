<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_api_lppm extends CI_Model{
  public function __construct(){
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
  }

  public function penelitian(){
		return $this->db->get('lppm_penelitian');
    }
    
  public function pengabdian(){
		return $this->db->get('lppm_pengabdian');
  }
  
  public function luaranPenelitianMandiri(){
		return $this->db->get('lppm_luaran_penelitian_mandiri');
  }
  
  public function luaranPengabdianMandiri(){
		return $this->db->get('lppm_luaran_pengabdian_mandiri');
  }
  
  public function dataSubmissionPenelitian(){
		return $this->db->get('lppm_data_submission_penelitian');
  }
  
  public function dataSubmissionPenelitianAnggota(){
		return $this->db->get('lppm_data_submission_penelitian_anggota');
  }
  
  public function dataSubmissionPenelitianKetua(){
		return $this->db->get('lppm_data_submission_penelitian_ketua');
  }
  
  public function dataSubmissionPenelitianLuaran(){
		return $this->db->get('lppm_data_submission_penelitian_luaran');
  }
  
  //============== api aktivitas untuk simremlink ========================
  public function luaranPenelitianMandiriSimremlink(){
    return $this->db->query("SELECT periode, kode_pegawai,capaian,peran as kode_peran,volume,dokumen_pendukung, 
    cast('LPPM' as char) as sumber
    FROM `lppm_luaran_penelitian_mandiri`
    WHERE periode IS NOT NULL AND periode != ''");
  }

  public function luaranPengabddianMandiriSimremlink(){
    return $this->db->query("SELECT periode, kode_pegawai,capaian,peran as kode_peran,volume,dokumen_pendukung, 
    cast('LPPM' as char) as sumber
    FROM `lppm_luaran_pengabdian_mandiri`
    WHERE periode IS NOT NULL AND periode != ''");
  }
  //============== end api aktivitas untuk simremlink ====================

  public function luaranValidTambahan(){
    return $this->db->select('lppm_luaran_valid_tambahan.uuid, nip, kd_sumber_dana, kd_luaran, kd_skim, dana, kd_status_penulis
                      jenis_ciptaan, judul, nomor, tanggal, tahun, penerbit, isbn, keanggotaan_ikapi, volume, issn, nama_prosiding, nama_jurnal, 
                      berkas, url, lppm_sumber_dana.nama as sumberdana, lppm_jenis_luaran.nama as luaran, lppm_skim.nama as skim, lppm_status_penulis.nama as status_penulis')
                    ->join('lppm_sumber_dana','lppm_sumber_dana.uuid = lppm_luaran_valid_tambahan.kd_sumber_dana')
                    ->join('lppm_jenis_luaran','lppm_jenis_luaran.uuid = lppm_luaran_valid_tambahan.kd_luaran')
                    ->join('lppm_skim','lppm_skim.uuid = lppm_luaran_valid_tambahan.kd_skim')
                    ->join('lppm_status_penulis','lppm_status_penulis.uuid = lppm_luaran_valid_tambahan.kd_status_penulis')
                    ->get('lppm_luaran_valid_tambahan');
  }

  public function luaranValidWajib(){
    return $this->db->select('lppm_luaran_valid_wajib.uuid, nip, kd_sumber_dana, kd_luaran, kd_skim, dana, kd_status_penulis
                      jenis_ciptaan, judul, nomor, tanggal, tahun, penerbit, isbn, keanggotaan_ikapi, volume, issn, nama_prosiding, nama_jurnal, 
                      berkas, url, lppm_sumber_dana.nama as sumberdana, lppm_jenis_luaran.nama as luaran, lppm_skim.nama as skim, lppm_status_penulis.nama as status_penulis')
                    ->join('lppm_sumber_dana','lppm_sumber_dana.uuid = lppm_luaran_valid_wajib.kd_sumber_dana')
                    ->join('lppm_jenis_luaran','lppm_jenis_luaran.uuid = lppm_luaran_valid_wajib.kd_luaran')
                    ->join('lppm_skim','lppm_skim.uuid = lppm_luaran_valid_wajib.kd_skim')
                    ->join('lppm_status_penulis','lppm_status_penulis.uuid = lppm_luaran_valid_wajib.kd_status_penulis')
                    ->get('lppm_luaran_valid_wajib');
  }

  public function jenisLuaran(){
		return $this->db->get('lppm_jenis_luaran');
  }

  public function skim(){
		return $this->db->get('lppm_skim');
  }

  public function statusPenulis(){
		return $this->db->get('lppm_status_penulis');
  }

  public function sumberDana(){
		return $this->db->get('lppm_sumber_dana');
  }

  public function luaranValid(){
		return $this->db->get('lppm_luaran_validasi');
  }

  public function luaranValidLainnya(){
		return $this->db->get('lppm_luaran_validasi_lainnya');
  }

  public function luaranValidPengabdian(){
		return $this->db->get('lppm_luaran_validasi_pengabdian');
  }

  public function jenisLuaranByKode($nip,$kode_bkd){
		$data1 =  $this->db->query("SELECT  
      lppm_luaran_validasi.uuid as id, 
      lppm_luaran_validasi.id_penelitian as kode_kegiatan,
      lppm_luaran_validasi.id_penelitian as uuid_sk,
      nip,
      lppm_user.nama as nama,
      SUBSTRING(lppm_luaran_validasi.tanggal,1,4) as tahun,
      lppm_jenis_luaran.kode_bkd as id_pendbkd,
      lppm_submission.judul as kegiatan,
      CAST(0 as char) as sks_bebankerja,
      lppm_luaran_validasi.berkas as bukti
      FROM lppm_luaran_validasi
      JOIN lppm_jenis_luaran ON(lppm_jenis_luaran.uuid = lppm_luaran_validasi.kd_luaran) 
      JOIN lppm_user ON(lppm_user.username = lppm_luaran_validasi.nip)
      JOIN lppm_submission ON(lppm_submission.id_submission = lppm_luaran_validasi.id_penelitian)
      WHERE nip = $nip AND lppm_jenis_luaran.kode_bkd = $kode_bkd")->result();

    $data2 =  $this->db->query("SELECT  
      lppm_luaran_validasi_lainnya.uuid as id, 
      CAST(NULL as char) as kode_kegiatan,
      CAST(NULL as char) as uuid_sk,
      nip,
      lppm_user.nama as nama,
      SUBSTRING(lppm_luaran_validasi_lainnya.tanggal,1,4) as tahun,
      lppm_jenis_luaran.kode_bkd as id_pendbkd,
      lppm_luaran_validasi_lainnya.nama_penelitian as kegiatan,
      CAST(0 as char) as sks_bebankerja,
      lppm_luaran_validasi_lainnya.berkas as bukti
      FROM lppm_luaran_validasi_lainnya
      JOIN lppm_jenis_luaran ON(lppm_jenis_luaran.uuid = lppm_luaran_validasi_lainnya.kd_luaran) 
      JOIN lppm_user ON(lppm_user.username = lppm_luaran_validasi_lainnya.nip)  
      WHERE nip = $nip AND lppm_jenis_luaran.kode_bkd = $kode_bkd")->result();

    $data3 =  $this->db->query("SELECT  
      lppm_luaran_validasi_pengabdian.uuid as id, 
      lppm_luaran_validasi_pengabdian.id_penelitian as kode_kegiatan,
      lppm_luaran_validasi_pengabdian.id_penelitian as uuid_sk,
      nip,
      lppm_user.nama as nama,
      SUBSTRING(lppm_luaran_validasi_pengabdian.tanggal,1,4) as tahun,
      lppm_jenis_luaran.kode_bkd as id_pendbkd,
      lppm_pengabdian.judul as kegiatan,
      CAST(0 as char) as sks_bebankerja,
      lppm_luaran_validasi_pengabdian.berkas as bukti
      FROM lppm_luaran_validasi_pengabdian
      JOIN lppm_jenis_luaran ON(lppm_jenis_luaran.uuid = lppm_luaran_validasi_pengabdian.kd_luaran) 
      JOIN lppm_user ON(lppm_user.username = lppm_luaran_validasi_pengabdian.nip)
      JOIN lppm_pengabdian ON(lppm_pengabdian.id_submission = lppm_luaran_validasi_pengabdian.id_penelitian)
      WHERE nip = $nip AND lppm_jenis_luaran.kode_bkd = $kode_bkd")->result();
      $data = array_merge($data1,$data2,$data3);
      return $data;
  }
}