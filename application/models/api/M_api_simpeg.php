<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_api_simpeg extends CI_Model{
    public function __construct(){
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }

    public function bagian(){
		return $this->db->get('simpeg_bagian');
    }
    
    public function dosenPhl(){
		return $this->db->get('simpeg_dosen_phl');
    }
    
    public function dosenPns(){
		return $this->db->get('simpeg_dosen_pns');
    }
    
    public function dosenPnsSimremlink(){
		return $this->db->get('simpeg_dosen_pns_simremlink');
	}

    public function golonganDosenPegawai(){
		return $this->db->get('simpeg_golongan_dosen_pegawai');
    }
    

    public function jabatanFungsionalDosen(){
		return $this->db->get('simpeg_jabatan_fungsional_dosen');
	}
    public function jenisTugasTambahanDosen(){
		return $this->db->get('simpeg_jenis_tugas_tambahan_dosen');
    }
    
    public function jurusan(){
		return $this->db->get('simpeg_jurusan');
	}

    public function kategoriJabatanRemunPegawaiPns(){
		return $this->db->get('simpeg_kategori_jabatan_remun_pegawai_pns');
	}

    public function keududkanDosenPegawai(){
		return $this->db->get('simpeg_kedudukan_dosen_pegawai');
    }
    
    public function pegawaiPhl(){
		return $this->db->get('simpeg_pegawai_phl');
    }
    
    public function pegawaiPns(){
		return $this->db->get('simpeg_pegawai_pns');
	}

    public function pegawaiPnsSimremlink(){
		return $this->db->get('simpeg_pegawai_pns_simremlink');
    }

    public function prodi(){
		return $this->db->get('simpeg_prodi');
    }
    
    public function riwayatJabatanFungsional(){
		return $this->db->get('simpeg_riwayat_jabatan_fungsional');
    }
    
    public function riwayatJabatanRemunDosenPns(){
		return $this->db->get('simpeg_riwayat_jabatan_remun_dosen_pns');
    }
    
    public function riwayatJabatanRemunPegawaiPns(){
		return $this->db->get('simpeg_riwayat_jabatan_remun_pegawai_pns');
	}

    public function riwayatJabatanTugasTambahanDosenPns(){
		return $this->db->get('simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns');
    }
    
    public function riwayatPenghargaanDosenPns(){
		return $this->db->get('simpeg_riwayat_penghargaan_dosen_pns');
	}

    public function riwayatPenghargaanPegawaiPns(){
		return $this->db->get('simpeg_riwayat_penghargaan_pegawai_pns');
    }
    
    public function status(){
		return $this->db->get('simpeg_status');
    }
    
    public function statusAktifNonAktif(){
		return $this->db->get('simpeg_status_aktif_non_aktif');
    }
    
    public function unit(){
		return $this->db->get('simpeg_unit');
    }

    public function capaianSkpDosen(){
      return $this->db->get('simpeg_capaian_skp_dosen');
    }
    
    public function nilaiSikapDanPerilakuPegawai(){
      return $this->db->get('simpeg_nilai_sikap_dan_perilaku_pegawai');
    }

    public function capaianSkpPegawai(){
      return $this->db->get('simpeg_capaian_skp_pegawai');
    }

    public function dosenPegawaiSimremlink(){
      $data1 =  $this->db->select('simpeg_dosen_pns_simremlink.uuid, simpeg_dosen_pns_simremlink.kode_dosen as kode_pegawai, simpeg_dosen_pns_simremlink.nama, simpeg_dosen_pns_simremlink.nip, simpeg_dosen_pns_simremlink.nidn, jk, agama, alamat,nama_status as status_aktif')
                      ->join('simpeg_status_aktif_non_aktif','simpeg_status_aktif_non_aktif.kode = simpeg_dosen_pns_simremlink.status_aktif')
                      ->join('simpeg_dosen_pns','simpeg_dosen_pns.kode_dosen = simpeg_dosen_pns_simremlink.kode_dosen')
                      ->get('simpeg_dosen_pns_simremlink')
                      ->result();
      $data2 = $this->db->select('simpeg_pegawai_pns_simremlink.uuid,simpeg_pegawai_pns_simremlink.kode_pegawai,simpeg_pegawai_pns_simremlink.nama,simpeg_pegawai_pns_simremlink.nip, 0 AS nidn,
jk, agama, alamat, nama_status as status_aktif')
                      ->join('simpeg_status_aktif_non_aktif','simpeg_status_aktif_non_aktif.kode = simpeg_pegawai_pns_simremlink.status_aktif')
                      ->join('simpeg_pegawai_pns','simpeg_pegawai_pns.kode_pegawai = simpeg_pegawai_pns_simremlink.kode_pegawai')
                      ->get('simpeg_pegawai_pns_simremlink')
                      ->result();
      return array_merge($data1,$data2);
    }

    /** riwayat dosen pegawai simremlink */
    public function riwayatDosenPegawaiSimremlinkSatu(){
      return $this->db->select('kode_dosen as kode_pegawai, nip, nama, kode_jabremun as kode_jabatan, nama_jabremun as nama_jabatan, tgl_sk, tmt_sk,
      1 AS asal')
                      ->get('simpeg_riwayat_jabatan_remun_dosen_pns');
    }

    public function riwayatDosenPegawaiSimremlinkDua(){
      return $this->db->select('kode_pegawai, nip, nama, kode_jabremun as kode_jabatan, nama_jabremun as nama_jabatan, tgl_sk, tmt_sk,
      2 AS asal')
                      ->get('simpeg_riwayat_jabatan_remun_pegawai_pns');
    }

    public function riwayatDosenPegawaiSimremlinkTiga(){
      return $this->db->select('kode_dosen as kode_pegawai, nip, nama, kode_tugastambahan as kode_jabatan, nama_tugastambahan as nama_jabatan, tgl_sk, tmt_sk,
      3 AS asal')
                      ->get('simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns');
    }

    public function riwayatDosenPegawaiSimremlinkEmpat(){
      return $this->db->select('kode_dosen, nip, nama, kode_jft as kode_jabatan, nama_jft as nama_jabatan, tgl_sk, tmt_sk, 
      4 AS asal')
                      ->get('simpeg_riwayat_jabatan_fungsional');
    }
    /** end riwayat dosen pegawai simremlink */

    //============== api aktivitas untuk simremlink ============================
    public function riwayatPenghargaanDosenPnsSimremlink(){
      return $this->db->query("SELECT periode_remun as periode, kode_dosen as kode_pegawai,kode_peran, 
      cast(NULL as char) as capaian,
      cast(NULL as char) as volume, 
      cast(NULL as char) as dokumen_pendukung, 
      cast('SIMPEG' as char) as sumber
      FROM `simpeg_riwayat_penghargaan_dosen_pns`
      WHERE periode_remun IS NOT NULL AND periode_remun != ''");
  }

  public function riwayatPenghargaanPegawaiPnsSimremlink(){
      return $this->db->query("SELECT periode_remun as periode, kode_peg AS kode_pegawai,kode_peran, 
      cast(NULL as char) as capaian,
      cast(NULL as char) as volume, 
      cast(NULL as char) as dokumen_pendukung, 
      cast('SIMPEG' as char) as sumber
      FROM `simpeg_riwayat_penghargaan_pegawai_pns`
      WHERE periode_remun IS NOT NULL AND periode_remun != ''");
  }

  public function capaianSkpDosenSimremlink(){
    return $this->db->query("SELECT periode, KODE_PEG as kode_pegawai,capaian, 
    cast(NULL as char) as kode_peran,
    cast(NULL as char) as volume, 
    cast(NULL as char) as dokumen_pendukung, 
    cast('SIMPEG' as char) as sumber
    FROM `simpeg_capaian_skp_dosen`
    WHERE periode IS NOT NULL AND periode != ''");
  }

  public function capaianSkpPeriodeSimremlink(){
    return $this->db->query("SELECT periode, kode_peg as kode_pegawai,capaian,kode_peran,
    cast(NULL as char) as volume, 
    cast(NULL as char) as dokumen_pendukung, 
    cast('SIMPEG' as char) as sumber
    FROM `simpeg_capaian_skp_pegawai`
    WHERE periode IS NOT NULL AND periode != ''");
  }

  public function nilaiSikapPerilakuSimremlink(){
    return $this->db->query("SELECT periode, kode_peg as kode_pegawai,capaian,kode_peran,
    cast(NULL as char) as volume, 
    cast(NULL as char) as dokumen_pendukung, 
    cast('SIMPEG' as char) as sumber
    FROM `simpeg_nilai_sikap_dan_perilaku_pegawai`
    WHERE periode IS NOT NULL AND periode != ''");
  }
  //============== end api aktivitas untuk simremlink ========================

  //================= CRUD KELOMPOK JABATAN =================================//
  //================= jabatan fungsional dosen =================================//
    public function saveJabatanFungsionalDosen(){ 
          //from input
          $kode_jabatan_fungsional = ucwords(trim($this->input->post('kode_jabatan_fungsional','true')));
          $nama_jabatan_fungsional = ucwords(trim($this->input->post('nama_jabatan_fungsional','true')));
          $grade_fungsional        = ucwords(trim($this->input->post('grade_fungsional','true')));
          $score_fungsional        = ucwords(trim($this->input->post('score_fungsional','true')));
          $koofisien               = ucwords(trim($this->input->post('koofisien','true')));

          //check data
          $checkData = $this->db->where('kode_jabatan_fungsional',$kode_jabatan_fungsional)
                                ->or_where('nama_jabatan_fungsional',$nama_jabatan_fungsional)
                                ->or_where('grade_fungsional',$grade_fungsional)
                                ->or_where('score_fungsional',$score_fungsional)
                                ->or_where('koofisien',$koofisien)
                                ->get('simpeg_jabatan_fungsional_dosen')
                                ->num_rows();

          $this->db->trans_begin();
          $this->db->insert('simpeg_jabatan_fungsional_dosen',[
                          'kode_jabatan_fungsional' => $kode_jabatan_fungsional,
                          'nama_jabatan_fungsional' => $nama_jabatan_fungsional,
                          'grade_fungsional'        => $grade_fungsional,
                          'score_fungsional'        => $score_fungsional,
                          'koofisien'               => $koofisien,
                      ]);
          
          if($this->db->trans_status() === FALSE || $checkData > 0):
              $this->db->trans_rollback();
              if($checkData > 0):
                  $txt    =  'Data sudah ada';
                  $icon   =  'warning';
              else:
                  $txt    = 'Terjadi kesalahan saat menyimpan data';
                  $icon   = 'error';
              endif;
              $msg = ['msg' => $txt,'status'=>FALSE,'icon'=>$icon];
          else:
                  $msg = ['status'=>TRUE];
            $this->db->trans_commit();
          endif;
          return $msg;
      }

      public function deleteJabatanFungsionalDosen($id){
          $checkData = $this->db->get_where('simpeg_jabatan_fungsional_dosen',['kode_jabatan_fungsional'=>$id])->num_rows();
          
          $this->db->trans_begin();
          $this->db->delete('simpeg_jabatan_fungsional_dosen',['kode_jabatan_fungsional'=>$id]);
          if($this->db->trans_status() === FALSE && $checkData == 0){
            $this->db->trans_rollback();
            return FALSE;
          }else{
            $this->db->trans_commit();
            return TRUE;
          }
      }

      public function getJabatanFungsionalDosen($id){
          $cekData = $this->db->get_where('simpeg_jabatan_fungsional_dosen',['kode_jabatan_fungsional'=>$id]);
          if($cekData->num_rows() > 0 ){
              return $cekData->row_array();
          }else{
              return FALSE;
          }   
      }

      public function updateSaveJabatanFungsionalDosen(){ 
        //from input
        $id = ucwords(trim($this->input->post('id','true')));
        $nama_jabatan_fungsional = ucwords(trim($this->input->post('nama_jabatan_fungsional','true')));
        $grade_fungsional        = ucwords(trim($this->input->post('grade_fungsional','true')));
        $score_fungsional        = ucwords(trim($this->input->post('score_fungsional','true')));
        $koofisien               = ucwords(trim($this->input->post('koofisien','true')));


        $this->db->trans_begin();
        $this->db->where('kode_jabatan_fungsional',$id)
                  ->update('simpeg_jabatan_fungsional_dosen',[
                        'nama_jabatan_fungsional' => $nama_jabatan_fungsional,
                        'grade_fungsional'        => $grade_fungsional,
                        'score_fungsional'        => $score_fungsional,
                        'koofisien'               => $koofisien,
                    ]);
        
        if($this->db->trans_status() === FALSE):
            $this->db->trans_rollback();
            $txt    = 'Terjadi kesalahan saat menyimpan data';
            $icon   = 'error';
            $msg = ['msg' => $txt,'status'=>FALSE,'icon'=>$icon];
        else:
                $msg = ['status'=>TRUE];
          $this->db->trans_commit();
        endif;
        return $msg;
    }
  //================= jabatan fungsional dosen =================================//
  //================= jenis tugas tambahan dosen =================================//
    public function saveJenisTugasTambahanDosen(){ 
          //from input
          $kode_tugastambahan = ucwords(trim($this->input->post('kode_tugastambahan','true')));
          $nama_tugastambahan = ucwords(trim($this->input->post('nama_tugastambahan','true')));
          $grade_tugastambahan        = ucwords(trim($this->input->post('grade_tugastambahan','true')));
          $score_tugastambahan        = ucwords(trim($this->input->post('score_tugastambahan','true')));
          $koofisien               = ucwords(trim($this->input->post('koofisien','true')));

          //check data
          $checkData = $this->db->where('kode_tugastambahan',$kode_tugastambahan)
                                ->or_where('nama_tugastambahan',$nama_tugastambahan)
                                ->or_where('grade_tugastambahan',$grade_tugastambahan)
                                ->or_where('score_tugastambahan',$score_tugastambahan)
                                ->or_where('koofisien',$koofisien)
                                ->get('simpeg_jenis_tugas_tambahan_dosen')
                                ->num_rows();

          $this->db->trans_begin();
          $this->db->insert('simpeg_jenis_tugas_tambahan_dosen',[
                          'kode_tugastambahan' => $kode_tugastambahan,
                          'nama_tugastambahan' => $nama_tugastambahan,
                          'grade_tugastambahan'        => $grade_tugastambahan,
                          'score_tugastambahan'        => $score_tugastambahan,
                          'koofisien'               => $koofisien,
                      ]);
          
          if($this->db->trans_status() === FALSE || $checkData > 0):
              $this->db->trans_rollback();
              if($checkData > 0):
                  $txt    =  'Data sudah ada';
                  $icon   =  'warning';
              else:
                  $txt    = 'Terjadi kesalahan saat menyimpan data';
                  $icon   = 'error';
              endif;
              $msg = ['msg' => $txt,'status'=>FALSE,'icon'=>$icon];
          else:
                  $msg = ['status'=>TRUE];
            $this->db->trans_commit();
          endif;
          return $msg;
      }

      public function deleteJenisTugasTambahanDosen($id){
          $checkData = $this->db->get_where('simpeg_jenis_tugas_tambahan_dosen',['kode_tugastambahan'=>$id])->num_rows();
          
          $this->db->trans_begin();
          $this->db->delete('simpeg_jenis_tugas_tambahan_dosen',['kode_tugastambahan'=>$id]);
          if($this->db->trans_status() === FALSE && $checkData == 0){
            $this->db->trans_rollback();
            return FALSE;
          }else{
            $this->db->trans_commit();
            return TRUE;
          }
      }

      public function getJenisTugasTambahanDosen($id){
          $cekData = $this->db->get_where('simpeg_jenis_tugas_tambahan_dosen',['kode_tugastambahan'=>$id]);
          if($cekData->num_rows() > 0 ){
              return $cekData->row_array();
          }else{
              return FALSE;
          }   
      }

      public function updateSaveJenisTugasTambahanDosen(){ 
        //from input
        $id = ucwords(trim($this->input->post('id','true')));
        $nama_tugastambahan = ucwords(trim($this->input->post('nama_tugastambahan','true')));
        $grade_tugastambahan        = ucwords(trim($this->input->post('grade_tugastambahan','true')));
        $score_tugastambahan        = ucwords(trim($this->input->post('score_tugastambahan','true')));
        $koofisien               = ucwords(trim($this->input->post('koofisien','true')));


        $this->db->trans_begin();
        $this->db->where('kode_tugastambahan',$id)
                  ->update('simpeg_jenis_tugas_tambahan_dosen',[
                        'nama_tugastambahan' => $nama_tugastambahan,
                        'grade_tugastambahan'        => $grade_tugastambahan,
                        'score_tugastambahan'        => $score_tugastambahan,
                        'koofisien'               => $koofisien,
                    ]);
        
        if($this->db->trans_status() === FALSE):
            $this->db->trans_rollback();
            $txt    = 'Terjadi kesalahan saat menyimpan data';
            $icon   = 'error';
            $msg = ['msg' => $txt,'status'=>FALSE,'icon'=>$icon];
        else:
                $msg = ['status'=>TRUE];
          $this->db->trans_commit();
        endif;
        return $msg;
    }
  //================= jenis tugas tambahan dosen =================================//
  //================= CRUD KELOMPOK JABATAN =================================//

  public function dosenPegawai00(){
    $data1 =  $this->db->select('simpeg_dosen_pns_simremlink.uuid, simpeg_dosen_pns_simremlink.kode_dosen as kode_pegawai, simpeg_dosen_pns_simremlink.nama, simpeg_dosen_pns_simremlink.nip, simpeg_dosen_pns_simremlink.nidn')
		    //->select('simpeg_dosen_pns_simremlink.uuid, simpeg_dosen_pns_simremlink.kode_dosen as kode_pegawai, simpeg_dosen_pns_simremlink.nama, simpeg_dosen_pns_simremlink.nip, simpeg_dosen_pns_simremlink.nidn, jk, agama, alamat,nama_status as status_aktif')
                    //->join('simpeg_status_aktif_non_aktif','simpeg_status_aktif_non_aktif.kode = simpeg_dosen_pns_simremlink.status_aktif')
                    //->join('simpeg_dosen_pns','simpeg_dosen_pns.kode_dosen = simpeg_dosen_pns_simremlink.kode_dosen')
                    ->get_where('simpeg_dosen_pns_simremlink',['simpeg_dosen_pns_simremlink.status_aktif'=>'00'])
                    ->result();
    $data2 = $this->db->select('simpeg_pegawai_pns_simremlink.uuid,simpeg_pegawai_pns_simremlink.kode_pegawai,simpeg_pegawai_pns_simremlink.nama,simpeg_pegawai_pns_simremlink.nip, 0 AS nidn')
//->select('simpeg_pegawai_pns_simremlink.uuid,simpeg_pegawai_pns_simremlink.kode_pegawai,simpeg_pegawai_pns_simremlink.nama,simpeg_pegawai_pns_simremlink.nip, 0 AS nidn,jk, agama, alamat, nama_status as status_aktif')
                    //->join('simpeg_status_aktif_non_aktif','simpeg_status_aktif_non_aktif.kode = simpeg_pegawai_pns_simremlink.status_aktif')
                    //->join('simpeg_pegawai_pns','simpeg_pegawai_pns.kode_pegawai = simpeg_pegawai_pns_simremlink.kode_pegawai')
                    ->get_where('simpeg_pegawai_pns_simremlink',['simpeg_pegawai_pns_simremlink.status_aktif'=>'00'])
                    ->result();
    return array_merge($data1,$data2);
  }

  public function dosenPegawai00_nip($nip){
    $data1 =  $this->db->select('simpeg_dosen_pns_simremlink.uuid, simpeg_dosen_pns_simremlink.kode_dosen as kode_pegawai, simpeg_dosen_pns_simremlink.nama, simpeg_dosen_pns_simremlink.nip, simpeg_dosen_pns_simremlink.nidn')
		    //->select('simpeg_dosen_pns_simremlink.uuid, simpeg_dosen_pns_simremlink.kode_dosen as kode_pegawai, simpeg_dosen_pns_simremlink.nama, simpeg_dosen_pns_simremlink.nip, simpeg_dosen_pns_simremlink.nidn, jk, agama, alamat,nama_status as status_aktif')
                    //->join('simpeg_status_aktif_non_aktif','simpeg_status_aktif_non_aktif.kode = simpeg_dosen_pns_simremlink.status_aktif')
                    //->join('simpeg_dosen_pns','simpeg_dosen_pns.kode_dosen = simpeg_dosen_pns_simremlink.kode_dosen')
                    ->get_where('simpeg_dosen_pns_simremlink',['simpeg_dosen_pns_simremlink.status_aktif'=>'00', 'simpeg_dosen_pns_simremlink.nip' => $nip])
                    ->result();
    $data2 = $this->db->select('simpeg_pegawai_pns_simremlink.uuid,simpeg_pegawai_pns_simremlink.kode_pegawai,simpeg_pegawai_pns_simremlink.nama,simpeg_pegawai_pns_simremlink.nip, 0 AS nidn')
//->select('simpeg_pegawai_pns_simremlink.uuid,simpeg_pegawai_pns_simremlink.kode_pegawai,simpeg_pegawai_pns_simremlink.nama,simpeg_pegawai_pns_simremlink.nip, 0 AS nidn,jk, agama, alamat, nama_status as status_aktif')
                    //->join('simpeg_status_aktif_non_aktif','simpeg_status_aktif_non_aktif.kode = simpeg_pegawai_pns_simremlink.status_aktif')
                    //->join('simpeg_pegawai_pns','simpeg_pegawai_pns.kode_pegawai = simpeg_pegawai_pns_simremlink.kode_pegawai')
                    ->get_where('simpeg_pegawai_pns_simremlink',['simpeg_pegawai_pns_simremlink.status_aktif'=>'00', 'simpeg_pegawai_pns_simremlink.nip' => $nip])
                    ->result();
    return array_merge($data1,$data2);
  }
    
}