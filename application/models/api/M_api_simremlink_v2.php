<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_api_simremlink_v2 extends CI_Model{
        public function __construct(){
            parent::__construct();
            date_default_timezone_set("Asia/Jakarta");
            error_reporting(0);
        }

        //========================= function support ========================//
        /**
         * pengecekan jenis dosen
         */
        public function cekJenisDosen($nip){
            $arrContextOptions=array(
                "ssl"=>array(
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
                ),
            );
            $api = json_decode(file_get_contents('https://wsdb.unimed.ac.id/api/simpeg/riwayat-jabatan-tugas-tambahan-dosen-pns',false, stream_context_create($arrContextOptions)), TRUE);
            foreach($api as $key => $val):
                if(array_search($nip, array_column($api, "nip"))):
                    $jenis = "DT";
                else:
                    $jenis = "DB";
                endif;
            endforeach;
            return $jenis;
        }

        /**
         * pengecekan golongan
         */
        public function cekGol($nip){
            $hasil_dosen = $this->db->query("SELECT golongan FROM simpeg_dosen_pns_simremlink WHERE nip = '$nip' ");
            if($hasil_dosen->num_rows() > 0):
                $data_dosen = $hasil_dosen->row_array();
                $gol        = strtok($data_dosen['golongan'], '/');
            else:
                $hasil_pegawai = $this->db->query("SELECT golongan FROM simpeg_pegawai_pns_simremlink WHERE nip = '$nip' ");
                if($hasil_pegawai->num_rows() > 0):
                    $data_pegawai = $hasil_pegawai->row_array();
                    $gol          = strtok($data_pegawai['golongan'], '/');
                endif;
            endif;
            return $gol;
        }

        /**
         * mendapatkan tupoksi 
         */
        public function tupoksi($kodeDosen,$periode){
            $arrContextOptions=array(
                "ssl"=>array(
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
                ),
            );
            $queryPeran = $this->db->get('simremlink_data_peran')->result();
            //===== capaian skp dosen dt ======//
            $totalPoinCapaianSkpDosen = 0;
            $output1 = array();
            $i = 0;
            $apiCapaianSkpDosen = json_decode(file_get_contents('https://wsdb.unimed.ac.id/api/transaksi/capaian-skp-dosen-dt',false, stream_context_create($arrContextOptions)), TRUE);
            foreach ($apiCapaianSkpDosen as $key => $value) :
                if($value['kode_pegawai'] == $kodeDosen && $value['periode'] == $periode):
                    foreach ($queryPeran as $key => $valuePeran) :
                        if($value['peran'] == $valuePeran->kdeperan):
                            $poin = ( $valuePeran->ewkp * $value['capaian'] ) / 100;
                            $set = [
                                'poin' => $poin,
                            ];      
                            array_push($output1, $set);
                        endif;
                    endforeach;
                endif;
            endforeach;
            foreach ($output1 as $key => $value) if ($i++ < 1){
                $poin = $value['poin'];
                $totalPoinCapaianSkpDosen += $poin;   
                $i++;
            }

            // foreach ($apiCapaianSkpDosen as $key => $value) :
            //     if($value['kode_pegawai'] == $kodeDosen && $value['periode'] == $periode):
            //         foreach ($queryPeran as $key => $valuePeran) :
            //             if($value['peran'] == $valuePeran->kdeperan):
            //                 $poin = ( $valuePeran->ewkp * $value['capaian'] ) / 100;
            //                 $totalPoinCapaianSkpDosen += $poin;
            //             endif;
            //         endforeach;
            //     endif;
            // endforeach;
            //===== capaian skp dosen dt ======//

            //===== capaian skp pegawai======//
            $totalPoinCapaianSkpPegawai = 0;
            $apiCapaianSkpPegawai = json_decode(file_get_contents('https://wsdb.unimed.ac.id/api/transaksi/capaian-skp-pegawai',false, stream_context_create($arrContextOptions)), TRUE);
            foreach ($apiCapaianSkpPegawai as $key => $value) :
                if($value['kode_pegawai'] == $kodeDosen && $value['periode'] == $periode):
                    foreach ($queryPeran as $key => $valuePeran) :
                        if($value['peran'] == $valuePeran->kdeperan):
                            $poin = ( $valuePeran->ewkp * $value['capaian'] ) / 100;
                            $totalPoinCapaianSkpPegawai += $poin;
                        endif;
                    endforeach;
                endif;
            endforeach;
            //===== capaian skp pegawai ======//

            //===== nilai sikap dan perilaku pegawai ======
             $totalPoinNilaiSikapDanPerilakuPegawai = 0;
             $apiNilaiSikapDanPerilakuPegawai = json_decode(file_get_contents('https://wsdb.unimed.ac.id/api/transaksi/nilai-sikap-dan-perilaku-pegawai',false, stream_context_create($arrContextOptions)), TRUE);
             foreach ($apiNilaiSikapDanPerilakuPegawai as $key => $value) :
                 if($value['kode_pegawai'] == $kodeDosen && $value['periode'] == $periode):
                     foreach ($queryPeran as $key => $valuePeran) :
                         if($value['peran'] == $valuePeran->kdeperan):
                             $poin = ( $valuePeran->ewkp * $value['capaian'] ) / 100;
                             $totalPoinNilaiSikapDanPerilakuPegawai += $poin;
                         endif;
                     endforeach;
                 endif;
             endforeach;
            //===== nilai sikap dan perilaku pegawai ======//

            //===== capaian bkd dosen ======
            $totalPoinCapaianBkdDosen = 0;
            $apiCapaianBkdDosen = json_decode(file_get_contents('https://wsdb.unimed.ac.id/api/transaksi/capaian-bkd-dosen',false, stream_context_create($arrContextOptions)), TRUE);
            foreach ($apiCapaianBkdDosen as $key => $value) :
                if($value['kode_pegawai'] == $kodeDosen && $value['periode'] == $periode):
                    foreach ($queryPeran as $key => $valuePeran) :
                        if($value['peran'] == $valuePeran->kdeperan):
                            $poin = $valuePeran->ewkp;
                            $totalPoinCapaianBkdDosen += $poin;
                        endif;
                    endforeach;
                endif;
            endforeach;
           //===== capaian bkd dosen ======//

            //===== absensi pegawai tendik ======
              $totalPoinAbsensiPegawaiTendik = 0;
              $apiAbsensiPegawaiTendik = json_decode(file_get_contents('https://wsdb.unimed.ac.id/api/transaksi/absensi-pegawai-tendik',false, stream_context_create($arrContextOptions)), TRUE);
              foreach ($apiAbsensiPegawaiTendik as $key => $value) :
                  if($value['kode_pegawai'] == $kodeDosen && $value['periode'] == $periode):
                      foreach ($queryPeran as $key => $valuePeran) :
                          if($value['peran'] == $valuePeran->kdeperan):
                              $poin = ( $valuePeran->ewkp * $value['capaian'] ) / 100;
                              $totalPoinAbsensiPegawaiTendik += $poin;
                          endif;
                      endforeach;
                  endif;
              endforeach;
            //===== absensi pegawai tendik ======//

            //===== absensi dosen pns ======//
            $totalPoinAbsensiDosenPns = 0;
            $apiAbsensiDosenPns = json_decode(file_get_contents('https://wsdb.unimed.ac.id/api/transaksi/absensi-dosen-pns',false, stream_context_create($arrContextOptions)), TRUE);
            foreach ($apiAbsensiDosenPns as $key => $value) :
                if($value['kode_pegawai'] == $kodeDosen && $value['periode'] == $periode):
                    foreach ($queryPeran as $key => $valuePeran) :
                        if($value['peran'] == $valuePeran->kdeperan):
                            //$poin = ( $valuePeran->ewkp * $value['capaian'] ) / 100;
                            $poin = $value['poin_remlink'];
                            $totalPoinAbsensiDosenPns += $poin;
                        endif;
                    endforeach;
                endif;
            endforeach;
            //===== absensi dosen pns ======//

            //===== lhkasn lhkpn pegawai pns ======//
            $totalPoinLhkasnLhkpnPegawaiPns = 0;
            $apiLhkasnLhkpnPegawaiPns = json_decode(file_get_contents('https://wsdb.unimed.ac.id/api/transaksi/lhkasn-lhkpn-pegawai-pns',false, stream_context_create($arrContextOptions)), TRUE);
            foreach ($apiLhkasnLhkpnPegawaiPns as $key => $value) :
                if($value['kode_pegawai'] == $kodeDosen && $value['periode'] == $periode):
                    foreach ($queryPeran as $key => $valuePeran) :
                        if($value['peran'] == $valuePeran->kdeperan):
                            $poin = ( $valuePeran->ewkp * $value['capaian'] ) / 100;
                            $totalPoinLhkasnLhkpnPegawaiPns += $poin;
                        endif;
                    endforeach;
                endif;
            endforeach;
            //===== lhkasn lhkpn pegawai pns ======//

            //===== hukuman disiplin pegawai pns ======//
            $totalPoinHukumanDisiplinPegawaiPns = 0;
            $apiHukumanDisiplinPegawaiPns = json_decode(file_get_contents('https://wsdb.unimed.ac.id/api/transaksi/hukuman-disiplin-pegawai-pns',false, stream_context_create($arrContextOptions)), TRUE);
            foreach ($apiHukumanDisiplinPegawaiPns as $key => $value) :
                if($value['kode_pegawai'] == $kodeDosen && $value['periode'] == $periode):
                    foreach ($queryPeran as $key => $valuePeran) :
                        if($value['peran'] == $valuePeran->kdeperan):
                            $poin = ( $valuePeran->ewkp * $value['capaian'] ) / 100;
                            $totalPoinHukumanDisiplinPegawaiPns += $poin;
                        endif;
                    endforeach;
                endif;
            endforeach;
            //===== hukuman disiplin pegawai pns ======//
            return $totalPoin = $totalPoinAbsensiDosenPns + $totalPoinAbsensiPegawaiTendik + $totalPoinCapaianBkdDosen + $totalPoinCapaianSkpDosen + $totalPoinCapaianSkpPegawai + $totalPoinHukumanDisiplinPegawaiPns + $totalPoinLhkasnLhkpnPegawaiPns + $totalPoinNilaiSikapDanPerilakuPegawai;


        }

        /**
         * mendapatkan mengajar
         */
        public function mengajar($kodeDosen,$periode){
            $arrContextOptions=array(
                "ssl"=>array(
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
                ),
            );
            $queryPeran = $this->db->get('simremlink_data_peran')->result();
            $totalPoin = 0;
            $api = json_decode(file_get_contents('https://wsdb.unimed.ac.id/api/transaksi/kelebihan-mengajar',false, stream_context_create($arrContextOptions)), TRUE);
            foreach ($api as $key => $value) :
                if($value['kode_pegawai'] == $kodeDosen && $value['periode'] == $periode):
                    foreach ($queryPeran as $key => $valuePeran) :
                        if($value['peran'] == $valuePeran->kdeperan):
                            $poin = $valuePeran->ewkp * $value['volume'] ;
                            $totalPoin += $poin;
                        endif;
                    endforeach;
                endif;
            endforeach;
            return $totalPoin;
        }

        /**
         * mendapatkan mengajar lainnya
         */
        public function mengajarLainnya($kodeDosen, $periode){
            $arrContextOptions=array(
                "ssl"=>array(
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
                ),
            );
            $queryPeran = $this->db->get('simremlink_data_peran')->result();

            //============ pembimbing akademik ============//
            $totalPoinPembimbingAkademik = 0;
            $apiPembimbingAkademik = json_decode(file_get_contents('https://wsdb.unimed.ac.id/api/transaksi/pembimbing-akademik',false, stream_context_create($arrContextOptions)), TRUE);
            foreach ($apiPembimbingAkademik as $key => $value) :
                if($value['kode_pegawai'] == $kodeDosen && $value['periode'] == $periode):
                    foreach ($queryPeran as $key => $valuePeran) :
                        if($value['peran'] == $valuePeran->kdeperan):
                            $poin = $valuePeran->ewkp * $value['volume'] ;
                            $totalPoinPembimbingAkademik += $poin;
                        endif;
                    endforeach;
                endif;
            endforeach;
            //============ pembimbing akademik ============//

            //============ pembimbing skripsi tesis disertasi ============//
            $totalPoinPembimbingSkripsiDisertasi = 0;
            $apiPembimbingSkripsiDisertasi = json_decode(file_get_contents('https://wsdb.unimed.ac.id/api/transaksi/pembimbing-skripsi-tesis-disertasi',false, stream_context_create($arrContextOptions)), TRUE);
            foreach ($apiPembimbingSkripsiDisertasi as $key => $value) :
                if($value['kode_pegawai'] == $kodeDosen && $value['periode'] == $periode):
                    foreach ($queryPeran as $key => $valuePeran) :
                        if($value['peran'] == $valuePeran->kdeperan):
                            $poin = $valuePeran->ewkp * $value['volume'] ;
                            $totalPoinPembimbingSkripsiDisertasi += $poin;
                        endif;
                    endforeach;
                endif;
            endforeach;
            //============ pembimbing skripsi tesis disertasi ============//

            //============ penguji skripsi tesis disertasi ============//
            $totalPoinPengujiSkripsiTesisDisertasi = 0;
            $apiPengujiSkripsiTesisDisertasi = json_decode(file_get_contents('https://wsdb.unimed.ac.id/api/transaksi/penguji-skripsi-tesis-disertasi',false, stream_context_create($arrContextOptions)), TRUE);
            foreach ($apiPengujiSkripsiTesisDisertasi as $key => $value) :
                if($value['kode_pegawai'] == $kodeDosen && $value['periode'] == $periode):
                    foreach ($queryPeran as $key => $valuePeran) :
                        if($value['peran'] == $valuePeran->kdeperan):
                            $poin = $valuePeran->ewkp * $value['volume'] ;
                            $totalPoinPengujiSkripsiTesisDisertasi += $poin;
                        endif;
                    endforeach;
                endif;
            endforeach;
            //============ penguji skripsi tesis disertasi ============//

            //============ mengajar lainnya ============//
            $totalPoinMengajarLainnya = 0;
            $apiMengajarLainnya = json_decode(file_get_contents('https://wsdb.unimed.ac.id/api/transaksi/mengajar-lainnya',false, stream_context_create($arrContextOptions)), TRUE);
            foreach ($apiMengajarLainnya as $key => $value) :
                if($value['kode_pegawai'] == $kodeDosen && $value['periode'] == $periode):
                    foreach ($queryPeran as $key => $valuePeran) :
                        if($value['peran'] == $valuePeran->kdeperan):
                            $poin = $valuePeran->ewkp * $value['volume'] ;
                            $totalPoinMengajarLainnya += $poin;
                        endif;
                    endforeach;
                endif;
            endforeach;
            //============ mengajar lainnya ============//

            return $totalPoinPembimbingAkademik + $totalPoinPembimbingSkripsiDisertasi + $totalPoinPengujiSkripsiTesisDisertasi + $totalPoinMengajarLainnya;
        }

        /**
         * mendapatkan penunjang
         */
        public function penunjang($kodeDosen,$periode){
            $arrContextOptions=array(
                "ssl"=>array(
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
                ),
            );
            $queryPeran = $this->db->get('simremlink_data_peran')->result();

            $totalPoin1 = 0;
            $totalPoin2 = 0;
            $api = json_decode(file_get_contents('https://wsdb.unimed.ac.id/api/transaksi/aktivitas',false, stream_context_create($arrContextOptions)), TRUE);
            foreach ($api as $key => $value) :
                if($value['kode_pegawai'] == $kodeDosen && $value['periode'] == $periode):
                    foreach ($queryPeran as $key => $valuePeran) :
                        if($value['peran'] == $valuePeran->kdeperan && ( $value['peran'] == '51401' || $value['peran'] == '51402' || $value['peran'] == '51703' || $value['peran'] == '51704' || $value['peran'] == '52301' || $value['peran'] == '52302' || $value['peran'] == '52503')):
                            $poin1 = $valuePeran->ewkp * $value['volume'] ;
                            $totalPoin1 += $poin1;
                        elseif($value['peran'] == $valuePeran->kdeperan):
                            $poin2 = $valuePeran->ewkp;
                            $totalPoin2 += $poin2;
                        endif;
                    endforeach;
                endif;
            endforeach;
            return $totalPoin1 + $totalPoin2;
        }

        /**
         * mendapatkan penghargaan
         */
        public function penghargaan($kodeDosen,$periode){
            $arrContextOptions=array(
                "ssl"=>array(
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
                ),
            );
            $queryPeran = $this->db->get('simremlink_data_peran')->result();
            //================ riwayat penghargaan dosen ===================//
            $totalPoinRiwayatPenghargaanDosen = 0;            
            $apiRiwayatPenghargaanDosen = json_decode(file_get_contents('https://wsdb.unimed.ac.id/api/transaksi/riwayat-penghargaan-dosen',false, stream_context_create($arrContextOptions)), TRUE);
            foreach ($apiRiwayatPenghargaanDosen as $key => $value) :
                if($value['kode_pegawai'] == $kodeDosen && $value['periode'] == $periode):
                    foreach ($queryPeran as $key => $valuePeran) :
                        if($value['peran'] == $valuePeran->kdeperan):
                            $poin = $valuePeran->ewkp;
                            $totalPoinRiwayatPenghargaanDosen += $poin;
                        endif;
                    endforeach;
                endif;
            endforeach;
            //================ riwayat penghargaan dosen ===================//

            //================ riwayat penghargaan pns ===================//
            $totalPoinRiwayatPenghargaanPns = 0;            
            $apiRiwayatPenghargaanPns = json_decode(file_get_contents('https://wsdb.unimed.ac.id/api/transaksi/riwayat-pengharggan-pns',false, stream_context_create($arrContextOptions)), TRUE);
            foreach ($apiRiwayatPenghargaanPns as $key => $value) :
                if($value['kode_pegawai'] == $kodeDosen && $value['periode'] == $periode):
                    foreach ($queryPeran as $key => $valuePeran) :
                        if($value['peran'] == $valuePeran->kdeperan):
                            $poin = $valuePeran->ewkp;
                            $totalPoinRiwayatPenghargaanPns += $poin;
                        endif;
                    endforeach;
                endif;
            endforeach;
            //================ riwayat penghargaan pns ===================//

            //================ penghargaan ===================//
            $totalPoinRiwayatPenghargaan = 0;            
            $apiRiwayatPenghargaan = json_decode(file_get_contents('https://wsdb.unimed.ac.id/api/transaksi/penghargaan',false, stream_context_create($arrContextOptions)), TRUE);
            foreach ($apiRiwayatPenghargaan as $key => $value) :
                if($value['kode_pegawai'] == $kodeDosen && $value['periode'] == $periode):
                    foreach ($queryPeran as $key => $valuePeran) :
                        if($value['peran'] == $valuePeran->kdeperan):
                            $poin = $valuePeran->ewkp;
                            $totalPoinRiwayatPenghargaan += $poin;
                        endif;
                    endforeach;
                endif;
            endforeach;
            //================ penghargaan ===================//
            return $totalPoinRiwayatPenghargaan + $totalPoinRiwayatPenghargaanDosen + $totalPoinRiwayatPenghargaanPns;
        }

        /**
         * mendapatkan pir sekarang
         */
        public function pirSekarang($periode){
            header('Content-Type: application/json');
            $data_periode = $this->db->query("SELECT pir FROM simremlink_data_periode WHERE periode = '$periode' ")->row_array();
            return $data_periode['pir'];
        }

        /**
         * mendapatkan P2DT
         */
        public function poinP2DT($kode, $periode){
        	$tupoksi            = $this->tupoksi($kode,$periode);
            $mengajar           = $this->mengajar($kode,$periode);
            $mengajarLainnya    = $this->mengajarLainnya($kode,$periode);
            $penunjang          = $this->penunjang($kode,$periode);
            $penghargaan        = $this->penghargaan($kode,$periode);
            $total_semua_poin   = $tupoksi + $mengajar + $mengajarLainnya + $penunjang + $penghargaan;
            $poin_kinerja       = $tupoksi + $mengajar + $mengajarLainnya + $penunjang;
            $p2DT = $poin_kinerja - 12;
            if($p2DT > 28){
                $p2DT = 28;
            }else{
                $p2DT = $p2DT;
            };
            return $p2DT;
        }
        //========================= function support ========================//

        //========================= function penting ========================//
        /**
         * menampilkan riwayat DT dan DB
         */
        public function testing_riwayat_by_tgl(){
            $nip  = trim($this->input->get('nip','true'));
            $kode = substr(trim($this->input->get('kode','true')),0,3);
            $periode = trim($this->input->get('periode','true'));

            /**
             * penentuan case
             */
            if($kode == 'DOS'):
               $data  = $this->m_filter_kondisi_dosen->filterKondisiDosen($nip,$kode,$periode);
            elseif($kode == 'PEG'):
                $data = $this->m_filter_kondisi_pegawai->filterKondisiPegawai($nip,$kode, $periode);
            endif;
            
            /**
             * ekseskusi berdasarkan case
             */
            if($data == "CASE 1"):
                $response = $this->m_case_1->execute($nip,$kode, $periode);
            elseif($data == "CASE 6"):
                $response = $this->m_case_6->execute($nip,$kode, $periode);
            elseif($data == "CASE 2"):
                $response = $this->m_case_2->execute($nip,$kode, $periode);
            elseif($data == "CASE 3"):
                $response = $this->m_case_3->execute($nip,$kode, $periode);
            elseif($data == "CASE 4"):
                $response = $this->m_case_4->execute($nip,$kode, $periode);
            elseif($data == "CASE 5"):
                $response = $this->m_case_5->execute($nip,$kode, $periode);
            elseif($data == "CASE 7"):
                $response = $this->m_case_7->execute($nip,$kode, $periode);
            elseif($data == "CASE 8"):
                $response = $this->m_case_8->execute($nip,$kode, $periode);
            elseif($data == "CASE 9"):
                $response = $this->m_case_9->execute($nip,$kode, $periode);
            elseif($data == "CASE 10"):
                $response = FALSE;
            elseif($data == "CASE 11"):
                $response = FALSE;
            elseif($data == "CASE 12"):
                $response = FALSE;
            elseif($data == "CASE 13"):
                $response = $this->m_case_13->execute($nip,$kode, $periode);
            elseif($data == "CASE 14"):
                $response = $this->m_case_14->execute($nip,$kode, $periode);
            endif;

            return $response;
        }

        public function riwayatForPembayaran($nip,$kode){

            /**
             * penentuan case
             */
            if($kode == 'DOS'):
               $data  = $this->m_filter_kondisi_dosen->filterKondisiDosen($nip,$kode);
            elseif($kode == 'PEG'):
                $data = $this->m_filter_kondisi_pegawai->filterKondisiPegawai($nip,$kode);
            endif;
            
            /**
             * ekseskusi berdasarkan case
             */
            if($data == "CASE 1"):
                $response = $this->m_case_1->execute($nip,$kode);
            elseif($data == "CASE 6"):
                $response = $this->m_case_6->execute($nip,$kode);
            elseif($data == "CASE 2"):
                $response = $this->m_case_2->execute($nip,$kode);
            elseif($data == "CASE 3"):
                $response = $this->m_case_3->execute($nip,$kode);
            elseif($data == "CASE 4"):
                $response = $this->m_case_4->execute($nip,$kode);
            elseif($data == "CASE 5"):
                $response = $this->m_case_5->execute($nip,$kode);
            elseif($data == "CASE 7"):
                $response = $this->m_case_7->execute($nip,$kode);
            elseif($data == "CASE 8"):
                $response = $this->m_case_8->execute($nip,$kode);
            elseif($data == "CASE 9"):
                $response = $this->m_case_9->execute($nip,$kode);
            elseif($data == "CASE 10"):
                $response = FALSE;
            elseif($data == "CASE 11"):
                $response = FALSE;
            elseif($data == "CASE 12"):
                $response = FALSE;
            elseif($data == "CASE 13"):
                $response = $this->m_case_13->execute($nip,$kode);
            elseif($data == "CASE 14"):
                $response = $this->m_case_14->execute($nip,$kode);
            endif;

            return $response;
        }

        /**
         * menampilkan harga DT
         */
        public function harga_dt(){
            header('Content-Type: application/json');
            $nip        = trim($this->input->get('nip','true'));
            $periode    = trim($this->input->get('periode','true'));
            $kode       = substr(trim($this->input->get('kode','true')),0,3);

            /**
             * penentuan case
             */
            if($kode == 'DOS'):
                $data  = $this->m_filter_kondisi_dosen->filterKondisiDosen($nip,$kode,$periode);
            elseif($kode == 'PEG'):
                $data = $this->m_filter_kondisi_pegawai->filterKondisiPegawai($nip,$kode,$periode);
            endif;
             
            /**
            * ekseskusi berdasarkan case
            */
            if($data == "CASE 1"):
                $response = $this->m_case_1->executeHargaDt($nip,$kode,$periode);
            elseif($data == "CASE 6"):
                $response = $this->m_case_6->executeHargaDt($nip,$kode,$periode);
            elseif($data == "CASE 2"):
                $response = $this->m_case_2->executeHargaDt($nip,$kode,$periode);
            elseif($data == "CASE 3"):
                $response = $this->m_case_3->executeHargaDt($nip,$kode,$periode);
            elseif($data == "CASE 4"):
                $response = $this->m_case_4->executeHargaDt($nip,$kode,$periode);
            elseif($data == "CASE 5"):
                $response = $this->m_case_5->executeHargaDt($nip,$kode,$periode);
            elseif($data == "CASE 7"):
                $response = FALSE;
            elseif($data == "CASE 8"):
                $response = FALSE;
            elseif($data == "CASE 9"):
                $response = $this->m_case_9->executeHargaDt($nip,$kode,$periode);
            elseif($data == "CASE 10"):
                $response = FALSE;
            elseif($data == "CASE 11"):
                $response = FALSE;
            elseif($data == "CASE 12"):
                $response = FALSE;
            elseif($data == "CASE 13"):
                $response = FALSE;
            elseif($data == "CASE 14"):
                $response = FALSE;
            endif;

            return $response;
        }

        public function harga_dt_for_pembayaran($nip,$kode){
            $periode    = 20202;

            /**
             * penentuan case
             */
            if($kode == 'DOS'):
                $data  = $this->m_filter_kondisi_dosen->filterKondisiDosen($nip,$kode);
            elseif($kode == 'PEG'):
                $data = $this->m_filter_kondisi_pegawai->filterKondisiPegawai($nip,$kode);
            endif;
             
            /**
            * ekseskusi berdasarkan case
            */
            if($data == "CASE 1"):
                $response = $this->m_case_1->executeHargaDt($nip,$kode,$periode);
            elseif($data == "CASE 6"):
                $response = $this->m_case_6->executeHargaDt($nip,$kode,$periode);
            elseif($data == "CASE 2"):
                $response = $this->m_case_2->executeHargaDt($nip,$kode,$periode);
            elseif($data == "CASE 3"):
                $response = $this->m_case_3->executeHargaDt($nip,$kode,$periode);
            elseif($data == "CASE 4"):
                $response = $this->m_case_4->executeHargaDt($nip,$kode,$periode);
            elseif($data == "CASE 5"):
                $response = $this->m_case_5->executeHargaDt($nip,$kode,$periode);
            elseif($data == "CASE 7"):
                $response = FALSE;
            elseif($data == "CASE 8"):
                $response = FALSE;
            elseif($data == "CASE 9"):
                $response = $this->m_case_9->executeHargaDt($nip,$kode,$periode);
            elseif($data == "CASE 10"):
                $response = FALSE;
            elseif($data == "CASE 11"):
                $response = FALSE;
            elseif($data == "CASE 12"):
                $response = FALSE;
            elseif($data == "CASE 13"):
                $response = FALSE;
            elseif($data == "CASE 14"):
                $response = FALSE;
            endif;

            return $response;
        }

        /**
         * menampilkan harga DB
         */
        public function harga_db(){
            header('Content-Type: application/json');
            $nip        = trim($this->input->get('nip','true'));
            $kode       = substr(trim($this->input->get('kode','true')),0,3);
            $periode    = trim($this->input->get('periode','true'));

            /**
             * penentuan case
             */
            if($kode == 'DOS'):
                $data  = $this->m_filter_kondisi_dosen->filterKondisiDosen($nip,$kode,$periode);
            elseif($kode == 'PEG'):
                $data = $this->m_filter_kondisi_pegawai->filterKondisiPegawai($nip,$kode,$periode);
            endif;
             
            /**
            * ekseskusi berdasarkan case
            */
            if($data == "CASE 1"):
                $response = $this->m_case_1->executeHargaDb($nip,$kode,$periode);
            elseif($data == "CASE 6"):
                $response = $this->m_case_6->executeHargaDb($nip,$kode,$periode);
            elseif($data == "CASE 2"):
                $response = $this->m_case_2->executeHargaDb($nip,$kode,$periode);
            elseif($data == "CASE 3"):
                $response = $this->m_case_3->executeHargaDb($nip,$kode,$periode);
            elseif($data == "CASE 4"):
                $response = $this->m_case_4->executeHargaDb($nip,$kode,$periode);
            elseif($data == "CASE 5"):
                $response = $this->m_case_5->executeHargaDb($nip,$kode,$periode);
            elseif($data == "CASE 7"):
                $response = $this->m_case_7->executeHargaDb($nip,$kode,$periode);
            elseif($data == "CASE 8"):
                $response = $this->m_case_8->executeHargaDb($nip,$kode,$periode);
            elseif($data == "CASE 9"):
                $response = $this->m_case_9->executeHargaDb($nip,$kode,$periode);
            elseif($data == "CASE 10"):
                $response = FALSE;
            elseif($data == "CASE 11"):
                $response = FALSE;
            elseif($data == "CASE 12"):
                $response = FALSE;
            elseif($data == "CASE 13"):
                $response = $this->m_case_13->executeHargaDb($nip,$kode,$periode);
            elseif($data == "CASE 14"):
                $response = $this->m_case_14->executeHargaDb($nip,$kode,$periode);
            endif;

            return $response;
        }

        public function harga_db_for_pembayaran($nip,$kode){
            $periode    = 20202;

            /**
             * penentuan case
             */
            if($kode == 'DOS'):
                $data  = $this->m_filter_kondisi_dosen->filterKondisiDosen($nip,$kode);
            elseif($kode == 'PEG'):
                $data = $this->m_filter_kondisi_pegawai->filterKondisiPegawai($nip,$kode);
            endif;
             
            /**
            * ekseskusi berdasarkan case
            */
            if($data == "CASE 1"):
                $response = $this->m_case_1->executeHargaDb($nip,$kode,$periode);
            elseif($data == "CASE 6"):
                $response = $this->m_case_6->executeHargaDb($nip,$kode,$periode);
            elseif($data == "CASE 2"):
                $response = $this->m_case_2->executeHargaDb($nip,$kode,$periode);
            elseif($data == "CASE 3"):
                $response = $this->m_case_3->executeHargaDb($nip,$kode,$periode);
            elseif($data == "CASE 4"):
                $response = $this->m_case_4->executeHargaDb($nip,$kode,$periode);
            elseif($data == "CASE 5"):
                $response = $this->m_case_5->executeHargaDb($nip,$kode,$periode);
            elseif($data == "CASE 7"):
                $response = $this->m_case_7->executeHargaDb($nip,$kode,$periode);
            elseif($data == "CASE 8"):
                $response = $this->m_case_8->executeHargaDb($nip,$kode,$periode);
            elseif($data == "CASE 9"):
                $response = $this->m_case_9->executeHargaDb($nip,$kode,$periode);
            elseif($data == "CASE 10"):
                $response = FALSE;
            elseif($data == "CASE 11"):
                $response = FALSE;
            elseif($data == "CASE 12"):
                $response = FALSE;
            elseif($data == "CASE 13"):
                $response = $this->m_case_13->executeHargaDb($nip,$kode,$periode);
            elseif($data == "CASE 14"):
                $response = $this->m_case_14->executeHargaDb($nip,$kode,$periode);
            endif;

            return $response;
        }

        /**
         * menampilkan total poin remun
         */
        public function total_poin_remun(){
            $nip                = trim($this->input->get('nip','true'));
            $periode            = trim($this->input->get('periode','true'));
            $kode               = trim($this->input->get('kode','true'));
            $kode2              = substr($kode, 0, 3);

            /**
             * Cek jenis dosen
             */
            $jenisDosen         = $this->cekJenisDosen($nip);

            /**
             * mendapatkan harga jabatan
             */
            // $harga_dt           = $this->harga_dt_2($nip, $periode);
            // $harga_db           = $this->harga_db_2($nip, $periode, $kode2);
            //$golongan           = $this->cekGol($nip);
            
            /**
             * mendapatkan poin
             */
            $tupoksi            = $this->tupoksi($kode,$periode);
            $mengajar           = $this->mengajar($kode,$periode);
            $mengajarLainnya    = $this->mengajarLainnya($kode,$periode);
            $penunjang          = $this->penunjang($kode,$periode);
            $penghargaan        = $this->penghargaan($kode,$periode);
            $total_semua_poin   = $tupoksi + $mengajar + $mengajarLainnya + $penunjang + $penghargaan;
            $poin_kinerja       = $tupoksi + $mengajar + $mengajarLainnya + $penunjang ;
            $stts               = null;

            /**
             * penentuan case
             */
            if($kode2 == 'DOS'):
                $data  = $this->m_filter_kondisi_dosen->filterKondisiDosen($nip,$kode2,$periode);
            elseif($kode2 == 'PEG'):
                $data = $this->m_filter_kondisi_pegawai->filterKondisiPegawai($nip,$kode2,$periode);
            endif;
             
            /**
            * ekseskusi berdasarkan case
            */
            if($data == "CASE 1"):
                $response = $this->m_case_1->executeTotalPoinRemun($jenisDosen,$tupoksi,$mengajar,$mengajarLainnya,$penunjang,$penghargaan,$total_semua_poin,$poin_kinerja,$stts,$kode,$kode2);
            elseif($data == "CASE 6"):
                $response = $this->m_case_6->executeTotalPoinRemun($jenisDosen,$tupoksi,$mengajar,$mengajarLainnya,$penunjang,$penghargaan,$total_semua_poin,$poin_kinerja,$stts,$kode,$kode2);
            elseif($data == "CASE 2"):
                $response = $this->m_case_2->executeTotalPoinRemun($jenisDosen,$tupoksi,$mengajar,$mengajarLainnya,$penunjang,$penghargaan,$total_semua_poin,$poin_kinerja,$stts,$kode,$kode2);
            elseif($data == "CASE 3"):
                $response = $this->m_case_3->executeTotalPoinRemun($jenisDosen,$tupoksi,$mengajar,$mengajarLainnya,$penunjang,$penghargaan,$total_semua_poin,$poin_kinerja,$stts,$kode,$kode2);
            elseif($data == "CASE 4"):
                $response = $this->m_case_4->executeTotalPoinRemun($jenisDosen,$tupoksi,$mengajar,$mengajarLainnya,$penunjang,$penghargaan,$total_semua_poin,$poin_kinerja,$stts,$kode,$kode2);
            elseif($data == "CASE 5"):
                $response = $this->m_case_5->executeTotalPoinRemun($jenisDosen,$tupoksi,$mengajar,$mengajarLainnya,$penunjang,$penghargaan,$total_semua_poin,$poin_kinerja,$stts,$kode,$kode2);
            elseif($data == "CASE 7"):
                $response = $this->m_case_7->executeTotalPoinRemun($jenisDosen,$tupoksi,$mengajar,$mengajarLainnya,$penunjang,$penghargaan,$total_semua_poin,$poin_kinerja,$stts,$kode,$kode2);
            elseif($data == "CASE 8"):
                $response = $this->m_case_8->executeTotalPoinRemun($jenisDosen,$tupoksi,$mengajar,$mengajarLainnya,$penunjang,$penghargaan,$total_semua_poin,$poin_kinerja,$stts,$kode,$kode2);
            elseif($data == "CASE 9"):
                $response = $this->m_case_9->executeTotalPoinRemun($jenisDosen,$tupoksi,$mengajar,$mengajarLainnya,$penunjang,$penghargaan,$total_semua_poin,$poin_kinerja,$stts,$kode,$kode2);
            elseif($data == "CASE 10"):
                $response = FALSE;
            elseif($data == "CASE 11"):
                $response = FALSE;
            elseif($data == "CASE 12"):
                $response = FALSE;
            elseif($data == "CASE 13"):
                $response = $this->m_case_13->executeTotalPoinRemun($jenisDosen,$tupoksi,$mengajar,$mengajarLainnya,$penunjang,$penghargaan,$total_semua_poin,$poin_kinerja,$stts,$kode,$kode2);
            elseif($data == "CASE 14"):
                $response = $this->m_case_14->executeTotalPoinRemun($jenisDosen,$tupoksi,$mengajar,$mengajarLainnya,$penunjang,$penghargaan,$total_semua_poin,$poin_kinerja,$stts,$kode,$kode2);
            endif;
            
            return $response;
            
        }

        public function total_poin_remun_pembayaran($nip,$kode,$kode2){
            $periode            = 20202;

            /**
             * Cek jenis dosen
             */
            $jenisDosen         = $this->cekJenisDosen($nip);

            /**
             * mendapatkan harga jabatan
             */
            // $harga_dt           = $this->harga_dt_2($nip, $periode);
            // $harga_db           = $this->harga_db_2($nip, $periode, $kode2);
            //$golongan           = $this->cekGol($nip);
            
            /**
             * mendapatkan poin
             */
            $tupoksi            = $this->tupoksi($kode,$periode);
            $mengajar           = $this->mengajar($kode,$periode);
            $mengajarLainnya    = $this->mengajarLainnya($kode,$periode);
            $penunjang          = $this->penunjang($kode,$periode);
            $penghargaan        = $this->penghargaan($kode,$periode);
            $total_semua_poin   = $tupoksi + $mengajar + $mengajarLainnya + $penunjang + $penghargaan;
            $poin_kinerja       = $tupoksi + $mengajar + $mengajarLainnya + $penunjang ;
            $stts               = null;

            /**
             * penentuan case
             */
            if($kode2 == 'DOS'):
                $data  = $this->m_filter_kondisi_dosen->filterKondisiDosen($nip,$kode2);
            elseif($kode2 == 'PEG'):
                $data = $this->m_filter_kondisi_pegawai->filterKondisiPegawai($nip,$kode2);
            endif;
             
            /**
            * ekseskusi berdasarkan case
            */
            if($data == "CASE 1"):
                $response = $this->m_case_1->executeTotalPoinRemun($jenisDosen,$tupoksi,$mengajar,$mengajarLainnya,$penunjang,$penghargaan,$total_semua_poin,$poin_kinerja,$stts,$kode,$kode2);
            elseif($data == "CASE 6"):
                $response = $this->m_case_6->executeTotalPoinRemun($jenisDosen,$tupoksi,$mengajar,$mengajarLainnya,$penunjang,$penghargaan,$total_semua_poin,$poin_kinerja,$stts,$kode,$kode2);
            elseif($data == "CASE 2"):
                $response = $this->m_case_2->executeTotalPoinRemun($jenisDosen,$tupoksi,$mengajar,$mengajarLainnya,$penunjang,$penghargaan,$total_semua_poin,$poin_kinerja,$stts,$kode,$kode2);
            elseif($data == "CASE 3"):
                $response = $this->m_case_3->executeTotalPoinRemun($jenisDosen,$tupoksi,$mengajar,$mengajarLainnya,$penunjang,$penghargaan,$total_semua_poin,$poin_kinerja,$stts,$kode,$kode2);
            elseif($data == "CASE 4"):
                $response = $this->m_case_4->executeTotalPoinRemun($jenisDosen,$tupoksi,$mengajar,$mengajarLainnya,$penunjang,$penghargaan,$total_semua_poin,$poin_kinerja,$stts,$kode,$kode2);
            elseif($data == "CASE 5"):
                $response = $this->m_case_5->executeTotalPoinRemun($jenisDosen,$tupoksi,$mengajar,$mengajarLainnya,$penunjang,$penghargaan,$total_semua_poin,$poin_kinerja,$stts,$kode,$kode2);
            elseif($data == "CASE 7"):
                $response = $this->m_case_7->executeTotalPoinRemun($jenisDosen,$tupoksi,$mengajar,$mengajarLainnya,$penunjang,$penghargaan,$total_semua_poin,$poin_kinerja,$stts,$kode,$kode2);
            elseif($data == "CASE 8"):
                $response = $this->m_case_8->executeTotalPoinRemun($jenisDosen,$tupoksi,$mengajar,$mengajarLainnya,$penunjang,$penghargaan,$total_semua_poin,$poin_kinerja,$stts,$kode,$kode2);
            elseif($data == "CASE 9"):
                $response = $this->m_case_9->executeTotalPoinRemun($jenisDosen,$tupoksi,$mengajar,$mengajarLainnya,$penunjang,$penghargaan,$total_semua_poin,$poin_kinerja,$stts,$kode,$kode2);
            elseif($data == "CASE 10"):
                $response = FALSE;
            elseif($data == "CASE 11"):
                $response = FALSE;
            elseif($data == "CASE 12"):
                $response = FALSE;
            elseif($data == "CASE 13"):
                $response = $this->m_case_13->executeTotalPoinRemun($jenisDosen,$tupoksi,$mengajar,$mengajarLainnya,$penunjang,$penghargaan,$total_semua_poin,$poin_kinerja,$stts,$kode,$kode2);
            elseif($data == "CASE 14"):
                $response = $this->m_case_14->executeTotalPoinRemun($jenisDosen,$tupoksi,$mengajar,$mengajarLainnya,$penunjang,$penghargaan,$total_semua_poin,$poin_kinerja,$stts,$kode,$kode2);
            endif;
            
            return $response;
            
        }

        /**
         * menampilkan total uang remun
         */
        public function total_uang_remun(){
            $nip         = trim($this->input->get('nip','true'));
            $periode     = trim($this->input->get('periode','true'));
            $kode        = trim($this->input->get('kode','true'));
            $kode2       = substr($kode, 0, 3);

            /**
             * init function support
             */
            $pir         = $this->pirSekarang($periode);
            $p2DT        = $this->poinP2DT($kode, $periode);
            $penghargaan = $this->penghargaan($kode,$periode);

            /**
             * penentuan case
             */
            if($kode2 == 'DOS'):
                $data  = $this->m_filter_kondisi_dosen->filterKondisiDosen($nip,$kode2,$periode);
            elseif($kode2 == 'PEG'):
                $data = $this->m_filter_kondisi_pegawai->filterKondisiPegawai($nip,$kode2,$periode);
            endif;
             
            /**
            * ekseskusi berdasarkan case
            */
            if($data == "CASE 1"):
                $response = $this->m_case_1->executeUangRemun($kode,$kode2,$pir,$p2DT,$penghargaan,$nip);
            elseif($data == "CASE 6"):
                $response = $this->m_case_6->executeUangRemun($kode,$kode2,$pir,$p2DT,$penghargaan,$nip);
            elseif($data == "CASE 2"):
                $response = $this->m_case_2->executeUangRemun($kode,$kode2,$pir,$p2DT,$penghargaan,$nip);
            elseif($data == "CASE 3"):
                $response = $this->m_case_3->executeUangRemun($kode,$kode2,$pir,$p2DT,$penghargaan,$nip);
            elseif($data == "CASE 4"):
                $response = $this->m_case_4->executeUangRemun($kode,$kode2,$pir,$p2DT,$penghargaan,$nip);
            elseif($data == "CASE 5"):
                $response = $this->m_case_5->executeUangRemun($kode,$kode2,$pir,$p2DT,$penghargaan,$nip);
            elseif($data == "CASE 7"):
                $response = $this->m_case_7->executeUangRemun($kode,$kode2,$pir,$p2DT,$penghargaan,$nip);
            elseif($data == "CASE 8"):
                $response = $this->m_case_8->executeUangRemun($kode,$kode2,$pir,$p2DT,$penghargaan,$nip);
            elseif($data == "CASE 9"):
                $response = $this->m_case_9->executeUangRemun($kode,$kode2,$pir,$p2DT,$penghargaan,$nip);
            elseif($data == "CASE 10"):
                $response = FALSE;
            elseif($data == "CASE 11"):
                $response = FALSE;
            elseif($data == "CASE 12"):
                $response = FALSE;
            elseif($data == "CASE 13"):
                $response = $this->m_case_13->executeUangRemun($kode,$kode2,$pir,$p2DT,$penghargaan,$nip);
            elseif($data == "CASE 14"):
                $response = $this->m_case_14->executeUangRemun($kode,$kode2,$pir,$p2DT,$penghargaan,$nip);
            endif;
            
            return $response;
        }

        /**
         * menampilkan total uang remun
         */
        public function total_uang_remun_pembayaran_p1($nip,$periode,$kode){
            //$nip         = trim($this->input->get('nip','true'));
            //$periode     = trim($this->input->get('periode','true'));
            //$kode        = trim($this->input->get('kode','true'));
            $kode2       = substr($kode, 0, 3);

            /**
             * init function support
             */
            $pir         = $this->pirSekarang($periode);
            $p2DT        = $this->poinP2DT($kode, $periode);
            $penghargaan = $this->penghargaan($kode,$periode);

            /**
             * penentuan case
             */
            if($kode2 == 'DOS'):
                $data  = $this->m_filter_kondisi_dosen->filterKondisiDosen($nip,$kode2,$periode);
            elseif($kode2 == 'PEG'):
                $data = $this->m_filter_kondisi_pegawai->filterKondisiPegawai($nip,$kode2,$periode);
            endif;
             
            /**
            * ekseskusi berdasarkan case
            */
            if($data == "CASE 1"):
                $response = $this->m_case_1->executeUangRemun($kode,$kode2,$pir,$p2DT,$penghargaan,$nip);
            elseif($data == "CASE 6"):
                $response = $this->m_case_6->executeUangRemun($kode,$kode2,$pir,$p2DT,$penghargaan,$nip);
            elseif($data == "CASE 2"):
                $response = $this->m_case_2->executeUangRemun($kode,$kode2,$pir,$p2DT,$penghargaan,$nip);
            elseif($data == "CASE 3"):
                $response = $this->m_case_3->executeUangRemun($kode,$kode2,$pir,$p2DT,$penghargaan,$nip);
            elseif($data == "CASE 4"):
                $response = $this->m_case_4->executeUangRemun($kode,$kode2,$pir,$p2DT,$penghargaan,$nip);
            elseif($data == "CASE 5"):
                $response = $this->m_case_5->executeUangRemun($kode,$kode2,$pir,$p2DT,$penghargaan,$nip);
            elseif($data == "CASE 7"):
                $response = $this->m_case_7->executeUangRemun($kode,$kode2,$pir,$p2DT,$penghargaan,$nip);
            elseif($data == "CASE 8"):
                $response = $this->m_case_8->executeUangRemun($kode,$kode2,$pir,$p2DT,$penghargaan,$nip);
            elseif($data == "CASE 9"):
                $response = $this->m_case_9->executeUangRemun($kode,$kode2,$pir,$p2DT,$penghargaan,$nip);
            elseif($data == "CASE 10"):
                $response = FALSE;
            elseif($data == "CASE 11"):
                $response = FALSE;
            elseif($data == "CASE 12"):
                $response = FALSE;
            elseif($data == "CASE 13"):
                $response = $this->m_case_13->executeUangRemun($kode,$kode2,$pir,$p2DT,$penghargaan,$nip);
            elseif($data == "CASE 14"):
                $response = $this->m_case_14->executeUangRemun($kode,$kode2,$pir,$p2DT,$penghargaan,$nip);
            endif;
            
            return $response;
        }

        public function total_uang_remun_db(){
            $nip         = trim($this->input->get('nip','true'));
            $periode     = trim($this->input->get('periode','true'));
            $kode        = trim($this->input->get('kode','true'));
            $kode2       = substr($kode, 0, 3);

            /**
             * init function support
             */
            $pir         = $this->pirSekarang($periode);
            $p2DT        = $this->poinP2DT($kode, $periode);
            $penghargaan = $this->penghargaan($kode,$periode);
            $golongan    = $this->cekGol($nip);

            $tupoksi            = $this->tupoksi($kode,$periode);
            $mengajar           = $this->mengajar($kode,$periode);
            $mengajarLainnya    = $this->mengajarLainnya($kode,$periode);
            $penunjang          = $this->penunjang($kode,$periode);
            $penghargaan        = $this->penghargaan($kode,$periode);
            $total_semua_poin   = $tupoksi + $mengajar + $mengajarLainnya + $penunjang + $penghargaan;
            $poin_kinerja       = $tupoksi + $mengajar + $mengajarLainnya + $penunjang ;
            /**
             * penentuan case
             */
            if($kode2 == 'DOS'):
                $data  = $this->m_filter_kondisi_dosen->filterKondisiDosen($nip,$kode2,$periode);
            elseif($kode2 == 'PEG'):
                $data = $this->m_filter_kondisi_pegawai->filterKondisiPegawai($nip,$kode2,$periode);
            endif;
             
            /**
            * ekseskusi berdasarkan case
            */
            if($data == "CASE 1"):
                $response = $this->m_case_1->executeUangRemunDB($kode,$kode2,$pir,$p2DT,$penghargaan,$nip, $poin_kinerja, $golongan);
            elseif($data == "CASE 2"):
                $response = $this->m_case_2->executeUangRemunDB($kode,$kode2,$pir,$p2DT,$penghargaan,$nip, $poin_kinerja, $golongan);
            elseif($data == "CASE 3"):
                $response = $this->m_case_3->executeUangRemunDB($kode,$kode2,$pir,$p2DT,$penghargaan,$nip, $poin_kinerja, $golongan); 
            elseif($data == "CASE 4"):
                $response = $this->m_case_4->executeUangRemunDB($kode,$kode2,$pir,$p2DT,$penghargaan,$nip, $poin_kinerja, $golongan);
            elseif($data == "CASE 5"):
                $response = $this->m_case_5->executeUangRemunDB($kode,$kode2,$pir,$p2DT,$penghargaan,$nip, $poin_kinerja, $golongan);
            elseif($data == "CASE 6"):
                $response = $this->m_case_6->executeUangRemunDB($kode,$kode2,$pir,$p2DT,$penghargaan,$nip, $poin_kinerja, $golongan);
            elseif($data == "CASE 7"):
                $response = $this->m_case_7->executeUangRemunDB($kode,$kode2,$pir,$p2DT,$penghargaan,$nip, $poin_kinerja, $golongan);
            elseif($data == "CASE 8"):
                $response = $this->m_case_8->executeUangRemunDB($kode,$kode2,$pir,$p2DT,$penghargaan,$nip, $poin_kinerja, $golongan);
            elseif($data == "CASE 9"):
                $response = $this->m_case_9->executeUangRemunDB($kode,$kode2,$pir,$p2DT,$penghargaan,$nip, $poin_kinerja, $golongan);
            elseif($data == "CASE 10"):
                $response = FALSE;
            elseif($data == "CASE 11"):
                $response = FALSE;
            elseif($data == "CASE 12"):
                $response = FALSE;
            elseif($data == "CASE 13"):
                $response = $this->m_case_13->executeUangRemunDB($kode,$kode2,$pir,$p2DT,$penghargaan,$nip, $poin_kinerja, $golongan);
            elseif($data == "CASE 14"):
                $response = $this->m_case_14->executeUangRemunDB($kode,$kode2,$pir,$p2DT,$penghargaan,$nip, $poin_kinerja, $golongan);    
            endif;
            return $response;
        }

        public function total_uang_remun_db_for_pembayaran($nip,$kode,$kode2){
            $periode     = 20202;

            /**
             * init function support
             */
            $pir         = $this->pirSekarang($periode);
            $p2DT        = $this->poinP2DT($kode, $periode);
            $penghargaan = $this->penghargaan($kode,$periode);
            $golongan    = $this->cekGol($nip);

            $tupoksi            = $this->tupoksi($kode,$periode);
            $mengajar           = $this->mengajar($kode,$periode);
            $mengajarLainnya    = $this->mengajarLainnya($kode,$periode);
            $penunjang          = $this->penunjang($kode,$periode);
            $penghargaan        = $this->penghargaan($kode,$periode);
            $total_semua_poin   = $tupoksi + $mengajar + $mengajarLainnya + $penunjang + $penghargaan;
            $poin_kinerja       = $tupoksi + $mengajar + $mengajarLainnya + $penunjang ;
            /**
             * penentuan case
             */
            if($kode2 == 'DOS'):
                $data  = $this->m_filter_kondisi_dosen->filterKondisiDosen($nip,$kode2,$periode);
            elseif($kode2 == 'PEG'):
                $data = $this->m_filter_kondisi_pegawai->filterKondisiPegawai($nip,$kode2,$periode);
            endif;
             
            /**
            * ekseskusi berdasarkan case
            */
            if($data == "CASE 1"):
                $response = $this->m_case_1->executeUangRemunDB($kode,$kode2,$pir,$p2DT,$penghargaan,$nip, $poin_kinerja, $golongan);
            elseif($data == "CASE 2"):
                $response = $this->m_case_2->executeUangRemunDB($kode,$kode2,$pir,$p2DT,$penghargaan,$nip, $poin_kinerja, $golongan);
            elseif($data == "CASE 3"):
                $response = $this->m_case_3->executeUangRemunDB($kode,$kode2,$pir,$p2DT,$penghargaan,$nip, $poin_kinerja, $golongan); 
            elseif($data == "CASE 4"):
                $response = $this->m_case_4->executeUangRemunDB($kode,$kode2,$pir,$p2DT,$penghargaan,$nip, $poin_kinerja, $golongan);
            elseif($data == "CASE 5"):
                $response = $this->m_case_5->executeUangRemunDB($kode,$kode2,$pir,$p2DT,$penghargaan,$nip, $poin_kinerja, $golongan);
            elseif($data == "CASE 6"):
                $response = $this->m_case_6->executeUangRemunDB($kode,$kode2,$pir,$p2DT,$penghargaan,$nip, $poin_kinerja, $golongan);
            elseif($data == "CASE 7"):
                $response = $this->m_case_7->executeUangRemunDB($kode,$kode2,$pir,$p2DT,$penghargaan,$nip, $poin_kinerja, $golongan);
            elseif($data == "CASE 8"):
                $response = $this->m_case_8->executeUangRemunDB($kode,$kode2,$pir,$p2DT,$penghargaan,$nip, $poin_kinerja, $golongan);
            elseif($data == "CASE 9"):
                $response = $this->m_case_9->executeUangRemunDB($kode,$kode2,$pir,$p2DT,$penghargaan,$nip, $poin_kinerja, $golongan);
            elseif($data == "CASE 10"):
                $response = FALSE;
            elseif($data == "CASE 11"):
                $response = FALSE;
            elseif($data == "CASE 12"):
                $response = FALSE;
            elseif($data == "CASE 13"):
                $response = $this->m_case_13->executeUangRemunDB($kode,$kode2,$pir,$p2DT,$penghargaan,$nip, $poin_kinerja, $golongan);
            elseif($data == "CASE 14"):
                $response = $this->m_case_14->executeUangRemunDB($kode,$kode2,$pir,$p2DT,$penghargaan,$nip, $poin_kinerja, $golongan);    
            endif;
            return $response;
        }
        //========================= function penting ========================//  

        public function link($key){
            $arrContextOptions=array(
            "ssl"=>array(
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
                ),
            );  
            $json  = "https://dev.unimed.ac.id/sso-statistik/api/simremlink/data-aktivitas";

            if($key == 'tusi'):
            $array1 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag1  = 10101;
            $resources1 = array_filter($array1, function ($var1) use ($flag1) {
                return ($var1['kode_peran'] == $flag1);
            });
            $array2 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag2  = 10102;
            $resources2 = array_filter($array2, function ($var2) use ($flag2) {
                return ($var2['kode_peran'] == $flag2);
            });
            $array3 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag3 = 10201;
            $resources3 = array_filter($array3, function ($var3) use ($flag3) {
                return ($var3['kode_peran'] == $flag3);
            });
            $array4 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag4 = 10301;
            $resources4 = array_filter($array4, function ($var4) use ($flag4) {
                return ($var4['kode_peran'] == $flag4);
            });
            $array5 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag5 = 10302;
            $resources5 = array_filter($array5, function ($var5) use ($flag5) {
                return ($var5['kode_peran'] == $flag5);
            });
            $array6 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag6 = 10303;
            $resources6 = array_filter($array6, function ($var6) use ($flag6) {
                return ($var6['kode_peran'] == $flag6);
            });
            $array7 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag7 = 10304;
            $resources7 = array_filter($array7, function ($var7) use ($flag7) {
                return ($var7['kode_peran'] == $flag7);
            });
            $array8 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag8 = 10305;
            $resources8 = array_filter($array8, function ($var8) use ($flag8) {
                return ($var8['kode_peran'] == $flag8);
            });
            $array9 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag9 = 10401;
            $resources9 = array_filter($array9, function ($var9) use ($flag9) {
                return ($var9['kode_peran'] == $flag9);
            });
            $resources = array_merge($resources1,$resources2,$resources3,$resources4,$resources5,$resources6,$resources7,$resources8,$resources9);
            elseif($key == 'mengajar'):
            $array = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag = 20101;
            $resources = array_filter($array, function ($var) use ($flag) {
                return ($var['kode_peran'] == $flag);
            });
            $array1 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag1  = 20102;
            $resources1 = array_filter($array1, function ($var1) use ($flag1) {
                return ($var1['kode_peran'] == $flag1);
            });
            $array2 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag2  = 20103;
            $resources2 = array_filter($array2, function ($var2) use ($flag2) {
                return ($var2['kode_peran'] == $flag2);
            });
            $array3 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag3 = 20104;
            $resources3 = array_filter($array3, function ($var3) use ($flag3) {
                return ($var3['kode_peran'] == $flag3);
            });
            $array4 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag4 = 20105;
            $resources4 = array_filter($array4, function ($var4) use ($flag4) {
                return ($var4['kode_peran'] == $flag4);
            });
            $array5 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag5 = 20106;
            $resources5 = array_filter($array5, function ($var5) use ($flag5) {
                return ($var5['kode_peran'] == $flag5);
            });
            $array6 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag6 = 20107;
            $resources6 = array_filter($array6, function ($var6) use ($flag6) {
                return ($var6['kode_peran'] == $flag6);
            });
            $array7 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag7 = 20108;
            $resources7 = array_filter($array7, function ($var7) use ($flag7) {
                return ($var7['kode_peran'] == $flag7);
            });
            $array8 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag8 = 20109;
            $resources8 = array_filter($array8, function ($var8) use ($flag8) {
                return ($var8['kode_peran'] == $flag8);
            });
            $resources = array_merge($resources,$resources1,$resources2,$resources3,$resources4,$resources5,$resources6,$resources7,$resources8);
            elseif($key == 'mengajar-lainnya'):
            $array9 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag9 = 20201;
            $resources9 = array_filter($array9, function ($var9) use ($flag9) {
                return ($var9['kode_peran'] == $flag9);
            });
            $array10 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag10 = 20301;
            $resources10 = array_filter($array10, function ($var10) use ($flag10) {
                return ($var10['kode_peran'] == $flag10);
            });
            $array11 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag11 = 20302;
            $resources11 = array_filter($array11, function ($var11) use ($flag11) {
                return ($var11['kode_peran'] == $flag11);
            });
            $array12 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag12 = 20303;
            $resources12 = array_filter($array12, function ($var12) use ($flag12) {
                return ($var12['kode_peran'] == $flag12);
            });
            $array13 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag13 = 20304;
            $resources13 = array_filter($array13, function ($var13) use ($flag13) {
                return ($var13['kode_peran'] == $flag13);
            });
            $array14 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag14 = 20305;
            $resources14 = array_filter($array14, function ($var14) use ($flag14) {
                return ($var14['kode_peran'] == $flag14);
            });
            $array15 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag15 = 20401;
            $resources15 = array_filter($array15, function ($var15) use ($flag15) {
                return ($var15['kode_peran'] == $flag15);
            });
            $array16 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag16 = 20501;
            $resources16 = array_filter($array16, function ($var16) use ($flag16) {
                return ($var16['kode_peran'] == $flag16);
            });
            $array17 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag17 = 20502;
            $resources17 = array_filter($array17, function ($var17) use ($flag17) {
                return ($var17['kode_peran'] == $flag17);
            });
            $array18 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag18 = 20503;
            $resources18 = array_filter($array18, function ($var18) use ($flag18) {
                return ($var18['kode_peran'] == $flag18);
            });
            $array19 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag19 = 20504;
            $resources19 = array_filter($array19, function ($var19) use ($flag19) {
                return ($var19['kode_peran'] == $flag19);
            });
            $array20 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag20 = 20505;
            $resources20 = array_filter($array20, function ($var20) use ($flag20) {
                return ($var20['kode_peran'] == $flag20);
            });
            $array21 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag21 = 20601;
            $resources21 = array_filter($array21, function ($var21) use ($flag21) {
                return ($var21['kode_peran'] == $flag21);
            });
            $array22 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag22 = 20602;
            $resources22 = array_filter($array22, function ($var22) use ($flag22) {
                return ($var22['kode_peran'] == $flag22);
            });
            //-----
            $array23 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag23 = 20603;
            $resources23 = array_filter($array23, function ($var23) use ($flag23) {
                return ($var23['kode_peran'] == $flag23);
            });
            $array24 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag24 = 20604;
            $resources24 = array_filter($array24, function ($var24) use ($flag24) {
                return ($var24['kode_peran'] == $flag24);
            });
            $array25 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag25 = 20605;
            $resources25 = array_filter($array25, function ($var25) use ($flag25) {
                return ($var25['kode_peran'] == $flag25);
            });
            $array26 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag26 = 20606;
            $resources26 = array_filter($array26, function ($var26) use ($flag26) {
                return ($var26['kode_peran'] == $flag26);
            });
            $array27 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag27 = 20607;
            $resources27 = array_filter($array27, function ($var27) use ($flag27) {
                return ($var27['kode_peran'] == $flag27);
            });
            $array28 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag28 = 20608;
            $resources28 = array_filter($array28, function ($var28) use ($flag28) {
                return ($var28['kode_peran'] == $flag28);
            });
            $array29 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag29 = 20609;
            $resources29 = array_filter($array29, function ($var29) use ($flag29) {
                return ($var29['kode_peran'] == $flag29);
            });
            $array30 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag30 = 20610;
            $resources30 = array_filter($array30, function ($var30) use ($flag30) {
                return ($var30['kode_peran'] == $flag30);
            });
            $array31 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag31 = 20611;
            $resources31 = array_filter($array31, function ($var31) use ($flag31) {
                return ($var31['kode_peran'] == $flag31);
            });
            $array32 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag32 = 20612;
            $resources32 = array_filter($array32, function ($var32) use ($flag32) {
                return ($var32['kode_peran'] == $flag32);
            });
            $array33 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag33 = 20613;
            $resources33 = array_filter($array33, function ($var33) use ($flag33) {
                return ($var33['kode_peran'] == $flag33);
            });
            $resources = array_merge($resources10,$resources21,$resources22,$resources23,$resources24,$resources25,$resources26,
            $resources27,$resources28,$resources29,$resources30,$resources31,$resources32,$resources33);
            elseif($key == 'penelitian'):
            $array1 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag1  = 30101;
            $resources1 = array_filter($array1, function ($var1) use ($flag1) {
                return ($var1['kode_peran'] == $flag1);
            });
            $array2 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag2  = 30102;
            $resources2 = array_filter($array2, function ($var2) use ($flag2) {
                return ($var2['kode_peran'] == $flag2);
            });
            $resources = array_merge($resources1,$resources2);
            elseif($key == 'pengabdian'):
            $array1 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag1  = 30201;
            $resources1 = array_filter($array1, function ($var1) use ($flag1) {
                return ($var1['kode_peran'] == $flag1);
            });
            $array2 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag2  = 30202;
            $resources2 = array_filter($array2, function ($var2) use ($flag2) {
                return ($var2['kode_peran'] == $flag2);
            });
            $resources = array_merge($resources1,$resources2);
            elseif($key == 'penghargaan'):
            $array1 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag1 = 40101;
            $resources1 = array_filter($array1, function ($var1) use ($flag1) {
                return ($var9['kode_peran'] == $flag9);
            });
            $array2 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag2 = 40102;
            $resources2 = array_filter($array2, function ($var2) use ($flag2) {
                return ($var2['kode_peran'] == $flag2);
            });
            $array3 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag3 = 40103;
            $resources3 = array_filter($array3, function ($var3) use ($flag3) {
                return ($var3['kode_peran'] == $flag3);
            });
            $array4 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag4 = 40104;
            $resources4 = array_filter($array4, function ($var4) use ($flag4) {
                return ($var4['kode_peran'] == $flag4);
            });
            $array5 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag5 = 40105;
            $resources5 = array_filter($array5, function ($var5) use ($flag5) {
                return ($var5['kode_peran'] == $flag5);
            });
            $array6 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag6 = 40106;
            $resources6 = array_filter($array6, function ($var6) use ($flag6) {
                return ($var6['kode_peran'] == $flag6);
            });
            $array7 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag7 = 40201;
            $resources7 = array_filter($array7, function ($var7) use ($flag7) {
                return ($var7['kode_peran'] == $flag7);
            });
            $array8 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag8 = 40202;
            $resources8 = array_filter($array8, function ($var8) use ($flag8) {
                return ($var8['kode_peran'] == $flag8);
            });
            $array9 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag9 = 40203;
            $resources9 = array_filter($array9, function ($var9) use ($flag9) {
                return ($var9['kode_peran'] == $flag9);
            });
            $array10 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag10 = 40204;
            $resources10 = array_filter($array10, function ($var10) use ($flag10) {
                return ($var10['kode_peran'] == $flag10);
            });
            $array11 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag11 = 40205;
            $resources11 = array_filter($array11, function ($var11) use ($flag11) {
                return ($var11['kode_peran'] == $flag11);
            });
            $array12 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag12 = 40206;
            $resources12 = array_filter($array12, function ($var12) use ($flag12) {
                return ($var12['kode_peran'] == $flag12);
            });
            $array13 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag13 = 40301;
            $resources13 = array_filter($array13, function ($var13) use ($flag13) {
                return ($var13['kode_peran'] == $flag13);
            });
            $array14 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag14 = 40302;
            $resources14 = array_filter($array14, function ($var14) use ($flag14) {
                return ($var14['kode_peran'] == $flag14);
            });
            $array15 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag15 = 40303;
            $resources15 = array_filter($array15, function ($var15) use ($flag15) {
                return ($var15['kode_peran'] == $flag15);
            });
            $array16 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag16 = 40304;
            $resources16 = array_filter($array16, function ($var16) use ($flag16) {
                return ($var16['kode_peran'] == $flag16);
            });
            $array17 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag17 = 40305;
            $resources17 = array_filter($array17, function ($var17) use ($flag17) {
                return ($var17['kode_peran'] == $flag17);
            });
            $array18 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag18 = 40306;
            $resources18 = array_filter($array18, function ($var18) use ($flag18) {
                return ($var18['kode_peran'] == $flag18);
            });
            $array19 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag19 = 40307;
            $resources19 = array_filter($array19, function ($var19) use ($flag19) {
                return ($var19['kode_peran'] == $flag19);
            });
            $array20 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag20 = 40308;
            $resources20 = array_filter($array20, function ($var20) use ($flag20) {
                return ($var20['kode_peran'] == $flag20);
            });
            $array21 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag21 = 40309;
            $resources21 = array_filter($array21, function ($var21) use ($flag21) {
                return ($var21['kode_peran'] == $flag21);
            });
            $array22 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag22 = 40310;
            $resources22 = array_filter($array22, function ($var22) use ($flag22) {
                return ($var22['kode_peran'] == $flag22);
            });
            //-----
            $array23 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag23 = 40311;
            $resources23 = array_filter($array23, function ($var23) use ($flag23) {
                return ($var23['kode_peran'] == $flag23);
            });
            $array24 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag24 = 40312;
            $resources24 = array_filter($array24, function ($var24) use ($flag24) {
                return ($var24['kode_peran'] == $flag24);
            });
            $array25 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag25 = 40313;
            $resources25 = array_filter($array25, function ($var25) use ($flag25) {
                return ($var25['kode_peran'] == $flag25);
            });
            $array26 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag26 = 40314;
            $resources26 = array_filter($array26, function ($var26) use ($flag26) {
                return ($var26['kode_peran'] == $flag26);
            });
            $array27 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag27 = 40315;
            $resources27 = array_filter($array27, function ($var27) use ($flag27) {
                return ($var27['kode_peran'] == $flag27);
            });
            $array28 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag28 = 40316;
            $resources28 = array_filter($array28, function ($var28) use ($flag28) {
                return ($var28['kode_peran'] == $flag28);
            });
            $array29 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag29 = 40317;
            $resources29 = array_filter($array29, function ($var29) use ($flag29) {
                return ($var29['kode_peran'] == $flag29);
            });
            $array30 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag30 = 40318;
            $resources30 = array_filter($array30, function ($var30) use ($flag30) {
                return ($var30['kode_peran'] == $flag30);
            });
            $array31 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag31 = 40401;
            $resources31 = array_filter($array31, function ($var31) use ($flag31) {
                return ($var31['kode_peran'] == $flag31);
            });
            $array32 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag32 = 40402;
            $resources32 = array_filter($array32, function ($var32) use ($flag32) {
                return ($var32['kode_peran'] == $flag32);
            });
            $array33 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag33 = 40403;
            $resources33 = array_filter($array33, function ($var33) use ($flag33) {
                return ($var33['kode_peran'] == $flag33);
            });
            //-------
            $array34 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag34 = 40404;
            $resources34 = array_filter($array34, function ($var34) use ($flag34) {
                return ($var34['kode_peran'] == $flag34);
            });
            $array35 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag35 = 40405;
            $resources35 = array_filter($array35, function ($var35) use ($flag35) {
                return ($var35['kode_peran'] == $flag35);
            });
            $array36 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag36 = 40406;
            $resources36 = array_filter($array36, function ($var36) use ($flag36) {
                return ($var36['kode_peran'] == $flag36);
            });
            $array37 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag37 = 40501;
            $resources37 = array_filter($array37, function ($var37) use ($flag37) {
                return ($var37['kode_peran'] == $flag37);
            });
            $array38 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag38 = 40502;
            $resources38 = array_filter($array38, function ($var38) use ($flag38) {
                return ($var38['kode_peran'] == $flag38);
            });
            $array39 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag39 = 40503;
            $resources39 = array_filter($array39, function ($var39) use ($flag39) {
                return ($var39['kode_peran'] == $flag39);
            });
            $array40 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag40 = 40504;
            $resources40 = array_filter($array40, function ($var40) use ($flag40) {
                return ($var40['kode_peran'] == $flag40);
            });
            $array41 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag41 = 40505;
            $resources41 = array_filter($array41, function ($var41) use ($flag41) {
                return ($var41['kode_peran'] == $flag41);
            });
            $array42 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag42 = 40506;
            $resources42 = array_filter($array42, function ($var42) use ($flag42) {
                return ($var42['kode_peran'] == $flag42);
            });
            $array43 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag43 = 40507;
            $resources43 = array_filter($array43, function ($var43) use ($flag43) {
                return ($var43['kode_peran'] == $flag43);
            });
            $array44 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag44 = 40508;
            $resources44 = array_filter($array44, function ($var44) use ($flag44) {
                return ($var44['kode_peran'] == $flag44);
            });
            $array45 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag45 = 40509;
            $resources45 = array_filter($array45, function ($var45) use ($flag45) {
                return ($var45['kode_peran'] == $flag45);
            });
            $array46 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag46 = 40510;
            $resources46 = array_filter($array46, function ($var46) use ($flag46) {
                return ($var46['kode_peran'] == $flag46);
            });
            $array47 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag47 = 40511;
            $resources47 = array_filter($array47, function ($var47) use ($flag47) {
                return ($var47['kode_peran'] == $flag47);
            });
            $array48 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag48 = 40512;
            $resources48 = array_filter($array48, function ($var48) use ($flag48) {
                return ($var48['kode_peran'] == $flag48);
            });
            $array49 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag49 = 40513;
            $resources49 = array_filter($array49, function ($var49) use ($flag49) {
                return ($var49['kode_peran'] == $flag49);
            });
            $array50 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag50 = 40514;
            $resources50 = array_filter($array50, function ($var50) use ($flag50) {
                return ($var50['kode_peran'] == $flag50);
            });
            //----
            $array51 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag51 = 40601;
            $resources51 = array_filter($array51, function ($var51) use ($flag51) {
                return ($var51['kode_peran'] == $flag51);
            });
            $array52 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag52 = 40602;
            $resources52 = array_filter($array52, function ($var52) use ($flag52) {
                return ($var52['kode_peran'] == $flag52);
            });
            $array53 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag53 = 40603;
            $resources53 = array_filter($array53, function ($var53) use ($flag53) {
                return ($var53['kode_peran'] == $flag53);
            });
            $array54 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag54 = 40604;
            $resources54 = array_filter($array54, function ($var54) use ($flag54) {
                return ($var54['kode_peran'] == $flag54);
            });
            $array55 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag55 = 40605;
            $resources55 = array_filter($array55, function ($var55) use ($flag55) {
                return ($var55['kode_peran'] == $flag55);
            });
            $array56 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag56 = 40606;
            $resources56 = array_filter($array56, function ($var56) use ($flag56) {
                return ($var56['kode_peran'] == $flag56);
            });
            $array57 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag57 = 40607;
            $resources57 = array_filter($array57, function ($var57) use ($flag57) {
                return ($var57['kode_peran'] == $flag57);
            });
            $array58 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag58 = 40608;
            $resources58 = array_filter($array58, function ($var58) use ($flag58) {
                return ($var58['kode_peran'] == $flag58);
            });
            $array59 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag59 = 40609;
            $resources59 = array_filter($array59, function ($var59) use ($flag59) {
                return ($var59['kode_peran'] == $flag59);
            });
            $array60 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag60 = 40610;
            $resources60 = array_filter($array60, function ($var60) use ($flag60) {
                return ($var60['kode_peran'] == $flag60);
            });
            $array61 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag61 = 40611;
            $resources61 = array_filter($array61, function ($var61) use ($flag61) {
                return ($var61['kode_peran'] == $flag61);
            });
            $array62 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag62 = 40612;
            $resources62 = array_filter($array62, function ($var62) use ($flag62) {
                return ($var62['kode_peran'] == $flag62);
            });
            $array63 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag63 = 40613;
            $resources63 = array_filter($array63, function ($var63) use ($flag63) {
                return ($var63['kode_peran'] == $flag63);
            });
            $array64 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag64 = 40614;
            $resources64 = array_filter($array64, function ($var64) use ($flag64) {
                return ($var64['kode_peran'] == $flag64);
            });
            $array65 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag65 = 40615;
            $resources65 = array_filter($array65, function ($var65) use ($flag65) {
                return ($var65['kode_peran'] == $flag65);
            });
            $array66 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag66 = 40616;
            $resources66 = array_filter($array66, function ($var66) use ($flag66) {
                return ($var66['kode_peran'] == $flag66);
            });
            $array67 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag67 = 40617;
            $resources67 = array_filter($array67, function ($var67) use ($flag67) {
                return ($var67['kode_peran'] == $flag67);
            });
            //-------------
            $array68 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag68 = 40618;
            $resources68 = array_filter($array68, function ($var68) use ($flag68) {
                return ($var68['kode_peran'] == $flag68);
            });
            $array69 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag69 = 40619;
            $resources69 = array_filter($array69, function ($var69) use ($flag69) {
                return ($var69['kode_peran'] == $flag69);
            });
            $array70 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag70 = 40620;
            $resources70 = array_filter($array70, function ($var70) use ($flag70) {
                return ($var70['kode_peran'] == $flag70);
            });
            $array71 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag71 = 40621;
            $resources71 = array_filter($array71, function ($var71) use ($flag71) {
                return ($var71['kode_peran'] == $flag71);
            });
            $array72 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag72 = 40622;
            $resources72 = array_filter($array72, function ($var72) use ($flag72) {
                return ($var72['kode_peran'] == $flag72);
            });
            $array73 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag73 = 40623;
            $resources73 = array_filter($array73, function ($var73) use ($flag73) {
                return ($var73['kode_peran'] == $flag73);
            });
            $array74 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag74 = 40624;
            $resources74 = array_filter($array74, function ($var74) use ($flag74) {
                return ($var74['kode_peran'] == $flag74);
            });
            $array75 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag75 = 40625;
            $resources75 = array_filter($array75, function ($var75) use ($flag75) {
                return ($var75['kode_peran'] == $flag75);
            });
            $array76 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag76 = 40626;
            $resources76 = array_filter($array76, function ($var76) use ($flag76) {
                return ($var76['kode_peran'] == $flag76);
            });
            $array77 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag77 = 40627;
            $resources77 = array_filter($array77, function ($var77) use ($flag77) {
                return ($var77['kode_peran'] == $flag77);
            });
            $array78 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag78 = 40628;
            $resources78 = array_filter($array78, function ($var78) use ($flag78) {
                return ($var78['kode_peran'] == $flag78);
            });
            $array79 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag79 = 40629;
            $resources79 = array_filter($array79, function ($var79) use ($flag79) {
                return ($var79['kode_peran'] == $flag79);
            });
            $array80 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag80 = 40630;
            $resources80 = array_filter($array80, function ($var80) use ($flag80) {
                return ($var80['kode_peran'] == $flag80);
            });
            $array81 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag81 = 40631;
            $resources81 = array_filter($array81, function ($var81) use ($flag81) {
                return ($var81['kode_peran'] == $flag81);
            });
            $array82 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag82 = 40632;
            $resources82 = array_filter($array82, function ($var82) use ($flag82) {
                return ($var82['kode_peran'] == $flag82);
            });
            $array83 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag83 = 40633;
            $resources83 = array_filter($array83, function ($var83) use ($flag83) {
                return ($var83['kode_peran'] == $flag83);
            });
            $array84 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag84 = 40634;
            $resources84 = array_filter($array84, function ($var84) use ($flag84) {
                return ($var84['kode_peran'] == $flag84);
            });
            $array85 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag85 = 40635;
            $resources85 = array_filter($array85, function ($var85) use ($flag85) {
                return ($var85['kode_peran'] == $flag85);
            });
            $array86 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag86 = 40636;
            $resources86 = array_filter($array86, function ($var86) use ($flag86) {
                return ($var86['kode_peran'] == $flag86);
            });
            $array87 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag87 = 40637;
            $resources87 = array_filter($array87, function ($var87) use ($flag87) {
                return ($var87['kode_peran'] == $flag87);
            });
            $array88 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag88 = 40618;
            $resources88 = array_filter($array88, function ($var88) use ($flag88) {
                return ($var88['kode_peran'] == $flag88);
            });
            $array89 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag89 = 40619;
            $resources89 = array_filter($array89, function ($var89) use ($flag89) {
                return ($var89['kode_peran'] == $flag89);
            });
            $array90 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag90 = 40620;
            $resources90 = array_filter($array90, function ($var90) use ($flag90) {
                return ($var90['kode_peran'] == $flag68);
            });
            $array91 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag91 = 40621;
            $resources91 = array_filter($array91, function ($var91) use ($flag91) {
                return ($var91['kode_peran'] == $flag91);
            });
            $array92 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag92 = 40622;
            $resources92 = array_filter($array92, function ($var92) use ($flag92) {
                return ($var92['kode_peran'] == $flag92);
            });
            $array93 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag93 = 40623;
            $resources93 = array_filter($array93, function ($var93) use ($flag93) {
                return ($var93['kode_peran'] == $flag93);
            });
            //-----
            $array94 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag94 = 40624;
            $resources94 = array_filter($array94, function ($var94) use ($flag94) {
                return ($var94['kode_peran'] == $flag94);
            });
            $array95 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag95 = 40625;
            $resources95 = array_filter($array95, function ($var95) use ($flag95) {
                return ($var95['kode_peran'] == $flag95);
            });
            $array96 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag96 = 40626;
            $resources96 = array_filter($array96, function ($var96) use ($flag96) {
                return ($var96['kode_peran'] == $flag96);
            });
            $array97 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag97 = 40627;
            $resources97 = array_filter($array97, function ($var97) use ($flag97) {
                return ($var97['kode_peran'] == $flag97);
            });
            $array98 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag98 = 40628;
            $resources98 = array_filter($array98, function ($var98) use ($flag98) {
                return ($var98['kode_peran'] == $flag98);
            });
            $array100 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag100 = 40629;
            $resources100 = array_filter($array100, function ($var100) use ($flag100) {
                return ($var100['kode_peran'] == $flag100);
            });
            $array101 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag101 = 40630;
            $resources101 = array_filter($array101, function ($var101) use ($flag101) {
                return ($var101['kode_peran'] == $flag101);
            });
            $array102 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag102 = 40631;
            $resources102 = array_filter($array102, function ($var102) use ($flag102) {
                return ($var102['kode_peran'] == $flag102);
            });
            $array103 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag103 = 40632;
            $resources103 = array_filter($array103, function ($var103) use ($flag103) {
                return ($var103['kode_peran'] == $flag103);
            });
            $array104 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag104 = 40633;
            $resources104 = array_filter($array104, function ($var104) use ($flag104) {
                return ($var104['kode_peran'] == $flag104);
            });
            $array105 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag105 = 40634;
            $resources105 = array_filter($array105, function ($var105) use ($flag105) {
                return ($var105['kode_peran'] == $flag105);
            });
            $array106 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag106 = 40635;
            $resources106 = array_filter($array106, function ($var106) use ($flag106) {
                return ($var106['kode_peran'] == $flag106);
            });
            $array107 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag107 = 40636;
            $resources107 = array_filter($array107, function ($var107) use ($flag107) {
                return ($var107['kode_peran'] == $flag107);
            });
            $array108 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag108 = 40637;
            $resources108 = array_filter($array108, function ($var108) use ($flag108) {
                return ($var108['kode_peran'] == $flag108);
            });
            $array109 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag109 = 40638;
            $resources109 = array_filter($array109, function ($var109) use ($flag109) {
                return ($var109['kode_peran'] == $flag109);
            });
            $array110 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag110 = 40639;
            $resources110 = array_filter($array110, function ($var110) use ($flag110) {
                return ($var110['kode_peran'] == $flag110);
            });
            //----
            $array111 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag111 = 40640;
            $resources111 = array_filter($array111, function ($var111) use ($flag111) {
                return ($var111['kode_peran'] == $flag111);
            });
            $array112 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag112 = 40641;
            $resources112 = array_filter($array112, function ($var112) use ($flag112) {
                return ($var112['kode_peran'] == $flag112);
            });
            $array113 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
            $flag113 = 40642;
            $resources113 = array_filter($array113, function ($var113) use ($flag113) {
                return ($var113['kode_peran'] == $flag113);
            });

            $resources = array_merge(
                $resources1,$resources2,$resources3,$resources4,$resources5,$resources6,$resources7,$resources8,$resources9,$resources10,
                $resources11,$resources12,$resources13,$resources14,$resources15,$resources16,$resources17,$resources18,$resources19,$resources20,
                $resources21,$resources22,$resources23,$resources24,$resources25,$resources26,$resources27,$resources28,$resources29,$resources30,
                $resources31,$resources32,$resources33,$resources34,$resources35,$resources36,$resources37,$resources38,$resources39,$resources40,
                $resources41,$resources42,$resources43,$resources44,$resources45,$resources46,$resources47,$resources48,$resources49,$resources50,
                $resources51,$resources52,$resources53,$resources54,$resources55,$resources56,$resources57,$resources58,$resources59,$resources60,
                $resources61,$resources62,$resources63,$resources64,$resources65,$resources66,$resources67,$resources68,$resources69,$resources70,
                $resources71,$resources72,$resources73,$resources74,$resources75,$resources76,$resources77,$resources78,$resources79,$resources80,
                $resources81,$resources82,$resources83,$resources84,$resources85,$resources86,$resources87,$resources88,$resources89,$resources90,
                $resources91,$resources92,$resources93,$resources94,$resources95,$resources96,$resources97,$resources98,$resources99,$resources100,
                $resources10,$resources102,$resources103,$resources104,$resources105,$resources106,$resources107,$resources108,$resources109,$resources110,
                $resources111,$resources112,$resources113
            );
            elseif($key == 'penunjang'):
                $array1 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag1 = 50101;
                $resources1 = array_filter($array1, function ($var1) use ($flag1) {
                    return ($var1['kode_peran'] == $flag1);
                });
                $array2 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag2 = 50102;
                $resources2 = array_filter($array2, function ($var2) use ($flag2) {
                    return ($var2['kode_peran'] == $flag2);
                });
                $array3 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag3 = 50103;
                $resources3 = array_filter($array3, function ($var3) use ($flag3) {
                    return ($var3['kode_peran'] == $flag3);
                });
                $array4 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag4 = 50104;
                $resources4 = array_filter($array4, function ($var4) use ($flag4) {
                    return ($var4['kode_peran'] == $flag4);
                });
                $array5 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag5 = 50105;
                $resources5 = array_filter($array5, function ($var5) use ($flag5) {
                    return ($var5['kode_peran'] == $flag5);
                });
                $array6 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag6 = 50106;
                $resources6 = array_filter($array6, function ($var6) use ($flag6) {
                    return ($var6['kode_peran'] == $flag6);
                });
                $array7 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag7 = 50107;
                $resources7 = array_filter($array7, function ($var7) use ($flag7) {
                    return ($var7['kode_peran'] == $flag7);
                });
                $array8 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag8 = 50108;
                $resources8 = array_filter($array8, function ($var8) use ($flag8) {
                    return ($var8['kode_peran'] == $flag8);
                });
                $array9 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag9 = 50201;
                $resources9 = array_filter($array9, function ($var9) use ($flag9) {
                    return ($var9['kode_peran'] == $flag9);
                });
                $array10 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag10 = 50202;
                $resources10 = array_filter($array10, function ($var10) use ($flag10) {
                    return ($var10['kode_peran'] == $flag10);
                });
                $array11 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag11 = 50203;
                $resources11 = array_filter($array11, function ($var11) use ($flag11) {
                    return ($var11['kode_peran'] == $flag11);
                });
                $array12 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag12 = 50301;
                $resources12 = array_filter($array12, function ($var12) use ($flag12) {
                    return ($var12['kode_peran'] == $flag12);
                });
                $array13 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag13 = 50302;
                $resources13 = array_filter($array13, function ($var13) use ($flag13) {
                    return ($var13['kode_peran'] == $flag13);
                });
                $array14 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag14 = 50401;
                $resources14 = array_filter($array14, function ($var14) use ($flag14) {
                    return ($var14['kode_peran'] == $flag14);
                });
                $array15 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag15 = 50402;
                $resources15 = array_filter($array15, function ($var15) use ($flag15) {
                    return ($var15['kode_peran'] == $flag15);
                });
                $array16 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag16 = 50501;
                $resources16 = array_filter($array16, function ($var16) use ($flag16) {
                    return ($var16['kode_peran'] == $flag16);
                });
                $array17 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag17 = 50502;
                $resources17 = array_filter($array17, function ($var17) use ($flag17) {
                    return ($var17['kode_peran'] == $flag17);
                });
                $array18 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag18 = 50503;
                $resources18 = array_filter($array18, function ($var18) use ($flag18) {
                    return ($var18['kode_peran'] == $flag18);
                });
                $array19 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag19 = 50504;
                $resources19 = array_filter($array19, function ($var19) use ($flag19) {
                    return ($var19['kode_peran'] == $flag19);
                });
                $array20 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag20 = 50505;
                $resources20 = array_filter($array20, function ($var20) use ($flag20) {
                    return ($var20['kode_peran'] == $flag20);
                });
                $array21 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag21 = 50506;
                $resources21 = array_filter($array21, function ($var21) use ($flag21) {
                    return ($var21['kode_peran'] == $flag21);
                });
                $array22 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag22 = 50507;
                $resources22 = array_filter($array22, function ($var22) use ($flag22) {
                    return ($var22['kode_peran'] == $flag22);
                });
                //-----
                $array23 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag23 = 50508;
                $resources23 = array_filter($array23, function ($var23) use ($flag23) {
                    return ($var23['kode_peran'] == $flag23);
                });
                $array24 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag24 = 50509;
                $resources24 = array_filter($array24, function ($var24) use ($flag24) {
                    return ($var24['kode_peran'] == $flag24);
                });
                $array25 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag25 = 50510;
                $resources25 = array_filter($array25, function ($var25) use ($flag25) {
                    return ($var25['kode_peran'] == $flag25);
                });
                $array26 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag26 = 50511;
                $resources26 = array_filter($array26, function ($var26) use ($flag26) {
                    return ($var26['kode_peran'] == $flag26);
                });
                $array27 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag27 = 50512;
                $resources27 = array_filter($array27, function ($var27) use ($flag27) {
                    return ($var27['kode_peran'] == $flag27);
                });
                $array28 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag28 = 50513;
                $resources28 = array_filter($array28, function ($var28) use ($flag28) {
                    return ($var28['kode_peran'] == $flag28);
                });
                $array29 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag29 = 50514;
                $resources29 = array_filter($array29, function ($var29) use ($flag29) {
                    return ($var29['kode_peran'] == $flag29);
                });
                $array30 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag30 = 50515;
                $resources30 = array_filter($array30, function ($var30) use ($flag30) {
                    return ($var30['kode_peran'] == $flag30);
                });
                $array31 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag31 = 50516;
                $resources31 = array_filter($array31, function ($var31) use ($flag31) {
                    return ($var31['kode_peran'] == $flag31);
                });
                $array32 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag32 = 50601;
                $resources32 = array_filter($array32, function ($var32) use ($flag32) {
                    return ($var32['kode_peran'] == $flag32);
                });
                $array33 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag33 = 50602;
                $resources33 = array_filter($array33, function ($var33) use ($flag33) {
                    return ($var33['kode_peran'] == $flag33);
                });
                //-------
                $array34 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag34 = 50701;
                $resources34 = array_filter($array34, function ($var34) use ($flag34) {
                    return ($var34['kode_peran'] == $flag34);
                });
                $array35 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag35 = 50702;
                $resources35 = array_filter($array35, function ($var35) use ($flag35) {
                    return ($var35['kode_peran'] == $flag35);
                });
                $array36 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag36 = 50703;
                $resources36 = array_filter($array36, function ($var36) use ($flag36) {
                    return ($var36['kode_peran'] == $flag36);
                });
                $array37 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag37 = 50704;
                $resources37 = array_filter($array37, function ($var37) use ($flag37) {
                    return ($var37['kode_peran'] == $flag37);
                });
                $array38 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag38 = 50705;
                $resources38 = array_filter($array38, function ($var38) use ($flag38) {
                    return ($var38['kode_peran'] == $flag38);
                });
                $array39 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag39 = 50706;
                $resources39 = array_filter($array39, function ($var39) use ($flag39) {
                    return ($var39['kode_peran'] == $flag39);
                });
                $array40 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag40 = 50707;
                $resources40 = array_filter($array40, function ($var40) use ($flag40) {
                    return ($var40['kode_peran'] == $flag40);
                });
                $array41 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag41 = 50708;
                $resources41 = array_filter($array41, function ($var41) use ($flag41) {
                    return ($var41['kode_peran'] == $flag41);
                });
                $array42 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag42 = 50709;
                $resources42 = array_filter($array42, function ($var42) use ($flag42) {
                    return ($var42['kode_peran'] == $flag42);
                });
                $array43 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag43 = 50710;
                $resources43 = array_filter($array43, function ($var43) use ($flag43) {
                    return ($var43['kode_peran'] == $flag43);
                });
                $array44 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag44 = 50711;
                $resources44 = array_filter($array44, function ($var44) use ($flag44) {
                    return ($var44['kode_peran'] == $flag44);
                });
                $array45 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag45 = 50712;
                $resources45 = array_filter($array45, function ($var45) use ($flag45) {
                    return ($var45['kode_peran'] == $flag45);
                });
                $array46 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag46 = 50713;
                $resources46 = array_filter($array46, function ($var46) use ($flag46) {
                    return ($var46['kode_peran'] == $flag46);
                });
                $array47 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag47 = 50714;
                $resources47 = array_filter($array47, function ($var47) use ($flag47) {
                    return ($var47['kode_peran'] == $flag47);
                });
                $array48 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag48 = 50801;
                $resources48 = array_filter($array48, function ($var48) use ($flag48) {
                    return ($var48['kode_peran'] == $flag48);
                });
                $array49 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag49 = 50802;
                $resources49 = array_filter($array49, function ($var49) use ($flag49) {
                    return ($var49['kode_peran'] == $flag49);
                });
                $array50 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag50 = 50803;
                $resources50 = array_filter($array50, function ($var50) use ($flag50) {
                    return ($var50['kode_peran'] == $flag50);
                });
                //----
                $array51 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag51 = 50804;
                $resources51 = array_filter($array51, function ($var51) use ($flag51) {
                    return ($var51['kode_peran'] == $flag51);
                });
                $array52 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag52 = 50805;
                $resources52 = array_filter($array52, function ($var52) use ($flag52) {
                    return ($var52['kode_peran'] == $flag52);
                });
                $array53 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag53 = 50806;
                $resources53 = array_filter($array53, function ($var53) use ($flag53) {
                    return ($var53['kode_peran'] == $flag53);
                });
                $array54 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag54 = 50901;
                $resources54 = array_filter($array54, function ($var54) use ($flag54) {
                    return ($var54['kode_peran'] == $flag54);
                });
                $array55 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag55 = 50902;
                $resources55 = array_filter($array55, function ($var55) use ($flag55) {
                    return ($var55['kode_peran'] == $flag55);
                });
                $array56 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag56 = 50903;
                $resources56 = array_filter($array56, function ($var56) use ($flag56) {
                    return ($var56['kode_peran'] == $flag56);
                });
                $array57 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag57 = 50904;
                $resources57 = array_filter($array57, function ($var57) use ($flag57) {
                    return ($var57['kode_peran'] == $flag57);
                });
                $array58 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag58 = 51001;
                $resources58 = array_filter($array58, function ($var58) use ($flag58) {
                    return ($var58['kode_peran'] == $flag58);
                });
                $array59 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag59 = 51002;
                $resources59 = array_filter($array59, function ($var59) use ($flag59) {
                    return ($var59['kode_peran'] == $flag59);
                });
                $array60 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag60 = 51003;
                $resources60 = array_filter($array60, function ($var60) use ($flag60) {
                    return ($var60['kode_peran'] == $flag60);
                });
                $array61 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag61 = 51004;
                $resources61 = array_filter($array61, function ($var61) use ($flag61) {
                    return ($var61['kode_peran'] == $flag61);
                });
                $array62 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag62 = 51101;
                $resources62 = array_filter($array62, function ($var62) use ($flag62) {
                    return ($var62['kode_peran'] == $flag62);
                });
                $array63 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag63 = 51102;
                $resources63 = array_filter($array63, function ($var63) use ($flag63) {
                    return ($var63['kode_peran'] == $flag63);
                });
                $array64 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag64 = 51103;
                $resources64 = array_filter($array64, function ($var64) use ($flag64) {
                    return ($var64['kode_peran'] == $flag64);
                });
                $array65 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag65 = 51104;
                $resources65 = array_filter($array65, function ($var65) use ($flag65) {
                    return ($var65['kode_peran'] == $flag65);
                });
                $array66 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag66 = 51201;
                $resources66 = array_filter($array66, function ($var66) use ($flag66) {
                    return ($var66['kode_peran'] == $flag66);
                });
                $array67 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag67 = 51301;
                $resources67 = array_filter($array67, function ($var67) use ($flag67) {
                    return ($var67['kode_peran'] == $flag67);
                });
                //-------------
                $array68 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag68 = 51302;
                $resources68 = array_filter($array68, function ($var68) use ($flag68) {
                    return ($var68['kode_peran'] == $flag68);
                });
                $array69 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag69 = 51303;
                $resources69 = array_filter($array69, function ($var69) use ($flag69) {
                    return ($var69['kode_peran'] == $flag69);
                });
                $array70 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag70 = 51304;
                $resources70 = array_filter($array70, function ($var70) use ($flag70) {
                    return ($var70['kode_peran'] == $flag70);
                });
                $array71 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag71 = 51305;
                $resources71 = array_filter($array71, function ($var71) use ($flag71) {
                    return ($var71['kode_peran'] == $flag71);
                });
                $array72 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag72 = 51306;
                $resources72 = array_filter($array72, function ($var72) use ($flag72) {
                    return ($var72['kode_peran'] == $flag72);
                });
                $array73 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag73 = 51307;
                $resources73 = array_filter($array73, function ($var73) use ($flag73) {
                    return ($var73['kode_peran'] == $flag73);
                });
                $array74 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag74 = 51308;
                $resources74 = array_filter($array74, function ($var74) use ($flag74) {
                    return ($var74['kode_peran'] == $flag74);
                });
                $array75 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag75 = 51309;
                $resources75 = array_filter($array75, function ($var75) use ($flag75) {
                    return ($var75['kode_peran'] == $flag75);
                });
                $array76 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag76 = 51310;
                $resources76 = array_filter($array76, function ($var76) use ($flag76) {
                    return ($var76['kode_peran'] == $flag76);
                });
                $array77 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag77 = 51311;
                $resources77 = array_filter($array77, function ($var77) use ($flag77) {
                    return ($var77['kode_peran'] == $flag77);
                });
                $array78 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag78 = 51312;
                $resources78 = array_filter($array78, function ($var78) use ($flag78) {
                    return ($var78['kode_peran'] == $flag78);
                });
                $array79 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag79 = 51313;
                $resources79 = array_filter($array79, function ($var79) use ($flag79) {
                    return ($var79['kode_peran'] == $flag79);
                });
                $array80 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag80 = 51314;
                $resources80 = array_filter($array80, function ($var80) use ($flag80) {
                    return ($var80['kode_peran'] == $flag80);
                });
                $array81 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag81 = 51401;
                $resources81 = array_filter($array81, function ($var81) use ($flag81) {
                    return ($var81['kode_peran'] == $flag81);
                });
                $array82 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag82 = 51402;
                $resources82 = array_filter($array82, function ($var82) use ($flag82) {
                    return ($var82['kode_peran'] == $flag82);
                });
                $array83 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag83 = 51403;
                $resources83 = array_filter($array83, function ($var83) use ($flag83) {
                    return ($var83['kode_peran'] == $flag83);
                });
                $array84 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag84 = 51404;
                $resources84 = array_filter($array84, function ($var84) use ($flag84) {
                    return ($var84['kode_peran'] == $flag84);
                });
                $array85 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag85 = 51501;
                $resources85 = array_filter($array85, function ($var85) use ($flag85) {
                    return ($var85['kode_peran'] == $flag85);
                });
                $array86 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag86 = 51502;
                $resources86 = array_filter($array86, function ($var86) use ($flag86) {
                    return ($var86['kode_peran'] == $flag86);
                });
                $array87 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag87 = 51601;
                $resources87 = array_filter($array87, function ($var87) use ($flag87) {
                    return ($var87['kode_peran'] == $flag87);
                });
                $array88 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag88 = 51602;
                $resources88 = array_filter($array88, function ($var88) use ($flag88) {
                    return ($var88['kode_peran'] == $flag88);
                });
                $array89 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag89 = 51603;
                $resources89 = array_filter($array89, function ($var89) use ($flag89) {
                    return ($var89['kode_peran'] == $flag89);
                });
                $array90 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag90 = 51604;
                $resources90 = array_filter($array90, function ($var90) use ($flag90) {
                    return ($var90['kode_peran'] == $flag90);
                });
                $array91 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag91 = 51605;
                $resources91 = array_filter($array91, function ($var91) use ($flag91) {
                    return ($var91['kode_peran'] == $flag91);
                });
                $array92 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag92 = 51606;
                $resources92 = array_filter($array92, function ($var92) use ($flag92) {
                    return ($var92['kode_peran'] == $flag92);
                });
                $array93 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag93 = 51701;
                $resources93 = array_filter($array93, function ($var93) use ($flag93) {
                    return ($var93['kode_peran'] == $flag93);
                });
                //-----
                $array94 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag94 = 51702;
                $resources94 = array_filter($array94, function ($var94) use ($flag94) {
                    return ($var94['kode_peran'] == $flag94);
                });
                $array95 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag95 = 51801;
                $resources95 = array_filter($array95, function ($var95) use ($flag95) {
                    return ($var95['kode_peran'] == $flag95);
                });
                $array96 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag96 = 51802;
                $resources96 = array_filter($array96, function ($var96) use ($flag96) {
                    return ($var96['kode_peran'] == $flag96);
                });
                $array97 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag97 = 51803;
                $resources97 = array_filter($array97, function ($var97) use ($flag97) {
                    return ($var97['kode_peran'] == $flag97);
                });
                $array98 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag98 = 51804;
                $resources98 = array_filter($array98, function ($var98) use ($flag98) {
                    return ($var98['kode_peran'] == $flag98);
                });
                $array100 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag100 = 51901;
                $resources100 = array_filter($array100, function ($var100) use ($flag100) {
                    return ($var100['kode_peran'] == $flag100);
                });
                $array101 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag101 = 51902;
                $resources101 = array_filter($array101, function ($var101) use ($flag101) {
                    return ($var101['kode_peran'] == $flag101);
                });
                $array102 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag102 = 51903;
                $resources102 = array_filter($array102, function ($var102) use ($flag102) {
                    return ($var102['kode_peran'] == $flag102);
                });
                $array103 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag103 = 51904;
                $resources103 = array_filter($array103, function ($var103) use ($flag103) {
                    return ($var103['kode_peran'] == $flag103);
                });
                $array104 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag104 = 51905;
                $resources104 = array_filter($array104, function ($var104) use ($flag104) {
                    return ($var104['kode_peran'] == $flag104);
                });
                $array105 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag105 = 51906;
                $resources105 = array_filter($array105, function ($var105) use ($flag105) {
                    return ($var105['kode_peran'] == $flag105);
                });
                $array106 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag106 = 52001;
                $resources106 = array_filter($array106, function ($var106) use ($flag106) {
                    return ($var106['kode_peran'] == $flag106);
                });
                $array107 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag107 = 52002;
                $resources107 = array_filter($array107, function ($var107) use ($flag107) {
                    return ($var107['kode_peran'] == $flag107);
                });
                $array108 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag108 = 52003;
                $resources108 = array_filter($array108, function ($var108) use ($flag108) {
                    return ($var108['kode_peran'] == $flag108);
                });
                $array109 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag109 = 52004;
                $resources109 = array_filter($array109, function ($var109) use ($flag109) {
                    return ($var109['kode_peran'] == $flag109);
                });
                $array110 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag110 = 52005;
                $resources110 = array_filter($array110, function ($var110) use ($flag110) {
                    return ($var110['kode_peran'] == $flag110);
                });
                //----
                $array111 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag111 = 52006;
                $resources111 = array_filter($array111, function ($var111) use ($flag111) {
                    return ($var111['kode_peran'] == $flag111);
                });
                $array112 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag112 = 52007;
                $resources112 = array_filter($array112, function ($var112) use ($flag112) {
                    return ($var112['kode_peran'] == $flag112);
                });
                $array113 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag113 = 52008;
                $resources113 = array_filter($array113, function ($var113) use ($flag113) {
                    return ($var113['kode_peran'] == $flag113);
                });
                //----------
                $array114 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag114 = 52101;
                $resources114 = array_filter($array114, function ($var114) use ($flag114) {
                    return ($var114['kode_peran'] == $flag114);
                });
                $array115 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag115 = 52102;
                $resources115 = array_filter($array115, function ($var115) use ($flag115) {
                    return ($var115['kode_peran'] == $flag115);
                });
                $array116 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag116 = 52103;
                $resources116 = array_filter($array116, function ($var116) use ($flag116) {
                    return ($var116['kode_peran'] == $flag116);
                });
                $array117 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag117 = 52104;
                $resources117 = array_filter($array117, function ($var117) use ($flag117) {
                    return ($var117['kode_peran'] == $flag117);
                });
                $array118 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag118 = 52201;
                $resources118 = array_filter($array118, function ($var118) use ($flag118) {
                    return ($var118['kode_peran'] == $flag118);
                });
                $array119 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag119 = 52202;
                $resources119 = array_filter($array119, function ($var119) use ($flag119) {
                    return ($var119['kode_peran'] == $flag119);
                });
                $array120 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag120 = 52203;
                $resources120 = array_filter($array120, function ($var120) use ($flag120) {
                    return ($var120['kode_peran'] == $flag120);
                });
                $array121 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag121 = 52204;
                $resources121 = array_filter($array121, function ($var121) use ($flag121) {
                    return ($var121['kode_peran'] == $flag121);
                });
                $array122 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag122 = 52205;
                $resources122 = array_filter($array122, function ($var122) use ($flag122) {
                    return ($var122['kode_peran'] == $flag122);
                });
                $array123 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag123 = 52206;
                $resources123 = array_filter($array123, function ($var123) use ($flag123) {
                    return ($var123['kode_peran'] == $flag123);
                });
                $array124 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag124 = 52301;
                $resources124 = array_filter($array124, function ($var124) use ($flag124) {
                    return ($var124['kode_peran'] == $flag124);
                });
                $array125 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag125 = 52302;
                $resources125 = array_filter($array125, function ($var125) use ($flag125) {
                    return ($var125['kode_peran'] == $flag125);
                });

                $array126 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag126 = 52401;
                $resources126 = array_filter($array126, function ($var126) use ($flag126) {
                    return ($var126['kode_peran'] == $flag126);
                });
                $array127 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag127 = 52402;
                $resources127 = array_filter($array127, function ($var127) use ($flag127) {
                    return ($var127['kode_peran'] == $flag127);
                });
                $array128 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag128 = 52403;
                $resources128 = array_filter($array128, function ($var128) use ($flag128) {
                    return ($var128['kode_peran'] == $flag128);
                });
                $array129 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag129 = 52501;
                $resources129 = array_filter($array129, function ($var129) use ($flag129) {
                    return ($var129['kode_peran'] == $flag129);
                });
                $array130 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag130 = 52502;
                $resources130 = array_filter($array130, function ($var130) use ($flag130) {
                    return ($var130['kode_peran'] == $flag130);
                });
                $array131 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag131 = 52503;
                $resources131 = array_filter($array131, function ($var131) use ($flag131) {
                    return ($var131['kode_peran'] == $flag131);
                });
                $array132 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag132 = 52601;
                $resources132 = array_filter($array132, function ($var132) use ($flag132) {
                    return ($var132['kode_peran'] == $flag132);
                });
                $array133 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag133 = 52602;
                $resources133 = array_filter($array133, function ($var133) use ($flag133) {
                    return ($var133['kode_peran'] == $flag133);
                });
                $array134 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag134 = 52603;
                $resources134 = array_filter($array134, function ($var134) use ($flag134) {
                    return ($var134['kode_peran'] == $flag134);
                });
                $array135 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag135 = 52701;
                $resources135 = array_filter($array135, function ($var135) use ($flag135) {
                    return ($var135['kode_peran'] == $flag135);
                });
                $array136 = json_decode(file_get_contents($json,false, stream_context_create($arrContextOptions)), true);
                $flag136 = 52702;
                $resources136 = array_filter($array136, function ($var136) use ($flag136) {
                    return ($var136['kode_peran'] == $flag136);
                });


                $resources = array_merge(
                    $resources1,$resources2,$resources3,$resources4,$resources5,$resources6,$resources7,$resources8,$resources9,$resources10,
                    $resources11,$resources12,$resources13,$resources14,$resources15,$resources16,$resources17,$resources18,$resources19,$resources20,
                    $resources21,$resources22,$resources23,$resources24,$resources25,$resources26,$resources27,$resources28,$resources29,$resources30,
                    $resources31,$resources32,$resources33,$resources34,$resources35,$resources36,$resources37,$resources38,$resources39,$resources40,
                    $resources41,$resources42,$resources43,$resources44,$resources45,$resources46,$resources47,$resources48,$resources49,$resources50,
                    $resources51,$resources52,$resources53,$resources54,$resources55,$resources56,$resources57,$resources58,$resources59,$resources60,
                    $resources61,$resources62,$resources63,$resources64,$resources65,$resources66,$resources67,$resources68,$resources69,$resources70,
                    $resources71,$resources72,$resources73,$resources74,$resources75,$resources76,$resources77,$resources78,$resources79,$resources80,
                    $resources81,$resources82,$resources83,$resources84,$resources85,$resources86,$resources87,$resources88,$resources89,$resources90,
                    $resources91,$resources92,$resources93,$resources94,$resources95,$resources96,$resources97,$resources98,$resources100,
                    $resources100,$resources102,$resources103,$resources104,$resources105,$resources106,$resources107,$resources108,$resources109,$resources110,
                    $resources111,$resources112,$resources113,$resources114,$resources115,$resources116,$resources117,$resources118,$resources119,$resources120,
                    $resources121,$resources122,$resources123,$resources124,$resources125,$resources126,$resources127,$resources128,$resources129,$resources130,
                    $resources131,$resources132,$resources133,$resources134,$resources135,$resources136
                );
            endif;
            return $resources;
        }

        public function riwayatPegawai(){
            $data1 = $this->db->query("SELECT uuid, kode_peg, nip, nama, id_jft as id_jabatan, jenis_jft as jabatan, grade, score, no_sk, cast('TENDIK' as char) as jenis_pegawai
            FROM `simpeg_riwayat_jft_pegawai` 
            where 
            tmt_sk IN (SELECT MAX(tmt_sk) FROM simpeg_riwayat_jft_pegawai GROUP BY nip )
            AND status = 'Y'")->result();

            $data2 = $this->db->query("SELECT uuid, kode_peg, nip, nama, id_jabumum as id_jabatan, jenis_jabumum as jabatan, grade, score, no_sk, cast('TENDIK' as char) as jenis_pegawai
            FROM `simpeg_riwayat_jfu_pegawai` 
            where 
            tmt_sk IN (SELECT MAX(tmt_sk) FROM simpeg_riwayat_jfu_pegawai GROUP BY nip )
            AND status = 'Y'")->result();

            $data3 = $this->db->query("SELECT uuid, kode_peg, nip, nama, id_jab_struktural as id_jabatan, nama_jabatan_struktural as jabatan, grade, score, no_sk, cast('TENDIK' as char) as jenis_pegawai
            FROM `simpeg_riwayat_struktural_pegawai` 
            where 
            tmt_sk IN (SELECT MAX(tmt_sk) FROM simpeg_riwayat_struktural_pegawai GROUP BY nip )
            AND status = 'Y'")->result();

            return array_merge($data1,$data2,$data3);
        }

        public function riwayatDosenByNip($nip){
            $data1 = $this->db->select("nip, nama, nama_tugastambahan as jabatan, tgl_sk, tmt_sk")
                            ->get_where('simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns',['nip'=>$nip])
                            ->result();
            $data2 = $this->db->select("nip, nama, nama_jft as jabatan, tgl_sk, tmt_sk")
                            ->get_where('simpeg_riwayat_jabatan_fungsional',['nip'=>$nip])
                            ->result();
            $data = array_merge($data1,$data2);
            return $data;
        }

        public function riwayatPegawaiByNip($nip){
            $data1 = $this->db->query("SELECT uuid, kode_peg, nip, nama, id_jft as id_jabatan, jenis_jft as jabatan, grade, score, no_sk, tmt_sk,
            cast('TENDIK' as char) as jenis_pegawai, cast('JFT' as char) as jenis, status
            FROM `simpeg_riwayat_jft_pegawai` 
            where nip = '$nip'")->result();

            $data2 = $this->db->query("SELECT uuid, kode_peg, nip, nama, id_jabumum as id_jabatan, jenis_jabumum as jabatan, grade, score, no_sk, tmt_sk,
            cast('TENDIK' as char) as jenis_pegawai, cast('JFU' as char) as jenis, status
            FROM `simpeg_riwayat_jfu_pegawai` 
            where nip = '$nip'")->result();

            $data3 = $this->db->query("SELECT uuid, kode_peg, nip, nama, id_jab_struktural as id_jabatan, nama_jabatan_struktural as jabatan, grade, score, no_sk, tmt_sk,
            cast('TENDIK' as char) as jenis_pegawai, cast('STRUKTURAL' as char) as jenis, status
            FROM `simpeg_riwayat_struktural_pegawai` 
            where nip = '$nip'")->result();
            $data = array_merge($data1,$data2,$data3);
            return $data;
        }

        public function riwayatDosenByNipLast($nip){
            return $this->db->select("nama_jft as jabatan")
                            ->order_by('tmt_sk','desc')
                            ->get_where('simpeg_riwayat_jabatan_fungsional',['nip'=>$nip])
                            ->row();
        }

        public function cekKode($kode){
          //return "PEG0557";
          $arrContextOptions=array(
                  "ssl"=>array(
                      "verify_peer"=>false,
                      "verify_peer_name"=>false,
                  ),
              );
          $api_pns = file_get_contents('https://dev.unimed.ac.id/sso-api/simremlinkjwt/pegawai',false, stream_context_create($arrContextOptions));
          $json_pns = json_decode($api_pns);
          foreach($json_pns as $key => $item_pns){
            if($kode == $item_pns->kode_dosen){
              return $item_pns->nip;
            }
            
          }
        }

        public function testing_riwayat_dt_dosen(){
          $nip = trim($this->input->get('nip','true')); 
          $data = $this->db->query("SELECT * FROM `simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns` WHERE nip = '$nip' ORDER BY `simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns`.`tgl_sk` DESC LIMIT 1");
          return $data;
        }

        public function testing_riwayat_dt_by_tgl_dosen(){
          $nip = trim($this->input->get('nip','true'));
          $thn = '2020';
          $tgl_awal = $thn.'-01-01';
          $tgl_akhir = $thn.'-12-31';
          
          //$data = $this->db->query("SELECT * FROM `simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns` WHERE nip = '$nip' and simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns.status_jabatan_utama = 'Y' and simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns.status_aktif = 'Y' HAVING tgl_sk BETWEEN '$tgl_awal' AND '$tgl_akhir' ORDER BY `simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns`.`tgl_sk` DESC");
          $data = $this->db->select("
                            kode_dosen,
                            nip,
                            kode_tugastambahan,
                            nama_tugastambahan,
                            tmt_sk,
                            grade_tugastambahan,
                            score_tugastambahan,
                            tgl_nonaktif,
                            tgl_nonaktif_2,
                            durasi,
                            hari,
                            round(hari/30) AS fix_durasi")
                            ->order_by('tmt_sk','asc')
                            ->get_where('transaksi_v_riwayat_jabatan_tugas_tambahan_dosen_2020',['nip'=>$nip, 'status_aktif' => 'Y', 'status_jabatan_utama' => 'Y'])
                            ->result();
            $output = [];
          foreach ($data as $key => $value) {
              if($value->tgl_nonaktif > '2020-12-31' || $value->tgl_nonaktif == '0000-00-00'):
                    $tgl_nonaktif = date_create('2020-12-31');
                    $tmt_sk = date_create($value->tmt_sk);
                    $selisih = $tgl_nonaktif->diff($tmt_sk)->days + 1;
                    $durasi = round($selisih/30);
              else:
                $durasi = str_replace('-','',$value->fix_durasi);
              endif;
              if($durasi > 6):
                $durasi = 6;
              else:
                $durasi = $durasi;
              endif;
              $set = [
                'kode_dosen' => $value->kode_dosen,
                'nip' => $value->nip,
                'kode_tugastambahan' => $value->kode_tugastambahan,
                'nama_tugastambahan' => $value->nama_tugastambahan,
                'tmt_sk' => $value->tmt_sk,
                'grade_tugastambahan' => $value->grade_tugastambahan,
                'score_tugastambahan' => $value->score_tugastambahan,
                'tgl_nonaktif' => $value->tgl_nonaktif,
                'tgl_nonaktif_2' => $value->tgl_nonaktif_2,
                'durasi' => $durasi
                ];
              array_push($output,$set);
          }
          return $output;

        }

        public function testing_riwayat_dt_pegawai(){
          $nip = trim($this->input->get('nip','true')); 
          $data = $this->db->query("SELECT * FROM `simpeg_riwayat_jabatan_remun_pegawai_pns` WHERE nip = '$nip' ORDER BY `simpeg_riwayat_jabatan_remun_pegawai_pns`.`tgl_sk` DESC LIMIT 1");
          return $data;
        }

        public function testing_riwayat_dt_by_tgl_pegawai(){
          $nip = trim($this->input->get('nip','true'));
          $thn = '2020';
          $tgl_awal = $thn.'-01-01';
          $tgl_akhir = $thn.'-12-31';
          
          $data = $this->db->query("SELECT * FROM `simpeg_riwayat_jabatan_remun_pegawai_pns` WHERE nip = '$nip' HAVING tgl_sk BETWEEN '$tgl_awal' AND '$tgl_akhir' ORDER BY `simpeg_riwayat_jabatan_remun_pegawai_pns`.`tgl_sk` DESC");
          return $data;
        }

        public function testing_riwayat(){
          $nip = trim($this->input->get('nip','true'));
          $kode = substr(trim($this->input->get('kode','true')),0,3);
          if($kode == 'DOS'):
            $data = $this->db->query("SELECT * FROM `simpeg_riwayat_jabatan_fungsional` WHERE nip = '$nip' ORDER BY `simpeg_riwayat_jabatan_fungsional`.`tgl_sk` DESC LIMIT 1");
          elseif($kode == 'PEG'):
            $dataJft = $this->db->query("SELECT *, jenis_jft as nama_jft
            FROM `simpeg_riwayat_jft_pegawai` 
            where tmt_sk IN (SELECT MAX(tmt_sk) FROM simpeg_riwayat_jft_pegawai GROUP BY nip )
            AND status = 'Y' AND nip ='$nip'");

            $dataJfu = $this->db->query("SELECT *, jenis_jabumum as nama_jft
            FROM `simpeg_riwayat_jfu_pegawai` 
            where tmt_sk IN (SELECT MAX(tmt_sk) FROM simpeg_riwayat_jfu_pegawai GROUP BY nip )
            AND status = 'Y'AND nip ='$nip'");

            $dataStruktural = $this->db->query("SELECT *, nama_jabatan_struktural as nama_jft
            FROM `simpeg_riwayat_struktural_pegawai` 
            where tmt_sk IN (SELECT MAX(tmt_sk) FROM simpeg_riwayat_struktural_pegawai GROUP BY nip )
            AND status = 'Y'AND nip ='$nip'");

            if($dataJfu->num_rows() > 0):
                $data = $dataJfu;
            elseif($dataJft->num_rows() > 0):
                $data = $dataJft;
            elseif($dataStruktural->num_rows() > 0):
                $data = $dataStruktural;
            endif;
          endif;
          return $data;
        }

        public function biodata_pegawai_by_nip(){
          $nip = trim($this->input->get('nip','true'));
          $data = $this->db->query("SELECT * FROM simpeg_pegawai_pns_simremlink WHERE nip = '$nip' ");
          return $data;
        }

        public function biodata_dosen_by_nip(){
          $nip = trim($this->input->get('nip','true'));
          $data = $this->db->query("SELECT * FROM simpeg_dosen_pns_simremlink WHERE nip = '$nip' ");
          return $data;
        }

        public function harga_dt_2($nip, $periode){
          header('Content-Type: application/json');
          $query_jabatan = $this->db->query("SELECT * FROM simpeg_dosen_pns_simremlink JOIN simpeg_jenis_tugas_tambahan_dosen ON (simpeg_dosen_pns_simremlink.id_tugastambahan=simpeg_jenis_tugas_tambahan_dosen.kode_tugastambahan) WHERE simpeg_dosen_pns_simremlink.nip = '$nip' ");
          $query_periode = $this->db->query("SELECT * FROM simremlink_data_periode WHERE periode = '$periode' "); 
          $data_jabatan = $query_jabatan->row_array();
          $data_periode = $query_periode->row_array();
          $harga_dt = ($data_jabatan['score_tugastambahan']*1*$data_periode['pir']*6)/40;
          return $harga_dt;

        }

        public function harga_db_2($nip, $periode, $peg){
            header('Content-Type: application/json');
          $query_periode = $this->db->query("SELECT * FROM simremlink_data_periode WHERE periode = '$periode' "); 
          $data_periode = $query_periode->row_array();
          if($peg == 'PEG'){
            $dataJft = $this->db->query("SELECT *, jenis_jft as nama_jft
            FROM `simpeg_riwayat_jft_pegawai` 
            where tmt_sk IN (SELECT MAX(tmt_sk) FROM simpeg_riwayat_jft_pegawai GROUP BY nip )
            AND status = 'Y' AND nip ='$nip'");

            $dataJfu = $this->db->query("SELECT *, jenis_jabumum as nama_jft
            FROM `simpeg_riwayat_jfu_pegawai` 
            where tmt_sk IN (SELECT MAX(tmt_sk) FROM simpeg_riwayat_jfu_pegawai GROUP BY nip )
            AND status = 'Y'AND nip ='$nip'");

            $dataStruktural = $this->db->query("SELECT *, nama_jabatan_struktural as nama_jft
            FROM `simpeg_riwayat_struktural_pegawai` 
            where tmt_sk IN (SELECT MAX(tmt_sk) FROM simpeg_riwayat_struktural_pegawai GROUP BY nip )
            AND status = 'Y'AND nip ='$nip'");
            
            if($dataJfu->num_rows() > 0):
                $data = $dataJfu->row();
                $score = $data->score;
            elseif($dataJft->num_rows() > 0):
                $data = $dataJft->row();
                $score = $data->score;
            elseif($dataStruktural->num_rows() > 0):
                $data = $dataStruktural->row();
                $score = $data->score;
            else:
                $score = 0;
            endif;
            $harga_db = ($score*1*$data_periode['pir']*6)/40;
          }else{
            $checkTransaksi = $this->db->select('*')
                                            ->get_where("transaksi_v_riwayat_jabatan_fungsional_dosen_2020",['nip'=>$nip]);
                if($checkTransaksi->num_rows() > 0):
                    $limit = $checkTransaksi->num_rows() + 1;
                    $result = $this->db->limit($limit)
                                    ->order_by('tmt_sk','desc')
                                    ->get_where("simpeg_riwayat_jabatan_fungsional",['nip'=>$nip]);
                    $getRowOne = $this->db->select('nip,hari_1,hari_2')
                                        ->limit(1)
                                        ->get_where("transaksi_v_riwayat_jabatan_fungsional_dosen_2020",['nip'=>$nip])
                                        ->row();
                    $harga_db = null;
                    foreach ($result->result() as $key => $value):
                        $tgl_nonaktif = date_create('2020-12-31');
                        $tmt_sk = date_create($value->tmt_sk);
                        $selisih = $tgl_nonaktif->diff($tmt_sk)->days + 1;
                        $durasi = round($selisih/30);
                        if($durasi > 6):
                            $durasi = str_replace('-','',round($getRowOne->hari_1/30));
                        else:
                            $durasi = $durasi;
                        endif;
                        if($durasi > 0):
                          $harga_db = ($value->score_jft * 1 * $data_periode['pir'] * 6) / 40;
                        endif;
                    endforeach;
                else:
                    $query = $this->db->select("kode_dosen,nip,kode_jft, nama_jft,tmt_sk,grade_jft,score_jft,cast(6 as char) as durasi")
                                    ->limit(1)
                                    ->order_by('tmt_sk','desc')
                                    ->get_where("simpeg_riwayat_jabatan_fungsional",['nip'=>$nip])
                                    ->result();
                    $harga_db = null;
                    foreach($query as $key => $value){
                      $harga_db = ($value->score_jft * 1 * $data_periode['pir'] * 6) / 40;
                    }
                endif;
          }
          
          return $harga_db;         
        }

        public function harga_db_2_pembayaran($nip, $periode, $peg){
            header('Content-Type: application/json');
          $query_periode = $this->db->query("SELECT * FROM simremlink_data_periode WHERE periode = '$periode' "); 
          $data_periode = $query_periode->row_array();
          if($peg == 'PEG'){
            $dataJft = $this->db->query("SELECT *, jenis_jft as nama_jft
            FROM `simpeg_riwayat_jft_pegawai` 
            where tmt_sk IN (SELECT MAX(tmt_sk) FROM simpeg_riwayat_jft_pegawai GROUP BY nip )
            AND status = 'Y' AND nip ='$nip'");

            $dataJfu = $this->db->query("SELECT *, jenis_jabumum as nama_jft
            FROM `simpeg_riwayat_jfu_pegawai` 
            where tmt_sk IN (SELECT MAX(tmt_sk) FROM simpeg_riwayat_jfu_pegawai GROUP BY nip )
            AND status = 'Y'AND nip ='$nip'");

            $dataStruktural = $this->db->query("SELECT *, nama_jabatan_struktural as nama_jft
            FROM `simpeg_riwayat_struktural_pegawai` 
            where tmt_sk IN (SELECT MAX(tmt_sk) FROM simpeg_riwayat_struktural_pegawai GROUP BY nip )
            AND status = 'Y'AND nip ='$nip'");
            
            if($dataJfu->num_rows() > 0):
                $data = $dataJfu->row();
                $score = $data->score;
            elseif($dataJft->num_rows() > 0):
                $data = $dataJft->row();
                $score = $data->score;
            elseif($dataStruktural->num_rows() > 0):
                $data = $dataStruktural->row();
                $score = $data->score;
            else:
                $score = 0;
            endif;
            $harga_db = ($score*1*$data_periode['pir']*6)/40;
          }else{
            $query_jabatan = $this->db->query("SELECT * FROM simpeg_dosen_pns_simremlink JOIN simpeg_jabatan_fungsional_dosen ON (simpeg_dosen_pns_simremlink.id_jabatan_fung=simpeg_jabatan_fungsional_dosen.kode_jabatan_fungsional) WHERE simpeg_dosen_pns_simremlink.nip = '$nip' ");
            
            $data_jabatan = $query_jabatan->row_array();
            
            $harga_db = ($data_jabatan['score_fungsional']*1*$data_periode['pir']*6)/40;
          }
          
          return $harga_db;         
        }


        public function pembayaranDosenPegawai($kode,$periode){
            $data1 = $this->db->query("SELECT 
                    kode_pegawai,
                    simpeg_pegawai_pns_simremlink.nip, 
                    simpeg_pegawai_pns_simremlink.nama,
                    simremlink_data_rekening.norek
                    FROM `simpeg_pegawai_pns_simremlink`
                    LEFT JOIN simremlink_data_rekening ON (simremlink_data_rekening.nip = simpeg_pegawai_pns_simremlink.nip)
                    WHERE simpeg_pegawai_pns_simremlink.status_aktif = 00 AND simpeg_pegawai_pns_simremlink.kode_unit = '$kode'
                    ")->result();
            $data2 = $this->db->query("SELECT 
                    kode_dosen as kode_pegawai,
                    simpeg_dosen_pns_simremlink.nip, 
                    simpeg_dosen_pns_simremlink.nama,
                    simremlink_data_rekening.norek
                    FROM `simpeg_dosen_pns_simremlink`
                    LEFT JOIN simremlink_data_rekening ON (simremlink_data_rekening.nip = simpeg_dosen_pns_simremlink.nip)
                    WHERE simpeg_dosen_pns_simremlink.status_aktif = 00 AND simpeg_dosen_pns_simremlink.kode_unit = '$kode'
                    ")->result();
            $data = array_merge($data1,$data2);
            return $data;
        }

        public function pembayaranP2($nip,$periode,$kode_jenis,$kode){
            // inputan
            $jenisDosen = $this->cekJenisDosen($nip);
            //--------- harga jabatan
            $harga_dt = $this->harga_dt_2($nip, $periode);
            $harga_db = $this->harga_db_2($nip, $periode, $kode_jenis);
            $golongan = $this->cekGol($nip);
            // total poin
            $tupoksi = $this->tupoksi($kode,$periode);
            $mengajar = $this->mengajar($kode,$periode);
            $mengajarLainnya = $this->mengajarLainnya($kode,$periode);
            $penunjang = $this->penunjang($kode,$periode);
            $penghargaan = $this->penghargaan($kode,$periode);
            $total_semua_poin = $tupoksi + $mengajar + $mengajarLainnya + $penunjang + $penghargaan;
            $poin_kinerja = $tupoksi + $mengajar + $mengajarLainnya + $penunjang ;
            $stts = null;
            
            //-------------------------------
            $p2DB = 0;
            $uang_dt = 0;
            $uang_p1_dt = 0;
            $uang_p2_dt = 0;
            $uang_p1_db = 0;
            $p2DT = 0;
            //------------------------------
            if($kode_jenis == 'PEG'){
                $stts = 'PEG';
                if($poin_kinerja > 54){
                    $poin_kinerja = 54;
                }else{
                    $poin_kinerja = $poin_kinerja;
                };
                //$p2DT = 28;
                $p2DT = $poin_kinerja - 12;
                if($p2DT > 28){
                  $p2DT = 28;
                }else{
                  $p2DT = $p2DT;
                };
                $p2DB = $poin_kinerja - 12;
                $uang_p1_db = 12 * $harga_db;
                $uang_p2_db = $p2DB * $harga_db;
                $uang_penghargaan = $penghargaan * $harga_db;
                $uang_total_p2 = $uang_p2_db + $uang_penghargaan;
                $pajak_p2 = 0;
                if($golongan == 'IV'){
                    $pajak_p2 = $uang_total_p2 * 0.15;
                }elseif($golongan == 'III'){
                    $pajak_p2 = $uang_total_p2 * 0.05;
                }elseif($golongan == 'I' || $golongan == 'II'){
                    $pajak_p2 = 0;
                };
                $uang_total_p2_pajak = $uang_total_p2 - $pajak_p2;
            }elseif($kode_jenis == 'DOS'){
                if($jenisDosen == 'DT'){
                    $stts = 'DT';
                    
                    if($poin_kinerja > 108){
                        $poin_kinerja = 108;
                    }else{
                        $poin_kinerja = $poin_kinerja;
                    };
                    //$p2DT = 28;
                    $p2DT = $poin_kinerja - 12;
                    if($p2DT > 28){
                      $p2DT = 28;
                    }else{
                      $p2DT = $p2DT;
                    };
                    $p2DB = $poin_kinerja - ($p2DT + 12);
                    $uang_p1_dt = 12 * $harga_dt;
                    $uang_p2_dt = $p2DT * $harga_dt;
                    $uang_p2_db = $p2DB * $harga_db;
                    $uang_penghargaan = $penghargaan * $harga_db;
                    $uang_total_p2 = $uang_p2_dt + $uang_p2_db + $uang_penghargaan;
                    $pajak_p2 = 0;
                    if($golongan == 'IV'){
                        $pajak_p2 = $uang_total_p2 * 0.15;
                    }elseif($golongan == 'III'){
                        $pajak_p2 = $uang_total_p2 * 0.05;
                    }elseif($golongan == 'I' || $golongan == 'II'){
                        $pajak_p2 = 0;
                    };
                    $uang_total_p2_pajak = $uang_total_p2 - $pajak_p2;
                }else if($jenisDosen == 'DB'){
                    $stts = 'DB';
                    if($poin_kinerja > 68){
                        $poin_kinerja = 68;
                    }else{
                        $poin_kinerja = $poin_kinerja;
                    };
                    //$p2DT = 28;
                    $p2DT = $poin_kinerja - 12;
                    if($p2DT > 28){
                      $p2DT = 28;
                    }else{
                      $p2DT = $p2DT;
                    };
                    $p2DB = $poin_kinerja - 12;
                    $uang_p1_db = 12 * $harga_db;
                    $uang_p2_db = $p2DB * $harga_db;
                    $uang_penghargaan = $penghargaan * $harga_db;
                    $uang_total_p2 = $uang_p2_db + $uang_penghargaan;
                    $pajak_p2 = 0;
                    if($golongan == 'IV'){
                        $pajak_p2 = $uang_total_p2 * 0.15;
                    }elseif($golongan == 'III'){
                        $pajak_p2 = $uang_total_p2 * 0.05;
                    }elseif($golongan == 'I' || $golongan == 'II'){
                        $pajak_p2 = 0;
                    };
                    $uang_total_p2_pajak = $uang_total_p2 - $pajak_p2;
                }
            }
            
            $output = array(
                'total_poin' => number_format((float)$total_semua_poin, 3),
                'poin_kinerja' => number_format((float)$poin_kinerja, 2),
                'p2DT' => number_format((float)$p2DT, 2),
                'p2DB' => number_format((float)$p2DB, 2),
                'poinPenghargaan' => number_format((float)$penghargaan, 0),
                'uang_p1_dt' => number_format((float)$uang_p1_dt ,0),
                'uang_p1_db' => number_format((float)$uang_p1_db ,0),
                'uang_p2_dt' => number_format((float)$uang_p2_dt ,0),
                'uang_p2_db' => number_format((float)$uang_p2_db ,0),
                'uang_penghargaan' => number_format((float)$uang_penghargaan ,0),
                'uang_total_p2' => number_format((float)$uang_total_p2 ,0,',','.'),
                'pajak_p2' => number_format((float)$pajak_p2 ,0),
                'uang_total_p2_pajak' => number_format((float)$uang_total_p2_pajak ,0,',','.'),
                'stts' => $stts,
                'jenis' => $jenisDosen
            );
            return $output;
        }

        public function pembayaranP1($nip,$periode,$jenis){
            $checkDataDosen = $this->db->get_where('simpeg_dosen_pns_simremlink',['nip'=>$nip]);
            $checkDataPegawai = $this->db->get_where('simpeg_pegawai_pns_simremlink',['nip'=>$nip]);
            if($checkDataDosen->num_rows() > 0){
                $data = $checkDataDosen->row();
                $kode = substr($data->kode_dosen,0,3);
                $kode_pegawai = $data->kode_dosen;
            }elseif ($checkDataPegawai->num_rows() > 0) {
                $data = $checkDataPegawai->row();
                $kode = substr($data->kode_pegawai,0,3);
                $kode_pegawai = $data->kode_pegawai;
            }
            $harga_dt = $this->harga_dt_2($nip, $periode);
            $harga_db = $this->harga_db_2_pembayaran($nip, $periode, $kode);
            
            
            if($jenis == 'P1'){
                $uang_db = ($harga_db * 12) / 6;
                $uang_total_p2 = 0;
                $uang_total_p2_pajak = 0;
            }elseif ($jenis == 'P3') {
                $penghargaan = $this->penghargaan($kode_pegawai,$periode);
                $uang_db = $harga_db * $penghargaan;
                $uang_total_p2 = 0;
                $uang_total_p2_pajak = 0;
            }elseif ($jenis == 'P2') {
                $pembayaranP2 = $this->pembayaranP2($nip,$periode,$kode,$kode_pegawai);
                $uang_total_p2 = $pembayaranP2['uang_total_p2'];
                $uang_total_p2_pajak = $pembayaranP2['uang_total_p2_pajak'];
                $uang_db = 0;
            }
            
            $data = [
                    "harga_db"=>number_format($uang_db,0,',','.'),
                    "uang_total_p2"=>$uang_total_p2,
                    "uang_total_p2_pajak"=>$uang_total_p2_pajak,
                    ];
            return $data;
        }

        public function syncPembayaran(){
            $kode_unit = trim($this->input->post('kode',true));
            $dosen = $this->db->select('simpeg_dosen_pns_simremlink.nip,simpeg_dosen_pns_simremlink.golongan, cast("DOS" as char) as kode, nama,  norek, kode_dosen as kode_pegawai')
                              ->join("simremlink_data_rekening","simremlink_data_rekening.nip = simpeg_dosen_pns_simremlink.nip","LEFT")
                              ->get_where('simpeg_dosen_pns_simremlink',['simpeg_dosen_pns_simremlink.kode_unit'=>$kode_unit,"status_aktif"=>"00"])
                              ->result();
            $pegawai = $this->db->select('simpeg_pegawai_pns_simremlink.nip,simpeg_pegawai_pns_simremlink.golongan, cast("PEG" as char) as kode, nama,  norek, kode_pegawai')
                                ->join("simremlink_data_rekening","simremlink_data_rekening.nip = simpeg_pegawai_pns_simremlink.nip","LEFT")
                                ->get_where('simpeg_pegawai_pns_simremlink',['simpeg_pegawai_pns_simremlink.kode_unit'=>$kode_unit,"status_aktif"=>"00"])
                                ->result();
            $data = array_merge($dosen,$pegawai);
            $a = [
                //["nip"=>"197612152006042001","kode"=>"DOS0997","norek"=>" - ","nama"=>"Rika Wahyuni, S.Pd., M.Si."],
                //["nip"=>"197110302008122001","kode"=>"DOS1078","norek"=>"0177859324","nama"=>"Faridawaty Marpaung, S.Si., M.Si"],
                ["nip"=>"197302112005011001","kode"=>"DOS0903","norek"=>"0062510656","nama"=>"Zulfahmi Indra, S.Si., M.Cs"],
                ["nip"=>"197703182005011001","kode"=>"DOS0908","norek"=>"0068709235","nama"=>"Said Iskandar Al Idrus, S.Si., M.Si"],
                ["nip"=>"198006202006042001","kode"=>"DOS0989","norek"=>"0109824357","nama"=>"Dr. Junifa Layla Sihombing, S.Si., M.Sc"],
                ["nip"=>"198304112008011005","kode"=>"DOS1032","norek"=>"0181775345","nama"=>"Dr. Muhammad Yusuf, S.Si., M.Si"],
                ["nip"=>"197808122008012021","kode"=>"DOS1035","norek"=>"0154783755","nama"=>"Ratna Sari Dewi, S.Si., M.Si"],
                ["nip"=>"197706172008012014","kode"=>"DOS1057","norek"=>"0154788925","nama"=>"Dr. Herlinawati, S.Si., M.Si"],
                ["nip"=>"198106182012121005","kode"=>"DOS1126","norek"=>"0275198745","nama"=>"Ahmad Nasir Pulungan, S.Si., M.Sc"],
                ["nip"=>"197911282005011002","kode"=>"DOS0909","norek"=>"0074156554","nama"=>"Muhammad Kadri, S.Si., M.Sc"],
                ["nip"=>"197007142008011010","kode"=>"DOS1031","norek"=>"0156149975","nama"=>"Abdul Rais, S.Pd., S.T., M.Si"],
                ["nip"=>"197706202008122001","kode"=>"DOS1093","norek"=>"0177750687","nama"=>"Dr. Maryati Evivani Doloksaribu, S,Si., M.Si"],
                ["nip"=>"198109232008122003","kode"=>"DOS1129","norek"=>"0177754478","nama"=>"Erniwati Halawa, M.Si"],
                ["nip"=>"198003082008122002","kode"=>"DOS1114","norek"=>"0177888957","nama"=>"Elida Hafni Siregar, S.Pd., M.Si"],
                ["nip"=>"198103212009121006","kode"=>"DOS1121","norek"=>"0197556449","nama"=>"Amrizal, S.Si., M.Pd"],
                ["nip"=>"197109202005011004","kode"=>"DOS0946","norek"=>"0074170089","nama"=>"Dr. Idramsa, M.Si"],
                ["nip"=>"197109132005011002","kode"=>"DOS0960","norek"=>"0074214698","nama"=>"Drs. Puji Prastowo, M.Si"],
                ["nip"=>"198105242008012014","kode"=>"DOS1034","norek"=>"0154780798","nama"=>"Khairiza Lubis, S.Si., M.Sc., Ph.D"],
                ["nip"=>"198203142006042005","kode"=>"DOS1133","norek"=>"0109797784","nama"=>"Wina Dyah Puspita Sari, S.Si., M.Si"],
                ["nip"=>"198307172008122004","kode"=>"DOS1150","norek"=>"0178198098","nama"=>"Aida Fitriani Sitompul, S.Pd., M.Si"],
                ["nip"=>"198104232008121002","kode"=>"DOS1122","norek"=>"0178140784","nama"=>"Halim Simatupang, S.Pd., M.Pd"],
                ["nip"=>"198108272006042003","kode"=>"DOS1009","norek"=>"0590603005","nama"=>"Eri Widyastuti, S.Pd., M.Sc"],
                ["nip"=>"198005212008011011","kode"=>"DOS1033","norek"=>"0155879991","nama"=>"Mangaratua Marianus Simanjorang, M.Pd., Ph.D"],
                ["nip"=>"198206262008122003","kode"=>"DOS1137","norek"=>"0425746229","nama"=>"Elfitra, S.Pd., M.Si"],
                ["nip"=>"198105022008122001","kode"=>"DOS1123","norek"=>"0177704120","nama"=>"Hanna Dewi Marina Hutabarat, S.Si., M.Si"],
                ["nip"=>"197906032006041003","kode"=>"DOS0988","norek"=>"0111107063","nama"=>"Rudi Munzirwan Siregar, S.Si., M.Si"],
                ["nip"=>"197605062008122001","kode"=>"DOS1088","norek"=>"0177763970","nama"=>"Elfrida Ginting, S.Si., M.Sc., Ph.D"],
                ["nip"=>"198005282014041001","kode"=>"DOS1116","norek"=>"0349789430","nama"=>"Moondra Zubir, S.Si., M.Si., Ph.D"],
                ["nip"=>"198209302008122001","kode"=>"DOS1139","norek"=>"0228916026","nama"=>"Jubaidah, S.Pd., M.Si"],
                ["nip"=>"196202031987031002","kode"=>"DOS1357","norek"=>"0758649907","nama"=>"Prof. Dr. Syawal Gultom, M.Pd"],
                ["nip"=>"196004261985031003","kode"=>"DOS1465","norek"=>"0057685646","nama"=>"Prof. Dr. Sahyar, M.S., M.M"],
                ["nip"=>"198905092019031012","kode"=>"DOS1287","norek"=>"0267215627","nama"=>"Eko Prasetya, S.Pd., M.Sc"],
                ["nip"=>"198808302014041001","kode"=>"DOS1268","norek"=>"0349667149","nama"=>"Wasis Wuyung Wisnu Brata, S.Pd., M.Pd"],
                ["nip"=>"198410312010121003","kode"=>"DOS1175","norek"=>"0218948652","nama"=>"Ahmad Shafwan S. Pulungan, S.Pd., M.Si"],
                ["nip"=>"198502112014041001","kode"=>"DOS1182","norek"=>"0349434839","nama"=>"Didi Febrian, S.Si., M.Sc"],
                ["nip"=>"196002231986031005","kode"=>"DOS1355","norek"=>"0166955893","nama"=>"Dr. Simson Tarigan, M.Pd"],
                ["nip"=>"198405052010122004","kode"=>"DOS1165","norek"=>"0221665250","nama"=>"Glory Indira Diana Purba, S.Si., M.Pd"],
                ["nip"=>"198504192008122002","kode"=>"DOS1185","norek"=>"0168244118","nama"=>"Ade Andriani, S.Pd., M.Pd"],
                ["nip"=>"198606042015041002","kode"=>"DOS1210","norek"=>"0414826164","nama"=>"Andrea Arifsyah Nasution, S.Pd., M.Sc"],
                ["nip"=>"198805282015042001","kode"=>"DOS1260","norek"=>"0395213338","nama"=>"Suci Frisnoiry, S.Pd., M.Pd"],
                ["nip"=>"197606212008122001","kode"=>"DOS1360","norek"=>"192509710 ","nama"=>"Dr. Arnita, M.Si."],
                ["nip"=>"198610232010121003","kode"=>"DOS1215","norek"=>"0224701503","nama"=>"Freddy Tua Musa Panggabean, S.Pd., M.Pd"],
                ["nip"=>"198708182012122002","kode"=>"DOS1239","norek"=>"0291564622","nama"=>"Dewi Syafriani, S.Pd., M.Pd"],
                ["nip"=>"198408212010122004","kode"=>"DOS1171","norek"=>"0222195394","nama"=>"Yeni Megalina, S.Pd., M.Si"],
                ["nip"=>"198507272008122003","kode"=>"DOS1193","norek"=>"0141256021","nama"=>"Yul Ifda Tanjung, S.Pd., M.Pd"],
                ["nip"=>"198802092014041001","kode"=>"DOS1248","norek"=>"0350333723","nama"=>"Teguh Febri Sudarma, S.Pd., M.Pd"],
                ["nip"=>"198805242014041001","kode"=>"DOS1257","norek"=>"0349181911","nama"=>"Deo Demonta Panggabean, S.Pd., M.Pd"],
                ["nip"=>"198506142012121003","kode"=>"DOS1189","norek"=>"0293365818","nama"=>"Irfandi, S.Pd., M.Si"],
                ["nip"=>"198509222014042001","kode"=>"DOS1197","norek"=>"0349244736","nama"=>"Salwa Rezeqi, S.Pd., M.Pd"],
                ["nip"=>"198811062015041002","kode"=>"DOS1275","norek"=>"0254517687","nama"=>"Dirga Purnama, S.Pd., M.Pd"],
                ["nip"=>"198406222008122006","kode"=>"DOS1167","norek"=>"0276794815","nama"=>"Selvia Dewi Pohan, S.Si., M.Si"],
                ["nip"=>"198703252015041002","kode"=>"DOS1227","norek"=>"0215684271","nama"=>"Muhammad Badzlan Darari, S.Pd., M.Pd"],
                ["nip"=>"198403032009122003","kode"=>"DOS1163","norek"=>"0197539467","nama"=>"Nice Rejoice Refisis, S.Pd., M.Si"],
                ["nip"=>"198401272012122001","kode"=>"DOS1159","norek"=>"0121188393","nama"=>"Rini Selly, S.Pd., M.Sc"],
                ["nip"=>"198501102015042003","kode"=>"DOS1179","norek"=>"0413995145","nama"=>"Makharany Dalimunthe, S.Pd., M.Pd"],
                ["nip"=>"198908242015041001","kode"=>"DOS1293","norek"=>"0413997812","nama"=>"Feri Andi Syuhada, S.Pd., M.Pd"],
                ["nip"=>"198406232008122002","kode"=>"DOS1168","norek"=>"2306198488","nama"=>"Yuni Warty, S.Pd., M.Si"],
                ["nip"=>"198702162015041001","kode"=>"DOS1221","norek"=>"0292522705","nama"=>"Satria Mihardi, S.Pd., M.Pd"],
                ["nip"=>"198903052015041001","kode"=>"DOS1283","norek"=>"0413990601","nama"=>"Muhammad Aswin Rangkuti, S.Pd., M.Pd"],
                ["nip"=>"198408122008122001","kode"=>"DOS1170","norek"=>"0177748396","nama"=>"Dina Handayani, S.Pd., M.Si"],
                ["nip"=>"198702102018032001","kode"=>"DOS1220","norek"=>"0710554269","nama"=>"Aswarina Nasution, M.Pd"],
                ["nip"=>"198712202015042001","kode"=>"DOS1246","norek"=>"0413990849","nama"=>"Dr. Widya Arwita, S.Pd., M.Pd"],
                ["nip"=>"199201052019031019","kode"=>"DOS1330","norek"=>"0830337086","nama"=>"Michael Christian Simanullang, S.Pd., M.Pd"],
                ["nip"=>"199303162019032021","kode"=>"DOS1479","norek"=>"0830385199","nama"=>"Fevi Rahmawati Suwanto, S.Pd., M.Pd"],
                ["nip"=>"199112032019032014","kode"=>"DOS1328","norek"=>"0838149063","nama"=>"Dinda Kartika, S.Pd., M.Si"],
                ["nip"=>"199008162019031015","kode"=>"DOS1310","norek"=>"0831792013","nama"=>"Kana Saputra S, S.Pd., M.Kom"],
                ["nip"=>"199102092019031012","kode"=>"DOS1316","norek"=>"0436111751","nama"=>"Insan Taufik, S.Kom., M.Kom"],
                ["nip"=>"199011192019031014","kode"=>"DOS1473","norek"=>"0833040399","nama"=>"Debi Yandra Niska, S.Kom., M.Kom"],
                ["nip"=>"198802242019032013","kode"=>"DOS1250","norek"=>"2422219882","nama"=>"Susilawati Amdayani, S.Si., M.Pd"],
                ["nip"=>"198807022019031012","kode"=>"DOS1264","norek"=>"0830102984","nama"=>"Haqqi Annazili Nasution, S.Pd., M.Pd"],
                ["nip"=>"199107142019032027","kode"=>"DOS1322","norek"=>"0831518915","nama"=>"Siti Rahmah, S.Pd., M.Sc"],
                ["nip"=>"198907062019031009","kode"=>"DOS1471","norek"=>"0358203968","nama"=>"Ricky Andi Syahputra, S.Pd., M.Sc"],
                ["nip"=>"199003252019031017","kode"=>"DOS1304","norek"=>"0829970524","nama"=>"Irham Ramadhani, S.Pd., M.Pd"],
                ["nip"=>"199211012019031013","kode"=>"DOS1477","norek"=>"0838679975","nama"=>"Rajo Hasim Lubis, S.Pd., M.Pd"],
                ["nip"=>"198708302019031011","kode"=>"DOS1240","norek"=>"0316002807","nama"=>"Budiman Nasution, S.Pd., M.Si"],
                ["nip"=>"198905312019032019","kode"=>"DOS1289","norek"=>"0832981532","nama"=>"Widia Ningsih, S.Pd., M.Pd"],
                ["nip"=>"198902042019032015","kode"=>"DOS1470","norek"=>"0348323936","nama"=>"Ayu Putri Ningsih, S.Si., M.Si"],
                ["nip"=>"198804272019032014","kode"=>"DOS1253","norek"=>"0831319045","nama"=>"Lastama Sinaga, S.Pd., M.Ed"],
                ["nip"=>"199201092019031015","kode"=>"DOS1331","norek"=>"0838452739","nama"=>"Aristo Hardinata, S.Pd., M.Pd"],
            ];
            $output = [];
            foreach ($data as $key => $value) {
                //$harga_dt   = $this->harga_dt_for_pembayaran($value->nip,$value->kode);
                //$harga_db   = $this->harga_db_for_pembayaran($value->nip,$value->kode);
                //$poin       = $this->total_poin_remun_pembayaran($value->nip,$value->kode_pegawai,$value->kode);
                $p2         = $this->total_uang_remun_db_for_pembayaran($value->nip,$value->kode_pegawai,$value->kode);
                $set = [
                    "nip" => $value->nip,
                    "nama" => $value->nama,
                    "no_rekening" => $value->norek,
                    // "harga_dt" => $harga_dt['harga'],
                    // "harga_db" => $harga_db['harga'],
                    // "total_poin" => $poin['total_poin'],
                    // "poin_kinerja" => $poin['poin_kinerja'],
                    // "poin_p1_dt" => 12,
                    // "poin_p2_dt" => $poin['p2DT'],
                    // "poin_p2_db" => $poin['p2DB'],
                    // "poin_penghargaan" => $poin['poinPenghargaan'],
                    "golongan" => $value->golongan,
                    "kode_unit" => $kode_unit,
                    "p2_sesudah_pajak" => $p2['p2_penghargaan_setelah_pajak'],
                    "p2_sebelum_pajak" => $p2['p2_penghargaan_sebelum_pajak'],
                    "jabatan" => $p2['jabatan'],
                    "grade" => $p2['grade'],
                ];
                //array_push($output,$set);
                $this->db->insert('transaksi_pembayaran_p2_v3', $set); 
               
            }
            //$this->db->insert_batch('transaksi_pembayaran_p2_v2', $output); 
        }
}