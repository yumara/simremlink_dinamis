<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_api_transaksi extends CI_Model{
    public function __construct(){
        parent::__construct();
        error_reporting(0);
        date_default_timezone_set("Asia/Jakarta");
    }

    public function kelebihanMengajar(){
        return $this->db->query("SELECT 
        priode as periode,
        kode_dosen as kode_pegawai, 
        kode_peran as peran, 
        CAST(NULL as char) as capaian,
        sks_lebih as volume,
        CAST(NULL as char) as dokumen_pendukung,
        CAST(simremlink_data_peran.ewkp as char) as poin_remlink,
        CAST(null as char) as tgl_nonaktif,
        CAST(null as char) as tipe
        FROM akad_kelebihan_mengajar
        JOIN simremlink_data_peran ON (simremlink_data_peran.kdeperan = akad_kelebihan_mengajar.kode_peran)");
    }
    
    public function pembimbingAkademik(){
        return $this->db->query("SELECT 
        priode as periode, 
        kode_dosen as kode_pegawai, 
        kode_peran as peran, 
        CAST(NULL as char) as capaian,
        jumlah as volume,
        CAST(NULL as char) as dokumen_pendukung,
        CAST(simremlink_data_peran.ewkp as char) as poin_remlink,
        CAST(null as char) as tgl_nonaktif,
        CAST(null as char) as tipe
        FROM akad_pembimbing_akademik
        JOIN simremlink_data_peran ON (simremlink_data_peran.kdeperan = akad_pembimbing_akademik.kode_peran)");
    }
    
    public function pembimbingSkripsiTesisDisertasi(){
        return $this->db->query("SELECT 
        priode as periode, 
        kode_dosen as kode_pegawai, 
        kdperan as peran, 
        CAST(NULL as char) as capaian,
        jumlah as volume,
        CAST(NULL as char) as dokumen_pendukung,
        CAST(simremlink_data_peran.ewkp as char) as poin_remlink,
        CAST(null as char) as tgl_nonaktif,
        CAST(null as char) as tipe
        FROM akad_pembimbing_skripsi_tesis_disertasi
        JOIN simremlink_data_peran ON (simremlink_data_peran.kdeperan = akad_pembimbing_skripsi_tesis_disertasi.kdperan)");
    }

    public function pengujiSkripsiTesisDisertasi(){
        return $this->db->query("SELECT 
        priode as periode, 
        kode_dosen as kode_pegawai, 
        kdperan as peran, 
        CAST(NULL as char) as capaian,
        jumlah as volume,
        CAST(NULL as char) as dokumen_pendukung,
        CAST(simremlink_data_peran.ewkp as char) as poin_remlink,
        CAST(null as char) as tgl_nonaktif,
        CAST(null as char) as tipe
        FROM akad_penguji_skripsi_tesis_disertasi
        JOIN simremlink_data_peran ON (simremlink_data_peran.kdeperan = akad_penguji_skripsi_tesis_disertasi.kdperan)");
    }

    public function mengajarLainnya(){
        return $this->db->query("SELECT 
        priode as periode, 
        kode_dosen as kode_pegawai, 
        kdperan as peran, 
        CAST(NULL as char) as capaian,
        jumlah as volume,
        CAST(NULL as char) as dokumen_pendukung,
        CAST(simremlink_data_peran.ewkp as char) as poin_remlink,
        CAST(null as char) as tgl_nonaktif,
        CAST(null as char) as tipe
        FROM akad_mengajar_lainnya
        JOIN simremlink_data_peran ON (simremlink_data_peran.kdeperan = akad_mengajar_lainnya.kdperan)");
    }

    public function riwayatPenghargaanDosen(){
        return $this->db->query("SELECT 
        periode_remun as periode, 
        kode_dosen as kode_pegawai, 
        kode_peran as peran, 
        CAST(NULL as char) as capaian,
        CAST(NULL as char) as volume,
        CAST(NULL as char) as dokumen_pendukung,
        CAST(simremlink_data_peran.ewkp as char) as poin_remlink,
        CAST(null as char) as tgl_nonaktif,
        CAST(null as char) as tipe
        FROM simpeg_riwayat_penghargaan_dosen_pns
        JOIN simremlink_data_peran ON (simremlink_data_peran.kdeperan = simpeg_riwayat_penghargaan_dosen_pns.kode_peran)");
    }

    public function riwayatPenghargaanPns(){
        return $this->db->query("SELECT 
        periode_remun as periode, 
        kode_peg as kode_pegawai, 
        kode_peran as peran, 
        CAST(NULL as char) as capaian,
        CAST(NULL as char) as volume,
        CAST(NULL as char) as dokumen_pendukung,
        CAST(simremlink_data_peran.ewkp as char) as poin_remlink,
        CAST(null as char) as tgl_nonaktif,
        CAST(null as char) as tipe
        FROM simpeg_riwayat_penghargaan_pegawai_pns
        JOIN simremlink_data_peran ON (simremlink_data_peran.kdeperan = simpeg_riwayat_penghargaan_pegawai_pns.kode_peran)");
    }

    public function capaianSkpDosenDt(){
        return $this->db->select('
                        periode, 
                        KODE_PEG as kode_pegawai, 
                        CAST(NULL as char) as peran,
                        capaian,
                        CAST(NULL as char) as volume,
                        CAST(NULL as char) as dokumen_pendukung,
                        CAST(NULL as char) as poin_remlink,
                        CAST(null as char) as tgl_nonaktif,
                        CAST(null as char) as tipe,
                        CAST("yes" as char) as rumus')
                        ->get('simpeg_capaian_skp_dosen');
    }

    public function capaianSkpPegawai(){
        return $this->db->query("SELECT 
        periode, 
        kode_peg as kode_pegawai, 
        kode_peran as peran, 
        capaian,
        CAST(NULL as char) as volume,
        CAST(NULL as char) as dokumen_pendukung,
        CAST(simremlink_data_peran.ewkp as char) as poin_remlink,
        CAST(null as char) as tgl_nonaktif,
        CAST(null as char) as tipe,
        CAST('yes' as char) as rumus
        FROM simpeg_capaian_skp_pegawai
        JOIN simremlink_data_peran ON (simremlink_data_peran.kdeperan = simpeg_capaian_skp_pegawai.kode_peran)");
    }

    public function nilaiSikapDanPerilakuPegawai(){
        return $this->db->query("SELECT 
        periode, 
        kode_peg as kode_pegawai, 
        kode_peran as peran, 
        capaian,
        CAST(NULL as char) as volume,
        CAST(NULL as char) as dokumen_pendukung,
        CAST(simremlink_data_peran.ewkp as char) as poin_remlink,
        CAST(null as char) as tgl_nonaktif,
        CAST(null as char) as tipe,
        CAST('yes' as char) as rumus
        FROM simpeg_nilai_sikap_dan_perilaku_pegawai
        JOIN simremlink_data_peran ON (simremlink_data_peran.kdeperan = simpeg_nilai_sikap_dan_perilaku_pegawai.kode_peran)");
    }

    public function capaianBkdDosen(){
        return $this->db->query("SELECT 
        periode, 
        kode_peg as kode_pegawai, 
        kode_peran as peran, 
        capaian_sks as capaian,
        CAST(NULL as char) as volume,
        CAST(NULL as char) as dokumen_pendukung,
        CAST(simremlink_data_peran.ewkp as char) as poin_remlink,
        CAST(null as char) as tgl_nonaktif,
        CAST(null as char) as tipe,
        CAST('no' as char) as rumus
        FROM simpeg_capaian_bkd_dosen
        JOIN simremlink_data_peran ON (simremlink_data_peran.kdeperan = simpeg_capaian_bkd_dosen.kode_peran)");
    }

    public function absensiPegawaiTendik(){
        return $this->db->query("SELECT 
        periode, 
        kode_peg as kode_pegawai,
        kode_peran as peran, 
        capaian,
        CAST(NULL as char) as volume,
        CAST(NULL as char) as dokumen_pendukung,
        CAST(simremlink_data_peran.ewkp*(capaian/100) as char) as poin_remlink,
        CAST(null as char) as tgl_nonaktif,
        CAST(null as char) as tipe,
        CAST('yes' as char) as rumus
        FROM simpeg_absensi_pegawai_tendik
        JOIN simremlink_data_peran ON (simremlink_data_peran.kdeperan = simpeg_absensi_pegawai_tendik.kode_peran)");
    }

    public function absensiDosenPns(){
        // return $this->db->query("SELECT 
        // periode, 
        // kode_peg as kode_pegawai,
        // kode_peran as peran, 
        // capaian,
        // CAST(NULL as char) as volume,
        // CAST(NULL as char) as dokumen_pendukung,
        // CAST(simremlink_data_peran.ewkp*(capaian/100) as char) as poin_remlink
        // FROM simpeg_absensi_dosen_pns
        // JOIN simremlink_data_peran ON (simremlink_data_peran.kdeperan = simpeg_absensi_dosen_pns.kode_peran)");
        $data = $this->db->query("SELECT
        simpeg_dosen_pns_simremlink.uuid,
        simpeg_dosen_pns_simremlink.kode_dosen,
        simpeg_dosen_pns_simremlink.nip,
        simpeg_dosen_pns_simremlink.nama,    
        IF(simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns.nama_tugastambahan != '' AND simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns.status_aktif = 'Y' AND simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns.status_jabatan_utama = 'Y','DT','DB') AS jenis_jabatan
        FROM `simpeg_dosen_pns_simremlink` 
        LEFT JOIN simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns ON(simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns.nip = simpeg_dosen_pns_simremlink.nip)
        LEFT JOIN simpeg_riwayat_jabatan_fungsional ON(simpeg_riwayat_jabatan_fungsional.nip = simpeg_dosen_pns_simremlink.nip)
        WHERE simpeg_riwayat_jabatan_fungsional.nama_jft != ''
        GROUP BY simpeg_dosen_pns_simremlink.nip")->result();
        $datass = [];
        foreach ($data as $key => $value) {
            if($value->jenis_jabatan == 'DT'){
                $p = 12;
            }else{
                $p = 6;
            }
            $datas = $this->db->query("SELECT 
                        periode, 
                        kode_peg as kode_pegawai,
                        kode_peran as peran, 
                        capaian,
                        CAST(NULL as char) as volume,
                        CAST(NULL as char) as dokumen_pendukung,
                        CAST($p*(capaian/100) as char) as poin_remlink,
                        CAST(null as char) as tgl_nonaktif,
                        CAST('absensi dosen' as char) as tipe
                        FROM simpeg_absensi_dosen_pns
                        JOIN simremlink_data_peran ON (simremlink_data_peran.kdeperan = simpeg_absensi_dosen_pns.kode_peran)
                        WHERE simpeg_absensi_dosen_pns.kode_peg = '$value->kode_dosen' ")->row_array();
            $output = [
                "periode"=> $datas['periode'],
                "kode_pegawai"=> $datas['kode_pegawai'],
                "peran"=> $datas['peran'],
                "capaian"=> $datas['capaian'],
                "volume"=> $datas['volume'],
                "dokumen_pendukung"=> $datas['dokumen_pendukung'],
                "poin_remlink"=> $datas['poin_remlink'],
                "tgl_nonaktif"=> $datas['tgl_nonaktif'],
                "tipe"=> $datas['tipe'],
                "rumus" => "yes"
            ];
            array_push($datass, $output);
        }
        return $datass;
    }

    public function lhkasnLhkpnPegawaiPns(){
        return $this->db->query("SELECT 
        periode_remun as periode, 
        kode_peg as kode_pegawai, 
        kode_peran as peran, 
        persen_lhkpn as capaian,
        CAST(NULL as char) as volume,
        CAST(NULL as char) as dokumen_pendukung,
        simremlink_data_peran.ewkp as poin_remlink,
        CAST(null as char) as tgl_nonaktif,
        CAST(null as char) as tipe,
        CAST('yes' as char) as rumus
        FROM simpeg_lhkasn_lhkpn_pegawai_pns
        JOIN simremlink_data_peran ON (simremlink_data_peran.kdeperan = simpeg_lhkasn_lhkpn_pegawai_pns.kode_peran)");
    }

    public function hukumanDisiplinPegawaiPns(){
        return $this->db->query("SELECT 
        periode_remun as periode, 
        kode_peg as kode_pegawai, 
        kode_peran as peran, 
        persen_hukdis as capaian,
        CAST(NULL as char) as volume,
        CAST(NULL as char) as dokumen_pendukung,
        CAST(simremlink_data_peran.ewkp as char) as poin_remlink,
        CAST(null as char) as tgl_nonaktif,
        CAST(null as char) as tipe,
        CAST('yes' as char) as rumus
        FROM simpeg_hukuman_disiplin_pegawai_pns
        JOIN simremlink_data_peran ON (simremlink_data_peran.kdeperan = simpeg_hukuman_disiplin_pegawai_pns.kode_peran)");
    }

    public function aktivitas(){
        return $this->db->query("SELECT 
        periode, 
        kode_pegawai, 
        peran, 
        capaian,
        volume,
        dokumen_pendukung,
        judul,
        CAST(simremlink_data_peran.ewkp as char) as poin_remlink,
        CAST(null as char) as tgl_nonaktif,
        CAST(null as char) as tipe
        FROM esk_aktivitas
        JOIN simremlink_data_peran ON (simremlink_data_peran.kdeperan = esk_aktivitas.peran)");
    }

    //===========================
    public function capaianSkpDosenDtKonversi(){
        return $this->db->query("SELECT 
        periode, 
        KODE_PEG as kode_pegawai, 
        CAST(NULL as char) as volume,
        CAST(NULL as char) as dokumen_pendukung,
        CASE
        WHEN simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns.grade_tugastambahan = 17 THEN 10302
        WHEN simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns.grade_tugastambahan = 16 THEN 10302
        WHEN simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns.grade_tugastambahan = 15 THEN 10303
        WHEN simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns.grade_tugastambahan = 14 THEN 10303
        WHEN simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns.grade_tugastambahan = 13 THEN 10304
        WHEN simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns.grade_tugastambahan = 12 THEN 10304
        WHEN simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns.grade_tugastambahan = 11 THEN 10305
        WHEN simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns.grade_tugastambahan = 10 THEN 10305
        END AS peran,
        CASE
        WHEN ( capaian >= 85 ) THEN 100
        WHEN ( capaian >= 80 AND capaian <= 84.99 ) THEN 90
        WHEN ( capaian >= 75 AND capaian <= 79.99 ) THEN 80
        WHEN ( capaian >= 70 AND capaian <= 74.99 ) THEN 70
        WHEN ( capaian >= 65 AND capaian <= 69.99 ) THEN 60
        WHEN capaian <= 64.99  THEN 50
        END AS capaian,
        CAST('njir' as char) as poin_remlink,
        CAST('yes' as char) as rumus,
        simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns.tgl_nonaktif,
        CAST('skp dosen' as char) as tipe,
        simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns.nama_tugastambahan
        FROM simpeg_capaian_skp_dosen
        JOIN simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns ON (simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns.nip = simpeg_capaian_skp_dosen.nipbaru)
        WHERE simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns.status_jabatan_utama = 'Y' AND simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns.status_aktif = 'Y' ORDER BY tmt_sk DESC ");
    }

    public function capaianSkpPegawaiKonversi(){
        return $this->db->query("SELECT 
        periode, 
        kode_peg as kode_pegawai, 
        kode_peran as peran, 
        CASE
        WHEN ( capaian >= 85 ) THEN 100
        WHEN ( capaian >= 80 AND capaian <= 84.99 ) THEN 90
        WHEN ( capaian >= 75 AND capaian <= 79.99 ) THEN 80
        WHEN ( capaian >= 70 AND capaian <= 74.99 ) THEN 70
        WHEN ( capaian >= 65 AND capaian <= 69.99 ) THEN 60
        WHEN capaian <= 64.99  THEN 50
        END AS capaian,
        CAST(NULL as char) as volume,
        CAST(NULL as char) as dokumen_pendukung,
        CAST('njir' as char) as poin_remlink,
        CAST(null as char) as tgl_nonaktif,
        CAST(null as char) as tipe,
        CAST('yes' as char) as rumus
        FROM simpeg_capaian_skp_pegawai");
    }

    public function nilaiSikapDanPerilakuPegawaiKonversi(){
        return $this->db->query("SELECT 
        periode, 
        kode_peg as kode_pegawai, 
        kode_peran as peran, 
        CASE
        WHEN ( capaian >= 85 ) THEN 100
        WHEN ( capaian >= 80 AND capaian <= 84.99 ) THEN 90
        WHEN ( capaian >= 75 AND capaian <= 79.99 ) THEN 80
        WHEN ( capaian >= 70 AND capaian <= 74.99 ) THEN 70
        WHEN ( capaian >= 65 AND capaian <= 69.99 ) THEN 60
        WHEN capaian <= 64.99  THEN 50
        END AS capaian,
        CAST(NULL as char) as volume,
        CAST(NULL as char) as dokumen_pendukung,
        CAST('njir' as char) as poin_remlink,
        CAST(null as char) as tgl_nonaktif,
        CAST(null as char) as tipe,
        CAST('yes' as char) as rumus
        FROM `simpeg_nilai_sikap_dan_perilaku_pegawai`");
    }

    public function penghargaan(){
        return $this->db->query("SELECT 
        periode, 
        kode_pegawai, 
        peran, 
        capaian,
        volume,
        dokumen_pendukung,
        CAST(simremlink_data_peran.ewkp as char) as poin_remlink,
        CAST(null as char) as tgl_nonaktif,
        CAST(null as char) as tipe
        FROM esk_penghargaan
        JOIN simremlink_data_peran ON (simremlink_data_peran.kdeperan = esk_penghargaan.peran)");
    }

}