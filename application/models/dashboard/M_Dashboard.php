<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_Dashboard extends CI_Model{
    public function __construct(){
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }

    public function refreshData(){
        $this->db->trans_begin();
        //akad
        $this->db->truncate('akad_kelebihan_mengajar');
        $this->db->truncate('akad_pembimbing_akademik');
        $this->db->truncate('akad_pembimbing_skripsi_tesis_disertasi');
        $this->db->truncate('akad_penguji_skripsi_tesis_disertasi');
        $this->db->truncate('akad_mengajar_lainnya');
        
        //simpeg
        $this->db->truncate('simpeg_bagian');
        $this->db->truncate('simpeg_dosen_phl');
        $this->db->truncate('simpeg_dosen_pns');
        $this->db->truncate('simpeg_dosen_pns_simremlink');
        $this->db->truncate('simpeg_golongan_dosen_pegawai');
        $this->db->truncate('simpeg_jabatan_fungsional_dosen');
        $this->db->truncate('simpeg_jenis_tugas_tambahan_dosen');
        $this->db->truncate('simpeg_jurusan');
        $this->db->truncate('simpeg_kategori_jabatan_remun_pegawai_pns');
        $this->db->truncate('simpeg_kedudukan_dosen_pegawai');
        $this->db->truncate('simpeg_pegawai_phl');
        $this->db->truncate('simpeg_pegawai_pns');
        $this->db->truncate('simpeg_pegawai_pns_simremlink');
        $this->db->truncate('simpeg_prodi');
        $this->db->truncate('simpeg_riwayat_jabatan_fungsional');
        $this->db->truncate('simpeg_riwayat_jabatan_remun_dosen_pns');
        $this->db->truncate('simpeg_riwayat_jabatan_remun_pegawai_pns');
        $this->db->truncate('simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns');
        $this->db->truncate('simpeg_riwayat_penghargaan_dosen_pns');
        $this->db->truncate('simpeg_riwayat_penghargaan_pegawai_pns');
        $this->db->truncate('simpeg_status');
        $this->db->truncate('simpeg_status_aktif_non_aktif');
        $this->db->truncate('simpeg_unit');
        $this->db->truncate('simpeg_capaian_skp_dosen');
        $this->db->truncate('simpeg_capaian_skp_pegawai');
        $this->db->truncate('simpeg_nilai_sikap_dan_perilaku_pegawai');

        //e-sk
        $this->db->truncate('esk_aktivitas');

        //lppm
        $this->db->truncate('lppm_penelitian');
        $this->db->truncate('lppm_pengabdian');
        $this->db->truncate('lppm_luaran_penelitian_mandiri');
        $this->db->truncate('lppm_luaran_pengabdian_mandiri');
        $this->db->truncate('lppm_data_submission_penelitian');
        $this->db->truncate('lppm_data_submission_penelitian_anggota');
        $this->db->truncate('lppm_data_submission_penelitian_ketua');
        $this->db->truncate('lppm_data_submission_penelitian_luaran');
        $this->db->truncate('lppm_luaran_valid_tambahan');
        $this->db->truncate('lppm_luaran_valid_wajib');
        $this->db->truncate('lppm_jenis_luaran');
        $this->db->truncate('lppm_skim');
        $this->db->truncate('lppm_status_penulis');
        $this->db->truncate('lppm_sumber_dana');
        $this->db->truncate('lppm_luaran_validasi');
        $this->db->truncate('lppm_luaran_validasi_lainnya');
        $this->db->truncate('lppm_luaran_validasi_pengabdian');

        //simremlink
        $this->db->truncate('simremlink_data_aktivitas');
        $this->db->truncate('simremlink_data_golongan_pns');
        $this->db->truncate('simremlink_data_grade');
        $this->db->truncate('simremlink_data_jabatan_akad');
        $this->db->truncate('simremlink_data_jenis_dokumen');
        $this->db->truncate('simremlink_data_jenis_pegawai');
        $this->db->truncate('simremlink_data_jenjang_pendidikan');
        $this->db->truncate('simremlink_data_kdn');
        $this->db->truncate('simremlink_data_kelas_jabatan');
        $this->db->truncate('simremlink_data_kelompok_jabatan');
        $this->db->truncate('simremlink_data_nama_jabatan');
        $this->db->truncate('simremlink_data_pekerjaan');
        $this->db->truncate('simremlink_data_peran');
        $this->db->truncate('simremlink_data_periode');
        $this->db->truncate('simremlink_data_rekening');
        $this->db->truncate('simremlink_data_status_pegawai');
        $this->db->truncate('simremlink_data_status_sipil');

        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );  
        
        //================== akad ==================
        //kelebihan mengajar
        $akadLinkKelebihanMengajar = "https://bak.unimed.ac.id/apiajar/tr_lebih2.php";
        $akadKelebihanMengajar = json_decode(file_get_contents($akadLinkKelebihanMengajar,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('akad_kelebihan_mengajar', $akadKelebihanMengajar);
        //pembimbing akademik
        $akadLinkKPembimbingAkademik = "https://bak.unimed.ac.id/apiajar/jsonbak.php?cek=pa";
        $akadKPembimbingAkademik = json_decode(file_get_contents($akadLinkKPembimbingAkademik,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('akad_pembimbing_akademik', $akadKPembimbingAkademik);
        //pembimbing skripsi tesis disertasi
        $akadLinkKPembimbingSkripsiTesisDisertasi = "https://bak.unimed.ac.id/apiajar/jsonbak.php?hal=pg";
        $akadKPembimbingSkripsiTesisDisertasi = json_decode(file_get_contents($akadLinkKPembimbingSkripsiTesisDisertasi,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('akad_pembimbing_skripsi_tesis_disertasi', $akadKPembimbingSkripsiTesisDisertasi);
        //penguji skripsi tesis disertasi
        $akadLinkKpengujiSkripsiTesisDisertasi = "https://bak.unimed.ac.id/apiajar/jsonbak.php?hal=pu";
        $akadKpengujiSkripsiTesisDisertasi = json_decode(file_get_contents($akadLinkKpengujiSkripsiTesisDisertasi,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('akad_penguji_skripsi_tesis_disertasi', $akadKpengujiSkripsiTesisDisertasi);
        // mengajar lainnya
        $simpegLinkMengajarLainnya = "https://bak.unimed.ac.id/apiajar/jsondos.php?hal=pp";
        $simpegMengajarLainnya = json_decode(file_get_contents($simpegLinkMengajarLainnya,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('akad_mengajar_lainnya', $simpegMengajarLainnya);
        //================== end akad ==================

        //================== simpeg ==================
        // bagian
        $simpegLinkBagian = "https://simpeg.unimed.ac.id/jsonsimpeg/data/unit_bagian.php?api_key=08c24cee598007e14e5ba4284b4d1aa7";
        $simpegBagian = json_decode(file_get_contents($simpegLinkBagian,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_bagian', $simpegBagian);
        // dosen phl
        $simpegLinkDosenPhl = "https://simpeg.unimed.ac.id/jsonsimpeg/data/tdosenphl.php?api_key=053f210055e44647a51b2a2d8282e6b2";
        $simpegDosenPhl = json_decode(file_get_contents($simpegLinkDosenPhl,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_dosen_phl', $simpegDosenPhl);
        // dosen pns
        $simpegLinkDosenPns = "https://simpeg.unimed.ac.id/jsonsimpeg/data/tdosen.php?api_key=e807f1fcf82d132f9bb018ca6738a19f";
        $simpegDosenPns = json_decode(file_get_contents($simpegLinkDosenPns,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_dosen_pns', $simpegDosenPns);
        // dosen pns simremlink
        $simpegLinkDosenPnsSimremlink = "https://simpeg.unimed.ac.id/jsonsimpeg/data/dosensimremlink.php?api_key=922c8c46fcecee6340ac2d0790e68be8";
        $simpegDosenPnsSimremlink = json_decode(file_get_contents($simpegLinkDosenPnsSimremlink,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_dosen_pns_simremlink', $simpegDosenPnsSimremlink);
        // golongan
        $simpegLinkGolongan = "https://simpeg.unimed.ac.id/jsonsimpeg/data/golongan.php?api_key=732ad3502ece5dc896b916b9b2e4db35";
        $simpegGolongan = json_decode(file_get_contents($simpegLinkGolongan,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_golongan_dosen_pegawai', $simpegGolongan);
        // jabatan
        $simpegLinkJabatan = "https://simpeg.unimed.ac.id/jsonsimpeg/data/jabatan_fungsional.php?api_key=7c950102d7f3d7f54a459a18b9896013";
        $simpegJabatan = json_decode(file_get_contents($simpegLinkJabatan,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_jabatan_fungsional_dosen', $simpegJabatan);
        // tugas tambahan
        $simpegLinkTugasTambahan = "https://simpeg.unimed.ac.id/jsonsimpeg/data/tugas_tambahan.php?api_key=dd7db5d1c2dd1eff113571d530ba1a90";
        $simpegTugasTambahan = json_decode(file_get_contents($simpegLinkTugasTambahan,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_jenis_tugas_tambahan_dosen', $simpegTugasTambahan);
        // jurusan
        $simpegLinkJurusan = "https://simpeg.unimed.ac.id/jsonsimpeg/data/jurusan.php?api_key=a409bb11ce3545bf46c17f2f96f36770";
        $simpegJurusan = json_decode(file_get_contents($simpegLinkJurusan,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_jurusan', $simpegJurusan);
        // kategori jabatan remun pegawai pns
        $simpegLinkKategoriJabatanRemunPegawaiPns = "https://simpeg.unimed.ac.id/jsonsimpeg/data/jab_remun.php?api_key=8c10e60c19e33a8b86698e88744915da";
        $simpegKategoriJabatanRemunPegawaiPns = json_decode(file_get_contents($simpegLinkKategoriJabatanRemunPegawaiPns,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_kategori_jabatan_remun_pegawai_pns', $simpegKategoriJabatanRemunPegawaiPns);
        // kedudukan
        $simpegLinkKedudukan = "https://simpeg.unimed.ac.id/jsonsimpeg/data/kedudukan.php?api_key=a7bcce93ba39a8614bc17f9528f817d1";
        $simpegKedudukan = json_decode(file_get_contents($simpegLinkKedudukan,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_kedudukan_dosen_pegawai', $simpegKedudukan);
        // pegawai phl
        $simpegLinPegawaiPhl = "https://simpeg.unimed.ac.id/jsonsimpeg/data/tpegawaiphl.php?api_key=83404a861e0215e0f6787aee808019c1";
        $simpegPegawaiPhl = json_decode(file_get_contents($simpegLinPegawaiPhl,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_pegawai_phl', $simpegPegawaiPhl);
        // pegawai pns
        $simpegLinkPegawaiPns = "https://simpeg.unimed.ac.id/jsonsimpeg/data/tpegawai.php?api_key=bb31ba2d3b18e743dc35c1aed6d80774";
        $simpegPegawaiPns = json_decode(file_get_contents($simpegLinkPegawaiPns,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_pegawai_pns', $simpegPegawaiPns);
        // pegawai pns simremlink
        $simpegLinkPegawaiPnsSimremlink = "https://simpeg.unimed.ac.id/jsonsimpeg/data/pegawaipns_simremlink.php?api_key=5121a251c84c06ebdfb10afdacf58f05";
        $simpegPegawaiPnsSimremlink = json_decode(file_get_contents($simpegLinkPegawaiPnsSimremlink,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_pegawai_pns_simremlink', $simpegPegawaiPnsSimremlink);
        // riwayat jabatan fungsional tertentu dosen
        $simpegLinkRiwayatJabatanFungsionalTertentuDosen = "https://simpeg.unimed.ac.id/jsonsimpeg/data/jabatan_jft_dosen.php?api_key=c645ad9b6432aa39be6551983be6c61c";
        $simpegRiwayatJabatanFungsionalTertentuDosen = json_decode(file_get_contents($simpegLinkRiwayatJabatanFungsionalTertentuDosen,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_riwayat_jabatan_fungsional', $simpegRiwayatJabatanFungsionalTertentuDosen);
        // riwayat jabatan remun dosen
        $simpegLinkRiwayatJabatanRemunDosenPns = "https://simpeg.unimed.ac.id/jsonsimpeg/data/jabatan_remun_dosen.php?api_key=2879b5e37589f43610663c95c7aa8dfa";
        $simpegRiwayatJabatanRemunDosenPns = json_decode(file_get_contents($simpegLinkRiwayatJabatanRemunDosenPns,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_riwayat_jabatan_remun_dosen_pns', $simpegRiwayatJabatanRemunDosenPns);
        // riwayat jabatan remun pegawai
        $simpegLinkRiwayatJabatanRemunPegawaiPns = "https://simpeg.unimed.ac.id/jsonsimpeg/data/rjabatan_remun_pegawai.php?api_key=33d78bd918e6eed6200dcfdae48200b5";
        $simpegRiwayatJabatanRemunPegawaiPns = json_decode(file_get_contents($simpegLinkRiwayatJabatanRemunPegawaiPns,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_riwayat_jabatan_remun_pegawai_pns', $simpegRiwayatJabatanRemunPegawaiPns);
        // riwayat jabatan tugas tambahan dosen pns
        $simpegLinkRiwayatJabatanRemunDosenPns = "https://simpeg.unimed.ac.id/jsonsimpeg/data/rtugas_tambahan_dosen.php?api_key=e244209c6fc4d7a335501963d07a4021";
        $simpegRiwayatJabatanRemunDosenPns = json_decode(file_get_contents($simpegLinkRiwayatJabatanRemunDosenPns,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns', $simpegRiwayatJabatanRemunDosenPns);
        // riwayat penghargaan dosen pns
        $simpegLinkRiwayatJPenghargaanDosenPns = "https://simpeg.unimed.ac.id/jsonsimpeg/data/r_penghargaan_dosen.php?api_key=039372cf8973d827f35032f382e52fbc";
        $simpegRiwayatJPenghargaanDosenPns = json_decode(file_get_contents($simpegLinkRiwayatJPenghargaanDosenPns,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_riwayat_penghargaan_dosen_pns', $simpegRiwayatJPenghargaanDosenPns);
        // riwayat penghargaan pegawai pns
        $simpegLinkRiwayatPenghargaanPegawaiPns = "https://simpeg.unimed.ac.id/jsonsimpeg/data/r_penghargaan_pegawai.php?api_key=2465f2b2ab4414d12cc764f101ce31b6";
        $simpegRiwayatPenghargaanPegawaiPns = json_decode(file_get_contents($simpegLinkRiwayatPenghargaanPegawaiPns,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_riwayat_penghargaan_pegawai_pns', $simpegRiwayatPenghargaanPegawaiPns);
        // prodi
        $simpegLinkProdi = "https://simpeg.unimed.ac.id/jsonsimpeg/data/prodi.php?api_key=90a860e51ed04a1c8ac0e79a0e82cb8d";
        $simpegProdi = json_decode(file_get_contents($simpegLinkProdi,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_prodi', $simpegProdi);
        // status
        $simpegLinkStatus = "https://simpeg.unimed.ac.id/jsonsimpeg/data/status_pegawai.php?api_key=32082e6f1cb84c159520806286dfd96f";
        $simpegStatus = json_decode(file_get_contents($simpegLinkStatus,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_status', $simpegStatus);
        // aktif non aktif
        $simpegLinkAktifNonAktif = "https://simpeg.unimed.ac.id/jsonsimpeg/data/status_aktif.php?api_key=7858b904c37985bfa23e2083f4c5478b";
        $simpegAktifNonAktif = json_decode(file_get_contents($simpegLinkAktifNonAktif,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_status_aktif_non_aktif', $simpegAktifNonAktif);
        // unit
        $simpegLinkUnit = "https://simpeg.unimed.ac.id/jsonsimpeg/data/unit.php?api_key=83b956f064c53aca3155c93e2aecd2e7";
        $simpegUnit = json_decode(file_get_contents($simpegLinkUnit,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_unit', $simpegUnit);
        // capaian skp dosen
        $simpegLinkCapaianSkpDosen = "https://simpeg.unimed.ac.id/jsonsimpeg/data/skp_capaian_dosen.php?api_key=d4fde9382eda0a35bd81fcbdf5594d4b";
        $simpegCapaianSkpDosen = json_decode(file_get_contents($simpegLinkCapaianSkpDosen,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_capaian_skp_dosen', $simpegCapaianSkpDosen);
        // capaian skp pegawai
        $simpegLinkCapaianSkpPegawai = "https://simpeg.unimed.ac.id/jsonsimpeg/data/skp_capaian_pegawai.php?api_key=193220653d34edffb6ace45748c856aa";
        $simpegCapaianSkpPegawai = json_decode(file_get_contents($simpegLinkCapaianSkpPegawai,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_capaian_skp_pegawai', $simpegCapaianSkpPegawai);
        // nilai sikap dan perilaku pegawai
        $simpegLinkNilaiSikapDanPerilakuPegawai = "https://simpeg.unimed.ac.id/jsonsimpeg/data/skp_capaian_pegawai_pk.php?api_key=c8673e28ac7b45a04daceb94eac9b912";
        $simpegNilaiSikapDanPerilakuPegawai = json_decode(file_get_contents($simpegLinkNilaiSikapDanPerilakuPegawai,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_nilai_sikap_dan_perilaku_pegawai', $simpegNilaiSikapDanPerilakuPegawai);
        //================== end akad ==================

        //================== e-sk ==================
        //http://dev.unimed.ac.id/e-SK/api/aktifitas
        $eskLinkAktivitas = "https://e-sk.unimed.ac.id/api/aktifitas";
        $eskAktivitas = json_decode(file_get_contents($eskLinkAktivitas,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('esk_aktivitas', $eskAktivitas);
        //================== end e-sk ==================

        //================== lppm ==================
        // penelitian
        $lppmLinkPenelitian = "https://lppm.unimed.ac.id/simppm/jsonpenelitian.php";
        $lppmPenelitian = json_decode(file_get_contents($lppmLinkPenelitian,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('lppm_penelitian', $lppmPenelitian);
        // pengabdian
        $lppmLinkPengabdian = "https://lppm.unimed.ac.id/simppm/jsonpengabdian.php";
        $lppmPengabdian = json_decode(file_get_contents($lppmLinkPengabdian,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('lppm_pengabdian', $lppmPengabdian);
        // luaran penelitian mandiri
        $lppmLinkLuaranPenelitianMandiri = "https://lppm.unimed.ac.id/simppm/jsonpenelitianmandiri.php";
        $lppmLuaranPenelitianMandiri = json_decode(file_get_contents($lppmLinkLuaranPenelitianMandiri,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('lppm_luaran_penelitian_mandiri', $lppmLuaranPenelitianMandiri);
        // luaran pengabdian mandiri
        $lppmLinkLuaranPengabdianMandiri = "https://lppm.unimed.ac.id/simppm/jsonpengabdianmandiri.php";
        $lppmKLuaranPengabdianMandiri = json_decode(file_get_contents($lppmLinkLuaranPengabdianMandiri,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('lppm_luaran_pengabdian_mandiri', $lppmKLuaranPengabdianMandiri);
        // data submission penelitian
        $lppmLinkLuaranDataSubmissionPenelitian = "https://lppm.unimed.ac.id/simppm2/data-submission-penelitian";
        $lppmKLuaranDataSubmissionPenelitian = json_decode(file_get_contents($lppmLinkLuaranDataSubmissionPenelitian,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('lppm_data_submission_penelitian', $lppmKLuaranDataSubmissionPenelitian);
        // data submission penelitian anggota
        $lppmLinkLuaranDataSubmissionPenelitianAnggota = "https://lppm.unimed.ac.id/simppm2/data-submission-penelitian-anggota";
        $lppmKLuaranDataSubmissionPenelitianAnggota = json_decode(file_get_contents($lppmLinkLuaranDataSubmissionPenelitianAnggota,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('lppm_data_submission_penelitian_anggota', $lppmKLuaranDataSubmissionPenelitianAnggota);
        // data submission penelitian ketua
        $lppmLinkDataSubmissionPenelitianKetua = "https://lppm.unimed.ac.id/simppm2/data-submission-penelitian-ketua";
        $lppmKDataSubmissionPenelitianKetua = json_decode(file_get_contents($lppmLinkDataSubmissionPenelitianKetua,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('lppm_data_submission_penelitian_ketua', $lppmKDataSubmissionPenelitianKetua);
        // data submission penelitian luaran
        $lppmLinkDataSubmissionPenelitianLuaran = "https://lppm.unimed.ac.id/simppm2/data-submission-penelitian-luaran";
        $lppmKDataSubmissionPenelitianLuaran = json_decode(file_get_contents($lppmLinkDataSubmissionPenelitianLuaran,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('lppm_data_submission_penelitian_luaran', $lppmKDataSubmissionPenelitianLuaran);
        // luaran valid tambahan
        $lppmLinkLuaranValidTambahan = "https://lppm.unimed.ac.id/simppm2/luaran-valid-tambahan";
        $lppmKLuaranValidTambahan = json_decode(file_get_contents($lppmLinkLuaranValidTambahan,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('lppm_luaran_valid_tambahan', $lppmKLuaranValidTambahan);
        // luaran valid wajib
        $lppmLinkLuaranWajibTambahan = "https://lppm.unimed.ac.id/simppm2/luaran-valid-wajib";
        $lppmKLuaranWajibTambahan = json_decode(file_get_contents($lppmLinkLuaranWajibTambahan,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('lppm_luaran_valid_wajib', $lppmKLuaranWajibTambahan);
        // jenis luaran
        $lppmLinkJenisLuaran = "https://lppm.unimed.ac.id/simppm2/jenis-luaran";
        $lppmKJenisLuaran = json_decode(file_get_contents($lppmLinkJenisLuaran,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('lppm_jenis_luaran', $lppmKJenisLuaran);
        // skim
        $lppmLinkSkim = "https://lppm.unimed.ac.id/simppm2/skim";
        $lppmKSkim = json_decode(file_get_contents($lppmLinkSkim,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('lppm_skim', $lppmKSkim);
        // status penulis
        $lppmLinkStatusPenulis = "https://lppm.unimed.ac.id/simppm2/status-penulis";
        $lppmKStatusPenulis = json_decode(file_get_contents($lppmLinkStatusPenulis,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('lppm_status_penulis', $lppmKStatusPenulis);
        // sumber dana
        $lppmLinkSumberDana = "https://lppm.unimed.ac.id/simppm2/sumberdana";
        $lppmKSumberDana = json_decode(file_get_contents($lppmLinkSumberDana,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('lppm_sumber_dana', $lppmKSumberDana);
        // luaran validasi
        $lppmLinkLuaranValidasi = "https://lppm.unimed.ac.id/simppm2/luaran-validasi";
        $lppmKLuaranValidasi = json_decode(file_get_contents($lppmLinkLuaranValidasi,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('lppm_luaran_validasi', $lppmKLuaranValidasi);
        // luaran validasi lainnya
        $lppmLinkLuaranValidasiLainnya = "https://lppm.unimed.ac.id/simppm2/luaran-validasi-lainnya";
        $lppmKLuaranValidasiLainnya = json_decode(file_get_contents($lppmLinkLuaranValidasiLainnya,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('lppm_luaran_validasi_lainnya', $lppmKLuaranValidasiLainnya);
        // luaran validasi pengabdian
        $lppmLinkLuaranValidasiPengabdian = "https://lppm.unimed.ac.id/simppm2/luaran-validasi-pengabdian";
        $lppmKLuaranValidasiPengabdian = json_decode(file_get_contents($lppmLinkLuaranValidasiPengabdian,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('lppm_luaran_validasi_pengabdian', $lppmKLuaranValidasiPengabdian);
        //================== end lppm ==================

        //================== simremlink ==================
        // aktivitas
        $simremlinkLinkAktivitas = "http://dev.unimed.ac.id/sso-api/simremlinkjwt/data-aktivitas-new2";
        $simremlinkAktivitas = json_decode(file_get_contents($simremlinkLinkAktivitas,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simremlink_data_aktivitas', $simremlinkAktivitas);
        // golongan
        $simremlinkLinkGolongan = "http://dev.unimed.ac.id/sso-api/simremlinkjwt/data-golongan-pns";
        $simremlinkGolongan = json_decode(file_get_contents($simremlinkLinkGolongan,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simremlink_data_golongan_pns', $simremlinkGolongan);
        // grade
        $simremlinkLinkGrade = "http://dev.unimed.ac.id/sso-api/simremlinkjwt/data-grade";
        $sirmemlinkGrade = json_decode(file_get_contents($simremlinkLinkGrade,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simremlink_data_grade', $sirmemlinkGrade);
        // jabatan akad
        $simremlinkLinJabatanAkad = "http://dev.unimed.ac.id/sso-api/simremlinkjwt/data-jabatan-akad";
        $simremlinkJabatanAkad = json_decode(file_get_contents($simremlinkLinJabatanAkad,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simremlink_data_jabatan_akad', $simremlinkJabatanAkad);
        // dokumen
        $simremlinkLinkDokumen = "http://dev.unimed.ac.id/sso-api/simremlinkjwt/data-jenis-dokumen";
        $simremlinkDokumen = json_decode(file_get_contents($simremlinkLinkDokumen,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simremlink_data_jenis_dokumen', $simremlinkDokumen);
        // pegawai
        $simremlinkLinkPegawai = "http://dev.unimed.ac.id/sso-api/simremlinkjwt/data-jenis-pegawai";
        $simremlinkPegawai = json_decode(file_get_contents($simremlinkLinkPegawai,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simremlink_data_jenis_pegawai', $simremlinkPegawai);
        // pendidikan
        $simremlinkLinkPendidikan = "http://dev.unimed.ac.id/sso-api/simremlinkjwt/data-jenjang-pendidikan";
        $simremlinkPendidikan = json_decode(file_get_contents($simremlinkLinkPendidikan,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simremlink_data_jenjang_pendidikan', $simremlinkPendidikan);
        // kdn
        $simremlinkLinkKdn = "http://dev.unimed.ac.id/sso-api/simremlinkjwt/data-kdn";
        $simremlinkKdn = json_decode(file_get_contents($simremlinkLinkKdn,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simremlink_data_kdn', $simremlinkKdn);
        // kelas jabatan
        $simremlinkLinkKelasJabatan = "http://dev.unimed.ac.id/sso-api/simremlinkjwt/data-kelas-jabatan";
        $simremlinkKelasJabatan = json_decode(file_get_contents($simremlinkLinkKelasJabatan,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simremlink_data_kelas_jabatan', $simremlinkKelasJabatan);
        // kelomok jabatan
        $simremlinkLinkKelompokJabatan = "http://dev.unimed.ac.id/sso-api/simremlinkjwt/data-kelompok-jabatan";
        $simremlinkKelompokJabatan = json_decode(file_get_contents($simremlinkLinkKelompokJabatan,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simremlink_data_kelompok_jabatan', $simremlinkKelompokJabatan);
        // nama jabatan
        $simremlinkLinkNamaJabatan = "http://dev.unimed.ac.id/sso-api/simremlinkjwt/data-nama-jabatan";
        $simremlinkNamaJabatan = json_decode(file_get_contents($simremlinkLinkNamaJabatan,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simremlink_data_nama_jabatan', $simremlinkNamaJabatan);
        // pekerjaan
        $simremlinkLinkPekerjaan = "http://dev.unimed.ac.id/sso-api/simremlinkjwt/data-pekerjaan";
        $simremlinkPekerjaan = json_decode(file_get_contents($simremlinkLinkPekerjaan,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simremlink_data_pekerjaan', $simremlinkPekerjaan);
        // peran
        $simremlinkLinkPeran = "http://dev.unimed.ac.id/sso-api/simremlinkjwt/data-peran-new2";
        $simremlinkPeran = json_decode(file_get_contents($simremlinkLinkPeran,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simremlink_data_peran', $simremlinkPeran);
        // periode
        $simremlinkLinkPeriode = "http://dev.unimed.ac.id/sso-api/simremlinkjwt/data-periode2";
        $simremlinkPeriode = json_decode(file_get_contents($simremlinkLinkPeriode,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simremlink_data_periode', $simremlinkPeriode);
        // rekening
        $simremlinkLinkRekening = "http://dev.unimed.ac.id/sso-api/simremlinkjwt/data-rekening";
        $simremlinkRekening = json_decode(file_get_contents($simremlinkLinkRekening,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simremlink_data_rekening', $simremlinkRekening);
        // status pegawai
        $simremlinkLinkStatusPegawai = "http://dev.unimed.ac.id/sso-api/simremlinkjwt/data-status-pegawai";
        $simremlinkStatusPegawai = json_decode(file_get_contents($simremlinkLinkStatusPegawai,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simremlink_data_status_pegawai', $simremlinkStatusPegawai);
        // status sipil
        $simremlinkLinkStatusSipil = "http://dev.unimed.ac.id/sso-api/simremlinkjwt/data-status-sipil";
        $simremlinkStatusSipil = json_decode(file_get_contents($simremlinkLinkStatusSipil,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simremlink_data_status_sipil', $simremlinkStatusSipil);
        //================== end simremlink ==================

        if($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return FALSE;
		}else{
			$this->db->trans_commit();
			return TRUE;
		}
	}
}