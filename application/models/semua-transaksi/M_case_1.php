<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_case_1 extends CI_Model{
    public function __construct(){
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
        error_reporting(0);
    }

    /**
     * Cek perpindahan riwayat jabatan tugas tambahan dosen (DT)
     */
    

    public function createTableView($periode_ke){
        $tahun = date("Y");
        $awal_periode = null;
        $akhir_periode = null;
        $mulai = null;
        $berakhir = null;
        $periode_ke2 = null;
        if($periode_ke == "1"){
            $awal_periode = "-01-01";
            $akhir_periode = "-06-30";
            $mulai = $tahun . $awal_periode;
            $berakhir = $tahun . $akhir_periode;
            $periode_ke2 = "1";
        }else{
            $awal_periode = "-07-01";
            $akhir_periode = "-12-31";
            $mulai = $tahun . $awal_periode;
            $berakhir = $tahun . $akhir_periode;
            $periode_ke2 = "2";
        };
        $query = "CREATE VIEW transaksi_v_riwayat_jabatan_tugas_tambahan_dosen_$tahun$periode_ke2 AS 
                select 
                `simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns`.`uuid` AS `uuid`,
                `simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns`.`kode_dosen` AS `kode_dosen`,
                `simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns`.`nip` AS `nip`,
                `simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns`.`nama` AS `nama`,
                `simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns`.`kode_tugastambahan` AS `kode_tugastambahan`,
                `simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns`.`nama_tugastambahan` AS `nama_tugastambahan`,
                `simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns`.`status_aktif` AS `status_aktif`,
                `simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns`.`status_jabatan_utama` AS `status_jabatan_utama`,
                `simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns`.`no_sk` AS `no_sk`,
                `simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns`.`tgl_sk` AS `tgl_sk`,
                `simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns`.`tmt_sk` AS `tmt_sk`,
                `simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns`.`bukti_sk` AS `bukti_sk`,
                `simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns`.`grade_tugastambahan` AS `grade_tugastambahan`,
                `simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns`.`score_tugastambahan` AS `score_tugastambahan`,
                `simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns`.`tgl_nonaktif` AS `tgl_nonaktif`,
                case 
                when `simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns`.`tgl_nonaktif` > '$berakhir' then '$berakhir' 
                when `simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns`.`tgl_nonaktif` = '0000-00-00' then '$berakhir' 
                when `simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns`.`tgl_nonaktif` <= '$berakhir' then `simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns`.`tgl_nonaktif` end AS `tgl_nonaktif_2`,timestampdiff(MONTH,'$mulai',`simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns`.`tgl_nonaktif`) AS `durasi`,to_days('$mulai') - to_days(`simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns`.`tgl_nonaktif`) AS `hari` 
                from `simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns` 
                where `simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns`.`tgl_nonaktif` 
                between '$mulai' and '$berakhir' or `simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns`.`tgl_nonaktif` = '0000-00-00' or `simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns`.`tgl_nonaktif` > '$berakhir'";
        $this->db->query($query);
    }

    public function perpindahanDt($nip,$kode,$periode){
        $tahun = substr($periode, 0,4);
        $periode_ke = substr($periode, 4,5);
        $tgl_nonaktif = null;
        // if(!$this->db->table_exists('transaksi_v_riwayat_jabatan_tugas_tambahan_dosen_'.$tahun.$periode_ke)){
        //     $this->createTableView($periode_ke);
        // };
        if($periode_ke == '1'){
            $tgl_nonaktif = date_create($tahun . "-6-30");
        }else{
            $tgl_nonaktif = date_create($tahun . "-12-31");
        };
        $data = $this->db->select("
                            kode_dosen,
                            nip,
                            kode_tugastambahan,
                            nama_tugastambahan,
                            tmt_sk,
                            grade_tugastambahan,
                            score_tugastambahan,
                            tgl_nonaktif,
                            tgl_nonaktif_2,
                            durasi,
                            hari,
                            round(hari/30) AS fix_durasi")
                         ->order_by('tmt_sk','asc')
                         ->get_where('transaksi_v_riwayat_jabatan_tugas_tambahan_dosen_'.$tahun.$periode_ke,['nip'=>$nip, 'status_aktif' => 'Y', 'status_jabatan_utama' => 'Y'])
                         ->row();

        if($data->tgl_nonaktif > $tgl_nonaktif || $data->tgl_nonaktif == '0000-00-00'):
            // $tgl_nonaktif = date_create('2020-12-31');
            $tmt_sk = date_create($data->tmt_sk);
            $selisih = $tgl_nonaktif->diff($tmt_sk)->days + 1;
            $durasi = round($selisih/30);
        else:
            $durasi = str_replace('-','',$data->fix_durasi);
        endif;

        if($durasi > 6):
            $durasi = 6;
        else:
            $durasi = $durasi;
        endif;
        $res = [
            'kode_dosen'=> $data->kode_dosen,
            'nip'       => $data->nip,
            'kode'      => $data->kode_tugastambahan,
            'nama'      => $data->nama_tugastambahan,
            'tmt_sk'    => $data->tmt_sk,
            'grade'     => $data->grade_tugastambahan,
            'score'     => $data->score_tugastambahan,
            'durasi'    => $durasi,
            "jenis"     => 'DT',
            "urutan_case" => "1"
        ];
        return $res;
    }

    /**
     * Cek perpindahan riwayat jabatan fungsional dosen (DB)
     */
    public function perpindahanDb($nip,$kode){
        $data = $this->db->select("kode_dosen,nip,kode_jft, nama_jft,tmt_sk,grade_jft,score_jft,cast(6 as char) as durasi")
                        ->limit(1)
                        ->order_by('tmt_sk','desc')
                        ->get_where("simpeg_riwayat_jabatan_fungsional",['nip'=>$nip])
                        ->row();
        $res = [
            'kode_dosen'=> $data->kode_dosen,
            'nip'       => $data->nip,
            'kode'      => $data->kode_jft,
            'nama'      => $data->nama_jft,
            'tmt_sk'    => $data->tmt_sk,
            'grade'     => $data->grade_jft,
            'score'     => $data->score_jft,
            'durasi'    => $data->durasi,
            "jenis"     => 'DB',
            "urutan_case" => "1"
        ];
        return $res;
    }

    /**
     * Filter kondiisi dosen
     */
    public function execute($nip,$kode,$periode){
        $checkPerpindahanDB = $this->perpindahanDb($nip,$kode);
        $checkPerpindahanDt = $this->perpindahanDt($nip,$kode,$periode);
        $data = array($checkPerpindahanDt,$checkPerpindahanDB);
        return $data;
    }

    /**
     * Ekseskusi harga DT
     */
    public function executeHargaDt($nip,$kode,$periode){
        $tahun = substr($periode, 0,4);
        $periode_ke = substr($periode, 4,5);
        $tgl_nonaktif = null;
        if($periode_ke == '1'){
            $tgl_nonaktif = date_create($tahun . "-6-30");
        }else{
            $tgl_nonaktif = date_create($tahun . "-12-31");
        };
        $data_jabatan   = $this->db->select("score_tugastambahan")
                                  ->order_by("tmt_sk","desc")
                                  ->get_where("transaksi_v_riwayat_jabatan_tugas_tambahan_dosen_".$tahun.$periode_ke, ["nip"=>$nip,"status_aktif" => "Y", "status_jabatan_utama" => "Y"])
                                  ->row();
        $data_periode   = $this->db->query("SELECT * FROM simremlink_data_periode WHERE periode = '$periode' ")
                                  ->row_array();
        $harga_dt = ($data_jabatan->score_tugastambahan*1*$data_periode['pir']*6)/40; 
        $output = [
            "harga" => $harga_dt,
            "skor"  => $data_jabatan->score_tugastambahan,
            "urutan_case" => "1"
        ];
        return $output;
    }

    /**
     * Ekseskusi harga DB
     */
    public function executeHargaDb($nip,$kode,$periode){
        $data_periode = $this->db->query("SELECT * FROM simremlink_data_periode WHERE periode = '$periode' ")->row_array();
        $query    = $this->db->select("kode_dosen,nip,kode_jft, nama_jft,tmt_sk,grade_jft,score_jft,cast(6 as char) as durasi")
                             ->limit(1)
                             ->order_by('tmt_sk','desc')
                             ->get_where("simpeg_riwayat_jabatan_fungsional",['nip'=>$nip])
                             ->row();
        $harga_db = ($query->score_jft * 1 * $data_periode['pir'] * 6) / 40;
        $data     = [
            'harga' => $harga_db,
            "urutan_case" => "1"];
        return $data;
    }

    /**
     * Eksekusi Total Poin Remun
     */
    public function executeTotalPoinRemun($jenisDosen,$tupoksi,$mengajar,$mengajarLainnya,$penunjang,$penghargaan,$total_semua_poin,$poin_kinerja,$stts,$kode,$kode2){
        /**
         * Init poin
         */
        $p2DB       = 0;
        $uang_dt    = 0;
        $uang_p1_dt = 0;
        $uang_p2_dt = 0;
        $uang_p1_db = 0;
        $p2DT       = 0;
        
        $stts = 'DT';
            
        /**
         * menentuan poin kinerja
         */
        if($poin_kinerja > 108):
            $poin_kinerja = 108;
        else:
            $poin_kinerja = $poin_kinerja;
        endif;

        /**
         * penentuan poin P2DT
         */
        $p2DT = $poin_kinerja - 12;
        if($p2DT > 28):
          $p2DT = 28;
        else:
          $p2DT = $p2DT;
        endif;

        /**
         * proses perhitungan
         */
        $p2DB               = $poin_kinerja - ($p2DT + 12);

        
        $output = array(
            'total_poin'            => number_format((float)$total_semua_poin, 3),
            'poin_kinerja'          => number_format((float)$poin_kinerja, 2),
            'p2DT'                  => number_format((float)$p2DT, 2),
            'p2DB'                  => number_format((float)$p2DB, 2),
            'poinPenghargaan'       => number_format((float)$penghargaan, 2),
            'stts'                  => $stts,
            'jenis'                 => $jenisDosen,
            "urutan_case" => "1"
        );

        return $output;
    }

    /**
     * Ekseskusi uang remun
     */
    public function executeUangRemun($kode,$kode2,$pir,$p2DT,$penghargaan,$nip){
        /**
         * inisialisasi
         */
        $init   = $this->perpindahanDt($nip,$kode2);
        $durasi = $init['durasi'];
        $score  = $init['score'];
        
        /**
         * perhitungan
         */
        $hargaDT = ($score * 1 * $pir * 6) / 40;
        $uang_P1DT = $durasi / 6 * 12 * $hargaDT;
        $uang_P2DT = $durasi / 6 * $p2DT * $hargaDT;

        $output = [
                'score_tugastambahan'   => $score,
                'durasi'                => $durasi,
                'uang_P1'               => number_format((float)$uang_P1DT, 0),
                'uang_P2'               => number_format((float)$uang_P2DT, 0),
                'total_P1'              => number_format((float)$uang_P1DT, 0),
                'total_P2'              => number_format((float)$uang_P2DT, 0),
                "urutan_case" => "1"
               ];
        return $output;
    }

    public function executeUangRemunDB($kode,$kode2,$pir,$p2DT,$penghargaan,$nip, $poin_kinerja, $golongan){
        /**
         * inisialisasi
         */
        $initDT   = $this->perpindahanDt($nip,$kode2);
        $durasiDT = $initDT['durasi'];
        $scoreDT  = $initDT['score'];
        $grade    = $initDT['grade'];
        $jabatan  = $initDT['nama'];
        //----------------
        $init   = $this->perpindahanDb($nip,$kode2);
        $durasi = $init['durasi'];
        $score  = $init['score'];
        if($poin_kinerja > 108){
            $poin_kinerja = 108;
        }else{
            $poin_kinerja = $poin_kinerja;
        }
        $p2DB   = $poin_kinerja - ($p2DT + 12);    
        $hargaDT = ($scoreDT * 1 * $pir * 6) / 40;
        $uang_P2DT = $durasiDT / 6 * $p2DT * $hargaDT;    
        /**
         * perhitungan
         */
        $hargaDB = ($score * 1 * $pir * 6) / 40;
        $uang_P2DB = $durasi / 6 * $p2DB * $hargaDB;
        $uang_penghargaan   = $penghargaan * $hargaDB;
        $totalP2 = $uang_P2DT + $uang_P2DB + $uang_penghargaan;
        //------ Total DIbawah --------
        $pajak_p2 = 0;
        if($golongan == 'IV'):
            $pajak_p2 = $totalP2 * 0.15;
        elseif($golongan == 'III'):
            $pajak_p2 = $totalP2 * 0.05;
        elseif($golongan == 'I' || $golongan == 'II'):
            $pajak_p2 = 0;
        endif;
        $output = [
                'gol' => $golongan,
                'score_tugastambahan'   => $score,
                'durasi'                => $durasi,
                'grade'                 => $grade,
                'jabatan'               => $jabatan,
                'total_P2DB'            => number_format((float)$uang_P2DB, 0),
                'uang_penghargaan'      => number_format((float)$uang_penghargaan, 0),
                'p2_penghargaan_sebelum_pajak' => number_format((float)$totalP2, 0),
                'pajak_p2' => number_format((float)$pajak_p2, 0),
                'p2_penghargaan_setelah_pajak' => number_format((float)$totalP2 - $pajak_p2, 0),
                'urutan_case'           => "1"
               ];
        return $output;
    }

    public function insertCase1(){

    }
}