<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_case_10 extends CI_Model{
    public function __construct(){
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
        error_reporting(0);
    }

    /**
     * Cek perpindahan riwayat jabatan fungsional dosen (DB)
     */
    public function perpindahanDb($nip,$kode){
        $data = $this->db->select("kode_dosen,nip,kode_jft, nama_jft,tmt_sk,grade_jft,score_jft,cast(6 as char) as durasi")
                                    ->limit(1)
                                    ->order_by('tmt_sk','desc')
                                    ->get_where("simpeg_riwayat_jabatan_fungsional",['nip'=>$nip])
                                    ->result();
        $output = [];
        foreach($data as $key => $value){
            $set = [
                'durasi'=> $value->durasi,
                'grade_jft'=> $value->grade_jft,
                'kode_dosen'=> $value->kode_dosen,
                'kode_jft'=> $value->kode_jft,
                'nama_jft'=> $value->nama_jft,
                'nip'=> $value->nip,
                'score_jft'=> $value->score_jft,
                'tmt_sk'=> $value->tmt_sk,
                'urutan_case' => '8'
            ];
            array_push($output, $set);
        }
        return $output;
        
    }

    /**
     * Filter kondiisi dosen
     */
    public function execute($nip,$kode){
        $checkPerpindahanDB = $this->perpindahanDb($nip,$kode);
        $data = array($checkPerpindahanDB);
        return $checkPerpindahanDB;
    }
}