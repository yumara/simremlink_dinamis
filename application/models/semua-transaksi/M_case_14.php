<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_case_14 extends CI_Model{
    public function __construct(){
        parent::__construct();
        error_reporting(0);
        date_default_timezone_set("Asia/Jakarta");
    }

    /**
     * Cek perpindahan riwayat jabatan fungsional dosen (DB)
     */
    public function perpindahanTendik($nip,$kode){
        $dataJft = $this->db->query("SELECT kode_peg, nip, nama, id_jft, grade, score, no_sk, tmt_sk, status, bukti_sk, jenis_jft as nama_jft,
            cast('6' as char) as durasi
            FROM `simpeg_riwayat_jft_pegawai` 
            where tmt_sk IN (SELECT MAX(tmt_sk) FROM simpeg_riwayat_jft_pegawai GROUP BY nip )
            AND status = 'Y' AND nip ='$nip'");

            $dataJfu = $this->db->query("SELECT kode_peg, nip, nama, id_jabumum, grade, score, no_sk, tmt_sk, status, bukti_sk, jenis_jabumum as nama_jft,
            cast('6' as char) as durasi
            FROM `simpeg_riwayat_jfu_pegawai` 
            where tmt_sk IN (SELECT MAX(tmt_sk) FROM simpeg_riwayat_jfu_pegawai GROUP BY nip )
            AND status = 'Y'AND nip ='$nip'");

            $dataStruktural = $this->db->query("SELECT kode_peg, nip, nama, id_jab_struktural, grade, score, no_sk, tmt_sk, status, bukti_sk, nama_jabatan_struktural as nama_jft,
            cast('6' as char) as durasi
            FROM `simpeg_riwayat_struktural_pegawai` 
            where tmt_sk IN (SELECT MAX(tmt_sk) FROM simpeg_riwayat_struktural_pegawai GROUP BY nip )
            AND status = 'Y'AND nip ='$nip'");

            $data = [];
            if($dataJfu->num_rows() > 0):
                $value = $dataJfu->row();
                    $setJfu = [
                        'kode_peg' => $value->kode_peg,
                        'nama' => $value->nama,
                        'nip' => $value->nip,
                        'id_jft' => $value->id_jabumum,
                        'grade' => $value->grade,
                        'score' => $value->score, 
                        'no_sk' => $value->no_sk,
                        'tmt_sk' => $value->tmt_sk,
                        'status' => $value->status,
                        'bukti_sk' => $value->bukti_sk,
                        'nama_jft' => $value->nama_jft,
                        'durasi' =>  $value->durasi,
                        'urutan_case' => '14'
                    ];
                    array_push($data, $setJfu);
                
            elseif($dataJft->num_rows() > 0):
                $value = $dataJft->row();
                    $setJft = [
                        'kode_peg' => $value->kode_peg,
                        'nama' => $value->nama,
                        'nip' => $value->nip,
                        'id_jft' => $value->id_jft,
                        'grade' => $value->grade,
                        'score' => $value->score, 
                        'no_sk' => $value->no_sk,
                        'tmt_sk' => $value->tmt_sk,
                        'status' => $value->status,
                        'bukti_sk' => $value->bukti_sk,
                        'nama_jft' => $value->nama_jft,
                        'durasi' =>  $value->durasi,
                        'urutan_case' => '14'
                    ];
                    array_push($data, $setJft);
                
            elseif($dataStruktural->num_rows() > 0):
                $value = $dataStruktural->row();
                    $setStruktural = [
                        'kode_peg' => $value->kode_peg,
                        'nama' => $value->nama,
                        'nip' => $value->nip,
                        'id_jft' => $value->id_jab_struktural,
                        'grade' => $value->grade,
                        'score' => $value->score, 
                        'no_sk' => $value->no_sk,
                        'tmt_sk' => $value->tmt_sk,
                        'status' => $value->status,
                        'bukti_sk' => $value->bukti_sk,
                        'nama_jft' => $value->nama_jft,
                        'durasi' =>  $value->durasi,
                        'urutan_case' => '14'
                    ];
                    array_push($data, $setStruktural);
                
            endif;
        return $data;
        
    }

    /**
     * Filter kondiisi dosen
     */
    public function execute($nip,$kode){
        $checkPerpindahanTendik = $this->perpindahanTendik($nip,$kode);
        $data = array($checkPerpindahanTendik);
        return $checkPerpindahanTendik;
    }

    /**
     * Ekseskusi harga DB
     */
    public function executeHargaDb($nip,$kode,$periode){
        $data_periode = $this->db->query("SELECT * FROM simremlink_data_periode WHERE periode = '$periode' ")->row_array();
        $dataJft = $this->db->query("SELECT *, jenis_jft as nama_jft
            FROM `simpeg_riwayat_jft_pegawai` 
            where tmt_sk IN (SELECT MAX(tmt_sk) FROM simpeg_riwayat_jft_pegawai GROUP BY nip )
            AND status = 'Y' AND nip ='$nip'");

        $dataJfu = $this->db->query("SELECT *, jenis_jabumum as nama_jft
            FROM `simpeg_riwayat_jfu_pegawai` 
            where tmt_sk IN (SELECT MAX(tmt_sk) FROM simpeg_riwayat_jfu_pegawai GROUP BY nip )
            AND status = 'Y'AND nip ='$nip'");

        $dataStruktural = $this->db->query("SELECT *, nama_jabatan_struktural as nama_jft
            FROM `simpeg_riwayat_struktural_pegawai` 
            where tmt_sk IN (SELECT MAX(tmt_sk) FROM simpeg_riwayat_struktural_pegawai GROUP BY nip )
            AND status = 'Y'AND nip ='$nip'");
            
        if($dataJfu->num_rows() > 0):
                $data = $dataJfu->row();
                $score = $data->score;
        elseif($dataJft->num_rows() > 0):
                $data = $dataJft->row();
                $score = $data->score;
        elseif($dataStruktural->num_rows() > 0):
                $data = $dataStruktural->row();
                $score = $data->score;
        else:
                $score = 0;
        endif;
            $harga_db = ($score*1*$data_periode['pir']*6)/40;
            $output = array(
                'harga' => $harga_db,
                'stts' => "PEG",
                "urutan_case" => "14"
        );
         return $output;
    }

    /**
     * Eksekusi Total Poin Remun
     */
    public function executeTotalPoinRemun($jenisDosen,$tupoksi,$mengajar,$mengajarLainnya,$penunjang,$penghargaan,$total_semua_poin,$poin_kinerja,$stts,$kode,$kode2){
        /**
         * Init poin
         */
        $p2DB       = 0;
        $uang_dt    = 0;
        $uang_p1_dt = 0;
        $uang_p2_dt = 0;
        $uang_p1_db = 0;
        $p2DT       = 0;
            
        /**
         * menentuan poin kinerja
         */
        if($poin_kinerja > 54):
            $poin_kinerja = 54;
        else:
            $poin_kinerja = $poin_kinerja;
        endif;

        /**
         * penentuan poin P2DT
         */
        $p2DT = $poin_kinerja - 12;
        if($p2DT > 28):
          $p2DT = 28;
        else:
          $p2DT = $p2DT;
        endif;

        /**
         * proses perhitungan
         */
        $p2DB               = $poin_kinerja - 12;

        
        $output = array(
            'total_poin'            => number_format((float)$total_semua_poin, 3),
            'poin_kinerja'          => number_format((float)$poin_kinerja, 2),
            'p2DT'                  => number_format((float)$p2DT, 2),
            'p2DB'                  => number_format((float)$p2DB, 2),
            'poinPenghargaan'       => number_format((float)$penghargaan, 2),
            'stts'                  => $stts,
            'jenis'                 => $jenisDosen,
            "urutan_case" => "14"
        );

        return $output;
    }

    public function executeUangRemun($kode,$kode2,$pir,$p2DT,$penghargaan,$nip){
        /**
         * inisialisasi
         */
        $init   = $this->perpindahanTendik($nip,$kode2);
        $durasi = $init[0]['durasi'];
        $score  = $init[0]['score'];
        
        /**
         * perhitungan
         */
        $hargaDT = ($score * 1 * $pir * 6) / 40;
        $uang_P1DT = $durasi / 6 * 12 * $hargaDT;

        $output = [
                'score_tugastambahan'   => $score,
                'durasi'                => $durasi,
                'total_P1'              => number_format((float)$uang_P1DT, 0),
                "urutan_case" => "14"
               ];
        return $output;
    }

    public function executeUangRemunDB($kode,$kode2,$pir,$p2DT,$penghargaan,$nip, $poin_kinerja, $golongan){
        /**
         * inisialisasi
         */
        // $initDT   = $this->perpindahanDt($nip,$kode2);
        // $durasiDT = $initDT['durasi'];
        // $scoreDT  = $initDT['score'];
        //----------------
        $init   = $this->perpindahanTendik($nip,$kode2);
        $durasi = $init[0]['durasi'];
        $score  = $init[0]['score'];
        $jabatan= $init[0]['nama_jft'];
        $grade  = $init[0]['grade'];
        if($poin_kinerja > 54):
            $poin_kinerja = 54;
        else:
            $poin_kinerja = $poin_kinerja;
        endif;
        $p2DB   = $poin_kinerja - 12;    
        // $hargaDT = ($scoreDT * 1 * $pir * 6) / 40;
        // $uang_P2DT = $durasiDT / 6 * $p2DT * $hargaDT;    
        /**
         * perhitungan
         */
        $hargaDB = ($score * 1 * $pir * 6) / 40;
        $uang_P2DB = $durasi / 6 * $p2DB * $hargaDB;
        $uang_penghargaan   = $penghargaan * $hargaDB;
        $totalP2 =  $uang_P2DB + $uang_penghargaan;
        //------ Total DIbawah --------
        $pajak_p2 = 0;
        if($golongan == 'IV'):
            $pajak_p2 = $totalP2 * 0.15;
        elseif($golongan == 'III'):
            $pajak_p2 = $totalP2 * 0.05;
        elseif($golongan == 'I' || $golongan == 'II'):
            $pajak_p2 = 0;
        endif;
        $output = [
                'gol' => $golongan,
                'score_tugastambahan'   => $score,
                'durasi'                => $durasi,
                'jabatan'               => $jabatan,
                'grade'                 => $grade,
                'total_P2DB'            => number_format((float)$uang_P2DB, 0),
                'uang_penghargaan'      => number_format((float)$uang_penghargaan, 0),
                'p2_penghargaan_sebelum_pajak' => number_format((float)$totalP2, 0),
                'pajak_p2' => number_format((float)$pajak_p2, 0),
                'p2_penghargaan_setelah_pajak' => number_format((float)$totalP2 - $pajak_p2, 0),
                'urutan_case'           => "14"
               ];
        return $output;
    }
}