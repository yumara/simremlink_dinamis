<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_case_2 extends CI_Model{
    public function __construct(){
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
        error_reporting(0);
    }

    /**
     * Cek perpindahan riwayat jabatan tugas tambahan dosen (DT)
     */
    public function perpindahanDt($nip,$kode,$periode){
        $tahun = substr($periode, 0,4);
        $periode_ke = substr($periode, 4,5);
        $tgl_nonaktif = null;
        if($periode_ke == '1'){
            $tgl_nonaktif = date_create($tahun . "-6-30");
        }else{
            $tgl_nonaktif = date_create($tahun . "-12-31");
        };
        $data = $this->db->select("
                            kode_dosen,
                            nip,
                            kode_tugastambahan,
                            nama_tugastambahan,
                            tmt_sk,
                            grade_tugastambahan,
                            score_tugastambahan,
                            tgl_nonaktif,
                            tgl_nonaktif_2,
                            durasi,
                            hari,
                            round(hari/30) AS fix_durasi")
                         ->order_by('tmt_sk','asc')
                         ->get_where('transaksi_v_riwayat_jabatan_tugas_tambahan_dosen_'.$tahun.$periode_ke,['nip'=>$nip, 'status_aktif' => 'Y', 'status_jabatan_utama' => 'Y'])
                         ->row();

        if($data->tgl_nonaktif > $tgl_nonaktif || $data->tgl_nonaktif == '0000-00-00'):
            // $tgl_nonaktif = date_create('2020-12-31');
            $tmt_sk = date_create($data->tmt_sk);
            $selisih = $tgl_nonaktif->diff($tmt_sk)->days + 1;
            $durasi = round($selisih/30);
        else:
            $durasi = str_replace('-','',$data->fix_durasi);
        endif;

        if($durasi > 6):
            $durasi = 6;
        else:
            $durasi = $durasi;
        endif;
        $res = [
            'kode_dosen'=> $data->kode_dosen,
            'nip'       => $data->nip,
            'kode'      => $data->kode_tugastambahan,
            'nama'      => $data->nama_tugastambahan,
            'tmt_sk'    => $data->tmt_sk,
            'grade'     => $data->grade_tugastambahan,
            'score'     => $data->score_tugastambahan,
            'durasi'    => $durasi,
            "jenis"     => 'DT',
            "urutan_case" => "2"
        ];
        return $res;
    }

    /**
     * Cek perpindahan riwayat jabatan fungsional dosen (DB)
     */
    public function perpindahanDb($nip,$kode,$periode){
        $tahun = substr($periode, 0,4);
        $periode_ke = substr($periode, 4,5);
        $tgl_nonaktif = null;
        if($periode_ke == '1'){
            $tgl_nonaktif = date_create($tahun . "-6-30");
        }else{
            $tgl_nonaktif = date_create($tahun . "-12-31");
        };
        $checkTransaksi = $this->db->get_where("transaksi_v_riwayat_jabatan_fungsional_dosen_".$tahun.$periode_ke,['nip'=>$nip]);
        $limit          = $checkTransaksi->num_rows() + 1;
        $result         = $this->db->limit($limit)
                                   ->order_by('tmt_sk','desc')
                                   ->get_where("simpeg_riwayat_jabatan_fungsional",['nip'=>$nip,'YEAR(tmt_sk)!=' => $tahun]);
        $getRowOne      = $this->db->select('nip,hari_1,hari_2')
                                   ->limit(1)
                                   ->get_where("transaksi_v_riwayat_jabatan_fungsional_dosen_".$tahun.$periode_ke,['nip'=>$nip])
                                   ->row();
        $data = [];
		$datas = $result->result_array();
        foreach ($datas as $key => $value):
			if($key == 0 ){
				$tgl_nonaktif   = $tgl_nonaktif;
			}else{
				$tglnya = $datas[$key - 1]['tmt_sk'];
				$tgl_nonaktif   = date_create($tglnya);
			}
            
            $tmt_sk         = date_create($value['tmt_sk']);
            $selisih        = $tgl_nonaktif->diff($tmt_sk)->days + 1;
            $durasi         = round($selisih/30);
            if($durasi > 6):
                $durasi = str_replace('-','',round($getRowOne->hari_1/30));
            else:
                $durasi = $durasi;
            endif;
            if($durasi > 0):
                $set = [
                        'kode_dosen'=> $value['kode_dosen'],
                        'nip'       => $value['nip'],
                        'kode'      => $value['kode_jft'],
                        'nama'      => $value['nama_jft'],
                        'tmt_sk'    => $value['tmt_sk'],
                        'grade'     => $value['grade_jft'],
                        'score'     => $value['score_jft'],
                        'durasi'    => $durasi,
                        "jenis"     => 'DB',
                        "urutan_case" => "2"
                       ];
                array_push($data,$set);
            endif;
        endforeach;
        return $data;
    }

    /**
     * Filter kondiisi dosen
     */
    public function execute($nip,$kode,$periode){
        $checkPerpindahanDB = $this->perpindahanDb($nip,$kode,$periode);
        $checkPerpindahanDt = $this->perpindahanDt($nip,$kode,$periode);
        $data = array_merge(array($checkPerpindahanDt),$checkPerpindahanDB);
        return $data;
    }

    /**
     * Ekseskusi harga DT
     */
    public function executeHargaDt($nip,$kode,$periode){
        $tahun = substr($periode, 0,4);
        $periode_ke = substr($periode, 4,5);
        $tgl_nonaktif = null;
        if($periode_ke == '1'){
            $tgl_nonaktif = date_create($tahun . "-6-30");
        }else{
            $tgl_nonaktif = date_create($tahun . "-12-31");
        };
        $data_jabatan   = $this->db->select("score_tugastambahan")
                                  ->order_by("tmt_sk","desc")
                                  ->get_where("transaksi_v_riwayat_jabatan_tugas_tambahan_dosen_".$tahun.$periode_ke, ["nip"=>$nip,"status_aktif" => "Y", "status_jabatan_utama" => "Y"])
                                  ->row();
        $data_periode   = $this->db->query("SELECT * FROM simremlink_data_periode WHERE periode = '$periode' ")
                                  ->row_array();
        $harga_dt = ($data_jabatan->score_tugastambahan*1*$data_periode['pir']*6)/40; 
        $output = [
            "harga" => $harga_dt,
            "skor"  => $data_jabatan->score_tugastambahan,
            "urutan_case" => "2"
        ];
        return $output;
    }

    /**
     * Ekseskusi harga DB
     */
    public function executeHargaDb($nip,$kode,$periode){
        $tahun = substr($periode, 0,4);
        $periode_ke = substr($periode, 4,5);
        $tgl_nonaktif = null;
        if($periode_ke == '1'){
            $tgl_nonaktif = date_create($tahun . "-6-30");
        }else{
            $tgl_nonaktif = date_create($tahun . "-12-31");
        };
        $data_periode       = $this->db->query("SELECT * FROM simremlink_data_periode WHERE periode = '$periode' ")->row_array();
        $checkTransaksi = $this->db->get_where("transaksi_v_riwayat_jabatan_fungsional_dosen_".$tahun.$periode_ke,['nip'=>$nip]);
        $limit          = $checkTransaksi->num_rows() + 1;
        $result         = $this->db->limit($limit)
                                   ->order_by('tmt_sk','desc')
                                   ->get_where("simpeg_riwayat_jabatan_fungsional",['nip'=>$nip]);
        $getRowOne      = $this->db->select('nip,hari_1,hari_2')
                                   ->limit(1)
                                   ->get_where("transaksi_v_riwayat_jabatan_fungsional_dosen_".$tahun.$periode_ke,['nip'=>$nip])
                                   ->row();
        $data = [];
        foreach ($result->result() as $key => $value):
            $tgl_nonaktif   = $tgl_nonaktif; //date_create('2020-12-31');
            $tmt_sk         = date_create($value->tmt_sk);
            $selisih        = $tgl_nonaktif->diff($tmt_sk)->days + 1;
            $durasi         = round($selisih/30);
            if($durasi > 6):
                $durasi = str_replace('-','',round($getRowOne->hari_1/30));
            else:
                $durasi = $durasi;
            endif;
            if($durasi > 0):
                $harga_db = ($value->score_jft * 1 * $data_periode['pir'] * 6) / 40;
                $set      = [
                                'harga' => $harga_db,
                                "urutan_case" => "2"
                            ];
                array_push($data,$set);
            endif;
        endforeach;
                    
        
        return $data;
    }

    /**
     * Eksekusi Total Poin Remun
     */
    public function executeTotalPoinRemun($jenisDosen,$tupoksi,$mengajar,$mengajarLainnya,$penunjang,$penghargaan,$total_semua_poin,$poin_kinerja,$stts,$kode,$kode2){
        /**
         * Init poin
         */
        $p2DB       = 0;
        $uang_dt    = 0;
        $uang_p1_dt = 0;
        $uang_p2_dt = 0;
        $uang_p1_db = 0;
        $p2DT       = 0;
        
        
            $stts = 'DT';
            
            /**
             * menentuan poin kinerja
             */
            if($poin_kinerja > 108):
                $poin_kinerja = 108;
            else:
                $poin_kinerja = $poin_kinerja;
            endif;

            /**
             * penentuan poin P2DT
             */
            $p2DT = $poin_kinerja - 12;
            if($p2DT > 28):
              $p2DT = 28;
            else:
              $p2DT = $p2DT;
            endif;

            /**
             * proses perhitungan
             */
            $p2DB               = $poin_kinerja - ($p2DT + 12);
        $output = array(
            'total_poin'            => number_format((float)$total_semua_poin, 3),
            'poin_kinerja'          => number_format((float)$poin_kinerja, 2),
            'p2DT'                  => number_format((float)$p2DT, 2),
            'p2DB'                  => number_format((float)$p2DB, 2),
            'poinPenghargaan'       => number_format((float)$penghargaan, 2),
            'stts'                  => $stts,
            'jenis'                 => $jenisDosen,
            "urutan_case" => "2"
        );

        return $output;
    }

    /**
     * Ekseskusi uang remun
     */
    public function executeUangRemun($kode,$kode2,$pir,$p2DT,$penghargaan,$nip){
        /**
         * inisialisasi
         */
        $init   = $this->perpindahanDt($nip,$kode2);
        $durasi = $init['durasi'];
        $score  = $init['score'];
        
        /**
         * perhitungan
         */
        $hargaDT = ($score * 1 * $pir * 6) / 40;
        $uang_P1DT = $durasi / 6 * 12 * $hargaDT;
        $uang_P2DT = $durasi / 6 * $p2DT * $hargaDT;

        $output = [
                'score_tugastambahan'   => $score,
                'durasi'                => $durasi,
                'uang_P1'               => number_format((float)$uang_P1DT, 0),
                'uang_P2'               => number_format((float)$uang_P2DT, 0),
                'total_P1'              => number_format((float)$uang_P1DT, 0),
                'total_P2'              => number_format((float)$uang_P2DT, 0),
                "urutan_case" => "2"
               ];
        return $output;
    }

    public function executeUangRemunDB($kode,$kode2,$pir,$p2DT,$penghargaan,$nip, $poin_kinerja, $golongan){
        /**
         * inisialisasi
         */
        $initDT   = $this->perpindahanDt($nip,$kode2);
        $durasiDT = $initDT['durasi'];
        $scoreDT  = $initDT['score'];
        $grade    = $initDT['grade'];
        $jabatan  = $initDT['nama'];
        //----------------
        
        $init   = $this->perpindahanDb($nip,$kode2);
        //----------------------
        $durasi1db = $init[0]['durasi'];
        $score1db  = $init[0]['score'];
        $durasi2db = $init[1]['durasi'];
        $score2db  = $init[1]['score'];
        // $durasi3db = $init[2]['durasi'];
        // $score3db  = $init[2]['score'];
        //-----------------------
        $p2DB   = $poin_kinerja - ($p2DT + 12);    
        $hargaDT = ($scoreDT * 1 * $pir * 6) / 40;
        $uang_P2DT = $durasiDT / 6 * $p2DT * $hargaDT;    
        /**
         * perhitungan
         */
        $hargaDB1 = ($score1db * 1 * $pir * 6) / 40;
        $hargaDB2 = ($score2db * 1 * $pir * 6) / 40;
        // $hargaDB3 = ($score3db * 1 * $pir * 6) / 40;
        $hargaMax = max($hargaDB1,$hargaDB2);
        $uang_P2DB1 = $durasi1db / 6 * $p2DB * $hargaDB1;
        $uang_P2DB2 = $durasi2db / 6 * $p2DB * $hargaDB2;
        $uang_P2DB = $uang_P2DB1 + $uang_P2DB2;
        // $uang_P2DB3 = $durasi / 6 * $p2DB * $hargaDB3;
        //-----------------------------
        $uang_penghargaan   = $penghargaan * $hargaDB2;
        $totalP2 = $uang_P2DT + $uang_P2DB1 + $uang_P2DB2 + $uang_penghargaan;
        //------ Total DIbawah --------
        $pajak_p2 = 0;
        if($golongan == 'IV'):
            $pajak_p2 = $totalP2 * 0.15;
        elseif($golongan == 'III'):
            $pajak_p2 = $totalP2 * 0.05;
        elseif($golongan == 'I' || $golongan == 'II'):
            $pajak_p2 = 0;
        endif;
        $output = [
                'gol' => $golongan,
                'grade'   => $grade,
                'jabatan'                => $jabatan,
                'total_P2DB'            => number_format((float)$uang_P2DB, 0),
                'uang_penghargaan'      => number_format((float)$uang_penghargaan, 0),
                'p2_penghargaan_sebelum_pajak' => number_format((float)$totalP2, 0),
                'pajak_p2' => number_format((float)$pajak_p2, 0),
                'p2_penghargaan_setelah_pajak' => number_format((float)$totalP2 - $pajak_p2, 0),
                'urutan_case'           => "6"
               ];
        return $output;
    }
}