<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_case_3 extends CI_Model{
    public function __construct(){
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
        error_reporting(0);
    }

    /**
     * Cek perpindahan riwayat jabatan tugas tambahan dosen (DT)
     */
    public function perpindahanDt($nip,$kode,$periode){
        $tahun = substr($periode, 0,4);
        $periode_ke = substr($periode, 4,5);
        $tgl_nonaktif = null;
        if($periode_ke == '1'){
            $tgl_nonaktif = date_create($tahun . "-6-30");
        }else{
            $tgl_nonaktif = date_create($tahun . "-12-31");
        };
        $data = $this->db->select("
                            kode_dosen,
                            nip,
                            kode_tugastambahan,
                            nama_tugastambahan,
                            tmt_sk,
                            grade_tugastambahan,
                            score_tugastambahan,
                            tgl_nonaktif,
                            tgl_nonaktif_2,
                            durasi,
                            hari,
                            round(hari/30) AS fix_durasi")
                         ->order_by('tmt_sk','asc')
                         ->get_where('transaksi_v_riwayat_jabatan_tugas_tambahan_dosen_'.$tahun.$periode_ke,['nip'=>$nip, 'status_aktif' => 'Y', 'status_jabatan_utama' => 'Y'])
                         ->result_array();
        $output = [];
        foreach ($data as $key => $value):
              if($value['tgl_nonaktif'] > $tgl_nonaktif || $value['tgl_nonaktif'] == '0000-00-00'):
				  if($key == 0 ){
						$tgl_nonaktif   = $tgl_nonaktif;
					}else{
						$tglnya = $data[$key - 1]['tmt_sk'];
						$tgl_nonaktif   = date_create($tglnya);
					}
			
                    //$tgl_nonaktif = date_create('2020-12-31');
                    $tmt_sk = date_create($value['tmt_sk']);
                    $selisih = $tgl_nonaktif->diff($tmt_sk)->days + 1;
                    $durasi = round($selisih/30);
              else:
                $durasi = str_replace('-','',$value['fix_durasi']);
              endif;
              if($durasi > 6):
                $durasi = 6;
              else:
                $durasi = $durasi;
              endif;
              $set = [
                'kode_dosen'=> $value['kode_dosen'],
                'nip'       => $value['nip'],
                'kode'      => $value['kode_tugastambahan'],
                'nama'      => $value['nama_tugastambahan'],
                'tmt_sk'    => $value['tmt_sk'],
                'grade'     => $value['grade_tugastambahan'],
                'score'     => $value['score_tugastambahan'],
                'durasi'    => $durasi,
                "jenis"     => 'DT',
                "urutan_case" => "3"
                ];
              array_push($output,$set);
        endforeach;

        return $output;
    }

    /**
     * Cek perpindahan riwayat jabatan fungsional dosen (DB)
     */
    public function perpindahanDb($nip,$kode){
        $data = $this->db->select("kode_dosen,nip,kode_jft, nama_jft,tmt_sk,grade_jft,score_jft,cast(6 as char) as durasi")
                        ->limit(1)
                        ->order_by('tmt_sk','desc')
                        ->get_where("simpeg_riwayat_jabatan_fungsional",['nip'=>$nip])
                        ->row();
        $res = [
            'kode_dosen'=> $data->kode_dosen,
            'nip'       => $data->nip,
            'kode'      => $data->kode_jft,
            'nama'      => $data->nama_jft,
            'tmt_sk'    => $data->tmt_sk,
            'grade'     => $data->grade_jft,
            'score'     => $data->score_jft,
            'durasi'    => $data->durasi,
            "jenis"     => 'DB',
                "urutan_case" => "3"
        ];
        return $res;
    }

    /**
     * Filter kondiisi dosen
     */
    public function execute($nip,$kode,$periode){
        $checkPerpindahanDB = $this->perpindahanDb($nip,$kode);
        $checkPerpindahanDt = $this->perpindahanDt($nip,$kode,$periode);
        $data = array_merge($checkPerpindahanDt,array($checkPerpindahanDB));
        return $data;
    }

    /**
     * Ekseskusi harga DT
     */
    public function executeHargaDt($nip,$kode,$periode){
        $tahun = substr($periode, 0,4);
        $periode_ke = substr($periode, 4,5);
        $tgl_nonaktif = null;
        if($periode_ke == '1'){
            $tgl_nonaktif = date_create($tahun . "-6-30");
        }else{
            $tgl_nonaktif = date_create($tahun . "-12-31");
        };
        $data_jabatan   = $this->db->select("score_tugastambahan")
                                  ->get_where("transaksi_v_riwayat_jabatan_tugas_tambahan_dosen_".$tahun.$periode_ke, ["nip"=>$nip,"status_aktif" => "Y", "status_jabatan_utama" => "Y"])
                                  ->result();
        $data_periode   = $this->db->query("SELECT * FROM simremlink_data_periode WHERE periode = '$periode' ")
                                  ->row_array();
        $output = [];
        foreach($data_jabatan as $key => $value):
            $harga_dt = ($value->score_tugastambahan*1*$data_periode['pir']*6)/40; 
            $set = [
                    "harga" => $harga_dt,
                    "skor"  => $value->score_tugastambahan,
                    "urutan_case" => "3"
                   ];
            array_push($output, $set); 
        endforeach;
        return $output;
    }

    /**
     * Ekseskusi harga DB
     */
    public function executeHargaDb($nip,$kode,$periode){
        $data_periode = $this->db->query("SELECT * FROM simremlink_data_periode WHERE periode = '$periode' ")->row_array();
        $query    = $this->db->select("kode_dosen,nip,kode_jft, nama_jft,tmt_sk,grade_jft,score_jft,cast(6 as char) as durasi")
                             ->limit(1)
                             ->order_by('tmt_sk','desc')
                             ->get_where("simpeg_riwayat_jabatan_fungsional",['nip'=>$nip])
                             ->row();
        $harga_db = ($query->score_jft * 1 * $data_periode['pir'] * 6) / 40;
        $data     = ['harga' => $harga_db, "urutan_case" => "3"];
        return $data;
    }

    /**
     * Eksekusi Total Poin Remun
     */
    public function executeTotalPoinRemun($jenisDosen,$tupoksi,$mengajar,$mengajarLainnya,$penunjang,$penghargaan,$total_semua_poin,$poin_kinerja,$stts,$kode,$kode2){
        /**
         * Init poin
         */
        $p2DB       = 0;
        $uang_dt    = 0;
        $uang_p1_dt = 0;
        $uang_p2_dt = 0;
        $uang_p1_db = 0;
        $p2DT       = 0;
        
        
            $stts = 'DT';
            
            /**
             * menentuan poin kinerja
             */
            if($poin_kinerja > 108):
                $poin_kinerja = 108;
            else:
                $poin_kinerja = $poin_kinerja;
            endif;

            /**
             * penentuan poin P2DT
             */
            $p2DT = $poin_kinerja - 12;
            if($p2DT > 28):
              $p2DT = 28;
            else:
              $p2DT = $p2DT;
            endif;

            /**
             * proses perhitungan
             */
            $p2DB               = $poin_kinerja - ($p2DT + 12);
        
        $output = array(
            'total_poin'            => number_format((float)$total_semua_poin, 3),
            'poin_kinerja'          => number_format((float)$poin_kinerja, 2),
            'p2DT'                  => number_format((float)$p2DT, 2),
            'p2DB'                  => number_format((float)$p2DB, 2),
            'poinPenghargaan'       => number_format((float)$penghargaan, 2),
            'stts'                  => $stts,
            'jenis'                 => $jenisDosen,
                "urutan_case" => "3"
        );

        return $output;
    }

    /**
     * Ekseskusi uang remun
     */
    public function executeUangRemun($kode,$kode2,$pir,$p2DT,$penghargaan,$nip){
        /**
         * inisialisasi
         */
        $init   = $this->perpindahanDt($nip,$kode2);
        $durasi1dt = $init[0]['durasi'];
        $score1dt  = $init[0]['score'];
        $durasi2dt = $init[1]['durasi'];
        $score2dt  = $init[1]['score'];
        
        /**
         * perhitungan
         */
        $hargaDT1 = ($score1dt * 1 * $pir * 6) / 40;
        $hargaDT2 = ($score2dt * 1 * $pir * 6) / 40;
        $uang_P1DT1 = $durasi1dt / 6 * 12 * $hargaDT1;
        $uang_P1DT2 = $durasi2dt / 6 * 12 * $hargaDT2;
        $uang_P1DT = $uang_P1DT1 + $uang_P1DT2;

        $uang_P2DT1 = $durasi1dt / 6 * $p2DT * $hargaDT1;
        $uang_P2DT2 = $durasi2dt / 6 * $p2DT * $hargaDT2;
        $uang_P2DT = $uang_P2DT1 + $uang_P2DT2;

        $output = [
                // 'score_tugastambahan'   => $score,
                // 'durasi'                => $durasi,
                'total_P1'              => number_format((float)$uang_P1DT, 0),
                'total_P2'              => number_format((float)$uang_P2DT, 0),
                "urutan_case" => "3"
               ];
        return $output;
    }

    public function executeUangRemunDB($kode,$kode2,$pir,$p2DT,$penghargaan,$nip, $poin_kinerja, $golongan){
        /**
         * inisialisasi
         */
        $initDT   = $this->perpindahanDt($nip,$kode2);
        $durasiDT1 = $initDT[0]['durasi'];
        $scoreDT1  = $initDT[0]['score'];
        $durasiDT2 = $initDT[1]['durasi'];
        $scoreDT2  = $initDT[1]['score'];
        $grade     = $initDT[1]['grade'];
        $jabatan   = $initDT[1]['nama'];
        //----------------
        $init   = $this->perpindahanDb($nip,$kode2);
        $durasi = $init['durasi'];
        $score  = $init['score'];
        $p2DB   = $poin_kinerja - ($p2DT + 12);    

        $hargaDT1 = ($scoreDT1 * 1 * $pir * 6) / 40;
        $uang_P2DT1 = $durasiDT1 / 6 * $p2DT * $hargaDT1;
        $hargaDT2 = ($scoreDT2 * 1 * $pir * 6) / 40;
        $uang_P2DT2 = $durasiDT2 / 6 * $p2DT * $hargaDT2;   
        $uang_P2DT = $uang_P2DT1 + $uang_P2DT2; 
        /**
         * perhitungan
         */
        $hargaDB = ($score * 1 * $pir * 6) / 40;
        $uang_P2DB = $durasi / 6 * $p2DB * $hargaDB;
        $uang_penghargaan   = $penghargaan * $hargaDB;
        $totalP2 = $uang_P2DT + $uang_P2DB + $uang_penghargaan;
        //------ Total DIbawah --------
        $pajak_p2 = 0;
        if($golongan == 'IV'):
            $pajak_p2 = $totalP2 * 0.15;
        elseif($golongan == 'III'):
            $pajak_p2 = $totalP2 * 0.05;
        elseif($golongan == 'I' || $golongan == 'II'):
            $pajak_p2 = 0;
        endif;
        $output = [
                'score_tugastambahan'   => $score,
                'durasi'                => $durasi,
                'grade'                 => $grade,
                'jabatan'               => $jabatan,
                'total_P2DB'            => number_format((float)$uang_P2DB, 0),
                'uang_penghargaan'      => number_format((float)$uang_penghargaan, 0),
                'p2_penghargaan_sebelum_pajak' => number_format((float)$totalP2, 0),
                'pajak_p2' => number_format((float)$pajak_p2, 0),
                'p2_penghargaan_setelah_pajak' => number_format((float)$totalP2 - $pajak_p2, 0),
                'urutan_case'           => "6"
               ];
        return $output;
    }
}