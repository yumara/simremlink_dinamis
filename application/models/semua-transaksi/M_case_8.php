<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_case_8 extends CI_Model{
    public function __construct(){
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
        error_reporting(0);
    }

    /**
     * Cek perpindahan riwayat jabatan fungsional dosen (DB)
     */
    public function perpindahanDb($nip,$kode){
        $data = $this->db->select("kode_dosen,nip,kode_jft, nama_jft,tmt_sk,grade_jft,score_jft,cast(6 as char) as durasi")
                        //->limit(1)
                        ->order_by('tmt_sk','desc')
                        ->get_where("simpeg_riwayat_jabatan_fungsional",['nip'=>$nip])
                        ->row();
        $res = [
            'kode_dosen'=> $data->kode_dosen,
            'nip'       => $data->nip,
            'kode'      => $data->kode_jft,
            'nama'      => $data->nama_jft,
            'tmt_sk'    => $data->tmt_sk,
            'grade'     => $data->grade_jft,
            'score'     => $data->score_jft,
            'durasi'    => $data->durasi,
            "jenis"     => 'DB',
            "urutan_case" => "8"
        ];
        return $res;
        
    }

    /**
     * Filter kondiisi dosen
     */
    public function execute($nip,$kode){
        $checkPerpindahanDB = $this->perpindahanDb($nip,$kode);
        $data = array($checkPerpindahanDB);
        return $data;//$checkPerpindahanDB;
    }

    /**
     * Ekseskusi harga DB
     */
    public function executeHargaDb($nip,$kode,$periode){
        $data_periode = $this->db->query("SELECT * FROM simremlink_data_periode WHERE periode = '$periode' ")->row_array();
        $query    = $this->db->select("kode_dosen,nip,kode_jft, nama_jft,tmt_sk,grade_jft,score_jft,cast(6 as char) as durasi")
                             ->limit(1)
                             ->order_by('tmt_sk','desc')
                             ->get_where("simpeg_riwayat_jabatan_fungsional",['nip'=>$nip])
                             ->row();
        $harga_db = ($query->score_jft * 1 * $data_periode['pir'] * 6) / 40;
        $data     = [
            'harga' => $harga_db,
            "urutan_case" => "8"];
        return $data;
    }

    /**
     * Eksekusi Total Poin Remun
     */
    public function executeTotalPoinRemun($jenisDosen,$tupoksi,$mengajar,$mengajarLainnya,$penunjang,$penghargaan,$total_semua_poin,$poin_kinerja,$stts,$kode,$kode2){
        /**
         * Init poin
         */
        $p2DB       = 0;
        $uang_dt    = 0;
        $uang_p1_dt = 0;
        $uang_p2_dt = 0;
        $uang_p1_db = 0;
        $p2DT       = 0;
        
        $stts = 'DT';
            
        /**
         * menentuan poin kinerja
         */
        if($poin_kinerja > 68):
            $poin_kinerja = 68;
        else:
            $poin_kinerja = $poin_kinerja;
        endif;

        /**
         * penentuan poin P2DT
         */
        $p2DT = $poin_kinerja - 12;
        if($p2DT > 28):
          $p2DT = 28;
        else:
          $p2DT = $p2DT;
        endif;

        /**
         * proses perhitungan
         */
        $p2DB               = $poin_kinerja - 12;

        
        $output = array(
            'total_poin'            => number_format((float)$total_semua_poin, 3),
            'poin_kinerja'          => number_format((float)$poin_kinerja, 2),
            'p2DT'                  => number_format((float)$p2DT, 2),
            'p2DB'                  => number_format((float)$p2DB, 2),
            'poinPenghargaan'       => number_format((float)$penghargaan, 2),
            'stts'                  => $stts,
            'jenis'                 => $jenisDosen,
            "urutan_case" => "8"
        );

        return $output;
    }

    public function executeUangRemunDB($kode,$kode2,$pir,$p2DT,$penghargaan,$nip, $poin_kinerja, $golongan){
        /**
         * inisialisasi
         */
        // $initDT   = $this->perpindahanDt($nip,$kode2);
        // $durasiDT = $initDT['durasi'];
        // $scoreDT  = $initDT['score'];
        //----------------
        $init   = $this->perpindahanDb($nip,$kode2);
        $durasi = $init['durasi'];
        $score  = $init['score'];
        $grade  = $init['grade'];
        $jabatan= $init['nama'];
        if($poin_kinerja > 68){
            $poin_kinerja = 68;
        }else{
            $poin_kinerja = $poin_kinerja;
        }
        $p2DB   = $poin_kinerja - 12;

        // $hargaDT = ($scoreDT * 1 * $pir * 6) / 40;
        // $uang_P2DT = $durasiDT / 6 * $p2DT * $hargaDT;    
        /**
         * perhitungan
         */
        $hargaDB = ($score * 1 * $pir * 6) / 40;
        $uang_P2DB = $durasi / 6 * $p2DB * $hargaDB;
        $uang_penghargaan   = $penghargaan * $hargaDB;
        $totalP2 =  $uang_P2DB + $uang_penghargaan;
        //------ Total DIbawah --------
        $pajak_p2 = 0;
        if($golongan == 'IV'):
            $pajak_p2 = $totalP2 * 0.15;
        elseif($golongan == 'III'):
            $pajak_p2 = $totalP2 * 0.05;
        elseif($golongan == 'I' || $golongan == 'II'):
            $pajak_p2 = 0;
        endif;
        $output = [
                'gol' => $golongan,
                'score_tugastambahan'   => $score,
                'durasi'                => $durasi,
                'grade'                 => $grade,
                'jabatan'               => $jabatan,
                'total_P2DB'            => number_format((float)$uang_P2DB, 0),
                'uang_penghargaan'      => number_format((float)$uang_penghargaan, 0),
                'p2_penghargaan_sebelum_pajak' => number_format((float)$totalP2, 0),
                'pajak_p2' => number_format((float)$pajak_p2, 0),
                'p2_penghargaan_setelah_pajak' => number_format((float)$totalP2 - $pajak_p2, 0),
                'urutan_case'           => "8"
               ];
        return $output;
    }

    public function executeUangRemun($kode,$kode2,$pir,$p2DT,$penghargaan,$nip){
        /**
         * inisialisasi
         */
        $init   = $this->perpindahanDb($nip,$kode2);
        $durasi = $init['durasi'];
        $score  = $init['score'];
        
        /**
         * perhitungan
         */
        $hargaDT = ($score * 1 * $pir * 6) / 40;
        $uang_P1DT = $durasi / 6 * 12 * $hargaDT;

        $output = [
                'score_tugastambahan'   => $score,
                'durasi'                => $durasi,
                'total_P1'              => number_format((float)$uang_P1DT, 0),
                "urutan_case" => "8"
               ];
        return $output;
    }
}