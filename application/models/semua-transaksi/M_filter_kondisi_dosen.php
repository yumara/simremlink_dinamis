<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_filter_kondisi_dosen extends CI_Model{
    public function __construct(){
        parent::__construct();
        error_reporting(0);
        date_default_timezone_set("Asia/Jakarta");
    }

    /**
     * Cek perpindahan riwayat jabatan tugas tambahan dosen (DT)
     */
    public function perpindahanDt($nip,$kode, $periode){
        $tahun = substr($periode, 0,4);
        $periode_ke = substr($periode, 4,5);
        $tgl_nonaktif = null;
        if($periode_ke == '1'){
            $tgl_nonaktif = date_create($tahun . "-6-30");
        }else{
            $tgl_nonaktif = date_create($tahun . "-12-31");
        };
        $data = $this->db->select("
                            kode_dosen,
                            nip,
                            kode_tugastambahan,
                            nama_tugastambahan,
                            tmt_sk,
                            grade_tugastambahan,
                            score_tugastambahan,
                            tgl_nonaktif,
                            tgl_nonaktif_2,
                            durasi,
                            hari,
                            round(hari/30) AS fix_durasi")
                         ->order_by('tmt_sk','asc')
                         ->get_where('transaksi_v_riwayat_jabatan_tugas_tambahan_dosen_'.$tahun.$periode_ke,['nip'=>$nip, 'status_aktif' => 'Y', 'status_jabatan_utama' => 'Y'])
                         ->row();

        if($data->tgl_nonaktif > $tgl_nonaktif || $data->tgl_nonaktif == '0000-00-00'):
            // $tgl_nonaktif = date_create('2020-12-31');
            $tmt_sk = date_create($data->tmt_sk);
            $selisih = $tgl_nonaktif->diff($tmt_sk)->days + 1;
            $durasi = round($selisih/30);
        else:
            $durasi = str_replace('-','',$data->fix_durasi);
        endif;

        if($durasi > 6):
            $durasi = 6;
        else:
            $durasi = $durasi;
        endif;
        return $durasi;
    }

    /**
     * pengecekan jumlah riwayat jabatan tugas tambahan dosen (DT)
     */
    public function pengecekanJumlahDt($nip, $kode, $periode){
        $tahun = substr($periode, 0,4);
        $periode_ke = substr($periode, 4,5);
        $tgl_nonaktif = null;
        if($periode_ke == '1'){
            $tgl_nonaktif = date_create($tahun . "-6-30");
        }else{
            $tgl_nonaktif = date_create($tahun . "-12-31");
        };
        return $this->db->select("
                            kode_dosen,
                            nip,
                            kode_tugastambahan,
                            nama_tugastambahan,
                            tmt_sk,
                            grade_tugastambahan,
                            score_tugastambahan,
                            tgl_nonaktif,
                            tgl_nonaktif_2,
                            durasi,
                            hari,
                            round(hari/30) AS fix_durasi")
                        ->order_by('tmt_sk','asc')
                        ->get_where('transaksi_v_riwayat_jabatan_tugas_tambahan_dosen_'.$tahun.$periode_ke,['nip'=>$nip, 'status_aktif' => 'Y', 'status_jabatan_utama' => 'Y', 'tmt_sk <='=>date_format($tgl_nonaktif, "Y-m-d")])
                        ->num_rows();
    }

    /**
     * Cek perpindahan riwayat jabatan fungsional dosen (DB)
     */
    public function perpindahanDb($nip,$kode, $periode){
        $tahun = substr($periode, 0,4);
        $periode_ke = substr($periode, 4,5);
        $tgl_nonaktiff = null;
        if($periode_ke == '1'){
            $tgl_nonaktiff = date_create($tahun . "-6-30");
        }else{
            $tgl_nonaktiff = date_create($tahun . "-12-31");
        };
        $checkTransaksi = $this->db->select('*')
                                    ->get_where("transaksi_v_riwayat_jabatan_fungsional_dosen_".$tahun.$periode_ke,['nip'=>$nip]);
        if($checkTransaksi->num_rows() > 0):
            $limit  = $checkTransaksi->num_rows() + 1;
            $result = $this->db->limit($limit)
                                    ->order_by('tmt_sk','desc')
                                    ->get_where("simpeg_riwayat_jabatan_fungsional",['nip'=>$nip])
                                    ->row();
            $getRowOne = $this->db->select('nip,hari_1,hari_2')
                                    ->limit(1)
                                    ->get_where("transaksi_v_riwayat_jabatan_fungsional_dosen_".$tahun.$periode_ke,['nip'=>$nip])
                                    ->row();

            $tgl_nonaktif = date_create($tgl_nonaktiff);
            $tmt_sk       = date_create($result->tmt_sk);
            $selisih      = $tgl_nonaktif->diff($tmt_sk)->days + 1;
            $durasi       = round($selisih/30);
            if($durasi > 6):
                $durasi = str_replace('-','',round($getRowOne->hari_1/30));
            else:
                $durasi = $durasi;
            endif;
        else:
            $data = $this->db->select("kode_dosen,nip,kode_jft, nama_jft,tmt_sk,grade_jft,score_jft,cast(6 as char) as durasi")
                             ->limit(1)
                             ->order_by('tmt_sk','desc')
                             ->get_where("simpeg_riwayat_jabatan_fungsional",['nip'=>$nip])
                             ->row();
            $durasi = $data->durasi;
        endif;
        return $durasi;
    }

    /**
     * Cek perpindahan riwayat jabatan fungsional dosen (DB)
     */
    public function pengecekanJumlahDb($nip,$kode, $periode){
        $tahun = substr($periode, 0,4);
        $periode_ke = substr($periode, 4,5);
        $tgl_nonaktif = null;
        if($periode_ke == '1'){
            $tgl_nonaktif = date_create($tahun . "-6-30");
        }else{
            $tgl_nonaktif = date_create($tahun . "-12-31");
        };
        $dataA = $this->db->get_where("transaksi_v_riwayat_jabatan_fungsional_dosen_".$tahun.$periode_ke,['nip'=>$nip]);
        $dataB = $this->db->order_by('tmt_sk','desc')->get_where("simpeg_riwayat_jabatan_fungsional",['nip'=>$nip]);
        if($dataA->num_rows() > 0):
            return $dataA->num_rows();
        else:
            return $dataB->num_rows();
        endif;
        
    }

    /**
     * Filter kondiisi dosen
     */
    public function filterKondisiDosen($nip,$kode,$periode){
        $tahun = substr($periode, 0,4);
        $periode_ke = substr($periode, 4,5);
        $tgl_nonaktif = null;
        $awal_periode = null;
        $akhir_periode = null;
        $mulai = null;
        $berakhir = null;
        if($periode_ke == '1'){
            $awal_periode = "-01-01";
            $akhir_periode = "-06-30";
            $mulai = $tahun . $awal_periode;
            $berakhir = $tahun . $akhir_periode;
            $tgl_nonaktif = date_create($tahun . "-6-30");
        }else{
            $awal_periode = "-07-01";
            $akhir_periode = "-12-31";
            $mulai = $tahun . $awal_periode;
            $berakhir = $tahun . $akhir_periode;
            $tgl_nonaktif = date_create($tahun . "-12-31");
        };
        $checkPerpindahanDB = $this->perpindahanDb($nip,$kode, $periode);
        $cekJumlahDT = $this->pengecekanJumlahDt($nip,$kode,$periode);

        if($cekJumlahDT > 0):
            $checkPerpindahanDt = $this->perpindahanDt($nip,$kode, $periode);
            if($checkPerpindahanDt >= 6 && $checkPerpindahanDB >= 6):
                /**
                 * DT jabatan DT tidak berubah, DB tidak berubah
                 */
                $case = "CASE 1";
            elseif($checkPerpindahanDt >= 6 && $checkPerpindahanDB < 6):
                /**
                 * DT jabatan DT tidak berubah, DB berubah 
                 */
                $case = "CASE 2";
            elseif($checkPerpindahanDt < 6 && $checkPerpindahanDB >= 6):
                /**
                 * pengecekan jumlah DT dan DB
                 */
                $cekJumlahDT = $this->pengecekanJumlahDt($nip,$kode, $periode);
                $cekJumlahDB = $this->pengecekanJumlahDb($nip,$kode, $periode);
                /**
                 * DT jabatan DT berubah, DB tidak berubah
                 */
                if($cekJumlahDT > 1 && ($cekJumlahDB <= 1 || $checkPerpindahanDB >= 1)):
                    if($checkPerpindahanDB >= 6):
                        $case = "CASE 3";
                    endif;
                /**`
                 * DT yang berubah jadi DB -----> jabatan DB tidak berubah
                 */
                elseif($cekJumlahDT <= 1 && $cekJumlahDB >= 1):
                    $dataDt = $this->db->select("tgl_nonaktif")
                                      ->order_by('tmt_sk','asc')
                                      ->get_where('transaksi_v_riwayat_jabatan_tugas_tambahan_dosen_'.$tahun.$periode_ke,['nip'=>$nip, 'status_aktif' => 'Y', 'status_jabatan_utama' => 'Y'])
                                      ->row();
                    if($dataDt->tgl_nonaktif >= $awal_periode && $dataDt->tgl_nonaktif < $akhir_periode):
                        $case = "CASE 6";
                    else:
                        $case = "CASE 9";
                    endif;
                /**`
                 * DB yang berubah jadi DT -----> jabatan DT tidak berubah, DB tidak berubah
                 */
                // elseif($cekJumlahDT <= 1 && $cekJumlahDB <= 1):
                //     $case = "CASE 9";
                /**`
                 * DB yang berubah jadi DT -----> jabatan DT berubah, DB tidak berubah
                 */
                elseif($cekJumlahDT >= 1 && $cekJumlahDB <= 1):
                    $case = "CASE 11";
                endif;
            elseif($checkPerpindahanDt < 6 && $checkPerpindahanDB < 6):
                /**
                 * pengecekan jumlah DT dan DB
                 */
                $cekJumlahDT = $this->pengecekanJumlahDt($nip,$kode, $periode);
                $cekJumlahDB = $this->pengecekanJumlahDb($nip,$kode, $periode);
                /**
                 * DT jabatan DT berubah, DB berubah
                 */
                if($cekJumlahDT > 1 && $cekJumlahDB > 1):

                    $case = "CASE 4";
                /**`
                 * DT yang berubah jadi DB -----> jabatan DB berubah
                 * contoh : DT 4 bulan, 2 bulan berikutnya DB
                 */
                elseif($cekJumlahDT <= 1 && $cekJumlahDB >= 1):
                    $case = "CASE 5";
                /**`
                 * DB yang berubah jadi DT -----> jabatan DT tidak berubah, DB berubah
                 */
                elseif($cekJumlahDT <= 1 && $cekJumlahDB >= 1):
                    $case = "CASE 10";
                /**`
                 * DB yang berubah jadi DT -----> jabatan DT berubah, DB berubah
                 */
                elseif($cekJumlahDT >= 1 && $cekJumlahDB >= 1):
                    $case = "CASE 12";
                endif;
            endif;
        else:
            $cekJumlahDB = $this->pengecekanJumlahDb($nip,$kode, $periode);
            if($checkPerpindahanDB < 6 && $cekJumlahDB >= 1):
                /**
                 * DB jabatan DB berubah
                 */
                $case = "CASE 7";
            elseif($checkPerpindahanDB >= 6 && $cekJumlahDB >= 1):
                /**
                 * DB jabatan DB tidak berubah
                 */
                $case = "CASE 8";
            endif;
            
        endif;
        return $case;
    }
}