<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_filter_kondisi_pegawai extends CI_Model{
    public function __construct(){
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
        error_reporting(0);
    }

    /**
     * Filter kondiisi pegawai
     */
    public function filterKondisiPegawai($nip,$kode){
        $dataJft        = $this->db->select("kode_peg, nip, nama, id_jabumum, grade, score, no_sk, tmt_sk, status, bukti_sk, nama_jft, round(hari/30) AS durasi")
                                    ->get_where('transaksi_v_riwayat_jfu_pegawai_2020',['nip'=>$nip]);
        $dataJfu        = $this->db->select("kode_peg, nip, nama, id_jft, grade, score, no_sk, tmt_sk, status, bukti_sk, nama_jft, round(hari/30) AS durasi")
                                    ->get_where('transaksi_v_riwayat_jft_pegawai_2020',['nip'=>$nip]);
        $dataStruktural = $this->db->select("kode_peg, nip, nama, id_jab_struktural, grade, score, no_sk, tmt_sk, status, bukti_sk, nama_jft, round(hari/30) AS durasi")
                                           ->get_where('transaksi_v_riwayat_struktural_pegawai_2020',['nip'=>$nip]);
        if($dataJft->num_rows() > 0 || $dataJfu->num_rows() > 0 || $dataStruktural->num_rows() > 0):
            $case = "CASE 13";
        else:
            $case = "CASE 14";
        endif;
        return $case;
    }
}