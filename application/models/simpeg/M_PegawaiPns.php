<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_PegawaiPns extends CI_Model{
    public function __construct(){
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }

    public function refreshPegawaiPns(){
		$this->db->trans_begin();
        $this->db->truncate('simpeg_pegawai_pns');
        if($this->db->trans_status() === FALSE && $checkData > 0){
			$this->db->trans_rollback();
			return FALSE;
		}else{
			$this->db->trans_commit();
			$arrContextOptions=array(
				"ssl"=>array(
					"verify_peer"=>false,
					"verify_peer_name"=>false,
				),
			);  
			$link = "https://simpeg.unimed.ac.id/jsonsimpeg/data/tpegawai.php?api_key=bb31ba2d3b18e743dc35c1aed6d80774";
			$data = json_decode(file_get_contents($link,false, stream_context_create($arrContextOptions)),true);
			$this->db->insert_batch('simpeg_pegawai_pns', $data); 
			return TRUE;
		}
	}
}