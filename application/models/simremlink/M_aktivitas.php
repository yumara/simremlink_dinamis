<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_aktivitas extends CI_Model{
    public function __construct(){
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }

    public function refreshDosenPns(){
		$this->db->trans_begin();
        $this->db->truncate('simpeg_dosen_pns');
        if($this->db->trans_status() === FALSE && $checkData > 0){
			$this->db->trans_rollback();
			return FALSE;
		}else{
			$this->db->trans_commit();
			$arrContextOptions=array(
				"ssl"=>array(
					"verify_peer"=>false,
					"verify_peer_name"=>false,
				),
			);  
			$link = "https://simpeg.unimed.ac.id/jsonsimpeg/data/tdosen.php?api_key=e807f1fcf82d132f9bb018ca6738a19f";
			$data = json_decode(file_get_contents($link,false, stream_context_create($arrContextOptions)),true);
			$this->db->insert_batch('simpeg_dosen_pns', $data); 
			return TRUE;
		}
	}

	public function apiDosenPns(){
		return $this->db->get('simpeg_dosen_pns');
	}
}