<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_data_jenis_pegawai extends CI_Model{
    public function __construct(){
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }

    public function refreshDosenPnsSimremlink(){
		$this->db->trans_begin();
        $this->db->truncate('simpeg_dosen_pns_simremlink');
        if($this->db->trans_status() === FALSE && $checkData > 0){
			$this->db->trans_rollback();
			return FALSE;
		}else{
			$this->db->trans_commit();
			$arrContextOptions=array(
				"ssl"=>array(
					"verify_peer"=>false,
					"verify_peer_name"=>false,
				),
			);  
			$link = "https://simpeg.unimed.ac.id/jsonsimpeg/data/dosensimremlink.php?api_key=922c8c46fcecee6340ac2d0790e68be8";
			$data = json_decode(file_get_contents($link,false, stream_context_create($arrContextOptions)),true);
			$this->db->insert_batch('simpeg_dosen_pns_simremlink', $data); 
			return TRUE;
		}
	}
}