<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_data_peran extends CI_Model{
    public function __construct(){
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }

    public function refreshDosenPhl(){
		$this->db->trans_begin();
        $this->db->truncate('simpeg_dosen_phl');
        if($this->db->trans_status() === FALSE && $checkData > 0){
			$this->db->trans_rollback();
			return FALSE;
		}else{
			$this->db->trans_commit();
			$arrContextOptions=array(
				"ssl"=>array(
					"verify_peer"=>false,
					"verify_peer_name"=>false,
				),
			);  
			$link = "https://simpeg.unimed.ac.id/jsonsimpeg/data/tdosenphl.php?api_key=053f210055e44647a51b2a2d8282e6b2";
			$data = json_decode(file_get_contents($link,false, stream_context_create($arrContextOptions)),true);
			$this->db->insert_batch('simpeg_dosen_phl', $data); 
			return TRUE;
		}
	}
}