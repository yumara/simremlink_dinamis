<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_jabatan_fungsional_dosen_pegawai extends CI_Model{

    public function __construct(){
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }
    
    public function saveLos(){
        $created_at = date('Y-m-d H:i:s');
        $updated_at = date('Y-m-d H:i:s');

        //from input
        $id_pasar   = trim($this->input->post('id_pasar','true'));
        $id_blok    = trim($this->input->post('id_blok','true'));
        $id_lantai  = trim($this->input->post('id_lantai','true'));
        $los        = strtoupper(trim($this->input->post('los','true')));

        //check data
        $checkData  = $this->db->get_where('los',['id_pasar'=>$id_pasar,'id_blok'=>$id_blok, 'id_lantai'=>$id_lantai,'los'=>$los])->num_rows();
        $jlhData    = $this->db->get_where('los',['id_pasar'=>$id_pasar])->num_rows();
        $getPasar   = $this->m_data_lantai->getPasar($id_pasar)->row();

        $this->db->trans_begin();
        $this->db->insert('los',[
            'id_pasar'   => $id_pasar,
            'id_blok'    => $id_blok,
            'id_lantai'  => $id_lantai,
            'los'        => $los,
            'created_at' => $created_at,
			'updated_at' => $updated_at,
        ]);
        if($this->db->trans_status() === FALSE || $checkData > 0 || $jlhData >= $getPasar->pasar_jml_los){
			$this->db->trans_rollback();
            if($checkData > 0){
                $txt    =  'Data sudah ada';
                $icon   =  'warning';
            }elseif($jlhData >= $getPasar->pasar_jml_los){
                $txt    =  'Jumlah los berlebih';
                $icon   =  'warning';
            }else{
                $txt    = 'Terjadi kesalahan saat menyimpan data';
                $icon   = 'error';
            }
            $msg = ['msg' => $txt,'status'=>FALSE,'icon'=>$icon];
		}else{
            $msg = ['status'=>TRUE];
			$this->db->trans_commit();
        }
        return $msg;
    }

    public function updateLos(){
        $created_at = date('Y-m-d H:i:s');
        $updated_at = date('Y-m-d H:i:s');
        
        //from input
        $id_pasar   = trim($this->input->post('id_pasar','true'));
        $id_blok    = trim($this->input->post('id_blok','true'));
        $id_lantai  = trim($this->input->post('id_lantai','true'));
        $los        = strtoupper(trim($this->input->post('los','true')));
        $id         = trim($this->input->post('id','true'));

        //check data
        $checkData  = $this->db->get_where('los',['id_pasar'=>$id_pasar,'id_blok'=>$id_blok, 'id_lantai'=>$id_lantai,'los'=>$los])->num_rows();
        $jlhData    = $this->db->get_where('los',['id_pasar'=>$id_pasar])->num_rows();
        $getPasar   = $this->m_data_lantai->getPasar($id_pasar)->row();

        $this->db->trans_begin();
        $this->db->where(['id'=>$id])
                ->update('los',[
                    'id_pasar'   => $id_pasar,
                    'id_blok'    => $id_blok,
                    'id_lantai'  => $id_lantai,
                    'los'        => $los,
                    'created_at' => $created_at,
                    'updated_at' => $updated_at,
        ]);
        if($this->db->trans_status() === FALSE || $checkData > 0 || $jlhData > $getPasar->pasar_jml_los){
			$this->db->trans_rollback();
            if($checkData > 0){
                $txt    =  'Data sudah ada';
                $icon   =  'warning';
            }elseif($jlhData >= $getPasar->pasar_jml_los){
                $txt    =  'Jumlah los berlebih';
                $icon   =  'warning';
            }else{
                $txt    = 'Terjadi kesalahan saat menyimpan data';
                $icon   = 'error';
            }
            $msg = ['msg' => $txt,'status'=>FALSE,'icon'=>$icon];
		}else{
            $this->db->trans_commit();
            $msg    = ['status'=>TRUE];
        }
        return $msg;
    }

    public function viewLos(){
        return $this->db->select('*, count(los.id_pasar) as jlhlos, los.id as idlos, pasar.id as idpasar')
                        ->join('pasar','pasar.id = los.id_pasar')
                        ->group_by('pasar.pasar_nama')
                        ->order_by('los.id','desc')
                        ->get('los');
    }

    public function deleteLos($id){
        $checkData = $this->db->get_where('los',['id'=>$id])->num_rows();
        
        $this->db->trans_begin();
        $this->db->delete('los',['id'=>$id]);
        if($this->db->trans_status() === FALSE && $checkData > 0){
			$this->db->trans_rollback();
			return FALSE;
		}else{
			$this->db->trans_commit();
			return TRUE;
		}
    }

    public function getLos($id){
        $cekData = $this->db->get_where('los',['id'=>$id]);

        if($cekData->num_rows() > 0 ){
            return $cekData->row_array();
        }else{
            return FALSE;
        }   
    }

    public function detailLos($id){
        $cekData = $this->db->select('*, blok.id as idblok, lantai.id as idlantai, los.id as idlos')
                            ->join('blok','blok.id = los.id_blok')
                            ->join('lantai','lantai.id = los.id_lantai')
                            ->get_where('los',['los.id_pasar'=>$id]);

        if($cekData->num_rows() > 0 ){
            return $cekData->result_array();
        }else{
            return FALSE;
        }   
    }

    public function getLosByIdPasar($idpasar,$idlantai,$idblok){
        $cekData = $this->db->get_where('los',[
                                        'id_pasar'  =>$idpasar,
                                        'id_lantai' =>$idlantai,
                                        'id_blok'   =>$idblok
                                        ]);

        if($cekData->num_rows() > 0 ){
            return $cekData->result_array();
        }else{
            return FALSE;
        }   
    }
}