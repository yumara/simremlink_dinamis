<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_kdn extends CI_Model{
    public function __construct(){
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }

    public function refreshPegawaiPhl(){
		$this->db->trans_begin();
        $this->db->truncate('simpeg_pegawai_phl');
        if($this->db->trans_status() === FALSE && $checkData > 0){
			$this->db->trans_rollback();
			return FALSE;
		}else{
			$this->db->trans_commit();
			$arrContextOptions=array(
				"ssl"=>array(
					"verify_peer"=>false,
					"verify_peer_name"=>false,
				),
			);  
			$link = "https://simpeg.unimed.ac.id/jsonsimpeg/data/tpegawaiphl.php?api_key=83404a861e0215e0f6787aee808019c1";
			$data = json_decode(file_get_contents($link,false, stream_context_create($arrContextOptions)),true);
			$this->db->insert_batch('simpeg_pegawai_phl', $data); 
			return TRUE;
		}
	}
}