<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_sinkron_akad extends CI_Model{
    public function __construct(){
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }

    public function saveSinkronAkad(){
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );  

        $akad_kelebihan_mengajar                 = trim($this->input->post('kelebihan_mengajar','true'));
        $akad_pembimbing_akademik                = trim($this->input->post('pembimbing_akademik','true'));
        $akad_pembimbing_skripsi_tesis_disertasi = trim($this->input->post('pembimbing_skripsi_tesis_disertasi','true'));
        $akad_penguji_skripsi_tesis_disertasi    = trim($this->input->post('penguji_skripsi_tesis_disertasi','true'));
        $akad_mengajar_lainnya                   = trim($this->input->post('mengajar_lainnya','true'));

		$this->db->trans_begin();
        //akad
        if($akad_kelebihan_mengajar == 'ya'):
            $this->db->truncate('akad_kelebihan_mengajar');
            //kelebihan mengajar
            $akadLinkKelebihanMengajar = "https://bak.unimed.ac.id/apiajar/tr_lebih2.php";
            $akadKelebihanMengajar = json_decode(file_get_contents($akadLinkKelebihanMengajar,false, stream_context_create($arrContextOptions)),true);
            $this->db->insert_batch('akad_kelebihan_mengajar', $akadKelebihanMengajar);
        endif;

        if($akad_pembimbing_akademik == 'ya'):
            $this->db->truncate('akad_pembimbing_akademik');
            //pembimbing akademik
            $akadLinkKPembimbingAkademik = "https://bak.unimed.ac.id/apiajar/jsonbak.php?cek=pa";
            $akadKPembimbingAkademik = json_decode(file_get_contents($akadLinkKPembimbingAkademik,false, stream_context_create($arrContextOptions)),true);
            $this->db->insert_batch('akad_pembimbing_akademik', $akadKPembimbingAkademik);
        endif;

        if($akad_pembimbing_skripsi_tesis_disertasi == 'ya'):
            $this->db->truncate('akad_pembimbing_skripsi_tesis_disertasi');
            //pembimbing skripsi tesis disertasi
            $akadLinkKPembimbingSkripsiTesisDisertasi = "https://bak.unimed.ac.id/apiajar/jsonbak.php?hal=pg";
            $akadKPembimbingSkripsiTesisDisertasi = json_decode(file_get_contents($akadLinkKPembimbingSkripsiTesisDisertasi,false, stream_context_create($arrContextOptions)),true);
            $this->db->insert_batch('akad_pembimbing_skripsi_tesis_disertasi', $akadKPembimbingSkripsiTesisDisertasi);
        endif;

        if($akad_penguji_skripsi_tesis_disertasi == 'ya'):
            $this->db->truncate('akad_penguji_skripsi_tesis_disertasi');
            //penguji skripsi tesis disertasi
            $akadLinkKpengujiSkripsiTesisDisertasi = "https://bak.unimed.ac.id/apiajar/jsonbak.php?hal=pu";
            $akadKpengujiSkripsiTesisDisertasi = json_decode(file_get_contents($akadLinkKpengujiSkripsiTesisDisertasi,false, stream_context_create($arrContextOptions)),true);
            $this->db->insert_batch('akad_penguji_skripsi_tesis_disertasi', $akadKpengujiSkripsiTesisDisertasi);
        endif;

        if($akad_mengajar_lainnya == 'ya'):
            $this->db->truncate('akad_mengajar_lainnya');
            // mengajar lainnya
            $simpegLinkMengajarLainnya = "https://bak.unimed.ac.id/apiajar/jsondos.php?hal=pp";
            $simpegMengajarLainnya = json_decode(file_get_contents($simpegLinkMengajarLainnya,false, stream_context_create($arrContextOptions)),true);
            $this->db->insert_batch('akad_mengajar_lainnya', $simpegMengajarLainnya);
        endif;
        
        //================== end akad ==================
        if($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
                $txt    = 'Terjadi kesalahan saat menyimpan data';
                $icon   = 'error';
                $msg = ['msg' => $txt,'status'=>FALSE,'icon'=>$icon];
			return FALSE;
		}else{
            $this->db->trans_commit();
                $msg = ['status'=>TRUE];
			return TRUE;
        }
        return $msg;
    }
    
}