<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_sinkron_esk extends CI_Model{
    public function __construct(){
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }

    public function saveSinkronEsk(){
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );
        $aktivitas = trim($this->input->post('aktivitas'));
        $penghargaan = trim($this->input->post('penghargaan'));

        $this->db->trans_begin();

        if($aktivitas == 'ya'):
        //aktivitas
        $this->db->truncate('esk_aktivitas');
        //http://dev.unimed.ac.id/e-SK/api/aktifitas
        $eskLinkAktivitas = "https://e-sk.unimed.ac.id/api/aktifitas";
        $eskAktivitas = json_decode(file_get_contents($eskLinkAktivitas,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('esk_aktivitas', $eskAktivitas);
        endif;

        if($penghargaan == 'ya'):
          //penghargaan
          $this->db->truncate('esk_penghargaan');
          $eskLinkPenghargaan = "https://penghargaan.unimed.ac.id/api/aktifitas";
          $eskPenghargaan = json_decode(file_get_contents($eskLinkPenghargaan,false, stream_context_create($arrContextOptions)),true);
          $this->db->insert_batch('esk_penghargaan', $eskPenghargaan);
          endif;

        if($this->db->trans_status() === FALSE){
          $this->db->trans_rollback();
          $txt    = 'Terjadi kesalahan saat menyimpan data';
          $icon   = 'error';
          $msg = ['msg' => $txt,'status'=>FALSE,'icon'=>$icon];
          return FALSE;
        }else{
          $this->db->trans_commit();
          $msg = ['status'=>TRUE];
          return TRUE;
        }
          return $msg;
    }
    
}