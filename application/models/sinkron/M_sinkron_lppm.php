<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_sinkron_lppm extends CI_Model{
    public function __construct(){
      parent::__construct();
      date_default_timezone_set("Asia/Jakarta");
    }

    public function saveSinkronLppm(){
          $arrContextOptions=array(
              "ssl"=>array(
                  "verify_peer"=>false,
                  "verify_peer_name"=>false,
              ),
          );

          $evaluasi_dokumen                   = trim($this->input->post('evaluasi_dokumen'));
          $evaluasi_dokumen_lanjutan          = trim($this->input->post('evaluasi_dokumen_lanjutan'));
          $evaluasi_hasil                     = trim($this->input->post('evaluasi_hasil'));
          $evaluasi_hasil_pengabdian          = trim($this->input->post('evaluasi_hasil_pengabdian'));
          $evaluasi_monitoring                = trim($this->input->post('evaluasi_monitoring'));
          $evaluasi_monitoring_pengabdian     = trim($this->input->post('evaluasi_monitoring_pengabdian'));
          $evaluasi_pen_1                     = trim($this->input->post('evaluasi_pen_1'));
          $evaluasi_poster                    = trim($this->input->post('evaluasi_poster'));
          $kunjungan_lapangan                 = trim($this->input->post('kunjungan_lapangan'));
          $luaran                             = trim($this->input->post('luaran'));
          $luaran_lainnya                     = trim($this->input->post('luaran_lainnya'));
          $luaran_pengabdian                  = trim($this->input->post('luaran_pengabdian'));
          $luaran_pengajuan                   = trim($this->input->post('luaran_pengajuan'));
          $luaran_pengajuan_lainnya           = trim($this->input->post('luaran_pengajuan_lainnya'));
          $luaran_pengajuan_pengabdian        = trim($this->input->post('luaran_pengajuan_pengabdian'));
          $luaran_validasi                    = trim($this->input->post('luaran_validasi'));
          $luaran_validasi_lainnya            = trim($this->input->post('luaran_validasi_lainnya'));
          $luaran_validasi_pengabdian         = trim($this->input->post('luaran_validasi_pengabdian'));
          $luaran_tidak_valid                 = trim($this->input->post('luaran_tidak_valid'));
          $luaran_tidak_valid_lainnya         = trim($this->input->post('luaran_tidak_valid_lainnya'));
          $luaran_tidak_valid_pengabdian      = trim($this->input->post('luaran_tidak_valid_pengabdian'));
          $luaran_valid_tambahan              = trim($this->input->post('luaran_valid_tambahan'));
          $luaran_valid_wajib                 = trim($this->input->post('luaran_valid_wajib'));
          $jenis_luaran                       = trim($this->input->post('jenis_luaran'));
          $skim                               = trim($this->input->post('skim'));
          $status_penulis                     = trim($this->input->post('status_penulis'));
          $sumberdana                         = trim($this->input->post('sumberdana'));
          $penelitian_mandiri                 = trim($this->input->post('penelitian_mandiri'));
          $pengabdian_mandiri                 = trim($this->input->post('pengabdian_mandiri'));
          $pengabdian                         = trim($this->input->post('pengabdian'));
          $submission                         = trim($this->input->post('submission'));          
          $data_submission_penelitian         = trim($this->input->post('data_submission_penelitian'));
          $data_submission_penelitian_anggota = trim($this->input->post('data_submission_penelitian_anggota'));
          $data_submission_penelitian_ketua   = trim($this->input->post('data_submission_penelitian_ketua'));
          $data_submission_penelitian_luaran  = trim($this->input->post('data_submission_penelitian_luaran'));

          $this->db->trans_begin();

          if($evaluasi_dokumen == 'ya'):
            $this->db->truncate('lppm_evaluasi_dokumen');
            // evaluasi dokumen
            $lppmLinkEvaluasiDokumen = "https://lppm.unimed.ac.id/simppm2/evaluasi-dokumen";
            $lppmEvaluasiDokumen = json_decode(file_get_contents($lppmLinkEvaluasiDokumen,false, stream_context_create($arrContextOptions)),true);
            $this->db->insert_batch('lppm_evaluasi_dokumen', $lppmEvaluasiDokumen);
            endif;

          if($evaluasi_dokumen_lanjutan == 'ya'):
            $this->db->truncate('lppm_evaluasi_dokumen_lanjutan');
            // evaluasi dokumen lanjutan
            $lppmLinkEvaluasiDokumenLanjutan = "https://lppm.unimed.ac.id/simppm2/evaluasi-dokumen-lanjutan";
            $lppmEvaluasiDokumenLanjutan = json_decode(file_get_contents($lppmLinkEvaluasiDokumenLanjutan,false, stream_context_create($arrContextOptions)),true);
            $this->db->insert_batch('lppm_evaluasi_dokumen_lanjutan', $lppmEvaluasiDokumenLanjutan);
            endif;
          
          if($evaluasi_hasil == 'ya'):
            $this->db->truncate('lppm_evaluasi_hasil');
            // evaluasi hasil
            $lppmLinkEvaluasiHasil = "https://lppm.unimed.ac.id/simppm2/evaluasi-hasil";
            $lppmEvaluasiHasil = json_decode(file_get_contents($lppmLinkEvaluasiHasil,false, stream_context_create($arrContextOptions)),true);
            $this->db->insert_batch('lppm_evaluasi_hasil', $lppmEvaluasiHasil);
            endif;
            
          if($evaluasi_hasil_pengabdian == 'ya'):
            $this->db->truncate('lppm_evaluasi_hasil_pengabdian');
            // evaluasi hasil pengabdian
            $lppmLinkEvaluasiHasilPengabdian = "https://lppm.unimed.ac.id/simppm2/evaluasi-hasil-pengabdian";
            $lppmEvaluasiHasilPengabdian = json_decode(file_get_contents($lppmLinkEvaluasiHasilPengabdian,false, stream_context_create($arrContextOptions)),true);
            $this->db->insert_batch('lppm_evaluasi_hasil_pengabdian', $lppmEvaluasiHasilPengabdian);
            endif;
          
          if($evaluasi_monitoring == 'ya'):
            $this->db->truncate('lppm_evaluasi_monitoring');
            // evaluasi monitoring
            $lppmLinkEvaluasiMonitoring = "https://lppm.unimed.ac.id/simppm2/evaluasi-monitoring";
            $lppmEvaluasiMonitoring = json_decode(file_get_contents($lppmLinkEvaluasiMonitoring,false, stream_context_create($arrContextOptions)),true);
            $this->db->insert_batch('lppm_evaluasi_monitoring', $lppmEvaluasiMonitoring);
            endif;
          
          if($evaluasi_monitoring_pengabdian == 'ya'):
            $this->db->truncate('lppm_evaluasi_monitoring_pengabdian');
            // evaluasi monitoring pengabdian
            $lppmLinkEvaluasiMonitoringPengabdian = "https://lppm.unimed.ac.id/simppm2/evaluasi-monitoring-pengabdian";
            $lppmEvaluasiMonitoringPengabdian = json_decode(file_get_contents($lppmLinkEvaluasiMonitoringPengabdian,false, stream_context_create($arrContextOptions)),true);
            $this->db->insert_batch('lppm_evaluasi_monitoring_pengabdian', $lppmEvaluasiMonitoringPengabdian);
            endif;
          
          if($evaluasi_pen_1 == 'ya'):
            $this->db->truncate('lppm_evaluasi_pen_1');
            // evaluasi pen 1
            $lppmLinkEvaluasiPen1 = "https://lppm.unimed.ac.id/simppm2/evaluasi-pen-1";
            $lppmEvaluasiPen1 = json_decode(file_get_contents($lppmLinkEvaluasiPen1,false, stream_context_create($arrContextOptions)),true);
            $this->db->insert_batch('lppm_evaluasi_pen_1', $lppmEvaluasiPen1);
            endif;
          
          if($evaluasi_poster == 'ya'):
            $this->db->truncate('lppm_evaluasi_poster');
            // evaluasi poster
            $lppmLinkPoster = "https://lppm.unimed.ac.id/simppm2/evaluasi-poster";
            $lppmPoster = json_decode(file_get_contents($lppmLinkPoster,false, stream_context_create($arrContextOptions)),true);
            $this->db->insert_batch('lppm_evaluasi_poster', $lppmPoster);
            endif;
          
          if($kunjungan_lapangan == 'ya'):
            $this->db->truncate('lppm_kunjungan_lapangan');
            // evaluasi kunjungan lapangan
            $lppmLinkKunjunganLapangan = "https://lppm.unimed.ac.id/simppm2/kunjungan-lapangan";
            $lppmKunjunganLapangan = json_decode(file_get_contents($lppmLinkKunjunganLapangan,false, stream_context_create($arrContextOptions)),true);
            $this->db->insert_batch('lppm_kunjungan_lapangan', $lppmKunjunganLapangan);
            endif;

          if($luaran == 'ya'):
            $this->db->truncate('lppm_luaran');
            // evaluasi luaran
            $lppmLinkLuaran = "https://lppm.unimed.ac.id/simppm2/luaran";
            $lppmLuaran = json_decode(file_get_contents($lppmLinkLuaran,false, stream_context_create($arrContextOptions)),true);
            $this->db->insert_batch('lppm_luaran', $lppmLuaran);
            endif;
          
          if($luaran_lainnya == 'ya'):
            $this->db->truncate('lppm_luaran_lainnya');
            // evaluasi luaran lainnya
            $lppmLinkLuaranLainnya = "https://lppm.unimed.ac.id/simppm2/luaran-lainnya";
            $lppmLuaranLainnya = json_decode(file_get_contents($lppmLinkLuaranLainnya,false, stream_context_create($arrContextOptions)),true);
            $this->db->insert_batch('lppm_luaran_lainnya', $lppmLuaranLainnya);
            endif;
          
          if($luaran_pengabdian == 'ya'):
            $this->db->truncate('lppm_luaran_pengabdian');
            // evaluasi luaran pengabdian
            $lppmLinkLuaranPengabdian = "https://lppm.unimed.ac.id/simppm2/luaran-pengabdian";
            $lppmLuaranPengabdian = json_decode(file_get_contents($lppmLinkLuaranPengabdian,false, stream_context_create($arrContextOptions)),true);
            $this->db->insert_batch('lppm_luaran_pengabdian', $lppmLuaranPengabdian);
            endif;
          
          if($luaran_pengajuan == 'ya'):
            $this->db->truncate('lppm_luaran_pengajuan');
            // evaluasi luaran pengajuan
            $lppmLinkLuaranPengajuan = "https://lppm.unimed.ac.id/simppm2/luaran-pengajuan";
            $lppmLuaranPengajuan = json_decode(file_get_contents($lppmLinkLuaranPengajuan,false, stream_context_create($arrContextOptions)),true);
            $this->db->insert_batch('lppm_luaran_pengajuan', $lppmLuaranPengajuan);
            endif;
          
          if($luaran_pengajuan_lainnya == 'ya'):
            $this->db->truncate('lppm_luaran_pengajuan_lainnya');
            // evaluasi luaran pengajuan lainnya
            $lppmLinkLuaranPengajuanLainnya = "https://lppm.unimed.ac.id/simppm2/luaran-pengajuan-lainnya";
            $lppmLuaranPengajuanLainnya = json_decode(file_get_contents($lppmLinkLuaranPengajuanLainnya,false, stream_context_create($arrContextOptions)),true);
            $this->db->insert_batch('lppm_luaran_pengajuan_lainnya', $lppmLuaranPengajuanLainnya);
            endif;
          
          if($luaran_pengajuan_pengabdian == 'ya'):
            $this->db->truncate('lppm_luaran_pengajuan_pengabdian');
            // evaluasi luaran pengajuan pengabdian
            $lppmLinkLuaranPengajuanPengabdian = "https://lppm.unimed.ac.id/simppm2/luaran-pengajuan-pengabdian";
            $lppmLuaranPengajuanPengabdian = json_decode(file_get_contents($lppmLinkLuaranPengajuanPengabdian,false, stream_context_create($arrContextOptions)),true);
            $this->db->insert_batch('lppm_luaran_pengajuan_pengabdian', $lppmLuaranPengajuanPengabdian);
            endif;
          
          if($luaran_pengajuan_pengabdian == 'ya'):
            $this->db->truncate('lppm_luaran_pengajuan_pengabdian');
            // evaluasi luaran pengajuan pengabdian
            $lppmLinkLuaranPengajuanPengabdian = "https://lppm.unimed.ac.id/simppm2/luaran-pengajuan-pengabdian";
            $lppmLuaranPengajuanPengabdian = json_decode(file_get_contents($lppmLinkLuaranPengajuanPengabdian,false, stream_context_create($arrContextOptions)),true);
            $this->db->insert_batch('lppm_luaran_pengajuan_pengabdian', $lppmLuaranPengajuanPengabdian);
            endif;
          
          if($luaran_validasi == 'ya'):
            $this->db->truncate('lppm_luaran_validasi');
          // luaran validasi
          $lppmLinkLuaranValidasi = "https://lppm.unimed.ac.id/simppm2/luaran-validasi";
          $lppmKLuaranValidasi = json_decode(file_get_contents($lppmLinkLuaranValidasi,false, stream_context_create($arrContextOptions)),true);
          $this->db->insert_batch('lppm_luaran_validasi', $lppmKLuaranValidasi);
          endif;
          
          if($luaran_validasi_lainnya == 'ya'):
            $this->db->truncate('lppm_luaran_validasi_lainnya');
          // luaran validasi lainnya
          $lppmLinkLuaranValidasiLainnya = "https://lppm.unimed.ac.id/simppm2/luaran-validasi-lainnya";
          $lppmKLuaranValidasiLainnya = json_decode(file_get_contents($lppmLinkLuaranValidasiLainnya,false, stream_context_create($arrContextOptions)),true);
          $this->db->insert_batch('lppm_luaran_validasi_lainnya', $lppmKLuaranValidasiLainnya);
          endif;

          if($luaran_validasi_pengabdian == 'ya'):
            $this->db->truncate('lppm_luaran_validasi_pengabdian');
          // luaran validasi pengabdian
          $lppmLinkLuaranValidasiPengabdian = "https://lppm.unimed.ac.id/simppm2/luaran-validasi-pengabdian";
          $lppmKLuaranValidasiPengabdian = json_decode(file_get_contents($lppmLinkLuaranValidasiPengabdian,false, stream_context_create($arrContextOptions)),true);
          $this->db->insert_batch('lppm_luaran_validasi_pengabdian', $lppmKLuaranValidasiPengabdian);
          endif;

          if($luaran_tidak_valid == 'ya'):
            $this->db->truncate('lppm_luaran_tidak_valid');
          // luaran tidak valid
          $lppmLinkLuaranTidakValid = "https://lppm.unimed.ac.id/simppm2/luaran-tidak-valid";
          $lppmKLuaranTidakValid = json_decode(file_get_contents($lppmLinkLuaranTidakValid,false, stream_context_create($arrContextOptions)),true);
          $this->db->insert_batch('lppm_luaran_tidak_valid', $lppmKLuaranTidakValid);
          endif;

          if($luaran_tidak_valid_lainnya == 'ya'):
            $this->db->truncate('lppm_luaran_tidak_valid_lainnya');
          // luaran tidak valid lainnya
          $lppmLinkLuaranTidakValidLainnya = "https://lppm.unimed.ac.id/simppm2/luaran-tidak-valid-lainnya";
          $lppmKLuaranTidakValidLainnya = json_decode(file_get_contents($lppmLinkLuaranTidakValidLainnya,false, stream_context_create($arrContextOptions)),true);
          $this->db->insert_batch('lppm_luaran_tidak_valid_lainnya', $lppmKLuaranTidakValidLainnya);
          endif;

          if($luaran_tidak_valid_pengabdian == 'ya'):
            $this->db->truncate('lppm_luaran_tidak_valid_pengabdian');
          // luaran tidak valid lainnya
          $lppmLinkLuaranTidakValidLainnyaPengabdian = "https://lppm.unimed.ac.id/simppm2/luaran-tidak-valid-pengabdian";
          $lppmKLuaranTidakValidLainnyaPengabdian = json_decode(file_get_contents($lppmLinkLuaranTidakValidLainnyaPengabdian,false, stream_context_create($arrContextOptions)),true);
          $this->db->insert_batch('lppm_luaran_tidak_valid_pengabdian', $lppmKLuaranTidakValidLainnyaPengabdian);
          endif;

          if($luaran_valid_tambahan == 'ya'):
            $this->db->truncate('lppm_luaran_valid_tambahan');
            // luaran valid tambahan
            $lppmLinkLuaranValidTambahan = "https://lppm.unimed.ac.id/simppm2/luaran-valid-tambahan";
            $lppmKLuaranValidTambahan = json_decode(file_get_contents($lppmLinkLuaranValidTambahan,false, stream_context_create($arrContextOptions)),true);
            $this->db->insert_batch('lppm_luaran_valid_tambahan', $lppmKLuaranValidTambahan);
            endif;
          
          if($luaran_valid_wajib == 'ya'):
          $this->db->truncate('lppm_luaran_valid_wajib');
          // luaran valid wajib
          $lppmLinkLuaranWajibTambahan = "https://lppm.unimed.ac.id/simppm2/luaran-valid-wajib";
          $lppmKLuaranWajibTambahan = json_decode(file_get_contents($lppmLinkLuaranWajibTambahan,false, stream_context_create($arrContextOptions)),true);
          $this->db->insert_batch('lppm_luaran_valid_wajib', $lppmKLuaranWajibTambahan);
          endif;

          if($jenis_luaran == 'ya'):
            $this->db->truncate('lppm_jenis_luaran');
            // jenis luaran
            $lppmLinkJenisLuaran = "https://lppm.unimed.ac.id/simppm2/jenis-luaran";
            $lppmKJenisLuaran = json_decode(file_get_contents($lppmLinkJenisLuaran,false, stream_context_create($arrContextOptions)),true);
            $this->db->insert_batch('lppm_jenis_luaran', $lppmKJenisLuaran);
            endif;
          
            if($skim == 'ya'):
          $this->db->truncate('lppm_skim');
          // skim
          $lppmLinkSkim = "https://lppm.unimed.ac.id/simppm2/skim";
          $lppmKSkim = json_decode(file_get_contents($lppmLinkSkim,false, stream_context_create($arrContextOptions)),true);
          $this->db->insert_batch('lppm_skim', $lppmKSkim);
          endif;

          if($status_penulis == 'ya'):
            $this->db->truncate('lppm_status_penulis');
            // status penulis
            $lppmLinkStatusPenulis = "https://lppm.unimed.ac.id/simppm2/status-penulis";
            $lppmKStatusPenulis = json_decode(file_get_contents($lppmLinkStatusPenulis,false, stream_context_create($arrContextOptions)),true);
            $this->db->insert_batch('lppm_status_penulis', $lppmKStatusPenulis);
            endif;

          if($status_penulis == 'ya'):
          $this->db->truncate('lppm_sumber_dana');
          // sumber dana
          $lppmLinkSumberDana = "https://lppm.unimed.ac.id/simppm2/sumberdana";
          $lppmKSumberDana = json_decode(file_get_contents($lppmLinkSumberDana,false, stream_context_create($arrContextOptions)),true);
          $this->db->insert_batch('lppm_sumber_dana', $lppmKSumberDana);
          endif;
          
          if($penelitian_mandiri == 'ya'):
            $this->db->truncate('lppm_penelitian_mandiri');
            //  penelitian mandiri
            $lppmLinkPenelitianMandiri = "https://lppm.unimed.ac.id/simppm2/penelitian-mandiri";
            $lppmPenelitianMandiri = json_decode(file_get_contents($lppmLinkPenelitianMandiri,false, stream_context_create($arrContextOptions)),true);
            $this->db->insert_batch('lppm_penelitian_mandiri', $lppmPenelitianMandiri);
            endif;
          
          if($pengabdian_mandiri == 'ya'):
            $this->db->truncate('lppm_pengabdian_mandiri');
            // pengabdian mandiri
            $lppmLinkPengabdianMandiri = "https://lppm.unimed.ac.id/simppm2/pengabdian-mandiri";
            $lppmPengabdianMandiri = json_decode(file_get_contents($lppmLinkPengabdianMandiri,false, stream_context_create($arrContextOptions)),true);
            $this->db->insert_batch('lppm_pengabdian_mandiri', $lppmPengabdianMandiri);
            endif;
          
          if($pengabdian == 'ya'):
          $this->db->truncate('lppm_pengabdian');
          // pengabdian
          $lppmLinkPengabdian = "https://lppm.unimed.ac.id/simppm2/pengabdian";
          $lppmPengabdian = json_decode(file_get_contents($lppmLinkPengabdian,false, stream_context_create($arrContextOptions)),true);
          $this->db->insert_batch('lppm_pengabdian', $lppmPengabdian);
          endif;

          if($submission == 'ya'):
            $this->db->truncate('lppm_submission');
            // submission
            $lppmLinkSubmission = "https://lppm.unimed.ac.id/simppm2/submission";
            $lppmSubmission = json_decode(file_get_contents($lppmLinkSubmission,false, stream_context_create($arrContextOptions)),true);
            $this->db->insert_batch('lppm_submission', $lppmSubmission);
            endif;

          if($data_submission_penelitian == 'ya'):
          $this->db->truncate('lppm_data_submission_penelitian');
          // data submission penelitian
          $lppmLinkLuaranDataSubmissionPenelitian = "https://lppm.unimed.ac.id/simppm2/data-submission-penelitian";
          $lppmKLuaranDataSubmissionPenelitian = json_decode(file_get_contents($lppmLinkLuaranDataSubmissionPenelitian,false, stream_context_create($arrContextOptions)),true);
          $this->db->insert_batch('lppm_data_submission_penelitian', $lppmKLuaranDataSubmissionPenelitian);
          endif;

          if($data_submission_penelitian_anggota == 'ya'):
          $this->db->truncate('lppm_data_submission_penelitian_anggota');
          // data submission penelitian anggota
          $lppmLinkLuaranDataSubmissionPenelitianAnggota = "https://lppm.unimed.ac.id/simppm2/data-submission-penelitian-anggota";
          $lppmKLuaranDataSubmissionPenelitianAnggota = json_decode(file_get_contents($lppmLinkLuaranDataSubmissionPenelitianAnggota,false, stream_context_create($arrContextOptions)),true);
          $this->db->insert_batch('lppm_data_submission_penelitian_anggota', $lppmKLuaranDataSubmissionPenelitianAnggota);
          endif;

          if($data_submission_penelitian_ketua == 'ya'):
          $this->db->truncate('lppm_data_submission_penelitian_ketua');
          // data submission penelitian ketua
          $lppmLinkDataSubmissionPenelitianKetua = "https://lppm.unimed.ac.id/simppm2/data-submission-penelitian-ketua";
          $lppmKDataSubmissionPenelitianKetua = json_decode(file_get_contents($lppmLinkDataSubmissionPenelitianKetua,false, stream_context_create($arrContextOptions)),true);
          $this->db->insert_batch('lppm_data_submission_penelitian_ketua', $lppmKDataSubmissionPenelitianKetua);
          endif;

          if($data_submission_penelitian_luaran == 'ya'):
          $this->db->truncate('lppm_data_submission_penelitian_luaran');
          // data submission penelitian luaran
          $lppmLinkDataSubmissionPenelitianLuaran = "https://lppm.unimed.ac.id/simppm2/data-submission-penelitian-luaran";
          $lppmKDataSubmissionPenelitianLuaran = json_decode(file_get_contents($lppmLinkDataSubmissionPenelitianLuaran,false, stream_context_create($arrContextOptions)),true);
          $this->db->insert_batch('lppm_data_submission_penelitian_luaran', $lppmKDataSubmissionPenelitianLuaran);
          endif;

          if($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $txt    = 'Terjadi kesalahan saat menyimpan data';
            $icon   = 'error';
            $msg = ['msg' => $txt,'status'=>FALSE,'icon'=>$icon];
            return FALSE;
          }else{
            $this->db->trans_commit();
            $msg = ['status'=>TRUE];
            return TRUE;
          }
          return $msg;
    }
  
}