<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_sinkron_simpeg extends CI_Model{
    public function __construct(){
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }

    public function saveSinkronSimpeg(){
      $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );

        $bagian                                   = trim($this->input->post('bagian'));
        $dosen_phl                                = trim($this->input->post('dosen_phl'));
        $dosen_pns                                = trim($this->input->post('dosen_pns'));
        $dosen_pns_simremlink                     = trim($this->input->post('dosen_pns_simremlink'));
        $golongan_dosen_pegawai                   = trim($this->input->post('golongan_dosen_pegawai'));
        $jabatan_fungsional_dosen                 = trim($this->input->post('jabatan_fungsional_dosen'));
        $jenis_tugas_tambahan_dosen               = trim($this->input->post('jenis_tugas_tambahan_dosen'));
        $jurusan                                  = trim($this->input->post('jurusan'));
        $kategori_jabatan_remun_pegawai_pns       = trim($this->input->post('kategori_jabatan_remun_pegawai_pns'));
        $kedudukan_dosen_pegawai                  = trim($this->input->post('kedudukan_dosen_pegawai'));
        $pegawai_phl                              = trim($this->input->post('pegawai_phl'));
        $pegawai_pns                              = trim($this->input->post('pegawai_pns'));
        $pegawai_pns_simremlink                   = trim($this->input->post('pegawai_pns_simremlink'));
        $prodi                                    = trim($this->input->post('prodi'));
        $riwayat_jabatan_fungsional               = trim($this->input->post('riwayat_jabatan_fungsional'));
        $riwayat_jabatan_remun_dosen_pns          = trim($this->input->post('riwayat_jabatan_remun_dosen_pns'));
        $riwayat_jabatan_remun_pegawai_pns        = trim($this->input->post('riwayat_jabatan_remun_pegawai_pns'));
        $riwayat_jabatan_tugas_tambahan_dosen_pns = trim($this->input->post('riwayat_jabatan_tugas_tambahan_dosen_pns'));
        $riwayat_penghargaan_dosen_pns            = trim($this->input->post('riwayat_penghargaan_dosen_pns'));
        $riwayat_penghargaan_pegawai_pns          = trim($this->input->post('riwayat_penghargaan_pegawai_pns'));
        $status                                   = trim($this->input->post('status'));
        $status                                   = trim($this->input->post('status_aktif_non_aktif'));
        $unit                                     = trim($this->input->post('unit'));
        $capaian_skp_dosen                        = trim($this->input->post('capaian_skp_dosen'));
        $capaian_skp_pegawai                      = trim($this->input->post('capaian_skp_pegawai'));
        $nilai_sikap_dan_perilaku_pegawai         = trim($this->input->post('nilai_sikap_dan_perilaku_pegawai'));
        $riwayat_jft_pegawai                      = trim($this->input->post('riwayat_jft_pegawai'));
        $riwayat_jfu_pegawai                      = trim($this->input->post('riwayat_jfu_pegawai'));
        $riwayat_struktural_pegawai               = trim($this->input->post('riwayat_struktural_pegawai'));
        $capaian_bkd_dosen                        = trim($this->input->post('capaian_bkd_dosen'));
        $absensi_pegawai_tendik                   = trim($this->input->post('absensi_pegawai_tendik'));
        $absensi_dosen_pns                        = trim($this->input->post('absensi_dosen_pns'));
        $lhkasn_lhkpn_pegawai_pns                 = trim($this->input->post('lhkasn_lhkpn_pegawai_pns'));
        $hukuman_disiplin_pegawai_pns             = trim($this->input->post('hukuman_disiplin_pegawai_pns'));

        $this->db->trans_begin();
        
        if($bagian == 'ya'):
        $this->db->truncate('simpeg_bagian');
        // bagian
        $simpegLinkBagian = "https://simpeg.unimed.ac.id/jsonsimpeg/data/unit_bagian.php?api_key=08c24cee598007e14e5ba4284b4d1aa7";
        $simpegBagian = json_decode(file_get_contents($simpegLinkBagian,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_bagian', $simpegBagian);
        endif;


        if($dosen_phl                                == 'ya'):
          $this->db->truncate('simpeg_dosen_phl');
          // dosen phl
          $simpegLinkDosenPhl = "https://simpeg.unimed.ac.id/jsonsimpeg/data/tdosenphl.php?api_key=053f210055e44647a51b2a2d8282e6b2";
          $simpegDosenPhl = json_decode(file_get_contents($simpegLinkDosenPhl,false, stream_context_create($arrContextOptions)),true);
          $this->db->insert_batch('simpeg_dosen_phl', $simpegDosenPhl);
        endif;
        if($dosen_pns                                == 'ya'):
          $this->db->truncate('simpeg_dosen_pns');
          // dosen pns
        $simpegLinkDosenPns = "https://simpeg.unimed.ac.id/jsonsimpeg/data/tdosen.php?api_key=e807f1fcf82d132f9bb018ca6738a19f";
        $simpegDosenPns = json_decode(file_get_contents($simpegLinkDosenPns,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_dosen_pns', $simpegDosenPns);
        endif;
        if($dosen_pns_simremlink                     == 'ya'):
          $this->db->truncate('simpeg_dosen_pns_simremlink');
          // dosen pns simremlink
        $simpegLinkDosenPnsSimremlink = "https://simpeg.unimed.ac.id/jsonsimpeg/data/dosensimremlink.php?api_key=922c8c46fcecee6340ac2d0790e68be8";
        $simpegDosenPnsSimremlink = json_decode(file_get_contents($simpegLinkDosenPnsSimremlink,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_dosen_pns_simremlink', $simpegDosenPnsSimremlink);
        endif;
        if($golongan_dosen_pegawai                   == 'ya'):
          $this->db->truncate('simpeg_golongan_dosen_pegawai');
          // golongan
        $simpegLinkGolongan = "https://simpeg.unimed.ac.id/jsonsimpeg/data/golongan.php?api_key=732ad3502ece5dc896b916b9b2e4db35";
        $simpegGolongan = json_decode(file_get_contents($simpegLinkGolongan,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_golongan_dosen_pegawai', $simpegGolongan);
        endif;
        if($jabatan_fungsional_dosen                 == 'ya'):
          $this->db->truncate('simpeg_jabatan_fungsional_dosen');
          // jabatan
        $simpegLinkJabatan = "https://simpeg.unimed.ac.id/jsonsimpeg/data/jabatan_fungsional.php?api_key=7c950102d7f3d7f54a459a18b9896013";
        $simpegJabatan = json_decode(file_get_contents($simpegLinkJabatan,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_jabatan_fungsional_dosen', $simpegJabatan);
        endif;
        if($jenis_tugas_tambahan_dosen               == 'ya'):
          $this->db->truncate('simpeg_jenis_tugas_tambahan_dosen');
          // tugas tambahan
        $simpegLinkTugasTambahan = "https://simpeg.unimed.ac.id/jsonsimpeg/data/tugas_tambahan.php?api_key=dd7db5d1c2dd1eff113571d530ba1a90";
        $simpegTugasTambahan = json_decode(file_get_contents($simpegLinkTugasTambahan,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_jenis_tugas_tambahan_dosen', $simpegTugasTambahan);
        endif;
        if($jurusan                                  == 'ya'):
          $this->db->truncate('simpeg_jurusan');
          // jurusan
        $simpegLinkJurusan = "https://simpeg.unimed.ac.id/jsonsimpeg/data/jurusan.php?api_key=a409bb11ce3545bf46c17f2f96f36770";
        $simpegJurusan = json_decode(file_get_contents($simpegLinkJurusan,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_jurusan', $simpegJurusan);
        endif;
        if($kategori_jabatan_remun_pegawai_pns       == 'ya'):
          $this->db->truncate('simpeg_kategori_jabatan_remun_pegawai_pns');
          // kategori jabatan remun pegawai pns
        $simpegLinkKategoriJabatanRemunPegawaiPns = "https://simpeg.unimed.ac.id/jsonsimpeg/data/jab_remun.php?api_key=8c10e60c19e33a8b86698e88744915da";
        $simpegKategoriJabatanRemunPegawaiPns = json_decode(file_get_contents($simpegLinkKategoriJabatanRemunPegawaiPns,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_kategori_jabatan_remun_pegawai_pns', $simpegKategoriJabatanRemunPegawaiPns);
        endif;
        if($kedudukan_dosen_pegawai                  == 'ya'):
          $this->db->truncate('simpeg_kedudukan_dosen_pegawai');
          // kedudukan
        $simpegLinkKedudukan = "https://simpeg.unimed.ac.id/jsonsimpeg/data/kedudukan.php?api_key=a7bcce93ba39a8614bc17f9528f817d1";
        $simpegKedudukan = json_decode(file_get_contents($simpegLinkKedudukan,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_kedudukan_dosen_pegawai', $simpegKedudukan);
        endif;
        if($pegawai_phl                              == 'ya'):
          $this->db->truncate('simpeg_pegawai_phl');
          // pegawai phl
        $simpegLinPegawaiPhl = "https://simpeg.unimed.ac.id/jsonsimpeg/data/tpegawaiphl.php?api_key=83404a861e0215e0f6787aee808019c1";
        $simpegPegawaiPhl = json_decode(file_get_contents($simpegLinPegawaiPhl,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_pegawai_phl', $simpegPegawaiPhl);
        endif;
        if($pegawai_pns                              == 'ya'):
          $this->db->truncate('simpeg_pegawai_pns');
          // pegawai pns
        $simpegLinkPegawaiPns = "https://simpeg.unimed.ac.id/jsonsimpeg/data/tpegawai.php?api_key=bb31ba2d3b18e743dc35c1aed6d80774";
        $simpegPegawaiPns = json_decode(file_get_contents($simpegLinkPegawaiPns,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_pegawai_pns', $simpegPegawaiPns);
        endif;
        if($pegawai_pns_simremlink                   == 'ya'):
          $this->db->truncate('simpeg_pegawai_pns_simremlink');
          // pegawai pns simremlink
        $simpegLinkPegawaiPnsSimremlink = "https://simpeg.unimed.ac.id/jsonsimpeg/data/pegawaipns_simremlink.php?api_key=5121a251c84c06ebdfb10afdacf58f05";
        $simpegPegawaiPnsSimremlink = json_decode(file_get_contents($simpegLinkPegawaiPnsSimremlink,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_pegawai_pns_simremlink', $simpegPegawaiPnsSimremlink);
        endif;
        if($prodi                                    == 'ya'):
          $this->db->truncate('simpeg_prodi');
          // prodi
        $simpegLinkProdi = "https://simpeg.unimed.ac.id/jsonsimpeg/data/prodi.php?api_key=90a860e51ed04a1c8ac0e79a0e82cb8d";
        $simpegProdi = json_decode(file_get_contents($simpegLinkProdi,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_prodi', $simpegProdi);
        endif;
        if($riwayat_jabatan_fungsional               == 'ya'):
          $this->db->truncate('simpeg_riwayat_jabatan_fungsional');
          // riwayat jabatan fungsional tertentu dosen
        $simpegLinkRiwayatJabatanFungsionalTertentuDosen = "https://simpeg.unimed.ac.id/jsonsimpeg/data/jabatan_jft_dosen.php?api_key=c645ad9b6432aa39be6551983be6c61c";
        $simpegRiwayatJabatanFungsionalTertentuDosen = json_decode(file_get_contents($simpegLinkRiwayatJabatanFungsionalTertentuDosen,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_riwayat_jabatan_fungsional', $simpegRiwayatJabatanFungsionalTertentuDosen);
        endif;
        if($riwayat_jabatan_remun_dosen_pns          == 'ya'):
          $this->db->truncate('simpeg_riwayat_jabatan_remun_dosen_pns');
          // riwayat jabatan remun dosen
        $simpegLinkRiwayatJabatanRemunDosenPns = "https://simpeg.unimed.ac.id/jsonsimpeg/data/jabatan_remun_dosen.php?api_key=2879b5e37589f43610663c95c7aa8dfa";
        $simpegRiwayatJabatanRemunDosenPns = json_decode(file_get_contents($simpegLinkRiwayatJabatanRemunDosenPns,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_riwayat_jabatan_remun_dosen_pns', $simpegRiwayatJabatanRemunDosenPns);
        endif;
        if($riwayat_jabatan_remun_pegawai_pns        == 'ya'):
          $this->db->truncate('simpeg_riwayat_jabatan_remun_pegawai_pns');
          // riwayat jabatan remun pegawai
        $simpegLinkRiwayatJabatanRemunPegawaiPns = "https://simpeg.unimed.ac.id/jsonsimpeg/data/rjabatan_remun_pegawai.php?api_key=33d78bd918e6eed6200dcfdae48200b5";
        $simpegRiwayatJabatanRemunPegawaiPns = json_decode(file_get_contents($simpegLinkRiwayatJabatanRemunPegawaiPns,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_riwayat_jabatan_remun_pegawai_pns', $simpegRiwayatJabatanRemunPegawaiPns);
        endif;
        if($riwayat_jabatan_tugas_tambahan_dosen_pns == 'ya'):
          $this->db->truncate('simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns');
          // riwayat jabatan tugas tambahan dosen pns
        $simpegLinkRiwayatJabatanRemunDosenPns = "https://simpeg.unimed.ac.id/jsonsimpeg/data/rtugas_tambahan_dosen.php?api_key=e244209c6fc4d7a335501963d07a4021";
        $simpegRiwayatJabatanRemunDosenPns = json_decode(file_get_contents($simpegLinkRiwayatJabatanRemunDosenPns,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_riwayat_jabatan_tugas_tambahan_dosen_pns', $simpegRiwayatJabatanRemunDosenPns);
        endif;
        if($riwayat_penghargaan_dosen_pns            == 'ya'):
          $this->db->truncate('simpeg_riwayat_penghargaan_dosen_pns');
          // riwayat penghargaan dosen pns
        $simpegLinkRiwayatJPenghargaanDosenPns = "https://simpeg.unimed.ac.id/jsonsimpeg/data/r_penghargaan_dosen.php?api_key=039372cf8973d827f35032f382e52fbc";
        $simpegRiwayatJPenghargaanDosenPns = json_decode(file_get_contents($simpegLinkRiwayatJPenghargaanDosenPns,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_riwayat_penghargaan_dosen_pns', $simpegRiwayatJPenghargaanDosenPns);
        endif;
        if($riwayat_penghargaan_pegawai_pns          == 'ya'):
          $this->db->truncate('simpeg_riwayat_penghargaan_pegawai_pns');
          // riwayat penghargaan pegawai pns
        $simpegLinkRiwayatPenghargaanPegawaiPns = "https://simpeg.unimed.ac.id/jsonsimpeg/data/r_penghargaan_pegawai.php?api_key=2465f2b2ab4414d12cc764f101ce31b6";
        $simpegRiwayatPenghargaanPegawaiPns = json_decode(file_get_contents($simpegLinkRiwayatPenghargaanPegawaiPns,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_riwayat_penghargaan_pegawai_pns', $simpegRiwayatPenghargaanPegawaiPns);
        endif;
        if($status                                   == 'ya'):
          $this->db->truncate('simpeg_status');
          // status
        $simpegLinkStatus = "https://simpeg.unimed.ac.id/jsonsimpeg/data/status_pegawai.php?api_key=32082e6f1cb84c159520806286dfd96f";
        $simpegStatus = json_decode(file_get_contents($simpegLinkStatus,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_status', $simpegStatus);
        endif;
        if($status                                   == 'ya'):
          $this->db->truncate('simpeg_status_aktif_non_aktif');
          // aktif non aktif
        $simpegLinkAktifNonAktif = "https://simpeg.unimed.ac.id/jsonsimpeg/data/status_aktif.php?api_key=7858b904c37985bfa23e2083f4c5478b";
          $simpegAktifNonAktif = json_decode(file_get_contents($simpegLinkAktifNonAktif,false, stream_context_create($arrContextOptions)),true);
          $this->db->insert_batch('simpeg_status_aktif_non_aktif', $simpegAktifNonAktif);
        endif;
        if($unit                                     == 'ya'):
          $this->db->truncate('simpeg_unit');
          // unit
        $simpegLinkUnit = "https://simpeg.unimed.ac.id/jsonsimpeg/data/unit.php?api_key=83b956f064c53aca3155c93e2aecd2e7";
        $simpegUnit = json_decode(file_get_contents($simpegLinkUnit,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_unit', $simpegUnit);
        endif;
        if($capaian_skp_dosen                        == 'ya'):
          $this->db->truncate('simpeg_capaian_skp_dosen');
          // capaian skp dosen
        $simpegLinkCapaianSkpDosen = "https://simpeg.unimed.ac.id/jsonsimpeg/data/skp_capaian_dosen.php?api_key=d4fde9382eda0a35bd81fcbdf5594d4b";
        $simpegCapaianSkpDosen = json_decode(file_get_contents($simpegLinkCapaianSkpDosen,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_capaian_skp_dosen', $simpegCapaianSkpDosen);
        endif;
        if($capaian_skp_pegawai                      == 'ya'):
          $this->db->truncate('simpeg_capaian_skp_pegawai');
          // capaian skp pegawai
        $simpegLinkCapaianSkpPegawai = "https://simpeg.unimed.ac.id/jsonsimpeg/data/skp_capaian_pegawai.php?api_key=193220653d34edffb6ace45748c856aa";
        $simpegCapaianSkpPegawai = json_decode(file_get_contents($simpegLinkCapaianSkpPegawai,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_capaian_skp_pegawai', $simpegCapaianSkpPegawai);
        endif;
        if($nilai_sikap_dan_perilaku_pegawai         == 'ya'):
          $this->db->truncate('simpeg_nilai_sikap_dan_perilaku_pegawai');
          // nilai sikap dan perilaku pegawai
        $simpegLinkNilaiSikapDanPerilakuPegawai = "https://simpeg.unimed.ac.id/jsonsimpeg/data/skp_capaian_pegawai_pk.php?api_key=c8673e28ac7b45a04daceb94eac9b912";
        $simpegNilaiSikapDanPerilakuPegawai = json_decode(file_get_contents($simpegLinkNilaiSikapDanPerilakuPegawai,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_nilai_sikap_dan_perilaku_pegawai', $simpegNilaiSikapDanPerilakuPegawai);
        endif;

        if($riwayat_jft_pegawai         == 'ya'):
          $this->db->truncate('simpeg_riwayat_jft_pegawai');
          // riwayat jft pegawai
        $simpegLinkRiwayatJftPegawai = "https://simpeg.unimed.ac.id/jsonsimpeg/data/v_riwayat_jft_pegawai.php?api_key=ea3102366d62a7d2136aa00e86533055";
        $simpegRiwayatJftPegawai = json_decode(file_get_contents($simpegLinkRiwayatJftPegawai,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_riwayat_jft_pegawai', $simpegRiwayatJftPegawai);
        endif;

        if($riwayat_jfu_pegawai         == 'ya'):
          $this->db->truncate('simpeg_riwayat_jfu_pegawai');
          // riwayat jfu pegawai
        $simpegLinkRiwayatJfuPegawai = "https://simpeg.unimed.ac.id/jsonsimpeg/data/v_jfu_pegawai.php?api_key=91d2496cd5d3399aa8258467152a726c";
        $simpegRiwayatJfuPegawai = json_decode(file_get_contents($simpegLinkRiwayatJfuPegawai,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_riwayat_jfu_pegawai', $simpegRiwayatJfuPegawai);
        endif;

        if($riwayat_struktural_pegawai         == 'ya'):
          $this->db->truncate('simpeg_riwayat_struktural_pegawai');
          // riwayat strutkural pegawai
        $simpegLinkRiwayatStrukturalPegawai = "https://simpeg.unimed.ac.id/jsonsimpeg/data/v_struktural_pegawai.php?api_key=86c73337e8e5d70b935090579d9e7a70";
        $simpegRiwayatStrukturalPegawai = json_decode(file_get_contents($simpegLinkRiwayatStrukturalPegawai,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_riwayat_struktural_pegawai', $simpegRiwayatStrukturalPegawai);
        endif;

        if($capaian_bkd_dosen         == 'ya'):
          $this->db->truncate('simpeg_capaian_bkd_dosen');
          // capaian bkd dosen
        $simpegLinkCapaianBksDosen = "https://simpeg.unimed.ac.id/jsonsimpeg/data/skp_capaian_bkd.php?api_key=f80f85236b0cee581fc015a21a661fa8 ";
        $simpegCapaianBksDosen = json_decode(file_get_contents($simpegLinkCapaianBksDosen,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_capaian_bkd_dosen', $simpegCapaianBksDosen);
        endif;

        if($absensi_pegawai_tendik         == 'ya'):
          $this->db->truncate('simpeg_absensi_pegawai_tendik');
          // absensi pegawai tendik
        $simpegLinkAbsensiPegawaiTendik = "https://simpeg.unimed.ac.id/jsonsimpeg/data/skp_absen_remun.php?api_key=eb8d5d1e171695a335df120776eefdad";
        $simpegAbsensiPegawaiTendik = json_decode(file_get_contents($simpegLinkAbsensiPegawaiTendik,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_absensi_pegawai_tendik', $simpegAbsensiPegawaiTendik);
        endif;

        if($absensi_dosen_pns         == 'ya'):
          $this->db->truncate('simpeg_absensi_dosen_pns');
          // capaian dosen pns
        $simpegLinkCapaianDosenPns = "https://simpeg.unimed.ac.id/jsonsimpeg/data/skp_absen_remun_dosen.php?api_key=da95ea136d1053aad4f908a69b2c9be4";
        $simpegCapaianDosenPns = json_decode(file_get_contents($simpegLinkCapaianDosenPns,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_absensi_dosen_pns', $simpegCapaianDosenPns);
        endif;

        if($lhkasn_lhkpn_pegawai_pns         == 'ya'):
          $this->db->truncate('simpeg_lhkasn_lhkpn_pegawai_pns');
          // lhkasn lhkpn pegawai pns
        $simpegLinkLhkasnLhkpnPegawaiPns = "https://simpeg.unimed.ac.id/jsonsimpeg/data/v_lhkpnpeg.php?api_key=8a87cb3937ef753261c16c13af90e95b";
        $simpegLhkasnLhkpnPegawaiPns = json_decode(file_get_contents($simpegLinkLhkasnLhkpnPegawaiPns,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_lhkasn_lhkpn_pegawai_pns', $simpegLhkasnLhkpnPegawaiPns);
        endif;

        if($hukuman_disiplin_pegawai_pns         == 'ya'):
          $this->db->truncate('simpeg_hukuman_disiplin_pegawai_pns');
          // hukuman disiplin pegawai pns
        $simpegLinkHukumanDisiplinPegawaiPns = "https://simpeg.unimed.ac.id/jsonsimpeg/data/v_hukdispeg.php?api_key=77c4d51faf8c555884db1eea4218405e";
        $simpegHukumanDisiplinPegawaiPns = json_decode(file_get_contents($simpegLinkHukumanDisiplinPegawaiPns,false, stream_context_create($arrContextOptions)),true);
        $this->db->insert_batch('simpeg_hukuman_disiplin_pegawai_pns', $simpegHukumanDisiplinPegawaiPns);
        endif;

        //================== simpeg ==================
        if($this->db->trans_status() === FALSE){
          $this->db->trans_rollback();
          $txt    = 'Terjadi kesalahan saat menyimpan data';
          $icon   = 'error';
          $msg = ['msg' => $txt,'status'=>FALSE,'icon'=>$icon];
          return FALSE;
        }else{
          $this->db->trans_commit();
          $msg = ['status'=>TRUE];
          return TRUE;
        }
          return $msg;
    }
    
}