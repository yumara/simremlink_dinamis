<table class="table table-striped" id="konten">
    <thead>
        <tr>
            <th>No</th>
            <th>Keterangan</th>
            <th>Link</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1</td>
            <td>Kelebihan Mengajar</td>
            <td><a href="<?= base_url('api/akad/kelebihan-mengajar') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/akad/kelebihan-mengajar</a></td>
        </tr>
        <tr>
            <td>2</td>
            <td>Pembimbing Akademik</td>
            <td><a href="<?= base_url('api/akad/pembimbing-akademik') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/akad/pembimbing-akademik</a></td>
        </tr>

        <tr>
            <td>3</td>
            <td>Pembimbing Skripsi Tesis Disertasi</td>
            <td><a href="<?= base_url('api/akad/pembimbing-skripsi-tesis-disertasi') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/akad/pembimbing-skripsi-tesis-disertasi</a></td>
        </tr>

        <tr>
            <td>4</td>
            <td>Penguji Skripsi Tesis Disertasi</td>
            <td><a href="<?= base_url('api/akad/penguji-skripsi-tesis-disertasi') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/akad/penguji-skripsi-tesis-disertasi</a></td>
        </tr>

        <tr>
            <td>5</td>
            <td>Mengajar Lainnya</td>
            <td><a href="<?= base_url('api/akad/mengajar-lainnya') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/akad/mengajar-lainnya</a></td>
        </tr>
    </tbody>
</table>