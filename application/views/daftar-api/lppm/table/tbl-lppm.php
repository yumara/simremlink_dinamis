<table class="table table-striped" id="konten">
    <thead>
        <tr>
            <th>No</th>
            <th>Keterangan</th>
            <th>Link</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1</td>
            <td>Penelitian</td>
            <td><a href="<?= base_url('api/lppm/penelitian') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/penelitian</a></td>
        </tr>
        <tr>
            <td>2</td>
            <td>Pengabdian</td>
            <td><a href="<?= base_url('api/lppm/pengabdian') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/pengabdian</a></td>
        </tr>
        <tr>
            <td>3</td>
            <td>Luaran Penelitian Mandiri</td>
            <td><a href="<?= base_url('api/lppm/luaran-penelitian-mandiri') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/luaran-penelitian-mandiri</a></td>
        </tr>
        <tr>
            <td>4</td>
            <td>Luaran Pengabdian Mandiri</td>
            <td><a href="<?= base_url('api/lppm/luaran-pengabdian-mandiri') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/luaran-pengabdian-mandiri</a></td>
        </tr>
        <tr>
            <td>5</td>
            <td>Luaran Valid Tambahan</td>
            <td><a href="<?= base_url('api/lppm/luaran-valid-tambahan')?>"
                    target="_blank">https://wsdb.unimed.ac.id/luaran-valid-tambahan</a></td>
        </tr>
        <tr>
            <td>6</td>
            <td>Luaran Valid Wajib</td>
            <td><a href="<?= base_url('api/lppm/luaran-valid-wajib') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/luaran-valid-wajib</a></td>
        </tr>
        <tr>
            <td>7</td>
            <td>Jenis Luaran</td>
            <td><a href="<?= base_url('api/lppm/jenis-luaran') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/jenis-luaran</a></td>
        </tr>
        <tr>
            <td>8</td>
            <td>Skim</td>
            <td><a href="<?= base_url('api/lppm/skim') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/skim</a></td>
        </tr>
        <tr>
            <td>9</td>
            <td>status-penulis</td>
            <td><a href="<?= base_url('api/lppm/status-penulis') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/status-penulis</a></td>
        </tr>
        <tr>
            <td>10</td>
            <td>Sumberdana</td>
            <td><a href="<?= base_url('api/lppm/sumnerdana') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/sumnerdana</a></td>
        </tr>
        <tr>
            <td>11</td>
            <td>luaran-valid-gabungan</td>
            <td><a href="<?= base_url('api/lppm/luaran-valid-gabungan') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/luaran-valid-gabungan</a></td>
        </tr>
    </tbody>
</table>