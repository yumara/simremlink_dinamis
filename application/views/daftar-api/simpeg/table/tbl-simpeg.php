<table class="table table-striped" id="konten">
    <thead>
        <tr>
            <th>No</th>
            <th>Keterangan</th>
            <th>Link</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1</td>
            <td>Bagian</td>
            <td><a href="<?= base_url('api/simpeg/bagian') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simpeg/bagian</a></td>
        </tr>
        <tr>
            <td>2</td>
            <td>Dosen PHL</td>
            <td><a href="<?= base_url('api/simpeg/dosen-phl') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simpeg/dosen-phl</a></td>
        </tr>
        <tr>
            <td>3</td>
            <td>Dosen PNS</td>
            <td><a href="<?= base_url('api/simpeg/dosen-pns') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simpeg/dosen-pns</a></td>
        </tr>
        <tr>
            <td>4</td>
            <td>Dosen PNS Simremlink</td>
            <td><a href="<?= base_url('api/simpeg/dosen-pns-simremlink') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simpeg/dosen-pns-simremlink</a></td>
        </tr>
        <tr>
            <td>5</td>
            <td>Golongan</td>
            <td><a href="<?= base_url('api/simpeg/golongan-dosen-pegawai') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simpeg/golongan-dosen-pegawai</a></td>
        </tr>
        <tr>
            <td>6</td>
            <td>Jabatan</td>
            <td><a href="<?= base_url('api/simpeg/jabatan-fungsional-dosen') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simpeg/jabatan-fungsional-dosen</a></td>
        </tr>
        <tr>
            <td>7</td>
            <td>Tugas Tambahan</td>
            <td><a href="<?= base_url('api/simpeg/jenis-tugas-tambahan-dosen') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simpeg/jenis-tugas-tambahan-dosen</a></td>
        </tr>
        <tr>
            <td>8</td>
            <td>Jurusan</td>
            <td><a href="<?= base_url('api/simpeg/jurusan') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simpeg/jurusan</a></td>
        </tr>
        <tr>
            <td>9</td>
            <td>Kategori Jabatan Remun Pegawai PNS</td>
            <td><a href="<?= base_url('api/simpeg/kategori-jabatan-remun-pegawai-pns') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simpeg/kategori-jabatan-remun-pegawai-pns</a></td>
        </tr>
        <tr>
            <td>10</td>
            <td>Kedudukan</td>
            <td><a href="<?= base_url('api/simpeg/keududukan-dosen-pegawai') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simpeg/keududukan-dosen-pegawai</a></td>
        </tr>
        <tr>
            <td>11</td>
            <td>Pegawai PHL</td>
            <td><a href="<?= base_url('api/simpeg/pegawai-phl') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simpeg/pegawai-phl</a></td>
        </tr>
        <tr>
            <td>12</td>
            <td>Pegawai PNS</td>
            <td><a href="<?= base_url('api/simpeg/pegawai-pns') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simpeg/pegawai-pns</a></td>
        </tr>
        <tr>
            <td>13</td>
            <td>Pegawai PNS Simremlink</td>
            <td><a href="<?= base_url('api/simpeg/pegawai-pns-simremlink') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simpeg/pegawai-pns-simremlink</a></td>
        </tr>
        <tr>
            <td>14</td>
            <td>Riwayat Jabatan Fungsional Tertentu Dosen</td>
            <td><a href="<?= base_url('api/simpeg/riwayat-jabatan-fungsional') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simpeg/riwayat-jabatan-fungsional</a></td>
        </tr>
        <tr>
            <td>15</td>
            <td>Riwayat Jabatan Remun Dosen</td>
            <td><a href="<?= base_url('api/simpeg/riwayat-jabatan-remun-dosen-pns') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simpeg/riwayat-jabatan-remun-dosen-pns</a></td>
        </tr>
        <tr>
            <td>16</td>
            <td>Riwayat Jabatan Remun Pegawai</td>
            <td><a href="<?= base_url('api/simpeg/riwayat-jabatan-remun-pegawai-pns') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simpeg/riwayat-jabatan-remun-pegawai-pns</a></td>
        </tr>
        <tr>
            <td>17</td>
            <td>Riwayat Jabatan Tugas Tambahan Dosen PNS</td>
            <td><a href="<?= base_url('api/simpeg/riwayat-jabatan-tugas-tambahan-dosen-pns') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simpeg/riwayat-jabatan-tugas-tambahan-dosen-pns</a></td>
        </tr>
        <tr>
            <td>18</td>
            <td>Riwayat Penghargaan Dosen PNS</td>
            <td><a href="<?= base_url('api/simpeg/riwayat-penghargaan-dosen-pns') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simpeg/riwayat-penghargaan-dosen-pns</a></td>
        </tr>
        <tr>
            <td>19</td>
            <td>Riwayat Penghargaan Pegawai PNS</td>
            <td><a href="<?= base_url('api/simpeg/riwayat-penghargaan-pegawai-pns')?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simpeg/riwayat-penghargaan-pegawai-pn</a></td>
        </tr>
        <tr>
            <td>20</td>
            <td>Prodi</td>
            <td><a href="<?= base_url('api/simpeg/prodi') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simpeg/prodi</a></td>
        </tr>
        <tr>
            <td>21</td>
            <td>Status</td>
            <td><a href="<?= base_url('api/simpeg/status') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simpeg/status</a></td>
        </tr>
        <tr>
            <td>22</td>
            <td>Status Aktif Non Aktif</td>
            <td><a href="<?= base_url('api/simpeg/status-aktif-non-aktif') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simpeg/status-aktif-non-aktif</a></td>
        </tr>
        <tr>
            <td>23</td>
            <td>Unit</td>
            <td><a href="<?= base_url('api/simpeg/unit') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simpeg/unit</a></td>
        </tr>
        <tr>
            <td>24</td>
            <td>Capaian SKP Dosen</td>
            <td><a href="<?= base_url('api/simpeg/capaian-skp-dosen') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simpeg/capaian-skp-dosen</a></td>
        </tr>
        <tr>
            <td>25</td>
            <td>Capaian SKP Pegawai</td>
            <td><a href="<?= base_url('api/simpeg/capaian-skp-pegawai') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simpeg/capaian-skp-pegawai</a></td>
        </tr>
        <tr>
            <td>26</td>
            <td>Nilai Sikap dan Perilaku Pegawai</td>
            <td><a href="<?= base_url('api/simpeg/nilai-sikap-dan-perilaku-pegawai') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simpeg/nilai-sikap-dan-perilaku-pegawai</a></td>
        </tr>
        <tr>
            <td>27</td>
            <td>Gabungan Data Dosen Dan Pegawai</td>
            <td><a href="<?= base_url('api/simpeg/dosen-pegawai-pns') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simpeg/dosen-pegawai-pns</a></td>
        </tr>
        <tr>
            <td>28</td>
            <td>Gabungan Data Dosen Dan Pegawai Untuk Kebutuhan Simremlink</td>
            <td><a href="<?= base_url('api/simpeg/dosen-pegawai-simremlink') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simpeg/dosen-pegawai-simremlink</a></td>
        </tr>
        <tr>
            <td>29</td>
            <td>Gabungan Riwayat Dosen Dan Pegawai Untuk Kebutuhan Simremlink</td>
            <td><a href="<?= base_url('api/simpeg/riwayat-dosen-pegawai-simremlink') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simpeg/riwayat-dosen-pegawai-simremlink</a></td>
        </tr>

        <tr>
            <td>30</td>
            <td>Gabungan Dosen Dan Pegawai Dengan Status Aktif 00</td>
            <td><a href="<?= base_url('api/simpeg/dosen-pegawai-00') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simpeg/dosen-pegawai-00</a></td>
        </tr>
    </tbody>
</table>