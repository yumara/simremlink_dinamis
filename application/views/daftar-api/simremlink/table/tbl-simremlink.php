<table class="table table-striped" id="konten">
    <thead>
        <tr>
            <th>No</th>
            <th>Keterangan</th>
            <th>Link</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1</td>
            <td>Data Aktvitas</td>
            <td><a href="<?= base_url('api/simremlink/data-aktivitas') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simremlink/data-aktivitas</a></td>
        </tr>
        <tr>
            <td>2</td>
            <td>Golongan</td>
            <td><a href="<?= base_url('api/simremlink/data-golongan-pns') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simremlink/data-golongan-pns</a></td>
        </tr>
        <tr>
            <td>3</td>
            <td>Grade</td>
            <td><a href="<?= base_url('api/simremlink/data-grade') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simremlink/data-grade</a></td>
        </tr>
        <tr>
            <td>4</td>
            <td>Jabatan Akad</td>
            <td><a href="<?= base_url('api/simremlink/data-jabatan-akad') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simremlink/data-jabatan-akad</a></td>
        </tr>
        <tr>
            <td>5</td>
            <td>Jenis Dokumen</td>
            <td><a href="<?= base_url('api/simremlink/data-jenis-dokumen') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simremlink/data-jenis-dokumen</a></td>
        </tr>
        <tr>
            <td>6</td>
            <td>Kelompok Jabatan</td>
            <td><a href="<?= base_url('api/simremlink/data-kelompok-jabatan') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simremlink/data-kelompok-jabatan</a></td>
        </tr>
        <tr>
            <td>7</td>
            <td>Jenis Pegawai</td>
            <td><a href="<?= base_url('api/simremlink/data-jenis-pegawai') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simremlink/data-jenis-pegawai</a></td>
        </tr>
        <tr>
            <td>8</td>
            <td>Jenjang Pendidikan</td>
            <td><a href="<?= base_url('api/simremlink/data-jenjang-pendidikan') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simremlink/data-jenjang-pendidikan</a></td>
        </tr>
        <tr>
            <td>9</td>
            <td>KDN</td>
            <td><a href="<?= base_url('api/simremlink/data-kdn') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simremlink/data-kdn</a></td>
        </tr>
        <tr>
            <td>10</td>
            <td>Kelas Jabatan</td>
            <td><a href="<?= base_url('api/simremlink/data-kelas-jabatan') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simremlink/data-kelas-jabatan</a></td>
        </tr>
        <tr>
            <td>11</td>
            <td>Jabatan Akad</td>
            <td><a href="<?= base_url('api/simremlink/data-jabatan-akad') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simremlink/data-jabatan-akad</a></td>
        </tr>
        <tr>
            <td>12</td>
            <td>Nama Jabatan</td>
            <td><a href="<?= base_url('api/simremlink/data-nama-jabatan') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simremlink/data-nama-jabatan</a></td>
        </tr>
        <tr>
            <td>13</td>
            <td>Pekerjaan</td>
            <td><a href="<?= base_url('api/simremlink/data-pekerjaan') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simremlink/data-pekerjaan</a></td>
        </tr>
        <tr>
            <td>14</td>
            <td>Peran</td>
            <td><a href="<?= base_url('api/simremlink/data-peran') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simremlink/data-peran</a></td>
        </tr>
        <tr>
            <td>15</td>
            <td>Periode</td>
            <td><a href="<?= base_url('api/simremlink/data-periode') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simremlink/data-periode</a></td>
        </tr>
        <tr>
            <td>16</td>
            <td>Rekening</td>
            <td><a href="<?= base_url('api/simremlink/data-rekening') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simremlink/data-rekening</a></td>
        </tr>
        <tr>
            <td>17</td>
            <td>Status Pegawai</td>
            <td><a href="<?= base_url('api/simremlink/data-status-pegawai') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simremlink/data-status-pegawai</a></td>
        </tr>
        <tr>
            <td>18</td>
            <td>Status Sipil</td>
            <td><a href="<?= base_url('api/simremlink/data-status-sipil') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simremlink/data-status-sipil</a></td>
        </tr>
        <tr>
            <td>19</td>
            <td>Tusi</td>
            <td><a href="<?= base_url('api/simremlink/tusi') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simremlink/tusi</a></td>
        </tr>
        <tr>
            <td>20</td>
            <td>Data Mengajar</td>
            <td><a href="<?= base_url('api/simremlink/mengajar') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simremlink/mengajar</a></td>
        </tr>
        <tr>
            <td>21</td>
            <td>Data Mengajar Lainnya</td>
            <td><a href="<?= base_url('api/simremlink/mengajar-lainnya') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simremlink/mengajar-lainnya</a></td>
        </tr>
        <tr>
            <td>22</td>
            <td>Data Penelitian</td>
            <td><a href="<?= base_url('api/simremlink/penelitian') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simremlink/penelitian</a></td>
        </tr>
        <tr>
            <td>23</td>
            <td>Pengabdian</td>
            <td><a href="<?= base_url('api/simremlink/pengabdian') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simremlink/pengabdian</a></td>
        </tr>
        <tr>
            <td>24</td>
            <td>Penghargaan</td>
            <td><a href="<?= base_url('api/simremlink/penghargaan') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simremlink/penghargaan</a></td>
        </tr>
        <tr>
            <td>25</td>
            <td>Penunjang</td>
            <td><a href="<?= base_url('api/simremlink/penunjang') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/simremlink/penunjang</a></td>
        </tr>
    </tbody>
</table>