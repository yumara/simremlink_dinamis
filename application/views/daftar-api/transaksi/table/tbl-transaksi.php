<table class="table table-striped" id="konten">
    <thead>
        <tr>
            <th>No</th>
            <th>Keterangan</th>
            <th>Link</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1</td>
            <td>Kelebihan Mengajar</td>
            <td><a href="<?= base_url('api/transaksi/kelebihan-mengajar') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/transaksi/kelebihan-mengajar</a></td>
        </tr>
        <tr>
            <td>2</td>
            <td>Pembimbing Akademik</td>
            <td><a href="<?= base_url('api/transaksi/pembimbing-akademik') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/transaksi/pembimbing-akademik</a></td>
        </tr>

        <tr>
            <td>3</td>
            <td>Pembimbing Skripsi Tesis Disertasi</td>
            <td><a href="<?= base_url('api/transaksi/pembimbing-skripsi-tesis-disertasi') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/transaksi/pembimbing-skripsi-tesis-disertasi</a></td>
        </tr>

        <tr>
            <td>4</td>
            <td>Penguji Skripsi Tesis Disertasi</td>
            <td><a href="<?= base_url('api/transaksi/penguji-skripsi-tesis-disertasi') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/transaksi/penguji-skripsi-tesis-disertasi</a></td>
        </tr>

        <tr>
            <td>5</td>
            <td>Mengajar Lainnya</td>
            <td><a href="<?= base_url('api/transaksi/mengajar-lainnya') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/transaksi/mengajar-lainnya</a></td>
        </tr>

        <tr>
            <td>6</td>
            <td>Riwayat Penghargaan Dosen</td>
            <td><a href="<?= base_url('api/transaksi/riwayat-penghargaan-dosen') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/transaksi/riwayat-penghargaan-dosen</a></td>
        </tr>
        <tr>
            <td>7</td>
            <td>Riwayat Penghargaan PNS</td>
            <td><a href="<?= base_url('api/transaksi/riwayat-pengharggan-pns') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/transaksi/riwayat-pengharggan-pns</a></td>
        </tr>

        <tr>
            <td>8</td>
            <td>Capaian SKP Dosen DT Konversi</td>
            <td><a href="<?= base_url('api/transaksi/capaian-skp-dosen-dt') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/transaksi/capaian-skp-dosen-dt</a></td>
        </tr>

        <tr>
            <td>9</td>
            <td>Capaian SKP Pegawai Konversi</td>
            <td><a href="<?= base_url('api/transaksi/capaian-skp-pegawai') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/transaksi/capaian-skp-pegawai</a></td>
        </tr>

        <tr>
            <td>10</td>
            <td>Nilai Sikap Dan Perilaku Pegawai Konversi</td>
            <td><a href="<?= base_url('api/transaksi/nilai-sikap-dan-perilaku-pegawai') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/transaksi/nilai-sikap-dan-perilaku-pegawai</a></td>
        </tr>

        <tr>
            <td>11</td>
            <td>Capaian BKD Dosen</td>
            <td><a href="<?= base_url('api/transaksi/capaian-bkd-dosen') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/transaksi/capaian-bkd-dosen</a></td>
        </tr>

        <tr>
            <td>12</td>
            <td>Absensi Pegawai Tendik</td>
            <td><a href="<?= base_url('api/transaksi/absensi-pegawai-tendik') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/transaksi/absensi-pegawai-tendik</a></td>
        </tr>
        <tr>
            <td>13</td>
            <td>Absensi Dosen PNS</td>
            <td><a href="<?= base_url('api/transaksi/absensi-dosen-pns') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/transaksi/absensi-dosen-pns</a></td>
        </tr>

        <tr>
            <td>14</td>
            <td>LHKASN / LHKPN Pegawai PNS</td>
            <td><a href="<?= base_url('api/transaksi/lhkasn-lhkpn-pegawai-pns') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/transaksi/lhkasn-lhkpn-pegawai-pns</a></td>
        </tr>

        <tr>
            <td>15</td>
            <td>Hukuman Disiplin Pegawai PNS</td>
            <td><a href="<?= base_url('api/transaksi/hukuman-disiplin-pegawai-pns') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/transaksi/hukuman-disiplin-pegawai-pns</a></td>
        </tr>

        <tr>
            <td>16</td>
            <td>Aktivitas</td>
            <td><a href="<?= base_url('api/transaksi/aktivitas') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/transaksi/aktivitas</a></td>
        </tr>

        <tr>
            <td>17</td>
            <td>Penghargaan</td>
            <td><a href="<?= base_url('api/transaksi/penghargaan') ?>"
                    target="_blank">https://wsdb.unimed.ac.id/api/transaksi/penghargaan</a></td>
        </tr>
    </tbody>
</table>