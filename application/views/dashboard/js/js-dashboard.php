<script src="<?= base_url() ?>assets/js/dashboard1.js" type="text/javascript"></script>
<script>
$(window).on("load", function () {

//Get the context of the Chart canvas element we want to select
var ctx = $("#line-chart");

// Chart Options
var chartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    legend: {
        position: 'bottom',
    },
    hover: {
        mode: 'label'
    },
    scales: {
        xAxes: [{
            display: true,
            gridLines: {
                color: "#f3f3f3",
                drawTicks: false,
            },
            scaleLabel: {
                display: true,
                labelString: 'Grade'
            }
        }],
        yAxes: [{
            display: true,
            gridLines: {
                color: "#f3f3f3",
                drawTicks: false,
            },
            scaleLabel: {
                display: true,
                labelString: 'Value'
            }
        }]
    },
    title: {
        display: true,
        text: 'Grade Pegawai'
    }
};

    var datanyagrade = [];
    var gradenya = [];
    $.get("http://dev.unimed.ac.id/sso-api/simremlinkjwt/dashboard", function(data) {
        const json = data;
        $.each(json, function(index, obj) {
            gradenya.push(obj.grade);
            datanyagrade.push(obj.total);
            console.log(obj.grade);
        });
    }); 

    console.log(gradenya);
    console.log(datanyagrade);

// Chart Data
var chartData = {
    labels: gradenya,
    datasets: [{
        label: "Total Pegawai",
        data: datanyagrade,
        fill: false,
        borderDash: [5, 5],
        borderColor: "#00A5A8",
        pointBorderColor: "#00A5A8",
        pointBackgroundColor: "#FFF",
        pointBorderWidth: 2,
        pointHoverBorderWidth: 2,
        pointRadius: 4,
    }]
};

var config = {
    type: 'line',

    // Chart Options
    options: chartOptions,

    data: chartData
};

// Create the chart
var lineChart = new Chart(ctx, config);
});
</script>