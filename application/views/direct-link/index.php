<!DOCTYPE html>
<html class="loading" lang="en">
<head></head>
<body data-col="2-columns" class=" 2-columns ">
    <script src="<?= base_url() ?>assets/js/keycloak.js"></script>
    <script type="text/javascript">
    var kc = new Keycloak("assets/js/keycloak.json");
    window.onload = function() {
        kc.init({
            onLoad: 'login-required',
            checkLoginIframe: false,
        }).success(function() {
            var group = kc.tokenParsed['grup'];
            console.log(group);
            window.location.href="direct-link-to-dashboard"+"/"+group;
        });
    }
    </script>
</body>

</html>