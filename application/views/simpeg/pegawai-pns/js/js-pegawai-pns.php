<script>
$(window).on("load", function() {

    //  Donut Chart 1  Starts
    var donutChart1 = new Chartist.Pie('#donut-chart1', {
        series: [60, 40]
    }, {
        donut: true,
        donutWidth: 60,
        startAngle: 270,
        total: 200,
        showLabel: true
    });
    //  Donut Chart 1  Ends

});

$(window).on("load", function() {

    // line chart widget 1 configuration Starts
    var widgetlineChart = new Chartist.Line('#Widget-line-chart', {
        labels: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13"],
        series: [
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        ]
    }, {
        axisX: {
            showGrid: true,
            showLabel: false,
            offset: 0,
        },
        axisY: {
            showGrid: false,
            low: 40,
            showLabel: false,
            offset: 0,
        },
        lineSmooth: Chartist.Interpolation.cardinal({
            tension: 0
        }),
        fullWidth: true,
    });
    // line chart widget 1 configuration Ends

    // line chart widget 2 configuration Starts
    var widgetlineChart1 = new Chartist.Line('#Widget-line-chart1', {
        labels: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13"],
        series: [
            [50, 45, 60, 55, 70, 55, 60, 55, 65, 57, 60, 53, 53]
        ]
    }, {
        axisX: {
            showGrid: true,
            showLabel: false,
            offset: 0,
        },
        axisY: {
            showGrid: false,
            low: 40,
            showLabel: false,
            offset: 0,
        },
        lineSmooth: Chartist.Interpolation.cardinal({
            tension: 0
        }),
        fullWidth: true,
    });
    // line chart widget 2 configuration Ends

    // line chart widget 3 configuration Starts
    var widgetlineChart2 = new Chartist.Line('#Widget-line-chart2', {
        labels: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13"],
        series: [
            [50, 45, 60, 55, 70, 55, 60, 55, 65, 57, 60, 53, 53]
        ]
    }, {
        axisX: {
            showGrid: true,
            showLabel: false,
            offset: 0,
        },
        axisY: {
            showGrid: false,
            low: 40,
            showLabel: false,
            offset: 0,
        },
        lineSmooth: Chartist.Interpolation.cardinal({
            tension: 0
        }),
        fullWidth: true,
    });
    // line chart widget 3 configuration Ends

    // line chart widget 4 configuration Starts
    var widgetlineChart3 = new Chartist.Line('#Widget-line-chart3', {
        labels: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13"],
        series: [
            [50, 45, 60, 55, 70, 55, 60, 55, 65, 57, 60, 53, 53]
        ]
    }, {
        axisX: {
            showGrid: true,
            showLabel: false,
            offset: 0,
        },
        axisY: {
            showGrid: false,
            low: 40,
            showLabel: false,
            offset: 0,
        },
        lineSmooth: Chartist.Interpolation.cardinal({
            tension: 0
        }),
        fullWidth: true,
    });
    // line chart widget 4 configuration Ends


    //Get the context of the Chart canvas element we want to select
    var ctx = $("#simple-pie-chart");

    // Chart Options
    var chartOptions = {
        responsive: true,
        maintainAspectRatio: false,
        responsiveAnimationDuration: 500,
    };

    // Chart Data
    var chartData = {
        labels: ["January", "February", "March", "April", "May"],
        datasets: [{
            label: "My First dataset",
            data: [85, 65, 34, 45, 35],
            backgroundColor: ['#00A5A8', '#626E82', '#FF7D4D', '#FF4558', '#16D39A'],
        }]
    };

    var config = {
        type: 'pie',

        // Chart Options
        options: chartOptions,

        data: chartData
    };

    // Create the chart
    var pieSimpleChart = new Chart(ctx, config);

    //////////////////////////////////////////////////////////////////////////////////////

    //Get the context of the Chart canvas element we want to select
    var ctx = $("#column-chart");

    // Chart Options
    var chartOptions = {
        // Elements options apply to all of the options unless overridden in a dataset
        // In this case, we are setting the border of each bar to be 2px wide and green
        elements: {
            rectangle: {
                borderWidth: 2,
                borderColor: 'rgb(0, 255, 0)',
                borderSkipped: 'bottom'
            }
        },
        responsive: true,
        maintainAspectRatio: false,
        responsiveAnimationDuration: 500,
        legend: {
            position: 'top',
        },
        scales: {
            xAxes: [{
                display: true,
                gridLines: {
                    color: "#f3f3f3",
                    drawTicks: false,
                },
                scaleLabel: {
                    display: true,
                }
            }],
            yAxes: [{
                display: true,
                gridLines: {
                    color: "#f3f3f3",
                    drawTicks: false,
                },
                scaleLabel: {
                    display: true,
                }
            }]
        },
        title: {
            display: true,
            text: 'Chart.js Bar Chart'
        }
    };

    // Chart Data
    var chartData = {
        labels: ["January", "February", "March", "April", "May"],
        datasets: [{
            label: "My First dataset",
            data: [65, 59, 80, 81, 56],
            backgroundColor: "#16D39A",
            hoverBackgroundColor: "rgba(22,211,154,.9)",
            borderColor: "transparent"
        }, {
            label: "My Second dataset",
            data: [28, 48, 40, 19, 86],
            backgroundColor: "#F98E76",
            hoverBackgroundColor: "rgba(249,142,118,.9)",
            borderColor: "transparent"
        }]
    };

    var config = {
        type: 'bar',

        // Chart Options
        options: chartOptions,

        data: chartData
    };

    // Create the chart
    var lineChart = new Chart(ctx, config);

    ///////////////////////////////////////////////////////////////////////////////
});
</script>