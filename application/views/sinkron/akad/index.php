<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-12">
        <div class="card">
            <form method="post" id="form-add-new-data">
                <div class="card-content">
                    <div class="card-body">
                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="kelebihan_mengajar"
                                name="kelebihan_mengajar">
                            <label class="custom-control-label" for="kelebihan_mengajar">Kelebihan Mengajar</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="pembimbing_akademik"
                                name="pembimbing_akademik">
                            <label class="custom-control-label" for="pembimbing_akademik">Pembimbing Akademik</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input"
                                id="pembimbing_skripsi_tesis_disertasi" name="pembimbing_skripsi_tesis_disertasi">
                            <label class="custom-control-label" for="pembimbing_skripsi_tesis_disertasi">Pembimbing
                                Skripsi
                                Tesis Disertasi</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input"
                                id="penguji_skripsi_tesis_disertasi" name="penguji_skripsi_tesis_disertasi">
                            <label class="custom-control-label" for="penguji_skripsi_tesis_disertasi">Penguji Skripsi
                                Tesis
                                Disertasi</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="mengajar_lainnya"
                                name="mengajar_lainnya">
                            <label class="custom-control-label" for="mengajar_lainnya">Mengajar Lainnya</label>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" onclick="sinkronAll()" type="checkbox" class="custom-control-input" id="sinkron_all" name="sinkron_all">
                            <label class="custom-control-label" for="sinkron_all">Sinkron Semua</label>
                            
                            <button type="submit" class="float-right btn btn-primary"><i class="ft-repeat"></i> Sinkron</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>