<script>
function sinkronAll() {
    var readonlys = document.getElementById("sinkron_all");
    var tambah = document.getElementById("tambah");
    var edit = document.getElementById("edit");
    var hapus = document.getElementById("hapus");
    if (readonlys.checked == true) {
        $('#kelebihan_mengajar').attr('readonly', 'readonly');
        $('#pembimbing_akademik').attr('readonly', 'readonly');
        $('#pembimbing_skripsi_tesis_disertasi').attr('readonly', 'readonly');
        $('#mengajar_lainnya').attr('readonly', 'readonly');
        $('#penguji_skripsi_tesis_disertasi').attr('readonly', 'readonly');

        $('#kelebihan_mengajar').attr('checked', 'checked');
        $('#pembimbing_akademik').attr('checked', 'checked');
        $('#pembimbing_skripsi_tesis_disertasi').attr('checked', 'checked');
        $('#mengajar_lainnya').attr('checked', 'checked');
        $('#penguji_skripsi_tesis_disertasi').attr('checked', 'checked');
    } else {
        $('#kelebihan_mengajar').removeAttr('disabled', 'disabled');
        $('#pembimbing_akademik').removeAttr('disabled', 'disabled');
        $('#pembimbing_skripsi_tesis_disertasi').removeAttr('disabled', 'disabled');
        $('#mengajar_lainnya').removeAttr('disabled', 'disabled');
        $('#penguji_skripsi_tesis_disertasi').removeAttr('disabled', 'disabled');

        $('#form-add-new-data').trigger("reset");
        $('#kelebihan_mengajar').removeAttr('checked', 'checked');
        $('#pembimbing_akademik').removeAttr('checked', 'checked');
        $('#pembimbing_skripsi_tesis_disertasi').removeAttr('checked', 'checked');
        $('#mengajar_lainnya').removeAttr('checked', 'checked');
        $('#penguji_skripsi_tesis_disertasi').removeAttr('checked', 'checked');
    }
}

var ins = $('#form-add-new-data').on('submit', function(e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });
    $.ajax({
        url: "<?= base_url('sinkron/save-akad'); ?>",
        method: 'post',
        data: new FormData(this),
        dataType: "json",
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function() {
            swal({
                title: "Please Wait",
                text: "Updating data...",
                icon: "info",
                buttons: false,
                closeOnClickOutside: false,
                closeOnEsc: false,
                onBeforeOpen: () => {
                    Swal.showLoading()
                }
            });
        },
        success: function(r) {
            $('#form-add-new-data').trigger("reset");
            swal({
                title: "Success",
                icon: r.icon,
                text: r.msg,
                dangerMode: false,
                buttons: {
                    confirm: "Ok",
                }
            });
        }
    });
});
</script>