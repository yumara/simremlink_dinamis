<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-12">
        <div class="card gradient-green-tea">
            <div class="card-content">
                <div class="card-body pb-0">
                    <div class="media">
                        <div class="media-body white text-left">
                            <h3 class="font-large-1 mb-0">3500 Orang</h3>
                            <span>Total Pegawai dan Dosen Unimed Aktif dan Tidak Aktif</span>
                        </div>
                        <div class="media-right white text-right">
                            <i class="icon-user font-large-1"></i>
                        </div>
                    </div>
                </div>
                <div id="Widget-line-chart" class="height-75 WidgetlineChart WidgetlineChartshadow mb-2">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="card">
            <div class="card-content">
                <div class="card-body chartjs">
                    <canvas id="simple-pie-chart" height="400"></canvas>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="card">
            <div class="card-content">
                <div class="card-body chartjs">
                    <canvas id="polar-chart" height="400"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-12">
        <div class="card">
            <div class="card-content">
                <div class="card-body chartjs">
                    <canvas id="line-chart" height="400"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-12">
        <div class="card">
            <div class="card-content">
                <div class="card-body chartjs">
                    <canvas id="area-chart" height="400"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-12">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <div id="line-area2" class="height-400 lineArea1">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-12">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <div id="line-area3" class="height-400 lineArea1">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-12">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <div id="line-chart2" class="height-400 lineChart2 shadow">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>