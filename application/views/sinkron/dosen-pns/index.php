<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-12">
        <div class="card gradient-green-tea">
            <div class="card-content">
                <div class="card-body pb-0">
                    <div class="media">
                        <div class="media-body white text-left">
                            <h3 class="font-large-1 mb-0">3500 Orang</h3>
                            <span>Total Dosen PNS</span>
                        </div>
                        <div class="media-right white text-right">
                            <i class="icon-user font-large-1"></i>
                        </div>
                    </div>
                </div>
                <div id="Widget-line-chart" class="height-75 WidgetlineChart WidgetlineChartshadow mb-2">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="card">
            <div class="card-content">
                <div class="card-body" style="height:300px;">
                    <div class="chart-info mb-3 ml-3">
                        <span class="bg-primary d-inline-block rounded-circle mr-1"
                            style="width:15px; height:15px;"></span>
                        Aktif
                        <span class="bg-warning d-inline-block rounded-circle mr-1 ml-2"
                            style="width:15px; height:15px;"></span>
                        Tidak Aktif
                    </div>
                    <div id="donut-chart1" class="height-400">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="card">
            <div class="card-content">
                <div class="card-body chartjs" style="height:300px;">
                    <canvas id="radar-chart" height="400"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-12">
        <div class="card">
            <div class="card-content">
                <div class="card-body chartjs">
                    <canvas id="area-chart" height="400"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-12">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <table class="table table-striped table-bordered" id="konten">
                        <thead>
                            <tr>
                                <th>Kode Dosen</th>
                                <th>NIP</th>
                                <th>NIDN</th>
                                <th>Nama Dosen</th>
                                <th>Tempat Lahir</th>
                                <th>Tanggal Lahir</th>
                                <th>Agama</th>
                                <th>Alamat</th>
                                <th>Jenis Kelamin</th>
                                <th>Unit</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>