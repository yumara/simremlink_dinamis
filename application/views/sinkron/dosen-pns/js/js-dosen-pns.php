<script>
$(window).on("load", function() {

    //  Donut Chart 1  Starts
    var donutChart1 = new Chartist.Pie('#donut-chart1', {
        series: [60, 40]
    }, {
        donut: true,
        donutWidth: 60,
        startAngle: 270,
        total: 200,
        showLabel: true
    });
    //  Donut Chart 1  Ends

});

$(window).on("load", function() {

    // line chart widget 1 configuration Starts
    var widgetlineChart = new Chartist.Line('#Widget-line-chart', {
        labels: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13"],
        series: [
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        ]
    }, {
        axisX: {
            showGrid: true,
            showLabel: false,
            offset: 0,
        },
        axisY: {
            showGrid: false,
            low: 40,
            showLabel: false,
            offset: 0,
        },
        lineSmooth: Chartist.Interpolation.cardinal({
            tension: 0
        }),
        fullWidth: true,
    });
    // line chart widget 1 configuration Ends

    // line chart widget 2 configuration Starts
    var widgetlineChart1 = new Chartist.Line('#Widget-line-chart1', {
        labels: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13"],
        series: [
            [50, 45, 60, 55, 70, 55, 60, 55, 65, 57, 60, 53, 53]
        ]
    }, {
        axisX: {
            showGrid: true,
            showLabel: false,
            offset: 0,
        },
        axisY: {
            showGrid: false,
            low: 40,
            showLabel: false,
            offset: 0,
        },
        lineSmooth: Chartist.Interpolation.cardinal({
            tension: 0
        }),
        fullWidth: true,
    });
    // line chart widget 2 configuration Ends

    // line chart widget 3 configuration Starts
    var widgetlineChart2 = new Chartist.Line('#Widget-line-chart2', {
        labels: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13"],
        series: [
            [50, 45, 60, 55, 70, 55, 60, 55, 65, 57, 60, 53, 53]
        ]
    }, {
        axisX: {
            showGrid: true,
            showLabel: false,
            offset: 0,
        },
        axisY: {
            showGrid: false,
            low: 40,
            showLabel: false,
            offset: 0,
        },
        lineSmooth: Chartist.Interpolation.cardinal({
            tension: 0
        }),
        fullWidth: true,
    });
    // line chart widget 3 configuration Ends

    // line chart widget 4 configuration Starts
    var widgetlineChart3 = new Chartist.Line('#Widget-line-chart3', {
        labels: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13"],
        series: [
            [50, 45, 60, 55, 70, 55, 60, 55, 65, 57, 60, 53, 53]
        ]
    }, {
        axisX: {
            showGrid: true,
            showLabel: false,
            offset: 0,
        },
        axisY: {
            showGrid: false,
            low: 40,
            showLabel: false,
            offset: 0,
        },
        lineSmooth: Chartist.Interpolation.cardinal({
            tension: 0
        }),
        fullWidth: true,
    });
    // line chart widget 4 configuration Ends

    //Get the context of the Chart canvas element we want to select
    var ctx = $("#radar-chart");

    // Chart Options
    var chartOptions = {
        responsive: true,
        maintainAspectRatio: false,
        responsiveAnimationDuration: 500,
        legend: {
            position: 'top',
        },
        title: {
            display: true,
            text: 'Chart.js Radar Chart'
        },
        scale: {
            reverse: false,
            ticks: {
                beginAtZero: true
            }
        }
    };

    // Chart Data
    var chartData = {
        labels: ["Eating", "Drinking", "Sleeping", "Designing", "Coding", "Cycling", "Running"],
        datasets: [{
            label: "My First dataset",
            backgroundColor: "rgba(245,0,87,.3)",
            borderColor: "transparent",
            pointBorderColor: "#F50057",
            pointBackgroundColor: "#FFF",
            pointBorderWidth: 2,
            pointHoverBorderWidth: 2,
            pointRadius: 4,
            data: [65, 59, 80, 81, 56, 55, 40],
        }, {
            label: 'Hidden dataset',
            hidden: true,
            data: [45, 25, 16, 36, 67, 18, 76],
        }, {
            label: "My Second dataset",
            backgroundColor: "rgba(29,233,182,.6)",
            borderColor: "transparent",
            pointBorderColor: "#1DE9B6",
            pointBackgroundColor: "#FFF",
            pointBorderWidth: 2,
            pointHoverBorderWidth: 2,
            pointRadius: 4,
            data: [28, 48, 40, 19, 86, 27, 78],
        }, ]
    };

    var config = {
        type: 'radar',

        // Chart Options
        options: chartOptions,

        data: chartData
    };

    // Create the chart
    var polarChart = new Chart(ctx, config);
    /////////////////////////////////////////////

    //Get the context of the Chart canvas element we want to select
    var ctx = $("#area-chart");

    // Chart Options
    var chartOptions = {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
            position: 'bottom',
        },
        hover: {
            mode: 'label'
        },
        scales: {
            xAxes: [{
                display: true,
                gridLines: {
                    color: "#f3f3f3",
                    drawTicks: false,
                },
                scaleLabel: {
                    display: true,
                    labelString: 'Month'
                }
            }],
            yAxes: [{
                display: true,
                gridLines: {
                    color: "#f3f3f3",
                    drawTicks: false,
                },
                scaleLabel: {
                    display: true,
                    labelString: 'Value'
                }
            }]
        },
        title: {
            display: true,
            text: 'Chart.js Line Chart - Legend'
        }
    };

    // Chart Data
    var chartData = {
        labels: ["January", "February", "March", "April", "May", "June", "July"],
        datasets: [{
            label: "My First dataset",
            data: [0, 150, 140, 105, 190, 230, 270],
            backgroundColor: "rgba(209,212,219,.4)",
            borderColor: "transparent",
            pointBorderColor: "#D1D4DB",
            pointBackgroundColor: "#FFF",
            pointBorderWidth: 2,
            pointHoverBorderWidth: 2,
            pointRadius: 4,
        }, {
            label: "My Second dataset",
            data: [0, 90, 120, 240, 140, 250, 190],
            backgroundColor: "rgba(81,117,224,.7)",
            borderColor: "transparent",
            pointBorderColor: "#5175E0",
            pointBackgroundColor: "#FFF",
            pointBorderWidth: 2,
            pointHoverBorderWidth: 2,
            pointRadius: 4,
        }]
    };

    var config = {
        type: 'line',

        // Chart Options
        options: chartOptions,

        // Chart Data
        data: chartData
    };

    // Create the chart
    var areaChart = new Chart(ctx, config);

    //////////////////////////////////////////////////////////////////////////////////////////
});

$(document).ready(function() {
    $.ajaxSetup({
        headers: {
            'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
        }
    });
    $.ajax({
        'url':"<?php echo base_url('api/simpeg/dosen-pns'); ?>",
        'method': "GET",
        'dataType': "json",
        'contentType': false
    }).done( function(data) {
        $('#konten').dataTable( {
            "bDestroy": true,
            "responsive": true,
            "language": {
                "searchPlaceholder": 'Pencarian',
                "sSearch": '',
                "lengthMenu": '_MENU_ items/page',
            },
            "aaData": data,
            "columns": [
                { "data": "kode_dosen" },
                { "data": "nip" },
                { "data": "nidn" },
                { "data": "nama" },
                { "data": "tempat_lahir" },
                { "data": "tgl_lahir" },
                { "data": "agama" },
                { "data": "alamat" },
                { "data": "jk" },
                { "data": "unit" },

            ]
        })
    })
});
    
</script>