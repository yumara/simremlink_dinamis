<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-12">
        <div class="card">
            <form method="post" id="form-add-new-data">
                <div class="card-content">
                    <div class="card-body">
                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="aktivitas"
                                name="aktivitas">
                            <label class="custom-control-label" for="aktivitas">Aktivitas</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="penghargaan"
                                name="penghargaan">
                            <label class="custom-control-label" for="penghargaan">penghargaan</label>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" onclick="sinkronAll()" type="checkbox" class="custom-control-input" id="sinkron_all" name="sinkron_all">
                            <label class="custom-control-label" for="sinkron_all">Sinkron Semua</label>
                            <button type="submit" class="float-right btn btn-primary"><i class="ft-repeat"></i> Sinkron</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>