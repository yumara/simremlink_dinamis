<script>
function sinkronAll() {
    var readonlys = document.getElementById("sinkron_all");
    var tambah    = document.getElementById("tambah");
    var edit      = document.getElementById("edit");
    var hapus     = document.getElementById("hapus");
    if (readonlys.checked == true) {
        $('#aktivitas').attr('readonly', 'readonly');
        $('#penghargaan').attr('readonly', 'readonly');

        $('#aktivitas').attr('checked', 'checked');
        $('#penghargaan').attr('checked', 'checked');
    } else {

        $('#aktivitas').removeAttr('readonly', 'readonly');
        $('#penghargaan').removeAttr('readonly', 'readonly');

        $('#form-add-new-data').trigger("reset");	
        $('#aktivitas').removeAttr('checked', 'checked');
        $('#penghargaan').removeAttr('checked', 'checked');
    }
}

var ins = $('#form-add-new-data').on('submit', function(e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });
    $.ajax({
        url: "<?= base_url('sinkron/save-e-sk'); ?>",
        method: 'post',
        data: new FormData(this),
        dataType: "json",
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function() {
            swal({
                title: "Please Wait",
                text: "Updating data...",
                icon: "info",
                buttons: false,
                closeOnClickOutside: false,
                closeOnEsc: false,
                onBeforeOpen: () => {
                    Swal.showLoading()
                }
            });
        },
        success: function(r) {
            $('#form-add-new-data').trigger("reset");
            $('#aktivitas').removeAttr('checked', 'checked');
            swal({
                title: "Success",
                icon: r.icon,
                text: r.msg,
                dangerMode: false,
                buttons: {
                    confirm: "Ok",
                }
            });
        }
    });
});
</script>