<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-12">
        <div class="card">
            <form method="post" id="form-add-new-data">
                <div class="card-content">
                    <div class="card-body">
                        <!-- -->
                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="evaluasi_dokumen"
                                name="evaluasi_dokumen">
                            <label class="custom-control-label" for="evaluasi_dokumen">evaluasi dokumen</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="evaluasi_dokumen_lanjutan"
                                name="evaluasi_dokumen_lanjutan">
                            <label class="custom-control-label" for="evaluasi_dokumen_lanjutan">evaluasi dokumen lanjutan</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="evaluasi_hasil"
                                name="evaluasi_hasil">
                            <label class="custom-control-label" for="evaluasi_hasil">evaluasi hasil</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="evaluasi_hasil_pengabdian"
                                name="evaluasi_hasil_pengabdian">
                            <label class="custom-control-label" for="evaluasi_hasil_pengabdian">evaluasi hasil pengabdian</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="evaluasi_monitoring"
                                name="evaluasi_monitoring">
                            <label class="custom-control-label" for="evaluasi_monitoring">evaluasi monitoring</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="evaluasi_monitoring_pengabdian"
                                name="evaluasi_monitoring_pengabdian">
                            <label class="custom-control-label" for="evaluasi_monitoring_pengabdian">evaluasi monitoring pengabdian</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="evaluasi_pen_1"
                                name="evaluasi_pen_1">
                            <label class="custom-control-label" for="evaluasi_pen_1">evaluasi pen 1</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="evaluasi_poster"
                                name="evaluasi_poster">
                            <label class="custom-control-label" for="evaluasi_poster">evaluasi poster</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="kunjungan_lapangan"
                                name="kunjungan_lapangan">
                            <label class="custom-control-label" for="kunjungan_lapangan">kunjungan lapangan</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="luaran"
                                name="luaran">
                            <label class="custom-control-label" for="luaran">luaran</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="luaran_lainnya"
                                name="luaran_lainnya">
                            <label class="custom-control-label" for="luaran_lainnya">luaran lainnya</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="luaran_pengabdian"
                                name="luaran_pengabdian">
                            <label class="custom-control-label" for="luaran_pengabdian">luaran pengabdian</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="luaran_pengajuan"
                                name="luaran_pengajuan">
                            <label class="custom-control-label" for="luaran_pengajuan">luaran pengajuan</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="luaran_pengajuan_lainnya"
                                name="luaran_pengajuan_lainnya">
                            <label class="custom-control-label" for="luaran_pengajuan_lainnya">luaran pengajuan lainnya</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="luaran_pengajuan_pengabdian"
                                name="luaran_pengajuan_pengabdian">
                            <label class="custom-control-label" for="luaran_pengajuan_pengabdian">luaran pengajuan pengabdian</label>
                        </div>
                        
                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="luaran_validasi"
                                name="luaran_validasi">
                            <label class="custom-control-label" for="luaran_validasi">luaran validasi</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="luaran_validasi_lainnya"
                                name="luaran_validasi_lainnya">
                            <label class="custom-control-label" for="luaran_validasi_lainnya">luaran validasi lainnya</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="luaran_validasi_pengabdian"
                                name="luaran_validasi_pengabdian">
                            <label class="custom-control-label" for="luaran_validasi_pengabdian">luaran validasi pengabdian</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="luaran_tidak_valid"
                                name="luaran_tidak_valid">
                            <label class="custom-control-label" for="luaran_tidak_valid">luaran tidak valid</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="luaran_tidak_valid_lainnya"
                                name="luaran_tidak_valid_lainnya">
                            <label class="custom-control-label" for="luaran_tidak_valid_lainnya">luaran tidak valid lainnya</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="luaran_tidak_valid_pengabdian"
                                name="luaran_tidak_valid_pengabdian">
                            <label class="custom-control-label" for="luaran_tidak_valid_pengabdian">luaran tidak valid pengabdian</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="luaran_valid_tambahan"
                                name="luaran_valid_tambahan">
                            <label class="custom-control-label" for="luaran_valid_tambahan">luaran valid tambahan</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="luaran_valid_wajib"
                                name="luaran_valid_wajib">
                            <label class="custom-control-label" for="luaran_valid_wajib">luaran valid wajib</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="jenis_luaran"
                                name="jenis_luaran">
                            <label class="custom-control-label" for="jenis_luaran">jenis luaran</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="skim"
                                name="skim">
                            <label class="custom-control-label" for="skim">skim</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="status_penulis"
                                name="status_penulis">
                            <label class="custom-control-label" for="status_penulis">status penulis</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="sumberdana"
                                name="sumberdana">
                            <label class="custom-control-label" for="sumberdana">sumberdana</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value='ya' type="checkbox" class="custom-control-input" id="penelitian_mandiri"
                                name="penelitian_mandiri">
                            <label class="custom-control-label" for="penelitian_mandiri">penelitian mandiri</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya"  type="checkbox" class="custom-control-input" id="pengabdian"
                                name="pengabdian">
                            <label class="custom-control-label" for="pengabdian">pengabdian</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="pengabdian_mandiri"
                                name="pengabdian_mandiri">
                            <label class="custom-control-label" for="pengabdian_mandiri">pengabdian mandiri</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="submission"
                                name="submission">
                            <label class="custom-control-label" for="submission">submission</label>
                        </div>
                        <!-- -->

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="data_submission_penelitian"
                                name="data_submission_penelitian">
                            <label class="custom-control-label" for="data_submission_penelitian">data submission penelitian</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="data_submission_penelitian_anggota"
                                name="data_submission_penelitian_anggota">
                            <label class="custom-control-label" for="data_submission_penelitian_anggota">data submission penelitian anggota</label>
                        </div>
                        
                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="data_submission_penelitian_ketua"
                                name="data_submission_penelitian_ketua">
                            <label class="custom-control-label" for="data_submission_penelitian_ketua">data submission penelitian ketua</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="data_submission_penelitian_luaran"
                                name="data_submission_penelitian_luaran">
                            <label class="custom-control-label" for="data_submission_penelitian_luaran">data submission penelitian luaran</label>
                        </div>

                    </div>
                    <div class="card-footer">
                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" onclick="sinkronAll()" type="checkbox" class="custom-control-input" id="sinkron_all" name="sinkron_all">
                            <label class="custom-control-label" for="sinkron_all">Sinkron Semua</label>
                            <button type="submit" class="float-right btn btn-primary"><i class="ft-repeat"></i> Sinkron</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>