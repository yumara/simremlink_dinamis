<script>
function sinkronAll() {
    var readonlys = document.getElementById("sinkron_all");
    var tambah    = document.getElementById("tambah");
    var edit      = document.getElementById("edit");
    var hapus     = document.getElementById("hapus");
    if (readonlys.checked == true) {
        $('#evaluasi_dokumen').attr('readonly', 'readonly');
        $('#evaluasi_dokumen_lanjutan').attr('readonly', 'readonly');
        $('#evaluasi_hasil').attr('readonly', 'readonly');
        $('#evaluasi_hasil_pengabdian').attr('readonly', 'readonly');
        $('#evaluasi_monitoring').attr('readonly', 'readonly');
        $('#evaluasi_monitoring_pengabdian').attr('readonly', 'readonly');
        $('#evaluasi_pen_1').attr('readonly', 'readonly');
        $('#evaluasi_poster').attr('readonly', 'readonly');
        $('#kunjungan_lapangan').attr('readonly', 'readonly');
        $('#luaran').attr('readonly', 'readonly');
        $('#luaran_lainnya').attr('readonly', 'readonly');
        $('#luaran_pengabdian').attr('readonly', 'readonly');
        $('#luaran_pengajuan').attr('readonly', 'readonly');
        $('#luaran_pengajuan_lainnya').attr('readonly', 'readonly');
        $('#luaran_pengajuan_pengabdian').attr('readonly', 'readonly');
        $('#luaran_validasi').attr('readonly', 'readonly');
        $('#luaran_validasi_lainnya').attr('readonly', 'readonly');
        $('#luaran_validasi_pengabdian').attr('readonly', 'readonly');
        $('#luaran_tidak_valid').attr('readonly', 'readonly');
        $('#luaran_tidak_valid_lainnya').attr('readonly', 'readonly');
        $('#luaran_tidak_valid_pengabdian').attr('readonly', 'readonly');
        $('#luaran_valid_tambahan').attr('readonly', 'readonly');
        $('#luaran_valid_wajib').attr('readonly', 'readonly');
        $('#jenis_luaran').attr('readonly', 'readonly');
        $('#skim').attr('readonly', 'readonly');
        $('#status_penulis').attr('readonly', 'readonly');
        $('#sumberdana').attr('readonly', 'readonly');
        $('#penelitian_mandiri').attr('readonly', 'readonly');
        $('#pengabdian').attr('readonly', 'readonly');
        $('#pengabdian_mandiri').attr('readonly', 'readonly');
        $('#submission').attr('readonly', 'readonly');
        $('#data_submission_penelitian').attr('readonly', 'readonly');
        $('#data_submission_penelitian_anggota').attr('readonly', 'readonly');
        $('#data_submission_penelitian_ketua').attr('readonly', 'readonly');
        $('#data_submission_penelitian_luaran').attr('readonly', 'readonly');


        $('#evaluasi_dokumen').attr('checked', 'checked');
        $('#evaluasi_dokumen_lanjutan').attr('checked', 'checked');
        $('#evaluasi_hasil').attr('checked', 'checked');
        $('#evaluasi_hasil_pengabdian').attr('checked', 'checked');
        $('#evaluasi_monitoring').attr('checked', 'checked');
        $('#evaluasi_monitoring_pengabdian').attr('checked', 'checked');
        $('#evaluasi_pen_1').attr('checked', 'checked');
        $('#evaluasi_poster').attr('checked', 'checked');
        $('#kunjungan_lapangan').attr('checked', 'checked');
        $('#luaran').attr('checked', 'checked');
        $('#luaran_lainnya').attr('checked', 'checked');
        $('#luaran_pengabdian').attr('checked', 'checked');
        $('#luaran_pengajuan').attr('checked', 'checked');
        $('#luaran_pengajuan_lainnya').attr('checked', 'checked');
        $('#luaran_pengajuan_pengabdian').attr('checked', 'checked');
        $('#luaran_validasi').attr('checked', 'checked');
        $('#luaran_validasi_lainnya').attr('checked', 'checked');
        $('#luaran_validasi_pengabdian').attr('checked', 'checked');
        $('#luaran_tidak_valid').attr('checked', 'checked');
        $('#luaran_tidak_valid_lainnya').attr('checked', 'checked');
        $('#luaran_tidak_valid_pengabdian').attr('checked', 'checked');
        $('#luaran_valid_tambahan').attr('checked', 'checked');
        $('#luaran_valid_wajib').attr('checked', 'checked');
        $('#jenis_luaran').attr('checked', 'checked');
        $('#skim').attr('checked', 'checked');
        $('#status_penulis').attr('checked', 'checked');
        $('#sumberdana').attr('checked', 'checked');
        $('#penelitian_mandiri').attr('checked', 'checked');
        $('#pengabdian').attr('checked', 'checked');
        $('#pengabdian_mandiri').attr('checked', 'checked');
        $('#submission').attr('checked', 'checked');
        $('#data_submission_penelitian').attr('checked', 'checked');
        $('#data_submission_penelitian_anggota').attr('checked', 'checked');
        $('#data_submission_penelitian_ketua').attr('checked', 'checked');
        $('#data_submission_penelitian_luaran').attr('checked', 'checked');
        
    } else {
        $('#evaluasi_dokumen').removeAttr('readonly', 'readonly');
        $('#evaluasi_dokumen_lanjutan').removeAttr('readonly', 'readonly');
        $('#evaluasi_hasil').removeAttr('readonly', 'readonly');
        $('#evaluasi_hasil_pengabdian').removeAttr('readonly', 'readonly');
        $('#evaluasi_monitoring').removeAttr('readonly', 'readonly');
        $('#evaluasi_monitoring_pengabdian').removeAttr('readonly', 'readonly');
        $('#evaluasi_pen_1').removeAttr('readonly', 'readonly');
        $('#evaluasi_poster').removeAttr('readonly', 'readonly');
        $('#kunjungan_lapangan').removeAttr('readonly', 'readonly');
        $('#luaran').removeAttr('readonly', 'readonly');
        $('#luaran_lainnya').removeAttr('readonly', 'readonly');
        $('#luaran_pengabdian').removeAttr('readonly', 'readonly');
        $('#luaran_pengajuan').removeAttr('readonly', 'readonly');
        $('#luaran_pengajuan_lainnya').removeAttr('readonly', 'readonly');
        $('#luaran_pengajuan_pengabdian').removeAttr('readonly', 'readonly');
        $('#luaran_validasi').removeAttr('readonly', 'readonly');
        $('#luaran_validasi_lainnya').removeAttr('readonly', 'readonly');
        $('#luaran_validasi_pengabdian').removeAttr('readonly', 'readonly');
        $('#luaran_tidak_valid').removeAttr('readonly', 'readonly');
        $('#luaran_tidak_valid_lainnya').removeAttr('readonly', 'readonly');
        $('#luaran_tidak_valid_pengabdian').removeAttr('readonly', 'readonly');
        $('#luaran_valid_tambahan').removeAttr('readonly', 'readonly');
        $('#luaran_valid_wajib').removeAttr('readonly', 'readonly');
        $('#jenis_luaran').removeAttr('readonly', 'readonly');
        $('#skim').removeAttr('readonly', 'readonly');
        $('#status_penulis').removeAttr('readonly', 'readonly');
        $('#sumberdana').removeAttr('readonly', 'readonly');
        $('#penelitian_mandiri').removeAttr('readonly', 'readonly');
        $('#pengabdian').removeAttr('readonly', 'readonly');
        $('#pengabdian_mandiri').removeAttr('readonly', 'readonly');
        $('#submission').removeAttr('readonly', 'readonly');
        $('#data_submission_penelitian').removeAttr('readonly', 'readonly');
        $('#data_submission_penelitian_anggota').removeAttr('readonly', 'readonly');
        $('#data_submission_penelitian_ketua').removeAttr('readonly', 'readonly');
        $('#data_submission_penelitian_luaran').removeAttr('readonly', 'readonly');
        

        $('#form-add-new-data').trigger("reset");
        $('#evaluasi_dokumen').removeAttr('checked', 'checked');
        $('#evaluasi_dokumen_lanjutan').removeAttr('checked', 'checked');
        $('#evaluasi_hasil').removeAttr('checked', 'checked');
        $('#evaluasi_hasil_pengabdian').removeAttr('checked', 'checked');
        $('#evaluasi_monitoring').removeAttr('checked', 'checked');
        $('#evaluasi_monitoring_pengabdian').removeAttr('checked', 'checked');
        $('#evaluasi_pen_1').removeAttr('checked', 'checked');
        $('#evaluasi_poster').removeAttr('checked', 'checked');
        $('#kunjungan_lapangan').removeAttr('checked', 'checked');
        $('#luaran').removeAttr('checked', 'checked');
        $('#luaran_lainnya').removeAttr('checked', 'checked');
        $('#luaran_pengabdian').removeAttr('checked', 'checked');
        $('#luaran_pengajuan').removeAttr('checked', 'checked');
        $('#luaran_pengajuan_lainnya').removeAttr('checked', 'checked');
        $('#luaran_pengajuan_pengabdian').removeAttr('checked', 'checked');
        $('#luaran_validasi').removeAttr('checked', 'checked');
        $('#luaran_validasi_lainnya').removeAttr('checked', 'checked');
        $('#luaran_validasi_pengabdian').removeAttr('checked', 'checked');
        $('#luaran_tidak_valid').removeAttr('checked', 'checked');
        $('#luaran_tidak_valid_lainnya').removeAttr('checked', 'checked');
        $('#luaran_tidak_valid_pengabdian').removeAttr('checked', 'checked');
        $('#luaran_valid_tambahan').removeAttr('checked', 'checked');
        $('#luaran_valid_wajib').removeAttr('checked', 'checked');
        $('#jenis_luaran').removeAttr('checked', 'checked');
        $('#skim').removeAttr('checked', 'checked');
        $('#status_penulis').removeAttr('checked', 'checked');
        $('#sumberdana').removeAttr('checked', 'checked');
        $('#penelitian_mandiri').removeAttr('checked', 'checked');
        $('#pengabdian').removeAttr('checked', 'checked');
        $('#pengabdian_mandiri').removeAttr('checked', 'checked');
        $('#submission').removeAttr('checked', 'checked');
        $('#data_submission_penelitian').removeAttr('checked', 'checked');
        $('#data_submission_penelitian_anggota').removeAttr('checked', 'checked');
        $('#data_submission_penelitian_ketua').removeAttr('checked', 'checked');
        $('#data_submission_penelitian_luaran').removeAttr('checked', 'checked');   
    }
}

var ins = $('#form-add-new-data').on('submit', function(e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });
    $.ajax({
        url: "<?= base_url('sinkron/save-lppm'); ?>",
        method: 'post',
        data: new FormData(this),
        dataType: "json",
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function() {
            swal({
                title: "Please Wait",
                text: "Updating data...",
                icon: "info",
                buttons: false,
                closeOnClickOutside: false,
                closeOnEsc: false,
                onBeforeOpen: () => {
                    Swal.showLoading()
                }
            });
        },
        success: function(r) {
            $('#form-add-new-data').trigger("reset");
            swal({
                title: "Success",
                icon: r.icon,
                text: r.msg,
                dangerMode: false,
                buttons: {
                    confirm: "Ok",
                }
            });
        }
    });
});
</script>