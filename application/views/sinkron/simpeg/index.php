<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-12">
        <div class="card">
            <form method="post" id="form-add-new-data">
                <div class="card-content">
                    <div class="card-body">
                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="bagian"
                                name="bagian">
                            <label class="custom-control-label" for="bagian">Bagian</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="dosen_phl"
                                name="dosen_phl">
                            <label class="custom-control-label" for="dosen_phl">Dosen PHL</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="dosen_pns"
                                name="dosen_pns">
                            <label class="custom-control-label" for="dosen_pns">Dosen PNS</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input"
                                id="dosen_pns_simremlink" name="dosen_pns_simremlink">
                            <label class="custom-control-label" for="dosen_pns_simremlink">Dosen PNS Simrelink</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input"
                                id="golongan_dosen_pegawai" name="golongan_dosen_pegawai">
                            <label class="custom-control-label" for="golongan_dosen_pegawai">Golongan Dosen Pegawai</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="jabatan_fungsional_dosen"
                                name="jabatan_fungsional_dosen">
                            <label class="custom-control-label" for="jabatan_fungsional_dosen">Jabatan Fungsional Dosen</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="jenis_tugas_tambahan_dosen"
                                name="jenis_tugas_tambahan_dosen">
                            <label class="custom-control-label" for="jenis_tugas_tambahan_dosen">Jenis Tugas Tambahan Dosen</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="jurusan"
                                name="jurusan">
                            <label class="custom-control-label" for="jurusan">Jurusan</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="kategori_jabatan_remun_pegawai_pns"
                                name="kategori_jabatan_remun_pegawai_pns">
                            <label class="custom-control-label" for="kategori_jabatan_remun_pegawai_pns">Kategori Jabatan Remun Pegawai PNS</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="kedudukan_dosen_pegawai"
                                name="kedudukan_dosen_pegawai">
                            <label class="custom-control-label" for="kedudukan_dosen_pegawai">Kedudukan Dosen Pegawai</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="pegawai_phl"
                                name="pegawai_phl">
                            <label class="custom-control-label" for="pegawai_phl">Pegawai PHL</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="pegawai_pns"
                                name="pegawai_pns">
                            <label class="custom-control-label" for="pegawai_pns">Pegawai PNS</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="pegawai_pns_simremlink"
                                name="pegawai_pns_simremlink">
                            <label class="custom-control-label" for="pegawai_pns_simremlink">Pegawai PNS Simremlink</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="prodi"
                                name="prodi">
                            <label class="custom-control-label" for="prodi">Prodi</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="riwayat_jabatan_fungsional"
                                name="riwayat_jabatan_fungsional">
                            <label class="custom-control-label" for="riwayat_jabatan_fungsional">Riwayat Jabatan Fungsional</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input disabled type="checkbox" class="custom-control-input" id=""
                                name="">
                            <label class="custom-control-label" for=""><s>Riwayat Jabatan Remun Dosen PNS</s> <small><span class="text-danger">( link telah dihapus )</span></small></label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input disabled type="checkbox" class="custom-control-input" id=""
                                name="">
                            <label class="custom-control-label" for=""><s>Riwayat Jabatan Remun Pegawai PNS</s> <small><span class="text-danger">( link telah dihapus )</span></small></label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="riwayat_jabatan_tugas_tambahan_dosen_pns"
                                name="riwayat_jabatan_tugas_tambahan_dosen_pns">
                            <label class="custom-control-label" for="riwayat_jabatan_tugas_tambahan_dosen_pns">Riwayat jabatan Tugas Tambahan Dosen PNS</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="riwayat_penghargaan_dosen_pns"
                                name="riwayat_penghargaan_dosen_pns">
                            <label class="custom-control-label" for="riwayat_penghargaan_dosen_pns">Riwayat Penghargaan Dosen PNS</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="riwayat_penghargaan_pegawai_pns"
                                name="riwayat_penghargaan_pegawai_pns">
                            <label class="custom-control-label" for="riwayat_penghargaan_pegawai_pns">Riwayat Penghargaan Pegawai PNS</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="status"
                                name="status">
                            <label class="custom-control-label" for="status">Status</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="status_aktif_non_aktif"
                                name="status_aktif_non_aktif">
                            <label class="custom-control-label" for="status_aktif_non_aktif">Status Aktif Non Aktif</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="unit"
                                name="unit">
                            <label class="custom-control-label" for="unit">Unit</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="capaian_skp_dosen"
                                name="capaian_skp_dosen">
                            <label class="custom-control-label" for="capaian_skp_dosen">Capaian SKP Dosen</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="dosen_pegawai_pns"
                                name="dosen_pegawai_pns">
                            <label class="custom-control-label" for="dosen_pegawai_pns">Dosen Pegawai PNS</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="nilai_sikap_dan_perilaku_pegawai"
                                name="nilai_sikap_dan_perilaku_pegawai">
                            <label class="custom-control-label" for="nilai_sikap_dan_perilaku_pegawai">Nilai Sikap Dan Perilaku Pegawai</label>
                        </div>


                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="capaian_skp_pegawai"
                                name="capaian_skp_pegawai">
                            <label class="custom-control-label" for="capaian_skp_pegawai">Capaian SKP Pegawai</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="dosen_pegawai_simremlink"
                                name="dosen_pegawai_simremlink">
                            <label class="custom-control-label" for="dosen_pegawai_simremlink">Dosen Pegawai Simremlink</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="riwayat_dosen_pegawai_simremlink"
                                name="riwayat_dosen_pegawai_simremlink">
                            <label class="custom-control-label" for="riwayat_dosen_pegawai_simremlink">Riwayat Dosen Pegawai Simremlink</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="riwayat_jft_pegawai"
                                name="riwayat_jft_pegawai">
                            <label class="custom-control-label" for="riwayat_jft_pegawai">Riwayat JFT Pegawai</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="riwayat_jfu_pegawai"
                                name="riwayat_jfu_pegawai">
                            <label class="custom-control-label" for="riwayat_jfu_pegawai">Riwayat JFU Pegawai</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="riwayat_struktural_pegawai"
                                name="riwayat_struktural_pegawai">
                            <label class="custom-control-label" for="riwayat_struktural_pegawai">Riwayat Struktural Pegawai</label>
                        </div>
                        
                        <!----->
                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="capaian_bkd_dosen"
                                name="capaian_bkd_dosen">
                            <label class="custom-control-label" for="capaian_bkd_dosen">Capaian BKD Dosen</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="absensi_pegawai_tendik"
                                name="absensi_pegawai_tendik">
                            <label class="custom-control-label" for="absensi_pegawai_tendik">Absensi Pegawai Tendik</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="absensi_dosen_pns"
                                name="absensi_dosen_pns">
                            <label class="custom-control-label" for="absensi_dosen_pns">Absensi Dosen PNS</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="lhkasn_lhkpn_pegawai_pns"
                                name="lhkasn_lhkpn_pegawai_pns">
                            <label class="custom-control-label" for="lhkasn_lhkpn_pegawai_pns">LHKASN / LHKPN Pegawa PNS</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" type="checkbox" class="custom-control-input" id="hukuman_disiplin_pegawai_pns"
                                name="hukuman_disiplin_pegawai_pns">
                            <label class="custom-control-label" for="hukuman_disiplin_pegawai_pns">Hukuman Disiplin Pegawai PNS</label>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" onclick="sinkronAll()" type="checkbox" class="custom-control-input" id="sinkron_all" name="sinkron_all">
                            <label class="custom-control-label" for="sinkron_all">Sinkron Semua</label>
                            <button type="submit" class="float-right btn btn-primary"><i class="ft-repeat"></i> Sinkron</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>