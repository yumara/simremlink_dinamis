<script>
function sinkronAll() {
    var readonlys = document.getElementById("sinkron_all");
    var tambah    = document.getElementById("tambah");
    var edit      = document.getElementById("edit");
    var hapus     = document.getElementById("hapus");
    if (readonlys.checked == true) {
        $('#bagian').attr('readonly', 'readonly');
        $('#dosen_phl').attr('readonly', 'readonly');
        $('#dosen_pns').attr('readonly', 'readonly');
        $('#dosen_pns_simremlink').attr('readonly', 'readonly');
        $('#golongan_dosen_pegawai').attr('readonly', 'readonly');
        $('#jabatan_fungsional_dosen').attr('readonly', 'readonly');
        $('#jenis_tugas_tambahan_dosen').attr('readonly', 'readonly');
        $('#jurusan').attr('readonly', 'readonly');
        $('#kategori_jabatan_remun_pegawai_pns').attr('readonly', 'readonly');
        $('#kedudukan_dosen_pegawai').attr('readonly', 'readonly');
        $('#pegawai_phl').attr('readonly', 'readonly');
        $('#pegawai_pns').attr('readonly', 'readonly');
        $('#pegawai_pns_simremlink').attr('readonly', 'readonly');
        $('#prodi').attr('readonly', 'readonly');
        $('#riwayat_jabatan_fungsional').attr('readonly', 'readonly');
        $('#riwayat_jabatan_remun_dosen_pns').attr('readonly', 'readonly');
        $('#riwayat_jabatan_remun_pegawai_pns').attr('readonly', 'readonly');
        $('#riwayat_jabatan_tugas_tambahan_dosen_pns').attr('readonly', 'readonly');
        $('#riwayat_penghargaan_dosen_pns').attr('readonly', 'readonly');
        $('#riwayat_penghargaan_pegawai_pns').attr('readonly', 'readonly');
        $('#status').attr('readonly', 'readonly');
        $('#status_aktif_non_aktif').attr('readonly', 'readonly');
        $('#unit').attr('readonly', 'readonly');
        $('#capaian_skp_dosen').attr('readonly', 'readonly');
        $('#dosen_pegawai_pns').attr('readonly', 'readonly');
        $('#nilai_sikap_dan_perilaku_pegawai').attr('readonly', 'readonly');
        $('#capaian_skp_pegawai').attr('readonly', 'readonly');
        $('#dosen_pegawai_simremlink').attr('readonly', 'readonly');
        $('#riwayat_dosen_pegawai_simremlink').attr('readonly', 'readonly');
        $('#riwayat_jft_pegawai').attr('readonly', 'readonly');
        $('#riwayat_jfu_pegawai').attr('readonly', 'readonly');
        $('#riwayat_struktural_pegawai').attr('readonly', 'readonly');
        $('#capaian_bkd_dosen').attr('readonly', 'readonly');
        $('#absensi_pegawai_tendik').attr('readonly', 'readonly');
        $('#absensi_dosen_pns').attr('readonly', 'readonly');
        $('#lhkasn_lhkpn_pegawai_pns').attr('readonly', 'readonly');
        $('#hukuman_disiplin_pegawai_pns').attr('readonly', 'readonly');


        $('#bagian').attr('checked', 'checked');
        $('#dosen_phl').attr('checked', 'checked');
        $('#dosen_pns').attr('checked', 'checked');
        $('#dosen_pns_simremlink').attr('checked', 'checked');
        $('#golongan_dosen_pegawai').attr('checked', 'checked');
        $('#jabatan_fungsional_dosen').attr('checked', 'checked');
        $('#jenis_tugas_tambahan_dosen').attr('checked', 'checked');
        $('#jurusan').attr('checked', 'checked');
        $('#kategori_jabatan_remun_pegawai_pns').attr('checked', 'checked');
        $('#kedudukan_dosen_pegawai').attr('checked', 'checked');
        $('#pegawai_phl').attr('checked', 'checked');
        $('#pegawai_pns').attr('checked', 'checked');
        $('#pegawai_pns_simremlink').attr('checked', 'checked');
        $('#prodi').attr('checked', 'checked');
        $('#riwayat_jabatan_fungsional').attr('checked', 'checked');
        $('#riwayat_jabatan_remun_dosen_pns').attr('checked', 'checked');
        $('#riwayat_jabatan_remun_pegawai_pns').attr('checked', 'checked');
        $('#riwayat_jabatan_tugas_tambahan_dosen_pns').attr('checked', 'checked');
        $('#riwayat_penghargaan_dosen_pns').attr('checked', 'checked');
        $('#riwayat_penghargaan_pegawai_pns').attr('checked', 'checked');
        $('#status').attr('checked', 'checked');
        $('#status_aktif_non_aktif').attr('checked', 'checked');
        $('#unit').attr('checked', 'checked');
        $('#capaian_skp_dosen').attr('checked', 'checked');
        $('#dosen_pegawai_pns').attr('checked', 'checked');
        $('#nilai_sikap_dan_perilaku_pegawai').attr('checked', 'checked');
        $('#capaian_skp_pegawai').attr('checked', 'checked');
        $('#dosen_pegawai_simremlink').attr('checked', 'checked');
        $('#riwayat_dosen_pegawai_simremlink').attr('checked', 'checked');
        $('#riwayat_jft_pegawai').attr('checked', 'checked');
        $('#riwayat_jfu_pegawai').attr('checked', 'checked');
        $('#riwayat_struktural_pegawai').attr('checked', 'checked');
        $('#capaian_bkd_dosen').attr('checked', 'checked');
        $('#absensi_pegawai_tendik').attr('checked', 'checked');
        $('#absensi_dosen_pns').attr('checked', 'checked');
        $('#lhkasn_lhkpn_pegawai_pns').attr('checked', 'checked');
        $('#hukuman_disiplin_pegawai_pns').attr('checked', 'checked');
    } else {
        $('#bagian').removeAttr('readonly', 'readonly');
        $('#dosen_phl').removeAttr('readonly', 'readonly');
        $('#dosen_pns').removeAttr('readonly', 'readonly');
        $('#dosen_pns_simremlink').removeAttr('readonly', 'readonly');
        $('#golongan_dosen_pegawai').removeAttr('readonly', 'readonly');
        $('#jabatan_fungsional_dosen').removeAttr('readonly', 'readonly');
        $('#jenis_tugas_tambahan_dosen').removeAttr('readonly', 'readonly');
        $('#jurusan').removeAttr('readonly', 'readonly');
        $('#kategori_jabatan_remun_pegawai_pns').removeAttr('readonly', 'readonly');
        $('#kedudukan_dosen_pegawai').removeAttr('readonly', 'readonly');
        $('#pegawai_phl').removeAttr('readonly', 'readonly');
        $('#pegawai_pns').removeAttr('readonly', 'readonly');
        $('#pegawai_pns_simremlink').removeAttr('readonly', 'readonly');
        $('#prodi').removeAttr('readonly', 'readonly');
        $('#riwayat_jabatan_fungsional').removeAttr('readonly', 'readonly');
        $('#riwayat_jabatan_remun_dosen_pns').removeAttr('readonly', 'readonly');
        $('#riwayat_jabatan_remun_pegawai_pns').removeAttr('readonly', 'readonly');
        $('#riwayat_jabatan_tugas_tambahan_dosen_pns').removeAttr('readonly', 'readonly');
        $('#riwayat_penghargaan_dosen_pns').removeAttr('readonly', 'readonly');
        $('#riwayat_penghargaan_pegawai_pns').removeAttr('readonly', 'readonly');
        $('#status').removeAttr('readonly', 'readonly');
        $('#status_aktif_non_aktif').removeAttr('readonly', 'readonly');
        $('#unit').removeAttr('readonly', 'readonly');
        $('#capaian_skp_dosen').removeAttr('readonly', 'readonly');
        $('#dosen_pegawai_pns').removeAttr('readonly', 'readonly');
        $('#nilai_sikap_dan_perilaku_pegawai').removeAttr('readonly', 'readonly');
        $('#capaian_skp_pegawai').removeAttr('readonly', 'readonly');
        $('#dosen_pegawai_simremlink').removeAttr('readonly', 'readonly');
        $('#riwayat_dosen_pegawai_simremlink').removeAttr('readonly', 'readonly');
        $('#riwayat_jft_pegawai').removeAttr('readonly', 'readonly');
        $('#riwayat_jfu_pegawai').removeAttr('readonly', 'readonly');
        $('#riwayat_struktural_pegawai').removeAttr('readonly', 'readonly');
        $('#capaian_bkd_dosen').removeAttr('readonly', 'readonly');
        $('#absensi_pegawai_tendik').removeAttr('readonly', 'readonly');
        $('#absensi_dosen_pns').removeAttr('readonly', 'readonly');
        $('#lhkasn_lhkpn_pegawai_pns').removeAttr('readonly', 'readonly');
        $('#hukuman_disiplin_pegawai_pns').removeAttr('readonly', 'readonly');

        $('#form-add-new-data').trigger("reset");
        $('#bagian').removeAttr('checked', 'checked');
        $('#dosen_phl').removeAttr('checked', 'checked');
        $('#dosen_pns').removeAttr('checked', 'checked');
        $('#dosen_pns_simremlink').removeAttr('checked', 'checked');
        $('#golongan_dosen_pegawai').removeAttr('checked', 'checked');
        $('#jabatan_fungsional_dosen').removeAttr('checked', 'checked');
        $('#jenis_tugas_tambahan_dosen').removeAttr('checked', 'checked');
        $('#jurusan').removeAttr('checked', 'checked');
        $('#kategori_jabatan_remun_pegawai_pns').removeAttr('checked', 'checked');
        $('#kedudukan_dosen_pegawai').removeAttr('checked', 'checked');
        $('#pegawai_phl').removeAttr('checked', 'checked');
        $('#pegawai_pns').removeAttr('checked', 'checked');
        $('#pegawai_pns_simremlink').removeAttr('checked', 'checked');
        $('#prodi').removeAttr('checked', 'checked');
        $('#riwayat_jabatan_fungsional').removeAttr('checked', 'checked');
        $('#riwayat_jabatan_remun_dosen_pns').removeAttr('checked', 'checked');
        $('#riwayat_jabatan_remun_pegawai_pns').removeAttr('checked', 'checked');
        $('#riwayat_jabatan_tugas_tambahan_dosen_pns').removeAttr('checked', 'checked');
        $('#riwayat_penghargaan_dosen_pns').removeAttr('checked', 'checked');
        $('#riwayat_penghargaan_pegawai_pns').removeAttr('checked', 'checked');
        $('#status').removeAttr('checked', 'checked');
        $('#status_aktif_non_aktif').removeAttr('checked', 'checked');
        $('#unit').removeAttr('checked', 'checked');
        $('#capaian_skp_dosen').removeAttr('checked', 'checked');
        $('#dosen_pegawai_pns').removeAttr('checked', 'checked');
        $('#nilai_sikap_dan_perilaku_pegawai').removeAttr('checked', 'checked');
        $('#capaian_skp_pegawai').removeAttr('checked', 'checked');
        $('#dosen_pegawai_simremlink').removeAttr('checked', 'checked');
        $('#riwayat_dosen_pegawai_simremlink').removeAttr('checked', 'checked');
        $('#riwayat_jft_pegawai').removeAttr('checked', 'checked');
        $('#riwayat_jfu_pegawai').removeAttr('checked', 'checked');
        $('#riwayat_struktural_pegawai').removeAttr('checked', 'checked');
        $('#capaian_bkd_dosen').removeAttr('checked', 'checked');
        $('#absensi_pegawai_tendik').removeAttr('checked', 'checked');
        $('#absensi_dosen_pns').removeAttr('checked', 'checked');
        $('#lhkasn_lhkpn_pegawai_pns').removeAttr('checked', 'checked');
        $('#hukuman_disiplin_pegawai_pns').removeAttr('checked', 'checked');
    }
}

var ins = $('#form-add-new-data').on('submit', function(e) {
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });
    $.ajax({
        url: "<?= base_url('sinkron/save-simpeg'); ?>",
        method: 'post',
        data: new FormData(this),
        dataType: "json",
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function() {
            swal({
                title: "Please Wait",
                text: "Updating data...",
                icon: "info",
                buttons: false,
                closeOnClickOutside: false,
                closeOnEsc: false,
                onBeforeOpen: () => {
                    Swal.showLoading()
                }
            });
        },
        success: function(r) {
            $('#form-add-new-data').trigger("reset");
            swal({
                title: "Success",
                icon: r.icon,
                text: r.msg,
                dangerMode: false,
                buttons: {
                    confirm: "Ok",
                }
            });
        }
    });
});
</script>