<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-12">
        <div class="card">
            <form method="post" id="form-add-new-data">
                <div class="card-content">
                    <div class="card-body">
                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" disabled type="checkbox" class="custom-control-input" id="data_aktivitas"
                                name="data_aktivitas">
                            <label class="custom-control-label" for="data_aktivitas">data aktivitas</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" disabled type="checkbox" class="custom-control-input" id="data_periode"
                                name="data_periode">
                            <label class="custom-control-label" for="data_periode">data periode</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" disabled type="checkbox" class="custom-control-input" id="data_kdn"
                                name="data_kdn">
                            <label class="custom-control-label" for="data_kdn">data kdn</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" disabled type="checkbox" class="custom-control-input" id="data_dosen_tendik_pns"
                                name="data_dosen_tendik_pns">
                            <label class="custom-control-label" for="data_dosen_tendik_pns">data dosen tendik pns</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" disabled type="checkbox" class="custom-control-input" id="data_golongan_pns"
                                name="data_golongan_pns">
                            <label class="custom-control-label" for="data_golongan_pns">data golongan pns</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" disabled type="checkbox" class="custom-control-input" id="data_grade"
                                name="data_grade">
                            <label class="custom-control-label" for="data_grade">data grade</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" disabled type="checkbox" class="custom-control-input" id="data_jabatan_akad"
                                name="data_jabatan_akad">
                            <label class="custom-control-label" for="data_jabatan_akad">data jabatan akad</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" disabled type="checkbox" class="custom-control-input" id="data_jenjang_pendidikan"
                                name="data_jenjang_pendidikan">
                            <label class="custom-control-label" for="data_jenjang_pendidikan">data jenjang pendidikan</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" disabled type="checkbox" class="custom-control-input" id="data_jenis_dokumen"
                                name="data_jenis_dokumen">
                            <label class="custom-control-label" for="data_jenis_dokumen">data jenis dokumen</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" disabled type="checkbox" class="custom-control-input" id="data_jenis_pegawai"
                                name="data_jenis_pegawai">
                            <label class="custom-control-label" for="data_jenis_pegawai">data jenis pegawai</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" disabled type="checkbox" class="custom-control-input" id="data_kelompok_jabatan"
                                name="data_kelompok_jabatan">
                            <label class="custom-control-label" for="data_kelompok_jabatan">data kelompok jabatan</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" disabled type="checkbox" class="custom-control-input" id="data_kelas_jabatan"
                                name="data_kelas_jabatan">
                            <label class="custom-control-label" for="data_kelas_jabatan">data kelas jabatan</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" disabled type="checkbox" class="custom-control-input" id="data_nama_jabatan"
                                name="data_nama_jabatan">
                            <label class="custom-control-label" for="data_nama_jabatan">data nama jabatan</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" disabled type="checkbox" class="custom-control-input" id="data_rekening"
                                name="data_rekening">
                            <label class="custom-control-label" for="data_rekening">data rekening</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" disabled type="checkbox" class="custom-control-input" id="data_pekerjaan"
                                name="data_pekerjaan">
                            <label class="custom-control-label" for="data_pekerjaan">data pekerjaan</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" disabled type="checkbox" class="custom-control-input" id="data_status_pegawai"
                                name="data_status_pegawai">
                            <label class="custom-control-label" for="data_status_pegawai">data status pegawai</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" disabled type="checkbox" class="custom-control-input" id="data_status_sipil"
                                name="data_status_sipil">
                            <label class="custom-control-label" for="data_status_sipil">data status sipil</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" disabled type="checkbox" class="custom-control-input" id="tusi"
                                name="tusi">
                            <label class="custom-control-label" for="tusi">tusi</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" disabled type="checkbox" class="custom-control-input" id="mengajar"
                                name="mengajar">
                            <label class="custom-control-label" for="mengajar">mengajar</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" disabled type="checkbox" class="custom-control-input" id="mengajar_lainnya"
                                name="mengajar_lainnya">
                            <label class="custom-control-label" for="mengajar_lainnya">mengajar_lainnya</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" disabled type="checkbox" class="custom-control-input" id="penelitian"
                                name="penelitian">
                            <label class="custom-control-label" for="penelitian">penelitian</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" disabled type="checkbox" class="custom-control-input" id="pengabdian"
                                name="pengabdian">
                            <label class="custom-control-label" for="pengabdian">pengabdian</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" disabled type="checkbox" class="custom-control-input" id="penghargaan"
                                name="penghargaan">
                            <label class="custom-control-label" for="penghargaan">penghargaan</label>
                        </div>

                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" disabled type="checkbox" class="custom-control-input" id="penunjang"
                                name="penunjang">
                            <label class="custom-control-label" for="penunjang">penunjang</label>
                        </div>

                    </div>
                    <div class="card-footer">
                        <div class="custom-control custom-checkbox mb-2">
                            <input value="ya" disabled onclick="sinkronAll()" type="checkbox" class="custom-control-input" id="sinkron_all" name="sinkron_all">
                            <label class="custom-control-label" for="sinkron_all">Sinkron Semua</label>
                            <button type="submit" class="float-right btn btn-primary"><i class="ft-repeat"></i> Sinkron</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>