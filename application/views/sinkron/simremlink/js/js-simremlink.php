<script>
function sinkronAll() {
    var readonlys = document.getElementById("sinkron_all");
    var tambah = document.getElementById("tambah");
    var edit = document.getElementById("edit");
    var hapus = document.getElementById("hapus");
    if (readonlys.checked == true) {
        $('#data_peran').attr('readonly', 'readonly');
        $('#data_aktivitas').attr('readonly', 'readonly');
        $('#data_periode').attr('readonly', 'readonly');
        $('#data_kdn').attr('readonly', 'readonly');
        $('#data_dosen_tendik_pns').attr('readonly', 'readonly');
        $('#data_golongan_pns').attr('readonly', 'readonly');
        $('#data_grade').attr('readonly', 'readonly');
        $('#data_jabatan_akad').attr('readonly', 'readonly');
        $('#data_jenjang_pendidikan').attr('readonly', 'readonly');
        $('#data_jenis_dokumen').attr('readonly', 'readonly');
        $('#data_jenis_pegawai').attr('readonly', 'readonly');
        $('#data_kelompok_jabatan').attr('readonly', 'readonly');
        $('#data_kelas_jabatan').attr('readonly', 'readonly');
        $('#data_nama_jabatan').attr('readonly', 'readonly');
        $('#data_rekening').attr('readonly', 'readonly');
        $('#data_pekerjaan').attr('readonly', 'readonly');
        $('#data_status_sipil').attr('readonly', 'readonly');
        $('#tusi').attr('readonly', 'readonly');
        $('#mengajar').attr('readonly', 'readonly');
        $('#mengajar_lainnya').attr('readonly', 'readonly');
        $('#penelitian').attr('readonly', 'readonly');
        $('#pengabdian').attr('readonly', 'readonly');
        $('#penghargaan').attr('readonly', 'readonly');
        $('#penunjang').attr('readonly', 'readonly');
        $('#data_status_pegawai').attr('readonly', 'readonly');

        $('#data_peran').attr('disabled', 'disabled');
        $('#data_aktivitas').attr('disabled', 'disabled');
        $('#data_periode').attr('disabled', 'disabled');
        $('#data_kdn').attr('disabled', 'disabled');
        $('#data_dosen_tendik_pns').attr('disabled', 'disabled');
        $('#data_golongan_pns').attr('disabled', 'disabled');
        $('#data_grade').attr('disabled', 'disabled');
        $('#data_jabatan_akad').attr('disabled', 'disabled');
        $('#data_jenjang_pendidikan').attr('disabled', 'disabled');
        $('#data_jenis_dokumen').attr('disabled', 'disabled');
        $('#data_jenis_pegawai').attr('disabled', 'disabled');
        $('#data_kelompok_jabatan').attr('disabled', 'disabled');
        $('#data_kelas_jabatan').attr('disabled', 'disabled');
        $('#data_nama_jabatan').attr('disabled', 'disabled');
        $('#data_rekening').attr('disabled', 'disabled');
        $('#data_pekerjaan').attr('disabled', 'disabled');
        $('#data_status_sipil').attr('disabled', 'disabled');
        $('#tusi').attr('disabled', 'disabled');
        $('#mengajar').attr('disabled', 'disabled');
        $('#mengajar_lainnya').attr('disabled', 'disabled');
        $('#penelitian').attr('disabled', 'disabled');
        $('#pengabdian').attr('disabled', 'disabled');
        $('#penghargaan').attr('disabled', 'disabled');
        $('#penunjang').attr('disabled', 'disabled');
        $('#data_status_pegawai').attr('disabled', 'disabled');

        $('#data_peran').attr('checked', 'checked');
        $('#data_aktivitas').attr('checked', 'checked');
        $('#data_periode').attr('checked', 'checked');
        $('#data_kdn').attr('checked', 'checked');
        $('#data_dosen_tendik_pns').attr('checked', 'checked');
        $('#data_golongan_pns').attr('checked', 'checked');
        $('#data_grade').attr('checked', 'checked');
        $('#data_jabatan_akad').attr('checked', 'checked');
        $('#data_jenjang_pendidikan').attr('checked', 'checked');
        $('#data_jenis_dokumen').attr('checked', 'checked');
        $('#data_jenis_pegawai').attr('checked', 'checked');
        $('#data_kelompok_jabatan').attr('checked', 'checked');
        $('#data_kelas_jabatan').attr('checked', 'checked');
        $('#data_nama_jabatan').attr('checked', 'checked');
        $('#data_rekening').attr('checked', 'checked');
        $('#data_pekerjaan').attr('checked', 'checked');
        $('#data_status_sipil').attr('checked', 'checked');
        $('#tusi').attr('checked', 'checked');
        $('#mengajar').attr('checked', 'checked');
        $('#mengajar_lainnya').attr('checked', 'checked');
        $('#penelitian').attr('checked', 'checked');
        $('#pengabdian').attr('checked', 'checked');
        $('#penghargaan').attr('checked', 'checked');
        $('#penunjang').attr('checked', 'checked');
        $('#data_status_pegawai').attr('checked', 'checked');

    } else {
        $('#data_peran').removeAttr('readonly', 'readonly');
        $('#data_aktivitas').removeAttr('readonly', 'readonly');
        $('#data_periode').removeAttr('readonly', 'readonly');
        $('#data_kdn').removeAttr('readonly', 'readonly');
        $('#data_dosen_tendik_pns').removeAttr('readonly', 'readonly');
        $('#data_golongan_pns').removeAttr('readonly', 'readonly');
        $('#data_grade').removeAttr('readonly', 'readonly');
        $('#data_jabatan_akad').removeAttr('readonly', 'readonly');
        $('#data_jenjang_pendidikan').removeAttr('readonly', 'readonly');
        $('#data_jenis_dokumen').removeAttr('readonly', 'readonly');
        $('#data_jenis_pegawai').removeAttr('readonly', 'readonly');
        $('#data_kelompok_jabatan').removeAttr('readonly', 'readonly');
        $('#data_kelas_jabatan').removeAttr('readonly', 'readonly');
        $('#data_nama_jabatan').removeAttr('readonly', 'readonly');
        $('#data_rekening').removeAttr('readonly', 'readonly');
        $('#data_pekerjaan').removeAttr('readonly', 'readonly');
        $('#data_status_sipil').removeAttr('readonly', 'readonly');
        $('#tusi').removeAttr('readonly', 'readonly');
        $('#mengajar').removeAttr('readonly', 'readonly');
        $('#mengajar_lainnya').removeAttr('readonly', 'readonly');
        $('#penelitian').removeAttr('readonly', 'readonly');
        $('#pengabdian').removeAttr('readonly', 'readonly');
        $('#penghargaan').removeAttr('readonly', 'readonly');
        $('#penunjang').removeAttr('readonly', 'readonly');
        $('#data_status_pegawai').removeAttr('readonly', 'readonly');

        $('#data_peran').removeAttr('disabled', 'disabled');
        $('#data_aktivitas').removeAttr('disabled', 'disabled');
        $('#data_periode').removeAttr('disabled', 'disabled');
        $('#data_kdn').removeAttr('disabled', 'disabled');
        $('#data_dosen_tendik_pns').removeAttr('disabled', 'disabled');
        $('#data_golongan_pns').removeAttr('disabled', 'disabled');
        $('#data_grade').removeAttr('disabled', 'disabled');
        $('#data_jabatan_akad').removeAttr('disabled', 'disabled');
        $('#data_jenjang_pendidikan').removeAttr('disabled', 'disabled');
        $('#data_jenis_dokumen').removeAttr('disabled', 'disabled');
        $('#data_jenis_pegawai').removeAttr('disabled', 'disabled');
        $('#data_kelompok_jabatan').removeAttr('disabled', 'disabled');
        $('#data_kelas_jabatan').removeAttr('disabled', 'disabled');
        $('#data_nama_jabatan').removeAttr('disabled', 'disabled');
        $('#data_rekening').removeAttr('disabled', 'disabled');
        $('#data_pekerjaan').removeAttr('disabled', 'disabled');
        $('#data_status_sipil').removeAttr('disabled', 'disabled');
        $('#tusi').removeAttr('disabled', 'disabled');
        $('#mengajar').removeAttr('disabled', 'disabled');
        $('#mengajar_lainnya').removeAttr('disabled', 'disabled');
        $('#penelitian').removeAttr('disabled', 'disabled');
        $('#pengabdian').removeAttr('disabled', 'disabled');
        $('#penghargaan').removeAttr('disabled', 'disabled');
        $('#penunjang').removeAttr('disabled', 'disabled');
        $('#data_status_pegawai').removeAttr('disabled', 'disabled');

        $('#form-add-new-data').trigger("reset");
        $('#data_peran').removeAttr('checked', 'checked');
        $('#data_aktivitas').removeAttr('checked', 'checked');
        $('#data_periode').removeAttr('checked', 'checked');
        $('#data_kdn').removeAttr('checked', 'checked');
        $('#data_dosen_tendik_pns').removeAttr('checked', 'checked');
        $('#data_golongan_pns').removeAttr('checked', 'checked');
        $('#data_grade').removeAttr('checked', 'checked');
        $('#data_jabatan_akad').removeAttr('checked', 'checked');
        $('#data_jenjang_pendidikan').removeAttr('checked', 'checked');
        $('#data_jenis_dokumen').removeAttr('checked', 'checked');
        $('#data_jenis_pegawai').removeAttr('checked', 'checked');
        $('#data_kelompok_jabatan').removeAttr('checked', 'checked');
        $('#data_kelas_jabatan').removeAttr('checked', 'checked');
        $('#data_nama_jabatan').removeAttr('checked', 'checked');
        $('#data_rekening').removeAttr('checked', 'checked');
        $('#data_pekerjaan').removeAttr('checked', 'checked');
        $('#data_status_sipil').removeAttr('checked', 'checked');
        $('#tusi').removeAttr('checked', 'checked');
        $('#mengajar').removeAttr('checked', 'checked');
        $('#mengajar_lainnya').removeAttr('checked', 'checked');
        $('#penelitian').removeAttr('checked', 'checked');
        $('#pengabdian').removeAttr('checked', 'checked');
        $('#penghargaan').removeAttr('checked', 'checked');
        $('#penunjang').removeAttr('checked', 'checked');
        $('#data_status_pegawai').removeAttr('checked', 'checked');
    }
}

var ins = $('#form-add-new-data').on('submit', function(e) {
    $('#form-add-new-data').trigger("reset");
    swal({
        title: "Success",
        icon: "success",
        text: "Data berhsail disinkron",
        dangerMode: false,
        buttons: {
            confirm: "Ok",
        }
    });
});
</script>