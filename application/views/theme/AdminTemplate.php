<!DOCTYPE html>
<html class="loading" lang="en">
<!-- BEGIN : Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />
    <title><?= APP_NAME ?> - <?= $title ?></title>
    <link rel="shortcut icon" href="<?= base_url() ?>assets/logo.png" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-touch-fullscreen" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-sty le" content="default" />
    <link
        href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900|Montserrat:300,400,500,600,700,800,900"
        rel="stylesheet" />
    <!-- BEGIN VENDOR CSS-->
    <!-- font icons-->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/fonts/feather/style.min.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/fonts/simple-line-icons/style.css" />
    <link rel="stylesheet" type="text/css"
        href="<?= base_url() ?>assets/fonts/font-awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/vendors/css/perfect-scrollbar.min.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/vendors/css/prism.min.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/vendors/css/tables/datatable/datatables.min.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN APEX CSS-->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/vendors/css/chartist.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/app.css" />
    
    <!-- END APEX CSS-->
    <!-- BEGIN Page Level CSS-->
    <!-- END Page Level CSS-->
    <style>
    .logo-img img{
        width:35px;
    }
    </style>
</head>
<!-- END : Head-->

<!-- BEGIN : Body-->

<body data-col="2-columns" class=" 2-columns ">
<!-- ////////////////////////////////////////////////////////////////////////////-->
<div class="wrapper">
    <!-- main menu-->
      <?= $header ?>
      <?= $navbar ?>
        <div class="main-panel">
            <!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-wrapper">
                 <?= $content ?>

                </div>
            </div>
            
            <!-- END : End Main Content-->
            <?= $footer ?>
        </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->
    <?= $sidebar ?>

    <script src="<?= base_url() ?>assets/vendors/js/core/jquery-3.2.1.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/vendors/js/core/popper.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/vendors/js/core/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/vendors/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/vendors/js/prism.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/vendors/js/jquery.matchHeight-min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/vendors/js/screenfull.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/vendors/js/pace/pace.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/js/app-sidebar.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/js/notification-sidebar.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/js/customizer.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/js/horizontal-timeline.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/js/sweetalert.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/vendors/js/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/js/data-tables/datatable-basic.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/js/components-modal.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/js/keycloak.js"></script>
    <script src="<?= base_url() ?>assets/vendors/js/chartist.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/vendors/js/chart.min.js" type="text/javascript"></script> 
    

    <!-- END PAGE LEVEL JS-->
    <?= $js ?>
</body>
<!-- END : Body-->

</html>