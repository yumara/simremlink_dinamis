 <!-- BEGIN : Footer-->
 <footer class="footer footer-static footer-light">
     <p class="clearfix text-muted text-sm-center px-2">
         <span><?= APP_COPYRIGHT ?></span>
     </p>
 </footer>
 <!-- End : Footer-->