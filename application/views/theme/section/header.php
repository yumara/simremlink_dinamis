<!-- Navbar (Header) Starts-->
<nav class="navbar navbar-expand-lg navbar-light bg-faded header-navbar">
    <div class="container-fluid">
        <div class="navbar-header"></div>
        <div class="navbar-container">
            <div id="navbarSupportedContent" class="collapse navbar-collapse">
                <ul class="navbar-nav">
                    <li class="nav-item d-none d-lg-block">
                        <a href="javascript:;" class="nav-link position-relative notification-sidebar-toggle">
                            <i class="ft-grid font-medium-3 blue-grey darken-4"></i>
                            <p class="d-none">Notifications Sidebar</p>
                        </a>
                    </li>
                    <li class="dropdown nav-item">
                        <a id="dropdownBasic3" href="#" data-toggle="dropdown"
                            class="nav-link position-relative dropdown-toggle" aria-expanded="false">
                            <i class="ft-user font-medium-3 blue-grey darken-4"></i>
                        </a>
                        <div ngbdropdownmenu="" aria-labelledby="dropdownBasic3"
                            class="dropdown-menu text-left dropdown-menu-right">
                            <a onclick="keluar()" class="dropdown-item py-1"><i
                                    class="ft-log-out mr-2"></i><span>Keluar</span></a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
<!-- Navbar (Header) Ends-->