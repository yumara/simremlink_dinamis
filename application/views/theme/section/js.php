<?php
$uri = $this->uri->segment(1);
$uri2 = $this->uri->segment(2);
$uri3 = $this->uri->segment(3);
$uri4 = $this->uri->segment(4);
if($uri == "" || $uri == null || $uri == "dashboard"){
    $this->load->view('dashboard/js/js-dashboard');
}else{
    $this->load->view($uri.'/'.$uri2.'/js/js-'.$uri2);
}
?>
<script>
// var json = "<?= base_url('assets/js/keycloak.json') ?>";

// //=-----------------------
// var kc = new Keycloak(json);
// kc.init({
// 	onLoad: 'login-required',
// 	checkLoginIframe: false,
// });

function syncAll() {
    $.ajax({
        url: "<?= base_url('refresh-data'); ?>",
        type: "POST",
        dataType: "JSON",
        beforeSend: function() {
            swal({
                title:"Please Wait",
                text: "Updating data...",
                icon: "info",
                buttons:false,
                closeOnClickOutside:false,
                closeOnEsc:false,
                onBeforeOpen: () => {
                    Swal.showLoading()
                }
            });
        },
        success: function(r) {
            swal({
                title: "Success",
                icon: r.icon,
                text: r.msg,
                dangerMode: false,
                buttons: {
                    confirm: "Ok",
                }
            });
        }
    });
}

function underconstruction() {
    swal({
        title: "Underconstruction",
        text: "Menu Sedang Dalam Pengerjaan",
        icon: "info"
    });
}

function keluar() {
    swal({
        title: "Peringatan",
        icon: "warning",
        text: "Apakah anda yakin ingin keluar ?",
        dangerMode: true,
        buttons: {
            cancel: "Batal",
            confirm: "Keluar",
        }
    }).then((ok) => {
        if (ok) {
            $.ajax({
                url: "<?php echo base_url('keluar'); ?>",
                type: "POST",
                dataType: "JSON",
                success: function(r) {
                    swal({
                        title: "Berhasil",
                        icon: r.icon,
                        text: r.msg,
                        dangerMode: false,
                        buttons: {
                            confirm: "Ok",
                        }
                    }).then((ok) => {
                        kc.logout({
                            redirectUri: "https://portal.unimed.ac.id"
                        });
                    });
                }
            });
        } else {
            swal({
                title: "Dibatalkan",
                text: "Batal keluar",
                icon: "info"
            });
        }
    });
}

/** simpeg */
/*function simpegPegawaiPhl() {
    $.ajax({
        url: "<?= base_url('simpeg/refresh-pegawai-phl'); ?>",
        type: "POST",
        dataType: "JSON",
        beforeSend: function() {
            swal({
                title:"Please Wait",
                text: "Updating data...",
                icon: "info",
                buttons:false,
                closeOnClickOutside:false,
                closeOnEsc:false,
                onBeforeOpen: () => {
                    Swal.showLoading()
                }
            });
        },
        success: function(r) {
            swal({
                title: "Success",
                icon: r.icon,
                text: r.msg,
                dangerMode: false,
                buttons: {
                    confirm: "Ok",
                }
            }).then((ok) => {
                window.location.href = "<?= base_url() ?>" + r.link;
            });
        }
    });
}

function simpegDosenPhl() {
    $.ajax({
        url: "<?= base_url('simpeg/refresh-dosen-phl'); ?>",
        type: "POST",
        dataType: "JSON",
        beforeSend: function() {
            swal({
                title:"Please Wait",
                text: "Updating data...",
                icon: "info",
                buttons:false,
                closeOnClickOutside:false,
                closeOnEsc:false,
                onBeforeOpen: () => {
                    Swal.showLoading()
                }
            });
        },
        success: function(r) {
            swal({
                title: "Success",
                icon: r.icon,
                text: r.msg,
                dangerMode: false,
                buttons: {
                    confirm: "Ok",
                }
            }).then((ok) => {
                window.location.href = "<?= base_url() ?>" + r.link;
            });
        }
    });
}

function simpegPegawaiPns() {
    $.ajax({
        url: "<?= base_url('simpeg/refresh-pegawai-pns'); ?>",
        type: "POST",
        dataType: "JSON",
        beforeSend: function() {
            swal({
                title:"Please Wait",
                text: "Updating data...",
                icon: "info",
                buttons:false,
                closeOnClickOutside:false,
                closeOnEsc:false,
                onBeforeOpen: () => {
                    Swal.showLoading()
                }
            });
        },
        success: function(r) {
            swal({
                title: "Success",
                icon: r.icon,
                text: r.msg,
                dangerMode: false,
                buttons: {
                    confirm: "Ok",
                }
            }).then((ok) => {
                window.location.href = "<?= base_url() ?>" + r.link;
            });
        }
    });
}

function simpegDosenPns() {
    $.ajax({
        url: "<?= base_url('simpeg/refresh-dosen-pns'); ?>",
        type: "POST",
        dataType: "JSON",
        beforeSend: function() {
            swal({
                title:"Please Wait",
                text: "Updating data...",
                icon: "info",
                buttons:false,
                closeOnClickOutside:false,
                closeOnEsc:false,
                onBeforeOpen: () => {
                    Swal.showLoading()
                }
            });
        },
        success: function(r) {
            swal({
                title: "Success",
                icon: r.icon,
                text: r.msg,
                dangerMode: false,
                buttons: {
                    confirm: "Ok",
                }
            }).then((ok) => {
                window.location.href = "<?= base_url() ?>" + r.link;
            });
        }
    });
}

function simpegDosenPnsSimremlink () {
    $.ajax({
        url: "<?= base_url('simpeg/refresh-dosen-pns-simremlink'); ?>",
        type: "POST",
        dataType: "JSON",
        beforeSend: function() {
            swal({
                title:"Please Wait",
                text: "Updating data...",
                icon: "info",
                buttons:false,
                closeOnClickOutside:false,
                closeOnEsc:false,
                onBeforeOpen: () => {
                    Swal.showLoading()
                }
            });
        },
        success: function(r) {
            swal({
                title: "Success",
                icon: r.icon,
                text: r.msg,
                dangerMode: false,
                buttons: {
                    confirm: "Ok",
                }
            }).then((ok) => {
                window.location.href = "<?= base_url() ?>" + r.link;
            });
        }
    });
}*/
/** end simpeg */
</script>