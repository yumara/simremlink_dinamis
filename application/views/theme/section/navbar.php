<?php 
$uri = $this->uri->segment(1); 
$uri2 = $this->uri->segment(2);
$uri3 = $this->uri->segment(3);
?>
<!--.main-menu(class="#{menuColor} #{menuOpenType}", class=(menuShadow == true ? 'menu-shadow' : ''))-->
<div data-active-color="white" data-background-color="flickr" data-image="<?= base_url() ?>assets/img/sidebar-bg/01.jpg"
    class="app-sidebar">
    <!-- main menu header-->
    <!-- Sidebar Header starts-->
    <div class="sidebar-header">
        <div class="logo clearfix"><a href="index.html" class="logo-text float-left">
                <div class="logo-img"><img src="<?= base_url() ?>assets/logo.png" /></div><span
                    class="text align-middle">UNIMED</span>
            </a><a id="sidebarToggle" href="javascript:;" class="nav-toggle d-none d-sm-none d-md-none d-lg-block"><i
                    data-toggle="expanded" class="toggle-icon ft-toggle-right"></i></a><a id="sidebarClose"
                href="javascript:;" class="nav-close d-block d-md-block d-lg-none d-xl-none"><i class="ft-x"></i></a>
        </div>
    </div>
    <!-- main menu content-->
    <div class="sidebar-content">
        <div class="nav-container">
            <ul id="main-menu-navigation" data-menu="menu-navigation" data-scroll-to-active="true"
                class="navigation navigation-main">

                <li class="<?= ($active == 'dashboard') ? 'active' : ''; ?> nav-item"><a
                        href="<?= base_url('dashboard') ?>"><i class="ft-home"></i><span data-i18n=""
                            class="menu-title">Dashboard</span></a>
                </li>
                <!--  <li class="has-sub nav-item"><a href="#"><i class="ft-chevrons-right"></i><span data-i18n=""
                            class="menu-title">Akad</span></a>
                    <ul class="menu-content" style="">
                        <li class="<?= ($active == 'akad-a') ? 'active' : ''; ?> nav-item"><a href="#"
                                class="menu-item">Kelebihan Mengajar</a>
                        </li>
                    </ul>
                </li>
                <li class="has-sub nav-item"><a href="#"><i class="ft-chevrons-right"></i><span data-i18n=""
                            class="menu-title">Simpeg</span></a>
                    <ul class="menu-content" style="">
                        <li class="<?= ($active == 'simpeg-a') ? 'active' : ''; ?> nav-item"><a href="#"
                                class="menu-item">Dosen PNS Siremlink</a>
                        </li>
                        <li class="<?= ($active == 'simpeg-b') ? 'active' : ''; ?> nav-item"><a href="#" oncli
                                ck="simpegDosenPns();" class="menu-item">Dosen PNS</a>
                        </li>
                        <li class="<?= ($active == 'simpeg-c') ? 'active' : ''; ?> nav-item"><a href="#"
                                class="menu-item">Dosen PHL</a>
                        </li>
                        <li class="<?= ($active == 'simpeg-d') ? 'active' : ''; ?> nav-item"><a href="#"
                                class="menu-item">Pegawai PNS <br>Simremlink</a>
                        </li>
                        <li class="<?= ($active == 'simpeg-e') ? 'active' : ''; ?> nav-item"><a href="#"
                                class="menu-item">Pegawai PNS</a>
                        </li>
                        <li class="<?= ($active == 'simpeg-f') ? 'active' : ''; ?> nav-item"><a href="#"
                                class="menu-item">Pegawai PHL</a>
                        </li>
                        <li class="<?= ($active == 'simpeg-g') ? 'active' : ''; ?> nav-item"><a href="#"
                                class="menu-item">Riwayat Jabatan
                                <br>Fungsional</a>
                        </li>
                        <li class="<?= ($active == 'simpeg-h') ? 'active' : ''; ?> nav-item"><a href="#"
                                class="menu-item">Riwayat Jabatan
                                <br>Remun Dosen <br>PNS</a>
                        </li>
                        <li class="<?= ($active == 'simpeg-i') ? 'active' : ''; ?> nav-item"><a href="#"
                                class="menu-item">Riwayat Jabatan
                                <br>Remun Pegawai <br>PNS</a>
                        </li>
                        <li class="<?= ($active == 'simpeg-j') ? 'active' : ''; ?> nav-item"><a href="#"
                                class="menu-item">Riwayat Jabatan
                                <br>Tugas Tambahan <br>Dosen PNS</a>
                        </li>
                        <li class="<?= ($active == 'simpeg-k') ? 'active' : ''; ?> nav-item"><a href="#"
                                class="menu-item">Riwayat Penghargaan
                                <br>Dosen PNS</a>
                        </li>
                        <li class="<?= ($active == 'simpeg-l') ? 'active' : ''; ?> nav-item"><a href="#"
                                class="menu-item">Riwayat Penghargaan
                                <br>Pegawai PNS</a>
                        </li>
                        <li class="<?= ($active == 'simpeg-l') ? 'active' : ''; ?> nav-item"><a href="#"
                                class="menu-item">Data Pendukung
                                <br>Lainnya</a>
                        </li>
                    </ul>
                </li>
                <li class="has-sub nav-item"><a href="#"><i class="ft-chevrons-right"></i><span data-i18n=""
                            class="menu-title">SK</span></a>
                    <ul class="menu-content" style="">
                        <li class="<?= ($active == 'sk-a') ? 'active' : ''; ?> nav-item" onclick=underconstruction();><a
                                href="#" class="menu-item">Aktivitas</a>
                        </li>
                    </ul>
                </li>
                <li class="has-sub nav-item"><a href="#"><i class="ft-chevrons-right"></i><span data-i18n=""
                            class="menu-title">LPPM</span></a>
                    <ul class="menu-content" style="">
                        <li class="<?= ($active == 'lppm-a') ? 'active' : ''; ?> nav-item" onclick=lppmPenelitian();><a
                                href="#" class="menu-item">Penelitian</a>
                        </li>
                        <li class="<?= ($active == 'lppm-b') ? 'active' : ''; ?> nav-item" onclick=lppmPengabdian();><a
                                href="#" class="menu-item">Pengabdian</a>
                        </li>
                    </ul>
                </li>
                <li class="has-sub nav-item"><a href="#"><i class="ft-chevrons-right"></i><span data-i18n=""
                            class="menu-title">Simremlink</span></a>
                    <ul class="menu-content" style="">
                        <li class="<?= ($active == 'simremlink-a') ? 'active' : ''; ?> nav-item"><a href="#"
                                class="menu-item">Data Peran</a>
                        </li>
                        <li class="<?= ($active == 'simremlink-b') ? 'active' : ''; ?> nav-item"><a href="#"
                                class="menu-item">Data Periode</a>
                        </li>
                        <li class="<?= ($active == 'simremlink-c') ? 'active' : ''; ?> nav-item"><a href="#"
                                class="menu-item">Data Kdn</a>
                        </li>
                        <li class="<?= ($active == 'simremlink-d') ? 'active' : ''; ?> nav-item"><a href="#"
                                class="menu-item">Data Dosen Tendik PNS</a>
                        </li>
                    </ul>
                </li> -->
                <li class="has-sub nav-item"><a href="#"><i class="ft-chevrons-right"></i><span data-i18n=""
                            class="menu-title">Daftar API</span></a>
                    <ul class="menu-content" style="">
                        <li class="<?= ($active == 'api-a') ? 'active' : ''; ?> nav-item">
                            <a href="<?= base_url('daftar-api/akad') ?>" class="menu-item">API Akad</a>
                        </li>
                        <li class="<?= ($active == 'api-b') ? 'active' : ''; ?> nav-item">
                            <a href="<?= base_url('daftar-api/simpeg') ?>" class="menu-item">API Simpeg</a>
                        </li>
                        <li class="<?= ($active == 'api-c') ? 'active' : ''; ?> nav-item" onclick=simremlinkDataKdn();>
                            <a href="<?= base_url('daftar-api/esk') ?>" class="menu-item">API E-SK</a>
                        </li>
                        <li class="<?= ($active == 'api-d') ? 'active' : ''; ?> nav-item">
                            <a href="<?= base_url('daftar-api/lppm') ?>" class="menu-item">API Lppm</a>
                        </li>
                        <li class="<?= ($active == 'api-e') ? 'active' : ''; ?> nav-item">
                            <a href="<?= base_url('daftar-api/simremlink') ?>" class="menu-item">API Simremlink</a>
                        </li>
                        <li class="<?= ($active == 'api-f') ? 'active' : ''; ?> nav-item">
                            <a href="<?= base_url('daftar-api/transaksi') ?>" class="menu-item">API Transaksi</a>
                        </li>
                    </ul>
                </li>
                <li class="has-sub nav-item"><a href="#"><i class="ft-chevrons-right"></i><span data-i18n=""
                            class="menu-title">Sinkron Data</span></a>
                    <ul class="menu-content" style="">
                        <li class="<?= ($active == 'sinkron-a') ? 'active' : ''; ?> nav-item">
                            <a href="<?= base_url('sinkron/akad') ?>" class="menu-item">Sinkron Akad</a>
                        </li>
                        <li class="<?= ($active == 'sinkron-b') ? 'active' : ''; ?> nav-item">
                            <a href="<?= base_url('sinkron/simpeg') ?>" class="menu-item">Sinkron Simpeg</a>
                        </li>
                        <li class="<?= ($active == 'sinkron-c') ? 'active' : ''; ?> nav-item" onclick=simremlinkDataKdn();>
                            <a href="<?= base_url('sinkron/e-sk') ?>" class="menu-item">Sinkron E-SK</a>
                        </li>
                        <li class="<?= ($active == 'sinkron-d') ? 'active' : ''; ?> nav-item">
                            <a href="<?= base_url('sinkron/lppm') ?>" class="menu-item">Sinkron Lppm</a>
                        </li>
                        <li class="<?= ($active == 'sinkron-e') ? 'active' : ''; ?> nav-item">
                            <a href="<?= base_url('sinkron/simremlink') ?>" class="menu-item">Sinkron Simremlink</a>
                        </li>
                        <li class="nav-item">
                            <a href="#" onclick="underconstruction();" class="menu-item">Sinkron Semua</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <!-- main menu content-->
    <div class="sidebar-background"></div>
</div>
<!-- / main menu-->