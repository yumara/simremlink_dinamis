<!-- START Notification Sidebar-->
<aside id="notification-sidebar" class="notification-sidebar d-none d-sm-none d-md-block">
    <a class="notification-sidebar-close"><i class="ft-x font-medium-3"></i></a>
    <div class="side-nav notification-sidebar-content ps-container ps-theme-dark"
        data-ps-id="4d69f4ef-0062-f1ae-5677-3f333d12b948">
        <div class="row">
            <div class="col-12 mt-1">
                <ul class="nav nav-tabs">
                    <li class="nav-item"><a id="base-tab1" data-toggle="tab" aria-controls="tab1" href="#activity-tab"
                            aria-expanded="true" class="nav-link active">Portal</a></li>
                </ul>
                <div class="tab-content px-1 pt-1">
                    <div id="activity-tab" role="tabpanel" aria-expanded="true" aria-labelledby="base-tab1"
                        class="tab-pane active">
                        <div id="activity" class="col-12 timeline-left">
                            <div id="timeline" class="timeline-left timeline-wrapper">
                                <ul class="timeline">
                                    <li class="timeline-item">
                                        <div class="timeline-badge">
                                            <span data-toggle="tooltip" data-placement="right"
                                                title="Portfolio project work" class="bg-yellow bg-lighten-1"><i
                                                    class="ft-home"></i></span>
                                        </div>
                                        <div class="col s9 recent-activity-list-text">
                                            <a href="<?= base_url('halaman-utama') ?>"
                                                class="deep-purple-text medium-small">HALAMAN UTAMA</a>
                                            <p class="mt-0 mb-2 fixed-line-height font-weight-300 medium-small">Kembali
                                                kehalaman utama</p>
                                        </div>
                                    </li>

                                    <?php 
                                    //============== SIMPEG ==============//
                                    if($grup == 'Dosen' || $grup == 'Pegawai' || $grup == 'Admin_Aplikasi' ): ?>
                                    <li class="timeline-item">
                                        <div class="timeline-badge">
                                            <span data-toggle="tooltip" data-placement="right"
                                                title="Portfolio project work" class="bg-purple bg-lighten-1"><i
                                                    class="ft-users"></i></span>
                                        </div>
                                        <div class="col s9 recent-activity-list-text">
                                            <a href="<?= base_url('simpeg') ?>"
                                                class="deep-purple-text medium-small">SIMPEG</a>
                                            <p class="mt-0 mb-2 fixed-line-height font-weight-300 medium-small">Sistem
                                                Informasi Kepegawaian</p>
                                        </div>
                                    </li>
                                    <?php endif; 
                                    //============== akad ==============//
                                    if($grup == 'Dosen' || $grup == 'Admin_Aplikasi' || $grup == 'Mahasiswa'):?>
                                    <li class="timeline-item">
                                        <div class="timeline-badge">
                                            <span data-toggle="tooltip" data-placement="right"
                                                title="Portfolio project work" class="bg-info bg-lighten-1"><i
                                                    class="ft-codepen"></i></span>
                                        </div>
                                        <div class="col s9 recent-activity-list-text">
                                            <a href="<?= base_url('akad') ?>" class="cyan-text medium-small">AKAD</a>

                                            <p class="mt-0 mb-2 fixed-line-height font-weight-300 medium-small">Sistem
                                                Informasi Akademik</p>
                                        </div>
                                    </li>
                                    <?php endif;
                                    //============== ESK ==============//
                                    if($grup == 'Dosen' || $grup == 'Pegawai' || $grup == 'Admin_Aplikasi'): ?>
                                    <li class="timeline-item">
                                        <div class="timeline-badge">
                                            <span data-toggle="tooltip" data-placement="right"
                                                title="Portfolio project work" class="bg-success bg-lighten-1"><i
                                                    class="ft-folder"></i></span>
                                        </div>
                                        <div class="col s9 recent-activity-list-text">
                                            <a href="<?= base_url('e-sk') ?>" class="green-text medium-small">E-SK</a>

                                            <p class="mt-0 mb-2 fixed-line-height font-weight-300 medium-small">Surat
                                                Keterangan Elektronik</p>
                                        </div>
                                    </li>
                                    <?php endif;
                                    //============== LPPM ==============//
                                    if($grup == 'Dosen' || $grup == 'Admin_Aplikasi'): ?>
                                    <li class="timeline-item">
                                        <div class="timeline-badge">
                                            <span data-toggle="tooltip" data-placement="right"
                                                title="Portfolio project work" class="bg-warning bg-lighten-1"><i
                                                    class="ft-file-text"></i></span>
                                        </div>

                                        <div class="col s9 recent-activity-list-text">
                                            <a href="<?= base_url('lppm') ?>" class="amber-text medium-small">LPPM</a>

                                            <p class="mt-0 mb-2 fixed-line-height font-weight-300 medium-small">Sistem
                                                Laporan Penelitian</p>
                                        </div>
                                    </li>
                                    <?php endif; 
                                    //============== SIMREMLINK ==============//
                                    if($grup == 'Dosen' || $grup == 'Pegawai' || $grup == 'Admin_Aplikasi'): ?>
                                    <li class="timeline-item">
                                        <div class="timeline-badge">
                                            <span data-toggle="tooltip" data-placement="right"
                                                title="Portfolio project work" class="bg-red bg-lighten-1"><i
                                                    class="ft-layers"></i></span>
                                        </div>
                                        <div class="col s9 recent-activity-list-text">
                                            <a href="<?= base_url('simremlink') ?>"
                                                class="deep-orange-text medium-small">SIMREMLINK</a>

                                            <p class="mt-0 mb-2 fixed-line-height font-weight-300 medium-small">Sistem
                                                Remunerasi</p>
                                        </div>
                                    </li>
                                    <?php endif;
                                    //============== SIPDA ==============//
                                    if($grup == 'Dosen' || $grup == 'Admin_Aplikasi'): ?>
                                    <li class="timeline-item">
                                        <div class="timeline-badge">
                                            <span data-toggle="tooltip" data-placement="right"
                                                title="Portfolio project work" class="bg-purple bg-lighten-1"><i
                                                    class="ft-airplay"></i></span>
                                        </div>
                                        <div class="col s9 recent-activity-list-text">
                                            <a href="<?= base_url('sipda') ?>"
                                                class="deep-orange-text medium-small">SIPDA</a>

                                            <p class="mt-0 mb-2 fixed-line-height font-weight-300 medium-small">Sistem
                                                Pembelajaran Daring</p>
                                        </div>
                                    </li>
                                    <?php endif;
                                    //============== WEB SERVICE ==============//
                                    if($grup == 'Admin_Aplikasi'): ?>
                                    <li class="timeline-item">
                                        <div class="timeline-badge">
                                            <span data-toggle="tooltip" data-placement="right"
                                                title="Portfolio project work" class="bg-blue bg-lighten-1"><i
                                                    class="ft-bar-chart-2"></i></span>
                                        </div>
                                        <div class="col s9 recent-activity-list-text">
                                            <a href="<?= base_url('web-service') ?>"
                                                class="deep-orange-text medium-small">WEB SERVICE</a>

                                            <p class="mt-0 mb-2 fixed-line-height font-weight-300 medium-small">Aplikasi
                                                Database Web Service Terintegrasi</p>
                                        </div>
                                    </li>
                                    <?php endif;
                                    //============== MAIL ==============//
                                    if($grup == 'Dosen' || $grup == 'Pegawai' || $grup == 'Admin_Aplikasi' || $grup == 'Mahasiswa'): ?>
                                    <li class="timeline-item">
                                        <div class="timeline-badge">
                                            <span data-toggle="tooltip" data-placement="right"
                                                title="Portfolio project work" class="bg-success bg-lighten-1"><i
                                                    class="ft-mail"></i></span>
                                        </div>
                                        <div class="col s9 recent-activity-list-text">
                                            <a href="<?= base_url('mail-unimed') ?>"
                                                class="deep-orange-text medium-small">MAIL</a>

                                            <p class="mt-0 mb-2 fixed-line-height font-weight-300 medium-small">Sistem
                                                Email Universitas Negeri Medan</p>
                                        </div>
                                    </li>
                                    <?php endif;
                                    //============== SKP PEGAWAI ==============//
                                    if($grup == 'Admin_Aplikasi' || $grup == 'Pegawai' ): ?>
                                    <li class="timeline-item">
                                        <div class="timeline-badge">
                                            <span data-toggle="tooltip" data-placement="right"
                                                title="Portfolio project work" class="bg-warning bg-lighten-1"><i
                                                    class="ft-users"></i></span>
                                        </div>
                                        <div class="col s9 recent-activity-list-text">
                                            <a href="<?= base_url('skp-pegawai') ?>"
                                                class="deep-orange-text medium-small">SKP PEGAWAI</a>

                                            <p class="mt-0 mb-2 fixed-line-height font-weight-300 medium-small">Aplikasi
                                                SKP Pegawai</p>
                                        </div>
                                    </li>
                                    <?php endif;
                                    //============== BKD ==============//
                                    if($grup == 'Dosen' || $grup == 'Admin_Aplikasi'): ?>
                                    <li class="timeline-item">
                                        <div class="timeline-badge">
                                            <span data-toggle="tooltip" data-placement="right"
                                                title="Portfolio project work" class="bg-danger bg-lighten-1"><i
                                                    class="ft-user-check"></i></span>
                                        </div>
                                        <div class="col s9 recent-activity-list-text">
                                            <a href="<?= base_url('bkd') ?>"
                                                class="deep-orange-text medium-small">BKD</a>

                                            <p class="mt-0 mb-2 fixed-line-height font-weight-300 medium-small">Aplikasi
                                                Beban Kinerja Dosen</p>
                                        </div>
                                    </li>
                                    <?php endif;
                                    //============== SKP DOSEN ==============//
                                    if($grup == 'Dosen' || $grup == 'Admin_Aplikasi'): ?>
                                    <li class="timeline-item">
                                        <div class="timeline-badge">
                                            <span data-toggle="tooltip" data-placement="right"
                                                title="Portfolio project work" class="bg-purple bg-lighten-1"><i
                                                    class="ft-users"></i></span>
                                        </div>
                                        <div class="col s9 recent-activity-list-text">
                                            <a href="<?= base_url('skp-unimed') ?>"
                                                class="deep-orange-text medium-small">SKP DOSEN</a>

                                            <p class="mt-0 mb-2 fixed-line-height font-weight-300 medium-small">Aplikasi
                                                SKP Dosen</p>
                                        </div>
                                    </li>
                                    <?php endif; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;">
            <div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div>
        </div>
        <div class="ps-scrollbar-y-rail" style="top: 0px; height: 693px; right: 3px;">
            <div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 0px;"></div>
        </div>
    </div>
</aside>
<!-- END Notification Sidebar-->